/*------------------------------------------------------------------------------

	test/test_float.cpp

------------------------------------------------------------------------------*/

#include "pch.h"
#include "test.h"
#include <math.h>
#include <float.h>

#include "../lib/elements/numeric.h"

struct f32_info_t
{
	unsigned int isnan : 1;
	unsigned int finite : 1;
	unsigned int fpclass : 4;
	
	uint32_t mantissa : 24;
	int32_t exponent : 8;
	int32_t sign : 1;
};

#ifndef _MSC_VER
int _isnan(f64 v) { return isnan(v); }
int _finite(f64 v) { return isfinite(v); }
int _fpclass(f64 v) { return fpclassify(v); }
#define _FPCLASS_SNAN FP_NAN
#define _FPCLASS_QNAN FP_NAN
#define _FPCLASS_NINF FP_INFINITE
#define _FPCLASS_NN   FP_NORMAL
#define _FPCLASS_ND   FP_SUBNORMAL
#define _FPCLASS_NZ   FP_ZERO
#define _FPCLASS_PZ   FP_ZERO
#define _FPCLASS_PD   FP_SUBNORMAL
#define _FPCLASS_PN   FP_NORMAL
#define _FPCLASS_PINF FP_INFINITE
#endif

//
//
//

static void describe_flag(string_t &temp, int set, const char *desc)
{
	if (set)
	{
		if (temp == "")
			temp += " (";
		else
			temp += ", ";

		temp += desc;
	}
}

static void dump_ieee_common(f64 v)
{
	int nan = _isnan(v);
	int fin = _finite(v);

	TRACE(", _isnan()=%i, _finite()=%i", nan, fin);

	int cls = _fpclass(v);

	string_t temp;

#ifdef _MSC_VER
	describe_flag(temp, (cls & _FPCLASS_SNAN) != 0, "Signaling NaN");
	describe_flag(temp, (cls & _FPCLASS_QNAN) != 0, "Quiet NaN");
#else
	describe_flag(temp, (cls & FP_NAN) != 0, "NaN");
#endif
#if 0
	describe_flag(temp, (cls & _FPCLASS_NINF) != 0, "Negative infinity (-INF)");
	describe_flag(temp, (cls & _FPCLASS_NN)   != 0, "Negative normalized non-zero");
	describe_flag(temp, (cls & _FPCLASS_ND)   != 0, "Negative denormalized");
	describe_flag(temp, (cls & _FPCLASS_NZ)   != 0, "Negative zero (-0)");
	describe_flag(temp, (cls & _FPCLASS_PZ)   != 0, "Positive 0 (+0)");
	describe_flag(temp, (cls & _FPCLASS_PD)   != 0, "Positive denormalized");
	describe_flag(temp, (cls & _FPCLASS_PN)   != 0, "Positive normalized non-zero");
	describe_flag(temp, (cls & _FPCLASS_PINF) != 0, "Positive infinity (+INF)");
#else
	describe_flag(temp, (cls & (_FPCLASS_PZ | _FPCLASS_NZ)) != 0, "zero");
	describe_flag(temp, (cls & (_FPCLASS_PD | _FPCLASS_ND)) != 0, "denormalized");
	describe_flag(temp, (cls & (_FPCLASS_PN | _FPCLASS_NN)) != 0, "normalized");
	describe_flag(temp, (cls & (_FPCLASS_PINF | _FPCLASS_NINF)) != 0, "infinite");
#endif

	if (temp != "")
		temp += ")";

	TRACE(", _fpclass=0x%04X%s", cls, (const char *)temp);
}

static void dump_ieee(f32 v, const char *suffix = NULL)
{
	TRACE("%+.7e: ", v);

	f32_t test;

	test.f = v;

	int exp = 0;
	double sig = frexp(v, &exp);

	trace("0x", convert_to_hex(test.i));

	if (test.u.exponent == 0)
	{
		// special meaning

		trace(", special");
		trace(", exp=", test.u.exponent);
		trace(", significand=", (test.u.sign ? -1.0 : 1.0) * test.u.significand);

		if (test.u.significand == 0)
		{
		}
		else
		{
		}
	}
	else if (test.u.exponent == 255) // largest possible value
	{
		// special meaning

		trace(", special");
		trace(", exp=", test.u.exponent);
		trace(", significand=", test.u.significand);

		if (test.u.significand == 0)
		{
		}
		else
		{
		}
	}
	else
	{
		// normalized value

		f32 significand = (test.u.sign ? -1.0 : 1.0) * (1.0 + (s32)test.u.significand * pow(2.0, -23));

		if (sig != significand)
		{
			trace(", sig=", sig);
		}

		trace(", significand=", significand);
		trace(", exp=2^", (int)test.u.exponent - ((1 << (8 - 1)) - 1));
	}

	dump_ieee_common(v);

	if (suffix != NULL)
		trace(" ", suffix);

	trace("\n");
}

static void dump_ieee(f64 v, const char *suffix = NULL)
{
	TRACE("%+.16e: ", v);

	f64_t test;

	test.f = v;

	int exp = 0;
	double sig = frexp(v, &exp);

	trace("0x", convert_to_hex(test.i));

	if (test.u.exponent == 0)
	{
		// special meaning

		trace(", special");
		trace(", exp=", test.u.exponent);
		trace(", significand=", (test.u.sign ? -1.0 : 1.0) * (s64)test.u.significand);

		if (test.u.significand == 0)
		{
		}
		else
		{
		}
	}
	else if (test.u.exponent == 2047) // largest possible value
	{
		// special meaning

		trace(", special");
		trace(", exp=", test.u.exponent);
		trace(", significand=", test.u.significand);

		if (test.u.significand == 0)
		{
		}
		else
		{
		}
	}
	else
	{
		// normalized value

		f64 significand = (test.u.sign ? -1.0 : 1.0) * (1.0 + (s64)test.u.significand * pow(2.0, -52));

		if (sig != significand)
		{
			trace(", sig=", sig);
		}

		trace(", significand=", significand);
		trace(", exp=2^", (int)test.u.exponent - ((1 << (11 - 1)) - 1));
	}

	dump_ieee_common(v);

	if (suffix != NULL)
		trace(" ", suffix);

	trace("\n");
}

//
//
//

static void test_float1()
{
	u64 loss = 0;
	u64 match = 0;
	u64 no_match = 0;
	u64 last = 0;
	u64 contig = 0;

	for (u32 i = 0; i < ((u64)1 << 25) - 1; i++)
	{
		if (i % (16 * 1024 * 1024) == 0)
		{
			TRACE("test_float: i=%u\n", i);
			loss = 0;
		}

		f32_t f;

		f.f = i; // warning C4244: '=' : conversion from 'u32' to 'f32', possible loss of data

		u32 i2 = f.f; // warning C4244: 'initializing' : conversion from 'f32' to 'u32', possible loss of data

		if (i2 != i)
		{
			bool temp = i == last + 1;

			if (temp)
				contig++;

			last = i;

			no_match++;

			if (!temp)
			{
			loss++;

			if (loss < 10) // only show 1st 10 in each 16M block
				TRACE("test_float: i=%u, f=%f, i2=%u\n", i, f.f, i2);
			else if (loss == 100)
				TRACE("test_float: further mismatches not shown\n");
			}
		}
		else
		{
			match++;
		}
	}

	trace("match=", match, ", no_match=", no_match, "\n");
#if defined(_MSC_VER) && (_MSC_VER <= 1200)
	// unsigned __int64 to double not implemented on MSVC 6
#else
	trace("ratio=", (float)match / (match + no_match), "\n");
#endif
	trace("contig=", contig, "\n");
}

//
//
//

static void test_float2()
{
	double x = 0.0;     //                    0x0000000000000000
	double y = log(x);  // "-1.#INF000000000" 0xFFF0000000000000
	double z = 1.0 / x; // "1.#INF000000000"  0x7FF0000000000000
	double q = 0.0 / x; // "-1.#IND000000000" 0xFFF8000000000000

	static const f32_t neg0 = { 0x80000000 };

	dump_ieee(x, "zero");
	dump_ieee((double)neg0.f, "-0");
//	dump_ieee((double)FLT_EPSILON, "FLT_EPSILON");
	dump_ieee(DBL_EPSILON, "DBL_EPSILON"); // DBL_EPSILON is equivalent to 2^-52

	dump_ieee(1.0);
	dump_ieee(1.0 + DBL_EPSILON);
	dump_ieee(1.0 + FLT_EPSILON);

//	dump_ieee(10.0);
//	dump_ieee(100.0);
//	dump_ieee(0.1);

	dump_ieee(y);
	dump_ieee(z);
	dump_ieee(q);
	dump_ieee((double)float_qnan());
//	dump_ieee(float_pinf());
//	dump_ieee(float_ninf());
//	dump_ieee(float_pind());
//	dump_ieee(float_nind());

	// conversion to integer

	int i;

	if (0)
	{
		i = x;
		i = y;
	}

	x = -1.0;

	y = log(x);

	dump_ieee(y);

	// conversion from integer

	if (0)
	{
		i = y;
	}

	//
	//
	//

	f64 t;

	TRACE("decreasing powers of 2, until value is zero...\n");

	t = 1.0;
	i = 0;

	while (true)
	{
		if (i++ <= 10)
			dump_ieee(t);

		t /= 2.0;

		if (t == 0.0)
			break;
	}

	TRACE("...\n");
	dump_ieee(t);

	//

	TRACE("increasing powers of 2, until value is infinite...\n");

	t = 1.0;
	i = 0;

	while (true)
	{
		if (i++ <= 10)
			dump_ieee(t);

		t *= 2.0;

		if (!_finite(t))
			break;
	}

	TRACE("...\n");
	dump_ieee(t);

	//

	TRACE("decreasing powers of 10, until value is zero...\n");

	t = 1.0;
	i = 0;

	while (true)
	{
		if (i++ <= 10)
			dump_ieee(t);

		t /= 10.0;

		if (t == 0.0)
			break;
	}

	TRACE("...\n");
	dump_ieee(t);

	//

	TRACE("increasing powers of 10, until value is infinite...\n");

	t = 1.0;
	i = 0;

	while (true)
	{
		if (i++ <= 10)
			dump_ieee(t);

		t *= 10.0;

		if (!_finite(t))
			break;
	}

	TRACE("...\n");
	dump_ieee(t);
}

//
//
//

void test_float()
{
	test_float1();
	test_float2();
}

