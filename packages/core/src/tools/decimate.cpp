/*------------------------------------------------------------------------------
------------------------------------------------------------------------------*/

#include <mercury/core.h>
#include "tools.h"

#define MERCURY_LINK_SUBSYSTEM 1
#include <mercury/link_core.h>

int main(int argc, char *argv[])
{
	string_t filename;
	
	file_t in = io_open_input(filename);
	file_t out = io_stdout();
	
	unsigned int n = 100;
	
	unsigned int lineno = 0;
	
	for (;;)
	{
		string_t line;
		
		if (!read_line_buffered__(in, line))
			break;
	
		lineno++;
		
		if ((lineno % n) == 1)
			write(out, line, "\n");
	}

	return 0;
}

