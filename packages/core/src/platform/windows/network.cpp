/*------------------------------------------------------------------------------

	platform/windows/network.cpp - Windows I/O support

	dump of WSA protocols from i3 laptop:

	WSA protocol 1: MSAFD Tcpip [TCP/IP]        iAddressFamily=2  iSocketType=1 iProtocol=6
	WSA protocol 2: MSAFD Tcpip [UDP/IP]        iAddressFamily=2  iSocketType=2 iProtocol=17
	WSA protocol 3: MSAFD Tcpip [TCP/IPv6]      iAddressFamily=23 iSocketType=1 iProtocol=6
	WSA protocol 4: MSAFD Tcpip [UDP/IPv6]      iAddressFamily=23 iSocketType=2 iProtocol=17
	WSA protocol 5: RSVP TCPv6 Service Provider iAddressFamily=23 iSocketType=1 iProtocol=6
	WSA protocol 6: RSVP TCP Service Provider   iAddressFamily=2  iSocketType=1 iProtocol=6
	WSA protocol 7: RSVP UDPv6 Service Provider iAddressFamily=23 iSocketType=2 iProtocol=17
	WSA protocol 8: RSVP UDP Service Provider   iAddressFamily=2  iSocketType=2 iProtocol=17
	WSA protocol 9: MSAFD RfComm [Bluetooth]    iAddressFamily=32 iSocketType=1 iProtocol=3
	WSA protocol 10: VMCI sockets DGRAM         iAddressFamily=28 iSocketType=2 iProtocol=0
	WSA protocol 11: VMCI sockets STREAM        iAddressFamily=28 iSocketType=1 iProtocol=0


#define AF_UNSPEC       0
#define AF_INET         2
#define AF_INET6        23

#define SOCK_STREAM     1
#define SOCK_DGRAM      2
#define SOCK_RAW        3

#define IPPROTO_IP              0
#define IPPROTO_GGP             3
#define IPPROTO_TCP             6
#define IPPROTO_UDP             17

	socket parameter combinations supported on Windows

	af        type        protocol            description
	--        ----        --------            -----------
	AF_UNSPEC SOCK_RAW    IPPROTO_ICMP    --> Internet Control Message Protocol (ICMP)
	AF_INET   SOCK_RAW    IPPROTO_ICMP    -->
	AF_INET6  SOCK_RAW    IPPROTO_ICMP    -->
	AF_UNSPEC SOCK_RAW    IPPROTO_ICMP6   --> Internet Control Message Protocol (ICMPv6)
	AF_INET   SOCK_RAW    IPPROTO_ICMP6   -->
	AF_INET6  SOCK_RAW    IPPROTO_ICMP6   -->
	AF_UNSPEC SOCK_RAW    IPPROTO_IGMP    --> Internet Group Management Protocol (IGMP)
	AF_INET   SOCK_RAW    IPPROTO_IGMP    -->
	AF_INET6  SOCK_RAW    IPPROTO_IGMP    -->
	AF_INET   SOCK_RDM    IPPROTO_RM      -->
	AF_INET   SOCK_STREAM IPPROTO_TCP     --> TCP/IPv4
	AF_INET   SOCK_DGRAM  IPPROTO_UDP     --> UDP/IPv4
	AF_INET6  SOCK_STREAM IPPROTO_TCP     --> TCP/IPv6
	AF_INET6  SOCK_DGRAM  IPPROTO_UDP     --> UDP/IPv6
	AF_BTH    SOCK_STREAM BTHPROTO_RFCOMM --> Bluetooth RFCOMM

------------------------------------------------------------------------------*/

#include "pch.h"
#include <mercury/extensions/io.h>

#include "../../lib/io/network.h"

#if defined(_MSC_VER) && (_MSC_VER > 1200)
#include <ws2tcpip.h>
#endif

#ifndef SO_CONDITIONAL_ACCEPT
#define SO_CONDITIONAL_ACCEPT 0x3002 // defined in newer ws2def.h
#endif

#ifndef SO_BSP_STATE
#define SO_BSP_STATE    0x1009
#endif

// defined in newer SDK ws2ipdef.h

#ifndef _WS2IPDEF_

// Options to use with [gs]etsockopt at the IPPROTO_IP level.
// The values should be consistent with the IPv6 equivalents.

#define IP_OPTIONS                 1 // Set/get IP options.
#define IP_HDRINCL                 2 // Header is included with data.
#define IP_TOS                     3 // IP type of service.
#define IP_TTL                     4 // IP TTL (hop limit).
#define IP_MULTICAST_IF            9 // IP multicast interface.
#define IP_MULTICAST_TTL          10 // IP multicast TTL (hop limit).
#define IP_MULTICAST_LOOP         11 // IP multicast loopback.
#define IP_ADD_MEMBERSHIP         12 // Add an IP group membership.
#define IP_DROP_MEMBERSHIP        13 // Drop an IP group membership.
#define IP_DONTFRAGMENT           14 // Don't fragment IP datagrams.
#define IP_ADD_SOURCE_MEMBERSHIP  15 // Join IP group/source.
#define IP_DROP_SOURCE_MEMBERSHIP 16 // Leave IP group/source.
#define IP_BLOCK_SOURCE           17 // Block IP group/source.
#define IP_UNBLOCK_SOURCE         18 // Unblock IP group/source.
#define IP_PKTINFO                19 // Receive packet information.
#define IP_HOPLIMIT               21 // Receive packet hop limit.
#define IP_RECEIVE_BROADCAST      22 // Allow/block broadcast reception.
#define IP_RECVIF                 24 // Receive arrival interface.
#define IP_RECVDSTADDR            25 // Receive destination address.
#define IP_IFLIST                 28 // Enable/Disable an interface list.
#define IP_ADD_IFLIST             29 // Add an interface list entry.
#define IP_DEL_IFLIST             30 // Delete an interface list entry.
#define IP_UNICAST_IF             31 // IP unicast interface.
#define IP_RTHDR                  32 // Set/get IPv6 routing header.
#define IP_RECVRTHDR              38 // Receive the routing header.
#define IP_TCLASS                 39 // Packet traffic class.
#define IP_RECVTCLASS             40 // Receive packet traffic class.
#define IP_ORIGINAL_ARRIVAL_IF    47 // Original Arrival Interface Index.

#endif

//
//
//

#ifndef LITE_RTL

static lock_t  g_wsa_lock; // XXX: startup code
static int     g_wsa_refs;
static WSADATA g_wsa_data;
static int     g_wsa_err;

// TODO: put in a resource...
// TODO: use FormatSystemMessage

// REF: MSDN "Windows Sockets Version 2 API - Error Codes"

static const struct { int error; const char *name; const char *msg; } g_wsa_errors[] =
{
	{ WSAEACCES,                  "WSAEACCES",                  "Permission denied."                                 }, // (10013)
	{ WSAEADDRINUSE,              "WSAEADDRINUSE",              "Address already in use."                            }, // (10048)
	{ WSAEADDRNOTAVAIL,           "WSAEADDRNOTAVAIL",           "Cannot assign requested address."                   }, // (10049)
	{ WSAEAFNOSUPPORT,            "WSAEAFNOSUPPORT",            "Address family not supported by protocol family."   }, // (10047)
	{ WSAEALREADY,                "WSAEALREADY",                "Operation already in progress."                     }, // (10037)
	{ WSAECONNABORTED,            "WSAECONNABORTED",            "Software caused connection abort."                  }, // (10053)
	{ WSAECONNREFUSED,            "WSAECONNREFUSED",            "Connection refused."                                }, // (10061)
	{ WSAECONNRESET,              "WSAECONNRESET",              "Connection reset by peer."                          }, // (10054)
	{ WSAEDESTADDRREQ,            "WSAEDESTADDRREQ",            "Destination address required."                      }, // (10039)
	{ WSAEFAULT,                  "WSAEFAULT",                  "Bad address."                                       }, // (10014)
	{ WSAEHOSTDOWN,               "WSAEHOSTDOWN",               "Host is down."                                      }, // (10064)
	{ WSAEHOSTUNREACH,            "WSAEHOSTUNREACH",            "No route to host."                                  }, // (10065)
	{ WSAEINPROGRESS,             "WSAEINPROGRESS",             "Operation now in progress."                         }, // (10036)
	{ WSAEINTR,                   "WSAEINTR",                   "Interrupted function call."                         }, // (10004)
	{ WSAEINVAL,                  "WSAEINVAL",                  "Invalid argument."                                  }, // (10022)
	{ WSAEISCONN,                 "WSAEISCONN",                 "Socket is already connected."                       }, // (10056)
	{ WSAEMFILE,                  "WSAEMFILE",                  "Too many open files."                               }, // (10024)
	{ WSAEMSGSIZE,                "WSAEMSGSIZE",                "Message too long."                                  }, // (10040)
	{ WSAENETDOWN,                "WSAENETDOWN",                "Network is down."                                   }, // (10050)
	{ WSAENETRESET,               "WSAENETRESET",               "Network dropped connection on reset."               }, // (10052)
	{ WSAENETUNREACH,             "WSAENETUNREACH",             "Network is unreachable."                            }, // (10051)
	{ WSAENOBUFS,                 "WSAENOBUFS",                 "No buffer space available."                         }, // (10055)
	{ WSAENOPROTOOPT,             "WSAENOPROTOOPT",             "Bad protocol option."                               }, // (10042)
	{ WSAENOTCONN,                "WSAENOTCONN",                "Socket is not connected."                           }, // (10057)
	{ WSAENOTSOCK,                "WSAENOTSOCK",                "Socket operation on non-socket."                    }, // (10038)
	{ WSAEOPNOTSUPP,              "WSAEOPNOTSUPP",              "Operation not supported."                           }, // (10045)
	{ WSAEPFNOSUPPORT,            "WSAEPFNOSUPPORT",            "Protocol family not supported."                     }, // (10046)
	{ WSAEPROCLIM,                "WSAEPROCLIM",                "Too many processes."                                }, // (10067)
	{ WSAEPROTONOSUPPORT,         "WSAEPROTONOSUPPORT",         "Protocol not supported."                            }, // (10043)
	{ WSAEPROTOTYPE,              "WSAEPROTOTYPE",              "Protocol wrong type for socket."                    }, // (10041)
	{ WSAESHUTDOWN,               "WSAESHUTDOWN",               "Cannot send after socket shutdown."                 }, // (10058)
	{ WSAESOCKTNOSUPPORT,         "WSAESOCKTNOSUPPORT",         "Socket type not supported."                         }, // (10044)
	{ WSAETIMEDOUT,               "WSAETIMEDOUT",               "Connection timed out."                              }, // (10060)
	{ WSATYPE_NOT_FOUND,          "WSATYPE_NOT_FOUND",          "Class type not found."                              }, // (10109)
	{ WSAEWOULDBLOCK,             "WSAEWOULDBLOCK",             "Resource temporarily unavailable."                  }, // (10035)
	{ WSAHOST_NOT_FOUND,          "WSAHOST_NOT_FOUND",          "Host not found."                                    }, // (11001)
//	{ WSA_INVALID_HANDLE,         "WSA_INVALID_HANDLE",         "Specified event object handle is invalid."          }, // (OS dependent)
//	{ WSA_INVALID_PARAMETER,      "WSA_INVALID_PARAMETER",      "One or more parameters are invalid."                }, // (OS dependent)
	{ WSAEINVALIDPROCTABLE,       "WSAEINVALIDPROCTABLE",       "Invalid procedure table from service provider."     }, // (OS dependent)
	{ WSAEINVALIDPROVIDER,        "WSAEINVALIDPROVIDER",        "Invalid service provider version number."           }, // (OS dependent)
//	{ WSA_IO_INCOMPLETE,          "WSA_IO_INCOMPLETE",          "Overlapped I/O event object not in signaled state." }, // (OS dependent)
//	{ WSA_IO_PENDING,             "WSA_IO_PENDING",             "Overlapped operations will complete later."         }, // (OS dependent)
//	{ WSA_NOT_ENOUGH_MEMORY,      "WSA_NOT_ENOUGH_MEMORY",      "Insufficient memory available."                     }, // (OS dependent)
	{ WSANOTINITIALISED,          "WSANOTINITIALISED",          "Successful WSAStartup not yet performed."           }, // (10093)
	{ WSANO_DATA,                 "WSANO_DATA",                 "Valid name, no data record of requested type."      }, // (11004)
	{ WSANO_RECOVERY,             "WSANO_RECOVERY",             "This is a non-recoverable error."                   }, // (11003)
	{ WSAEPROVIDERFAILEDINIT,     "WSAEPROVIDERFAILEDINIT",     "Unable to initialize a service provider."           }, // (OS dependent)
	{ WSASYSCALLFAILURE,          "WSASYSCALLFAILURE",          "System call failure."                               }, // (OS dependent)
	{ WSASYSNOTREADY,             "WSASYSNOTREADY",             "Network subsystem is unavailable."                  }, // (10091)
	{ WSATRY_AGAIN,               "WSATRY_AGAIN",               "Non-authoritative host not found."                  }, // (11002)
	{ WSAVERNOTSUPPORTED,         "WSAVERNOTSUPPORTED",         "WINSOCK.DLL version out of range."                  }, // (10092)
	{ WSAEDISCON,                 "WSAEDISCON",                 "Graceful shutdown in progress."                     }, // (10094)
//	{ WSA_OPERATION_ABORTED,      "WSA_OPERATION_ABORTED",      "Overlapped operation aborted."                      }, // (OS dependent)

	{ WSAEBADF,                   "WSAEBADF",                   NULL }, //
	{ WSAETOOMANYREFS,            "WSAETOOMANYREFS",            NULL }, //
	{ WSAELOOP,                   "WSAELOOP",                   NULL }, //
	{ WSAENAMETOOLONG,            "WSAENAMETOOLONG",            NULL }, //
	{ WSAENOTEMPTY,               "WSAENOTEMPTY",               NULL }, //
	{ WSAEUSERS,                  "WSAEUSERS",                  NULL }, //
	{ WSAEDQUOT,                  "WSAEDQUOT",                  NULL }, //
	{ WSAESTALE,                  "WSAESTALE",                  NULL }, //
	{ WSAEREMOTE,                 "WSAEREMOTE",                 NULL }, //
	{ WSAENOMORE,                 "WSAENOMORE",                 NULL }, //
	{ WSAECANCELLED,              "WSAECANCELLED",              NULL }, //
	{ WSASERVICE_NOT_FOUND,       "WSASERVICE_NOT_FOUND",       NULL }, //
	{ WSA_E_NO_MORE,              "WSA_E_NO_MORE",              NULL }, //
	{ WSA_E_CANCELLED,            "WSA_E_CANCELLED",            NULL }, //
	{ WSAEREFUSED,                "WSAEREFUSED",                NULL }, //
	{ WSA_QOS_RECEIVERS,          "WSA_QOS_RECEIVERS",          NULL }, //
	{ WSA_QOS_SENDERS,            "WSA_QOS_SENDERS",            NULL }, //
	{ WSA_QOS_NO_SENDERS,         "WSA_QOS_NO_SENDERS",         NULL }, //
	{ WSA_QOS_NO_RECEIVERS,       "WSA_QOS_NO_RECEIVERS",       NULL }, //
	{ WSA_QOS_REQUEST_CONFIRMED,  "WSA_QOS_REQUEST_CONFIRMED",  NULL }, //
	{ WSA_QOS_ADMISSION_FAILURE,  "WSA_QOS_ADMISSION_FAILURE",  NULL }, //
	{ WSA_QOS_POLICY_FAILURE,     "WSA_QOS_POLICY_FAILURE",     NULL }, //
	{ WSA_QOS_BAD_STYLE,          "WSA_QOS_BAD_STYLE",          NULL }, //
	{ WSA_QOS_BAD_OBJECT,         "WSA_QOS_BAD_OBJECT",         NULL }, //
	{ WSA_QOS_TRAFFIC_CTRL_ERROR, "WSA_QOS_TRAFFIC_CTRL_ERROR", NULL }, //
	{ WSA_QOS_GENERIC_ERROR,      "WSA_QOS_GENERIC_ERROR",      NULL }, //
};

//
//
//

string_t get_wsa_error_string(long error)
{
	for (unsigned int i = 0; i < DIM(g_wsa_errors); i++)
	{
		if (g_wsa_errors[i].error == error)
			return g_wsa_errors[i].msg;
	}

	return "";
}

//
//
//

const char *get_socket_error_string(int error)
{
	for (unsigned int i = 0; i < DIM(g_wsa_errors); i++)
	{
		if (g_wsa_errors[i].error == error)
			return g_wsa_errors[i].name;
	}

	return "";
}

//
//
//

string_t get_wsa_error_string__(long error)
{
	return FormatSystemMessage(error);
}

//
//
//

static void dump_providers()
{
#ifdef _DEBUG
	WSAPROTOCOL_INFO protocols[32] = { 0 };

	DWORD len = sizeof(protocols);

	int n = WSAEnumProtocols(NULL, protocols, &len);

	for (int i = 0; i < n; i++)
	{
		trace("WSA protocol ", i + 1, ": ", to_ansi(protocols[i].szProtocol),
			"\n");

		trace("iAddressFamily=", protocols[i].iAddressFamily);
		trace(", iSocketType=", protocols[i].iSocketType);
		trace(", iProtocol=", protocols[i].iProtocol);

		trace("\n");
	}
#endif
}

//
//
//

int socket_init()
{
	bool show = false;

	g_wsa_lock.enter();

	if (g_wsa_refs == 0)
	{
		WORD wVersionRequested = MAKEWORD(2, 2);

		g_wsa_err = WSAStartup(wVersionRequested, &g_wsa_data);

		if (g_wsa_err != 0)
		{
			wVersionRequested = MAKEWORD(1, 1);

			g_wsa_err = WSAStartup(wVersionRequested, &g_wsa_data);
		}

		if (g_wsa_err == 0)
		{
			g_wsa_refs++;
		}

//		show = true;
	}
	else
	{
		g_wsa_refs++;
	}

	int stat = g_wsa_refs;

	g_wsa_lock.leave();

	if (show)
	{
		if (g_wsa_err == 0)
		{
			if (trace_enabled(g_trace_inet, trace_severity_debug))
			{
				TRACE("WSAStartup: succeeded, version=%i, description=\"%s\", status=\"%s\"\n",
					g_wsa_data.wVersion, g_wsa_data.szDescription, g_wsa_data.szSystemStatus);

				dump_providers();
			}
		}
		else
		{
			if (trace_enabled(g_trace_inet, trace_severity_error))
			{
				TRACE("WSAStartup: failed, error=%i\n", g_wsa_err);
			}
		}
	}

	return stat;
}

//
//
//

int socket_term()
{
	bool show = false;
	int stat = -1;

	g_wsa_lock.enter();

	if (g_wsa_refs > 0 && --g_wsa_refs == 0)
	{
		DBG_NOT_IMPLEMENTED();

		if (0)
		{
			stat = WSACleanup();
			show = true;
		}
	}

	int ans = g_wsa_refs;

	g_wsa_lock.leave();

	if (show)
	{
		if (trace_enabled(g_trace_inet, trace_severity_debug))
		{
			TRACE("WSACleanup: %i\n", stat);
		}
	}

	return ans;
}

//
//
//

int select_internal(int nfds, fd_set *readfds, fd_set *writefds, fd_set *exceptfds, const f32 *timeout)
{
	timeval tm;

	timeval *ptm = NULL;

	if (timeout != NULL)
	{
		tm.tv_sec = *timeout;
		tm.tv_usec = (*timeout - (int)*timeout) * 1000000;

		ptm = &tm;
	}

	int n;
	
	for (;;)
	{
		n = select(nfds, readfds, writefds, exceptfds, ptm);

		if (n == SOCKET_ERROR)
		{
			int error = WSAGetLastError();

			if (error == EINTR)
				continue;

			if (trace_enabled(g_trace_inet, trace_severity_info))
			{
				TRACE("NET: select() error: %i (%s)\n",
					error, get_socket_error_string(error));
			}
		}

		break;
	}

	return n;
}

// TODO: move into diag DLL...

#if 1

#ifdef _DEBUG

struct socket_option_t
{
	int          level;
	int          optname;
	int          type;    // 1=BOOL, 2=int, 3=GROUP, 4=LINGER, 5=unsigned int, ...
	const char * label;
};

static const socket_option_t g_socket_options[] =
{
	{ SOL_SOCKET,  SO_DEBUG,                  7, "SO_DEBUG"                  },
	{ SOL_SOCKET,  SO_ACCEPTCONN,             7, "SO_ACCEPTCONN"             }, // only valid on connection-oriented protocols
	{ SOL_SOCKET,  SO_REUSEADDR,              7, "SO_REUSEADDR"              },
	{ SOL_SOCKET,  SO_KEEPALIVE,              7, "SO_KEEPALIVE"              },
	{ SOL_SOCKET,  SO_DONTROUTE,              7, "SO_DONTROUTE"              },
	{ SOL_SOCKET,  SO_BROADCAST,              7, "SO_BROADCAST"              },
	{ SOL_SOCKET,  SO_USELOOPBACK,            7, "SO_USELOOPBACK"            },
	{ SOL_SOCKET,  SO_LINGER,                 4, "SO_LINGER"                 },
	{ SOL_SOCKET,  SO_OOBINLINE,              7, "SO_OOBINLINE"              },
	{ SOL_SOCKET,  SO_SNDBUF,                 7, "SO_SNDBUF"                 },
	{ SOL_SOCKET,  SO_RCVBUF,                 7, "SO_RCVBUF"                 },
	{ SOL_SOCKET,  SO_SNDLOWAT,               7, "SO_SNDLOWAT"               }, // not supported on earlier versions of Windows
	{ SOL_SOCKET,  SO_RCVLOWAT,               7, "SO_RCVLOWAT"               }, // not supported on earlier versions of Windows
	{ SOL_SOCKET,  SO_SNDTIMEO,               7, "SO_SNDTIMEO"               },
	{ SOL_SOCKET,  SO_RCVTIMEO,               7, "SO_RCVTIMEO"               },
	{ SOL_SOCKET,  SO_ERROR,                  7, "SO_ERROR"                  },
	{ SOL_SOCKET,  SO_TYPE,                   7, "SO_TYPE"                   },
	{ SOL_SOCKET,  SO_BSP_STATE,              0, "SO_BSP_STATE"              },
	{ SOL_SOCKET,  SO_GROUP_ID,               3, "SO_GROUP_ID"               },
	{ SOL_SOCKET,  SO_GROUP_PRIORITY,         2, "SO_GROUP_PRIORITY"         }, // reserved
	{ SOL_SOCKET,  SO_MAX_MSG_SIZE,           5, "SO_MAX_MSG_SIZE"           },
	{ SOL_SOCKET,  SO_CONDITIONAL_ACCEPT,     7, "SO_CONDITIONAL_ACCEPT"     },
	{ SOL_SOCKET,  SO_PROTOCOL_INFO,          6, "SO_PROTOCOL_INFO"          },
	{ SOL_SOCKET,  PVD_CONFIG,                0, "PVD_CONFIG"                },

	{ IPPROTO_IP,  IP_OPTIONS,                0, "IP_OPTIONS"                }, // 1 Set/get IP options.
	{ IPPROTO_IP,  IP_HDRINCL,                7, "IP_HDRINCL"                }, // 2 Header is included with data.
	{ IPPROTO_IP,  IP_TOS,                    7, "IP_TOS"                    }, // 3 IP type of service.
	{ IPPROTO_IP,  IP_TTL,                    7, "IP_TTL"                    }, // 4 IP TTL (hop limit).
	{ IPPROTO_IP,  IP_MULTICAST_IF,           7, "IP_MULTICAST_IF"           }, // 9 IP multicast interface.
	{ IPPROTO_IP,  IP_MULTICAST_TTL,          7, "IP_MULTICAST_TTL"          }, // 10 IP multicast TTL (hop limit).
	{ IPPROTO_IP,  IP_MULTICAST_LOOP,         7, "IP_MULTICAST_LOOP"         }, // 11 IP multicast loopback.
	{ IPPROTO_IP,  IP_ADD_MEMBERSHIP,         0, "IP_ADD_MEMBERSHIP"         }, // 12 Add an IP group membership.
	{ IPPROTO_IP,  IP_DROP_MEMBERSHIP,        0, "IP_DROP_MEMBERSHIP"        }, // 13 Drop an IP group membership.
	{ IPPROTO_IP,  IP_DONTFRAGMENT,           7, "IP_DONTFRAGMENT"           }, // 14 Don't fragment IP datagrams.
	{ IPPROTO_IP,  IP_ADD_SOURCE_MEMBERSHIP,  0, "IP_ADD_SOURCE_MEMBERSHIP"  }, // 15 Join IP group/source.
	{ IPPROTO_IP,  IP_DROP_SOURCE_MEMBERSHIP, 0, "IP_DROP_SOURCE_MEMBERSHIP" }, // 16 Leave IP group/source.
	{ IPPROTO_IP,  IP_BLOCK_SOURCE,           0, "IP_BLOCK_SOURCE"           }, // 17 Block IP group/source.
	{ IPPROTO_IP,  IP_UNBLOCK_SOURCE,         0, "IP_UNBLOCK_SOURCE"         }, // 18 Unblock IP group/source.
	{ IPPROTO_IP,  IP_PKTINFO,                7, "IP_PKTINFO"                }, // 19 Receive packet information.
	{ IPPROTO_IP,  IP_HOPLIMIT,               0, "IP_HOPLIMIT"               }, // 21 Receive packet hop limit.
	{ IPPROTO_IP,  IP_RECEIVE_BROADCAST,      7, "IP_RECEIVE_BROADCAST"      }, // 22 Allow/block broadcast reception.
	{ IPPROTO_IP,  IP_RECVIF,                 7, "IP_RECVIF"                 }, // 24 Receive arrival interface.
	{ IPPROTO_IP,  IP_RECVDSTADDR,            0, "IP_RECVDSTADDR"            }, // 25 Receive destination address.
	{ IPPROTO_IP,  IP_IFLIST,                 0, "IP_IFLIST"                 }, // 28 Enable/Disable an interface list.
	{ IPPROTO_IP,  IP_ADD_IFLIST,             0, "IP_ADD_IFLIST"             }, // 29 Add an interface list entry.
	{ IPPROTO_IP,  IP_DEL_IFLIST,             0, "IP_DEL_IFLIST"             }, // 30 Delete an interface list entry.
	{ IPPROTO_IP,  IP_UNICAST_IF,             7, "IP_UNICAST_IF"             }, // 31 IP unicast interface.
	{ IPPROTO_IP,  IP_RTHDR,                  0, "IP_RTHDR"                  }, // 32 Set/get IPv6 routing header.
	{ IPPROTO_IP,  IP_RECVRTHDR,              0, "IP_RECVRTHDR"              }, // 38 Receive the routing header.
	{ IPPROTO_IP,  IP_TCLASS,                 0, "IP_TCLASS"                 }, // 39 Packet traffic class.
	{ IPPROTO_IP,  IP_RECVTCLASS,             0, "IP_RECVTCLASS"             }, // 40 Receive packet traffic class.
	{ IPPROTO_IP,  IP_ORIGINAL_ARRIVAL_IF,    7, "IP_ORIGINAL_ARRIVAL_IF"    }, // 47 Original Arrival Interface Index.

	{ IPPROTO_TCP, TCP_NODELAY,               7, "TCP_NODELAY"               }, // Disables the Nagle algorithm for send coalescing.
//	{ IPPROTO_TCP, TCP_MAXSEG,                2, "TCP_MAXSEG"                }, // Get TCP maximum segment size

//	{ IPPROTO_UDP, UDP_NOCHECKSUM,            0, "UDP_NOCHECKSUM"            },
//	{ IPPROTO_UDP, UDP_CHECKSUM_COVERAGE,     0, "UDP_CHECKSUM_COVERAGE"     },
};

//
//
//

static void dump_socket_helper(SOCKET s, int level, int optname, int type, const char *label);

//
//
//

void dump_socket(SOCKET s)
{
	TRACE("Dumping socket %i...\n", s);

	trace_indent();

	sockaddr name = { 0 };

	int namelen;

	namelen = sizeof(name);

	if (getsockname(s, &name, &namelen) == 0)
		trace("getsockname: ", dump_addr(&name, namelen), "\n");

	ZERO_INIT(name);

	namelen = sizeof(name);

	if (getpeername(s, &name, &namelen) == 0)
		trace("getpeername: ", dump_addr(&name, namelen), "\n");

	for (unsigned int i = 0; i < DIM(g_socket_options); i++)
	{
		dump_socket_helper(s,
			g_socket_options[i].level,
			g_socket_options[i].optname,
			g_socket_options[i].type,
			g_socket_options[i].label);
	}

	trace_unindent();
}

//
//
//

static void dump_socket_helper(SOCKET s, int level, int optname, int type, const char *label)
{
	union
	{
		BOOL             b;
		int              i;
		GROUP            g;
		LINGER           l;
		unsigned int     u;
		WSAPROTOCOL_INFO p;
		DWORD            d;
		char             c;
	} value = { 0 };

	int optlen = sizeof(value);

	int result = getsockopt(s, level, optname, (char*)&value, &optlen);

	TRACE("%s=", label);

	int error;

	if (check_result(result, error))
	{
		switch (type)
		{
//			case 1: /*_ASSERT(optlen == sizeof(value.b));*/ TRACE("%i", value.b); break; // XXX: asserts (optlen == 1 && sizeof(value.b) == 4)
			case 1: if (optlen == 1) { _ASSERT(optlen == sizeof(value.c)); TRACE("%i", value.c); } else { _ASSERT(optlen == sizeof(value.b)); TRACE("%i", value.b); } break;
			case 2: _ASSERT(optlen == sizeof(value.i)); TRACE("%i", value.i); break;
			case 3: _ASSERT(optlen == sizeof(value.g)); TRACE("%i", value.g); break; // XXX: asserts on CE
			case 4: _ASSERT(optlen == sizeof(value.l)); TRACE("%i,%i", value.l.l_onoff, value.l.l_linger); break;
			case 5: _ASSERT(optlen == sizeof(value.u)); TRACE("%i", value.u); break;
			case 6: _ASSERT(optlen == sizeof(value.p)); TRACE("szProtocol=%s\n", value.p.szProtocol); hex_dump(value.p); break;
			case 7: _ASSERT(optlen == sizeof(value.d)); TRACE("%i", value.d); break;
			default: TRACE("\n"); hex_dump(&value, optlen); break;
		}

		TRACE("\n");
	}
	else
	{
	}
}

#endif

#endif

//
//
//

#if defined(_MSC_VER) && (_MSC_VER <= 1200)

struct ws2_32_exports_t
{
	INT (WSAAPI *getaddrinfo)(PCSTR pNodeName, PCSTR pServiceName, const ADDRINFOA *pHints, PADDRINFOA *ppResult);
	VOID (WSAAPI *freeaddrinfo)(PADDRINFOA pAddrInfo);
};

static export_module_t g_ws2_32_module = { "ws2_32.dll"  };

static ws2_32_exports_t g_ws2_32_exports;

INT
WSAAPI
getaddrinfo(
	PCSTR               pNodeName,
	PCSTR               pServiceName,
	const ADDRINFOA *   pHints,
	PADDRINFOA *        ppResult
	)
{
	static export_info_t export__ = { &g_ws2_32_module, "getaddrinfo", &g_ws2_32_exports.getaddrinfo };

	get_addr(export__);

	if (g_ws2_32_exports.getaddrinfo != NULL)
		return g_ws2_32_exports.getaddrinfo(pNodeName, pServiceName, pHints, ppResult);

	return -1;
}

VOID
WSAAPI
freeaddrinfo(
	PADDRINFOA      pAddrInfo
	)
{
	static export_info_t export__ = { &g_ws2_32_module, "freeaddrinfo", &g_ws2_32_exports.freeaddrinfo };

	get_addr(export__);

	if (g_ws2_32_exports.freeaddrinfo != NULL)
		g_ws2_32_exports.freeaddrinfo(pAddrInfo);
}

#endif

#endif

//
//
//

static void winsock_trace(const char *prefix, int result, int err)
{
	const char *text = get_socket_error_string(err);

	if (prefix != NULL && prefix[0] != 0)
		message(prefix, ": result=", result, ", error=", err, ", ", text, "\n");
	else
		message("result=", result, ", error=", err, ", ", text, "\n");
}

// XXX: copied from posix_call_failed (Windows doesn't appear to set errno since
// the send/recv calls don't communicate with the CRT)

int winsock_call_failed(const char *prefix, int result)
{
	// immediately capture error code in case we cause any side-effects here

	int err = WSAGetLastError();

	winsock_trace(prefix, result, err);

	return err;
}

