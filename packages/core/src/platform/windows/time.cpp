/*------------------------------------------------------------------------------

	platform/windows/time.cpp - Windows time support

------------------------------------------------------------------------------*/

#include "pch.h"
#include <time.h>
#include "core.h"

#include "../../lib/elements/time.h"
#include "../../lib/elements/numeric.h"
#include "../../lib/elements/string.h"
#include "../../lib/misc/misc.h"

#ifndef LITE_RTL

static const time_type_t g_time_type_filetime = { NULL };
static const time_type_t g_time_type_filetime_utc = { NULL };
static const time_type_t g_time_type_filetime_local = { NULL };
static const time_type_t g_time_type_100ns = { NULL };

const time_type_t g_time_type_perf = { NULL };

static const f64 FileTimePeriod = 1.0 / 10000000.0; // 100ns per FILETIME unit

/*

	art_time_type_100ns

	if we were to use nanoseconds as our clock resolution and get rid of the
	100ns unit, how does that affect our dynamic range for 64-bit integer
	representations?

	1000000000 nsec/sec

	60 sec/min * 60 min/hr * 24 hr/day = 86400 sec/day

	2^64/86400/1000000000=~213503.98 days @ 1 ns resolution (~586 years)

	log10(2^64)=~19.27 decimal digits in a 64-bit number

*/

//
//
//

bool time_compare(const art_time_t &t1, const art_time_t &ref, int &ans)
{
	FILETIME ft1, ft2;

	if (!time_to_filetime(time_utc(t1), ft1))
		return false;

	if (!time_to_filetime(time_utc(ref), ft2))
		return false;

	ans = CompareFileTime(&ft1, &ft2);

	return true;
}

/*

	NOTE:  The number of seconds from the FILETIME base date of midnight
	(00:00:00), January 1, 1601 to the time_t base date of midnight (00:00:00),
	January 1, 1970 is 116444736000000000.

	The number of years between these dates is 369.  There are 89 leap days
	during this time (369 / 4 minus 1700, 1800, and 1900).  At 365 days per
	year, this adds up to 134774 days (369 * 365 + 89).

	There are 86400 seconds in a day (24 * 60 * 60).

	134774 * 86400 = 11644473600

	FILETIME = (time_t * 10000000) + SecsTo1970
	time_t = (FILETIME - SecsTo1970) / 10000000

*/

#if 0
static const SYSTEMTIME g_unix_basis = { 1970, 1, 0, 1 };
#endif

#if defined(_MSC_VER)
static const u64 SecsTo1970 = 116444736000000000;
#elif defined (__GNUC__)
static const u64 SecsTo1970 = 116444736000000000LL;
#endif

// REF: MSDN Q167296 - HOWTO: Convert a UNIX time_t to a Win32 FILETIME or
// SYSTEMTIME

static void TimetToFileTime(time_t t, LPFILETIME pft)
{
	_ASSERT(pft != NULL);

	// filetime = (time_t * 10000000) + SecsTo1970
	
#ifdef _DEBUG

	SYSTEMTIME st = { 0 };

	st.wYear = 1970;
	st.wMonth = 1;
	st.wDay = 1;

	FILETIME ft;
	
	SystemTimeToFileTime(&st, &ft);

	u64 u = filetime_to_u64(ft);

	_ASSERT(u == SecsTo1970);
	
	ft = u64_to_filetime(u + 10000000); // add one second

	if (FileTimeToSystemTime(&ft, &st))
	{
	}

#endif

	*pft = u64_to_filetime(SecsTo1970 + UInt32x32To64(t, 10000000));
}

art_time_t time_from_time_t(s32 t)
{
	FILETIME filetime;
	
	TimetToFileTime(t, &filetime);

	return time_from_filetime_utc(filetime);
}

//
//
//

// XXX: what kind of FILETIME are we expecting?  UTC? Local? Span?

bool time_to_filetime(const art_time_t &data, FILETIME &ft)
{
	if (data.type == &g_time_type_100ns)
	{
		ft = u64_to_filetime(data.value);
		return true;
	}

	if (data.type == &g_time_type_filetime)
	{
		ft = u64_to_filetime(data.value);
		return true;
	}

	if (data.type == &g_time_type_filetime_utc)
	{
		ft = u64_to_filetime(data.value);
		return true;
	}

	if (data.type == &g_time_type_filetime_local)
	{
		ft = u64_to_filetime(data.value);
		return true;
	}

	u64 ms;
	
	if (time_milliseconds(data, &ms))
	{
		ft = u64_to_filetime(ms * 10000);
		return true;
	}

	if (data.type == &g_time_type_perf)
	{
		DBG_NOT_IMPLEMENTED();
		return false;
	}

	return false;
}

//
//
//

art_time_t time_from_filetime_utc(const FILETIME &ref)
{
	return time_data(&g_time_type_filetime_utc, filetime_to_u64(ref));
}

//
//
//

art_time_t time_from_filetime(const FILETIME &ref)
{
	// XXX: unknown time zone
	
	return time_data(&g_time_type_filetime, filetime_to_u64(ref));
}

//
//
//

art_time_t time_from_systemtime(const SYSTEMTIME &systime)
{
	FILETIME filetime = { 0 };

	if (SystemTimeToFileTime(&systime, &filetime))
	{
		// NOTE: the returned value has a precision of 1 millisecond

		return time_from_filetime(filetime); // XXX: unknown time zone
	}
	else
	{
		DWORD dwError = GetLastError();

		return time_error(dwError);
	}
}

//
//
//

bool time_to_systemtime(const art_time_t &data, SYSTEMTIME &st)
{
	if (data.type == &g_time_type_100ns)
	{
		FILETIME ft = { 0 };
			
		if (time_to_filetime(data, ft))
		{
			if (FileTimeToSystemTime(&ft, &st))
				return true;

			DWORD dwError = GetLastError();
		}
	}
	else if (data.type == &g_time_type_filetime ||
		data.type == &g_time_type_filetime_utc ||
		data.type == &g_time_type_filetime_local)
	{
		FILETIME ft = u64_to_filetime(data.value);
			
		if (FileTimeToSystemTime(&ft, &st))
			return true;

		DWORD dwError = GetLastError();
	}

	return false;
}

//
//
//

string_t time_local_time_zone_name()
{
	TIME_ZONE_INFORMATION tzi = { 0 };

	DWORD result = GetTimeZoneInformation(&tzi);
	
	switch (result)
	{
		case TIME_ZONE_ID_INVALID:
			return string_error();

		case TIME_ZONE_ID_UNKNOWN:
			return "";

		case TIME_ZONE_ID_STANDARD:
			return to_ansi_if_appropriate(tzi.StandardName);

		case TIME_ZONE_ID_DAYLIGHT:
			return to_ansi_if_appropriate(tzi.DaylightName);

		default:
			DBG_INVALID_CASE();
			return string_error();
	}
}

//
//
//

bool platform_time_delta_seconds(time_data_t delta, f64 &sec)
{
	if (delta.type == &g_time_type_perf)
	{
		sec = (s64)delta.value * get_timer_period();

		return true;
	}
	else if (delta.type == &g_time_type_100ns)
	{
		sec = (s64)delta.value * FileTimePeriod;

		return true;
	}

	return false;
}

art_time_t platform_time_delta(time_data_t t1, time_data_t t2)
{
	if (t1.type == &g_time_type_perf)
	{
		if (t2.type == &g_time_type_perf)
			return time_data(&g_time_type_perf, t2.value - t1.value);
	}

	FILETIME ft1, ft2;

	if (time_to_filetime(t1, ft1) && time_to_filetime(t2, ft2))
	{
		u64 start = filetime_to_u64(ft1);
		u64 stop = filetime_to_u64(ft2);

		return time_data(&g_time_type_100ns, stop - start);
	}

	return time_error();
}

art_time_t platform_time_add(time_data_t t1, time_data_t t2)
{
	//
	//
	//
	
	if (t1.type == &g_time_type_perf)
	{
		u64 ms;
		
		if (time_milliseconds(t2, &ms))
		{
			if (timer_info())
			{
				// NOTE: we're using signed 64 bit integers since we get the
				// following error with MSVC++ 6.0: error C2520: conversion from
				// unsigned __int64 to double not implemented, use signed __int64

				s64 start = t1.value;
				s64 stop = t2.value;

				stop = stop * g_core_global_data.timer_info.factor2__; // XXX: datatype conversion, possible loss of data

				return time_data(&g_time_type_perf, start + stop);
			}
		}
	}

	//
	//
	//

	FILETIME ft1, ft2;

	if (time_to_filetime(t1, ft1) && time_to_filetime(t2, ft2))
	{
		u64 li1 = filetime_to_u64(ft1);
		u64 li2 = filetime_to_u64(ft2);

		return time_data(&g_time_type_100ns, li1 + li2);
	}

	//
	//
	//

	return time_error();
}

art_time_t platform_time_local(art_time_t ref)
{
	if (ref.type == &g_time_type_filetime_local)
		return ref;

	if (ref.type == &g_time_type_filetime_utc)
	{
		FILETIME ftLocal, ftUTC;

		ftUTC = u64_to_filetime(ref.value);

		// NOTE: this function uses TimeZoneBias in the KUSER_SHARED_DATA area
		// to adjust the FILETIME, it always returns TRUE

		if (FileTimeToLocalFileTime(&ftUTC, &ftLocal))
			return time_data(&g_time_type_filetime_local, filetime_to_u64(ftLocal));

		DBG_BREAK();

		DWORD dwError = GetLastError();

		return time_error(dwError);
	}

	return time_error();
}

art_time_t platform_time_utc(art_time_t ref)
{
	if (ref.type == &g_time_type_filetime_utc)
		return ref;

	if (ref.type == &g_time_type_filetime_local)
	{
		FILETIME ftLocal, ftUTC;

		ftLocal = u64_to_filetime(ref.value);

		if (LocalFileTimeToFileTime(&ftLocal, &ftUTC))
			return time_from_filetime_utc(ftUTC);

		DWORD dwError = GetLastError();

		return time_error(dwError);
	}

	return time_error();
}

#ifndef REMOVE_THIS

static string_t format_time(u64 time)
{
	// NOTE: MSVC++ 6.0 unsigned 64 bit integer to double conversion is not implemented
	f64 sec = (s64)time / 10000000.0;

	f64 x = sec;

	int h = ((sec / 60.0) / 60.0); // XXX: double to int conversion
	x = x - h * (60.0 * 60.0);
	int m = (x / 60.0); // XXX: double to int conversion
	x = x - m * (60.0);
	int s = x; // XXX: double to int conversion
	x = x - s;
	int ms = ((1000.0 * x) + 0.5); // round to nearest integer // XXX: double to int conversion

//	return format_string("%f sec --> %i:%02i:%02i.%03i", sec, h, m, s, ms);
	return format_string("%i:%02i:%02i.%03i", h, m, s, ms);
}

static string_t format_time(const art_time_t &time)
{
	if (time.type == &g_time_type_100ns)
		return format_time(time.value);

	return "";
}

#endif

void platform_time_print(print_info_t *info, const time_data_t *obj)
{
	const time_data_t &time = *obj;
	
	bool has_date = false;
	bool has_time = false;

	if (time.type == &g_time_type_error)
	{
		print(info, string_error(FormatSystemMessage(time.value)));
		return;
	}
	else if (time.type == &g_time_type_100ns)
	{
		// XREF: string_t format_time(u64 time)

		// NOTE: MSVC++ 6.0 unsigned 64 bit integer to double conversion is not implemented
		f64 sec = (s64)time.value * FileTimePeriod;

		f64 x = sec;

		int h = ((sec / 60.0) / 60.0); // XXX: double to int conversion
		x = x - h * (60.0 * 60.0);
		int m = (x / 60.0); // XXX: double to int conversion
		x = x - m * (60.0);
		int s = x; // XXX: double to int conversion
		x = x - s;
		int ms = ((1000.0 * x) + 0.5); // round to nearest integer // XXX: double to int conversion

	//	return format_string("%f sec --> %i:%02i:%02i.%03i", sec, h, m, s, ms);
		print(info, format_string("%i:%02i:%02i.%03i", h, m, s, ms));
		return;
	}
	else if (time.type == &g_time_type_filetime)
	{
		has_date = true;
		has_time = true;
	}
	else if (time.type == &g_time_type_filetime_local)
	{
		has_date = true;
		has_time = true;
	}
	else if (time.type == &g_time_type_filetime_utc)
	{
		has_date = true;
		has_time = true;
	}
	else if (time.type == &g_time_type_perf)
	{
		DBG_NOT_IMPLEMENTED();
#ifdef _DEBUG
		print(info, "platform_time_print: not implemented for perf, value=", obj->value);
#endif
		return;
	}
	else
	{
		DBG_INVALID_CASE();
		return;
	}

	const char *format = "MMM dd',' yyyy"; // XXX: hard-coded

	string_t ans;

	char buffer[64];

	SYSTEMTIME st = { 0 };

	if (!time_to_systemtime(time, st))
		memset(&st, 0, sizeof(st));

	if (has_date)
	{
		// XXX: no UNICODE support
//		if (GetDateFormat(LOCALE_USER_DEFAULT, DATE_SHORTDATE, &st, NULL/*pszFormat*/, buffer, DIM(buffer)))
		if (GetDateFormatA(LOCALE_USER_DEFAULT, 0, &st, format, buffer, DIM(buffer)))
		{
			ans += buffer;
		}
		else
		{
			print(info, FormatLastSystemMessage());
			return;
		}
	}

	if (has_time)
	{
		DWORD dwFlags = 0;

/*
		if (time.m_flags_raw & TF_NOSEC)
			dwFlags |= TIME_NOSECONDS;
*/

		// XXX: no UNICODE support
//		if (GetTimeFormat(LOCALE_USER_DEFAULT, dwFlags, &st, NULL, buffer, DIM(buffer)))
		if (GetTimeFormatA(LOCALE_USER_DEFAULT, dwFlags, &st, "hh':'mm':'ss tt", buffer, DIM(buffer)))
		{
			if (ans.length() > 0)
				ans += ' ';

			ans += buffer;
		}
		else
		{
			print(info, FormatLastSystemMessage());
			return;
		}
	}

	print(info, ans);
}

time_data_t time_data(const FILETIME *ft)
{
	_ASSERT(ft != NULL);
	
	return time_data(&g_time_type_100ns, filetime_to_u64(*ft));
}

time_data_t time_data_100ns(u64 value)
{
	return time_data(&g_time_type_100ns, value);
}

#endif

