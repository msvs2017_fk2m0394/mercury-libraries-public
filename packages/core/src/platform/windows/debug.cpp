/*------------------------------------------------------------------------------

	platform/windows/debug.cpp - Windows diagnostics support

	XREF: "./diagnostics_dll.cpp"

------------------------------------------------------------------------------*/

#include "pch.h"
#include "core.h"
#include "debug.h"

#ifndef _WIN32_WCE
#include <signal.h>
#endif

#include "../../lib/debug/debug.h"
#include "../../lib/elements/string.h"

//
//
//

#ifndef LITE_RTL

static int g_debug_output_status; // 0 = unknown, -1 = no debugger, 1 = debugger

static struct 
{
	BOOL (WINAPI * IsDebuggerPresent)(VOID); // requires 98, NT 4.0, or later; not exported from kernel32.dll (4.00.950)
} g_kernel32_procs;

static export_info_t g_kernel32_exports[] = 
{
	{ &g_kernel32_module, "IsDebuggerPresent", &g_kernel32_procs.IsDebuggerPresent },
};

bool allow_debug_event_(int control)
{
	switch (control)
	{
		case 1:
		{
			// only output if being debugged, so we can avoid overhead if
			// we're not running under a debugger

//			if (g_debug_output_status == 0)
			{
				if (get_addr(g_kernel32_exports[0]) != NULL)
				{
				}
			}

			if (1)
			{
				//
				//  Check for debugger presence
				//

				if (g_kernel32_procs.IsDebuggerPresent != NULL)
				{
					// NOTE: IsDebuggerPresent() is a relatively fast call
					// since it simply returns a flag directly from the PEB
					// (accessible in user mode).

					if (g_kernel32_procs.IsDebuggerPresent())
					{
					}
					else
					{
						return false;
					}
				}
			}
			else
			{
				//
				//  Check IsDebuggerPresent only first time through
				//

				if (g_debug_output_status == 0)
				{
					if (g_kernel32_procs.IsDebuggerPresent != NULL)
					{
						if (g_kernel32_procs.IsDebuggerPresent())
						{
							g_debug_output_status = 1;
						}
						else
						{
							g_debug_output_status = -1;

							return false;
						}
					}
				}
				else if (g_debug_output_status == -1)
				{
					return false;
				}
				else
				{
					DBG_INVALID_CASE();
				}
			}

			return true;
		}

		default:
		{
			DBG_INVALID_CASE();

			break;
		}
	}

	DBG_BREAK();
	
	return true;
}

// XREF: debug_break()
// XREF: art_debug_break()

void win_debug_break()
{
	DebugBreak();
}

void win_debug_output_string(const char *text)
{
/*

	NOTE:  OutputDebugString sets up an exception frame and raises an exception
	with RaiseException(DBG_PRINTEXCEPTION_C, 0, 2, args).  If the process has a
	debugger attached, the debugger will ultimately get the notification from
	WaitForDebugEvent.  If the exception is unhandled, the system will do the
	section/event manipulation that DBMON (see MSDN library) can handle.

	The two parameters to the exception are:
	
	1) the number of bytes in the debug output text, including NULL terminator
	2) a pointer to a copy of the message, allocated from the heap

*/

	// NOTE: only UNICODE version is available under CE...
		
#ifdef UNICODE

	WCHAR line[512] = TEXT("");
	
	MultiByteToWideChar(CP_ACP, 0, text, strlen(text),
		line, DIM(line));

	OutputDebugString(line);

#else

	OutputDebugString(text);

#endif
}

//
//
//

#if CORE_CONFIG_TRACE_MEMORY

static HANDLE g_trace_memory_handle;

static void trace_callback_memory(const trace_callback_params_t *args)
{
	HANDLE hMapping = g_trace_memory_handle;

	if (hMapping == NULL)
	{
		DWORD flProtect = PAGE_READWRITE;
		
		HANDLE hFile = (HANDLE)0xFFFFFFFF;

		LPCTSTR lpName = NULL;
		
		string_t mapping_name = format_string("mercury_trace_%i", GetCurrentProcessId());
		
		hMapping = CreateFileMapping((HANDLE)0xFFFFFFFF, NULL, flProtect, SEC_COMMIT, 0, dwSize, mapping_name);

		if (hSection != NULL)
		{
			LPVOID data = MapViewOfFileEx(hSection, FILE_MAP_WRITE | FILE_MAP_READ, 0, 0, 0, NULL);

			if (data != NULL)
			{
			}
			else
			{
				GetLastError();
			}
		}
		else
		{
			GetLastError();
		}

/*
		VERIFY(UnMapViewOfFile(data));

		VERIFY(CloseHandle());
*/
	}
}

#endif

//
//
//

variant_t variant_system_error(DWORD code)
{
	return variant_error(FormatSystemMessage(code));

#if 0
	variant_error_t err;

	err.code = code;
	err.text = NULL;

	return make_variant(&err);
#endif
}

// TODO: replace most calls to this with variant_system_error()...

string_t FormatSystemMessage(DWORD dwMessageId)
{
	//
	//  Search for system message and update cache
	//
	
	LPTSTR lpMsgBuf;

	// use language lookup order defined by Win32 API FormatMessage
	DWORD dwLanguageId = 0; 

	DWORD dw = FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL, dwMessageId, dwLanguageId, (LPTSTR)&lpMsgBuf, 0, NULL);

	if (dw != 0)
	{
		trim_string(lpMsgBuf); // NOTE: Windows may have a CRLF at end of message format
		
		string_t msg;

		HRESULT hresult = HRESULT_FROM_WIN32(dwMessageId);
		
		if (SUCCEEDED(hresult))
			msg = lpMsgBuf;
		else
			msg = string_error(lpMsgBuf);

		VERIFY(LocalFree(lpMsgBuf) == NULL);

		return msg;
	}
	else
	{
		//
		//  Message not found.  Return a general error.
		//

		return string_error();
	}
}

//
//
//

static int g_debug_module_flag;
static module_t g_debug_module;
static debug_hooks_t g_debug_hooks;

int debug_hook_register(const debug_hooks_t *hooks)
{
	if (hooks != NULL)
	{
		if (hooks->version >= (sizeof(*hooks) / sizeof(uintptr_t)))
		{
			g_debug_hooks.ui_assert = hooks->ui_assert;
			g_debug_hooks.create_diagnostics_page = hooks->create_diagnostics_page;
			g_debug_hooks.create_memory_page = hooks->create_memory_page;
			g_debug_hooks.create_trace_output_page = hooks->create_trace_output_page;
			g_debug_hooks.create_profile_page = hooks->create_profile_page;
			g_debug_hooks.debug_flash = hooks->debug_flash;
			g_debug_hooks.draw_flash = hooks->draw_flash;
			g_debug_hooks.draw_blt_flash = hooks->draw_blt_flash;
			g_debug_hooks.draw_text_metrics = hooks->draw_text_metrics;

			return 1;
		}
	}
	else
	{
		ZERO_INIT(g_debug_hooks);
		
		return 1;
	}

	return 0;
}

debug_hooks_t debug_hooks()
{
	return g_debug_hooks;
}

void load_debug_module()
{
	if (g_debug_module_flag == 0)
	{
		// will call back into debug_hook_register from DllMain
		g_debug_module = module_load_extension("mercury-diag-3.2.dll");
		
		g_debug_module_flag = 1;
	}
}

#endif

