/*------------------------------------------------------------------------------

	db_odbc.h - support module for accessing ODBC data sources

------------------------------------------------------------------------------*/

#ifndef __DB_ODBC_H__
#define __DB_ODBC_H__

#include <mercury/odbc.h>

#include <sql.h>
#include <sqlext.h>

#if (_MSC_VER < 1400)
typedef SQLINTEGER SQLLEN;
typedef SQLUINTEGER SQLULEN;
#endif

#include <mercury/extensions/db.h>
#include <mercury/extensions/io.h>

//
//
//

struct odbc_msg_t
{
	RETCODE rc;
	SQLCHAR szMsg[SQL_MAX_MESSAGE_LENGTH];
	SQLCHAR szSqlState[SQL_SQLSTATE_SIZE];
	SDWORD  fNativeErr;
};

//
//
//

class odbc_environment_t : public base_object_t
{
public:
	virtual ~odbc_environment_t();

	HENV                    m_hEnv;          // ODBC environment handle
	ptr_array_t<odbc_msg_t> m_message_queue; //
};

//
//
//

class odbc_connection_t : public base_db_connection_t
{
public:
	// *** base_db_connection_t methods ***
	virtual bool close();
	virtual void attributes(db_cursor_t &cursor);
	virtual void catalog(int index, db_cursor_t &cursor);
	virtual void execute(db_execute_t &cmd, db_cursor_t &cursor);

	variant_t evaluate_field_connection_attribute(long i, int m_metadata_irow);
	unsigned int connection_attribute_count();

	object_ref_t<odbc_environment_t> m_environment;   //
	HDBC                             m_hdbc;          // ODBC connection handle
	ptr_array_t<odbc_msg_t>          m_message_queue; //

	OBJECT_CLASS_DECLARE()
};

//
//
//

struct odbc_column_t
{
	string_t       name;         //
	string_t       label;        //

	SQLSMALLINT    fSqlType;     //
	uintptr_t      maxlength;    //
	int            precision;    //
	int            scale;        //
	long           fDisplaySize; //
	long           fUnsigned;    //
	int            nullable;     //

#ifndef REMOVE_THIS

//	type_t         type__;       // format of bind_rgbValue buffer
	SQLSMALLINT    fCType;       //
	void *         rgbValue;     //
	SQLLEN *       pcbValue;     //

	data_binding_t binding;      //
#endif
};

//
//
//

#ifndef MOVE_THIS

template <typename T> struct dyn_array_t
{
	array_header_t header;
	T *            array;
};

#endif

//
//
//

struct odbc_row_buffer_t
{
	struct col_t { SQLSMALLINT fCType; SQLLEN *pcbValue; void *rgbValue; data_binding_t binding; };
	SQLULEN count;
	dyn_array_t<col_t> cols;
	void *ptr;
	SQLUSMALLINT *status; // row status indicators
};

//
//
//

class odbc_cursor_t : public base_db_cursor_t
{
public:
	virtual ~odbc_cursor_t();

	// *** base_db_cursor_t methods ***

	virtual bool      cancel();
	virtual bool      close();
	virtual void      attributes(db_cursor_t &cursor);
	virtual void      metadata(db_cursor_t &cursor);
	virtual int       fetch();
	virtual bool      more_results();
	virtual int       field_evaluate(int index, data_binding_t value);
	virtual variant_t evaluate_field(long i);
	virtual long      col_count();
	virtual string_t  col_name(long i);
	virtual string_t  col_label(long i);

	int field_evaluate__(unsigned int index, data_binding_t value);

#ifndef REMOVE_EXPERIMENTAL
	db_column_info_t column_info(const char *name);
#endif

	bool get_col_desc();
	void *alloc_rowbuf(unsigned int row_max, size_t cbrow);
	bool alloc_column_descriptors(unsigned int ccol);
	odbc_column_t &col_desc(unsigned int i) const;
	bool get_column_info();
	bool load_col_desc(unsigned int i);

	int fetch_sources();
	int fetch_drivers();

	enum CURSOR_MODE
	{
		MODE_NORMAL,
		MODE_DRIVERS,
		MODE_DATA_SOURCES,
		MODE_CONNECTION_ATTRIBUTES,
		MODE_CATALOG,
		MODE_CURSOR_ATTRIBUTES,
		MODE_METADATA,
		MODE_EXECPLAN
	};

	object_ref_t<odbc_connection_t>  m_connection;
	HSTMT                            m_hstmt;  // ODBC statement handle
	ptr_array_t<odbc_msg_t>          m_message_queue;

	bool                             m_colinfo_valid;
	dyn_array_t<odbc_column_t>       m_columns;
	odbc_row_buffer_t                m_row_buffer;

	CURSOR_MODE                      m_mode;
	int                              m_catalog;
	object_ref_t<odbc_environment_t> m_environment; //
	string_t                         m_dsn_name; // source or driver name
	string_t                         m_dsn_desc; // source or driver description
	unsigned int                     m_metadata_irow; // TODO: use db_cursor_object_t m_irec instead

	OBJECT_CLASS_DECLARE()
};

#ifndef REMOVE_EXPERIMENTAL

//
//  odbc_file_device_t - reads BLOB or large character string data
//

class odbc_file_device_t : public base_io_stream_t
{
public:
	odbc_file_device_t();

	virtual ~odbc_file_device_t();
	
	// *** base_io_stream_t methods ***
	virtual bool read(io_read_t &data);
	virtual bool write(io_write_t &data);
	virtual bool flush();
	virtual void get_position(file_pos_t &pos);
	virtual bool set_position(file_pos_t pos);
	virtual bool close();
	virtual bool read(void * buffer, u32 count, u32 &count_out);
	virtual bool write(const void *buffer, u32 count, u32 &count_out);
};

#endif

//
//
//

odbc_environment_t *odbc_env();

bool odbc_environment_init(object_ref_t<odbc_environment_t> &env);

bool check_result(odbc_environment_t *env, RETCODE rc);
bool check_result(odbc_connection_t *conn, RETCODE rc);
bool check_result(odbc_cursor_t *csr, RETCODE rc);

//
//
//

struct odbc_datatype_t
{
	SQLSMALLINT fSqlType;    // ODBC datatype (SQL_*)
	int         sign;        // 0=N/A, -1=signed, 1=unsigned
	SQLSMALLINT fCType;      // corresponding ODBC C datatype (SQL_C_*)
	type_t      type__;      // corresponding variant_t datatype
	size_t      size;        // ODBC C buffer size, 0 means variable length
//	int         scale__;
//	int         precision__;
};

const odbc_datatype_t * get_odbc_datatype(SQLSMALLINT fSqlType, long fUnsigned);
type_t                  odbc_to_variant_type(SQLSMALLINT fSqlType, long fUnsigned);
variant_t               odbc_to_variant(SQLSMALLINT fCType, const void *rgbValue, SQLLEN *pcbValue);

bool retrieve_messages(HENV henv, HDBC hdbc, HSTMT hstmt, RETCODE rc, ptr_array_t<odbc_msg_t> &queue);
bool odbc_success(RETCODE rc);

#ifndef REMOVE_EXPERIMENTAL
int odbc_field_evaluate(SQLSMALLINT fCType, const void *rgbValue, SQLLEN *pcbValue, data_binding_t value);
#endif

#endif // __DB_ODBC_H__

