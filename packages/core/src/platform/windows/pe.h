/*------------------------------------------------------------------------------

	platform/windows/pe.h - Portable Executable (PE) declarations

------------------------------------------------------------------------------*/

#ifndef __WIN_PE_H__
#define __WIN_PE_H__

//
//
//

#if !defined(_WIN32_WCE) && !defined(__GNUC__)
#include <delayimp.h>
#endif

#include "../../lib/misc/pe.h"

//
//
//

extern "C" {

const IMAGE_DOS_HEADER *     hmodule_dos_header(HMODULE hModule);
const IMAGE_FILE_HEADER *    hmodule_file_header(HMODULE hModule);
const IMAGE_NT_HEADERS32 *   hmodule_nt_headers32(HMODULE hModule);
const IMAGE_NT_HEADERS64 *   hmodule_nt_headers64(HMODULE hModule);
const void *                 hmodule_data_directory(HMODULE hModule, unsigned int index);
const IMAGE_DATA_DIRECTORY * hmodule_data_directory2(HMODULE hModule, unsigned int index);
const void *                 hmodule_rva_to_va(HMODULE hModule, uintptr_t addr);
const IMAGE_SECTION_HEADER * hmodule_rva_to_section(HMODULE hModule, uintptr_t rva);

const IMAGE_RESOURCE_DIRECTORY *module_resource_directory(module_t module);

}

extern "C" {

bool OS_LoadLibraryExA(LPCSTR lpLibFileName, DWORD dwFlags, HMODULE &hModule,
	DWORD &error);

HMODULE module_load2__(export_module_t *module, int mode, const void *caller);

}

//
//
//

extern "C" {

FARPROC OS_GetProcAddressA(HMODULE hModule, LPCSTR lpProcName);
FARPROC OS_GetProcAddressW(HMODULE hModule, LPCWSTR lpProcName);

}

extern "C" {

bool image_enum_imports__(HMODULE hModule, unsigned int index, struct module_symbol_t &symbol);
bool image_enum_exports__(HMODULE hModule, unsigned int index, struct module_symbol_t &symbol);

#if !defined(_WIN32_WCE) && !defined(__GNUC__)
extern "C" FARPROC WINAPI __delayLoadHelper(PCImgDelayDescr pidd, FARPROC *ppfnIATEntry);
#endif

}

//
//
//

#if !defined(_MSC_VER) || defined(_WIN32_WCE) || defined(__GNUC__)

#if 0

typedef IMAGE_THUNK_DATA *          PImgThunkData;
typedef const IMAGE_THUNK_DATA *    PCImgThunkData;

typedef struct ImgDelayDescr {
    DWORD           grAttrs;        // attributes
    LPCSTR          szName;         // pointer to DLL name
    HMODULE *       phmod;          // address of module handle
    PImgThunkData   pIAT;           // address of the IAT
    PCImgThunkData  pINT;           // address of the INT
    PCImgThunkData  pBoundIAT;      // address of the optional bound IAT
    PCImgThunkData  pUnloadIAT;     // address of optional copy of original IAT
    DWORD           dwTimeStamp;    // 0 if not bound,
                                    // O.W. date/time stamp of DLL bound to (Old BIND)
} ImgDelayDescr, * PImgDelayDescr;

typedef const ImgDelayDescr *   PCImgDelayDescr;

typedef struct DelayLoadProc {
    BOOL                fImportByName;
    union {
        LPCSTR          szProcName;
        DWORD           dwOrdinal;
        };
} DelayLoadProc;

typedef struct DelayLoadInfo {
    DWORD               cb;         // size of structure
    PCImgDelayDescr     pidd;       // raw form of data (everything is there)
    FARPROC *           ppfn;       // points to address of function to load
    LPCSTR              szDll;      // name of DLL
    DelayLoadProc       dlp;        // name or ordinal of procedure
    HMODULE             hmodCur;    // the hInstance of the library we have loaded
    FARPROC             pfnCur;     // the actual function that will be called
    DWORD               dwLastError;// error received (if an error notification)
} DelayLoadInfo, * PDelayLoadInfo;

#define FACILITY_VISUALCPP  ((LONG)0x6d)
#define VcppException(sev,err)  ((sev) | (FACILITY_VISUALCPP<<16) | err)

#define ExternC // extern "C"
#define __inline

#endif

#endif

#if !defined(_MSC_VER) || defined(_DELAY_IMP_VER) || defined(_WIN32_WCE)

// structure definitions for the list of unload records
typedef struct UnloadInfo * PUnloadInfo;
typedef struct UnloadInfo {
    PUnloadInfo     puiNext;
    PCImgDelayDescr pidd;
} UnloadInfo;

// the default delay load helper places the unloadinfo records in the list
// headed by the following pointer.
ExternC
extern
PUnloadInfo __puiHead__;

// utility function for calculating the count of imports given the base
// of the IAT.  NB: this only works on a valid IAT!
__inline unsigned
CountOfImports(PCImgThunkData pitdBase) {
    unsigned        cRet = 0;
    PCImgThunkData  pitd = pitdBase;
    while (pitd->u1.Function) {
        pitd++;
        cRet++;
        }
    return cRet;
    }

#endif

// from delayhlp.cpp...

extern "C" PUnloadInfo __puiHead__;

struct ULI : public UnloadInfo
{
    ULI(PCImgDelayDescr pidd_) {
        pidd = pidd_;
        Link();
        }

    ~ULI() {
        Unlink();
        }

    void *
    operator new(size_t cb) {
        return LocalAlloc(LPTR, cb);
        }

    void
    operator delete(void * pv) {
        LocalFree(pv);
        }

    void
    Unlink() {
        PUnloadInfo *   ppui = &__puiHead__;

        while (*ppui && *ppui != this) {
            ppui = &((*ppui)->puiNext);
            }
        if (*ppui == this) {
            *ppui = puiNext;
            }
        }

    void
    Link() {
        puiNext = __puiHead__;
        __puiHead__ = this;
        }
};

extern "C" FARPROC WINAPI core_mscrt_delayLoadHelper(
	const struct ImgDelayDescr *pidd, FARPROC *ppfnIATEntry);

#endif // __WIN_PE_H__

