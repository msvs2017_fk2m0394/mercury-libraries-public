/*------------------------------------------------------------------------------

	db_odbc.cpp - support module for accessing ODBC data sources

	advantages over raw ODBC:

	1.  callback mechanism for reliable message retrieval (a la DB-Lib)
	2.  automatic row and parameter buffer management for dynamic SQL
	3.  integrated with libraries through variant_t support
	4.  maintains current rowset and row numbers

	limitations vs. raw ODBC:

	1.  no cursor updates
	2.  no access to catalog functions
	3.  bulk operations
	4.  ...

	TODO: call SQLCancel if user aborts
	TODO: fetch more than one row at a time
	TODO: release environment handle when done

------------------------------------------------------------------------------*/

#include "pch.h"
#include "db_odbc.h"

time_data_t time_data(const FILETIME *ft);

#ifdef _MSC_VER
#pragma comment(lib, "odbc32.lib")
#endif

#ifndef REMOVE_THIS
#define vt_datetime dynamic_type_time()
#endif

//
//
//

static lock_t g_odbc_lock;
static odbc_environment_t *g_odbc_env;

//
//
//

static const odbc_datatype_t g_odbc_datatypes[] =
{
	{ SQL_CHAR,           0, SQL_C_CHAR,      type_code_string, 0                           },
//	{ SQL_NUMERIC,       -1, SQL_C_SSHORT,    type_code_s16,    sizeof(s16)                 }, // if scale == 0 and precision <= 5
//	{ SQL_NUMERIC,        1, SQL_C_USHORT,    type_code_u16,    sizeof(u16)                 }, // if scale == 0 and precision <= 5
//	{ SQL_NUMERIC,       -1, SQL_C_SLONG,     type_code_s32,    sizeof(s32)                 }, // if scale == 0 and precision <= 10
//	{ SQL_NUMERIC,        1, SQL_C_ULONG,     type_code_u32,    sizeof(u32)                 }, // if scale == 0 and precision <= 10
//	{ SQL_NUMERIC,        0, SQL_C_NUMERIC,   vt_numeric,       sizeof(SQL_NUMERIC_STRUCT)  },
//	{ SQL_DECIMAL,       -1, SQL_C_SSHORT,    type_code_s16,    sizeof(s16)                 }, // if scale == 0 and precision <= 5
//	{ SQL_DECIMAL,        1, SQL_C_USHORT,    type_code_u16,    sizeof(u16)                 }, // if scale == 0 and precision <= 5
//	{ SQL_DECIMAL,       -1, SQL_C_SLONG,     type_code_s32,    sizeof(s32)                 }, // if scale == 0 and precision <= 10
//	{ SQL_DECIMAL,        1, SQL_C_ULONG,     type_code_u32,    sizeof(u32)                 }, // if scale == 0 and precision <= 10
//	{ SQL_DECIMAL,        0, SQL_C_NUMERIC,   vt_numeric,       sizeof(SQL_NUMERIC_STRUCT)  },
	{ SQL_INTEGER,       -1, SQL_C_SLONG,     type_code_s32,    sizeof(s32)                 },
	{ SQL_INTEGER,        1, SQL_C_ULONG,     type_code_u32,    sizeof(u32)                 },
	{ SQL_SMALLINT,      -1, SQL_C_SSHORT,    type_code_s16,    sizeof(s16)                 },
	{ SQL_SMALLINT,       1, SQL_C_USHORT,    type_code_u16,    sizeof(u16)                 },
	{ SQL_FLOAT,          0, SQL_C_DOUBLE,    type_code_f64,    sizeof(f64)                 },
	{ SQL_REAL,           0, SQL_C_FLOAT,     type_code_f32,    sizeof(f32)                 },
	{ SQL_DOUBLE,         0, SQL_C_DOUBLE,    type_code_f64,    sizeof(f64)                 },
//	{ SQL_DATETIME,       0,                                                                },
	{ SQL_DATE,           0, SQL_C_DATE,      vt_datetime.init, sizeof(DATE_STRUCT)         }, // year/month/day
//	{ SQL_INTERVAL,       0,                                    sizeof(SQL_INTERVAL_STRUCT) },
	{ SQL_TIME,           0, SQL_C_TIME,      vt_datetime.init, sizeof(TIME_STRUCT)         }, // hour/minute/second
	{ SQL_TIMESTAMP,      0, SQL_C_TIMESTAMP, vt_datetime.init, sizeof(TIMESTAMP_STRUCT)    }, // year/month/day/hour/minute/second/fraction
	{ SQL_VARCHAR,        0, SQL_C_CHAR,      type_code_string, 0                           },

	{ SQL_LONGVARCHAR,    0, SQL_C_CHAR,      type_code_string, 0                           },
//	{ SQL_BINARY,         0, SQL_C_BINARY,    type_code_none,   0                           },
//	{ SQL_VARBINARY,      0, SQL_C_BINARY,    type_code_none,   0                           },
	{ SQL_NTS,            0, SQL_C_CHAR,      type_code_string, 0                           },
//	{ SQL_LONGVARBINARY,  0, SQL_C_BINARY,    type_code_none,   0                           },
	{ SQL_BIGINT,        -1, SQL_C_SBIGINT,   type_code_s64,    sizeof(s64)                 },
	{ SQL_BIGINT,         1, SQL_C_UBIGINT,   type_code_u64,    sizeof(u64)                 },
	{ SQL_TINYINT,       -1, SQL_C_STINYINT,  type_code_s8,     sizeof(s8)                  },
	{ SQL_TINYINT,        1, SQL_C_UTINYINT,  type_code_u8,     sizeof(u8)                  },
	{ SQL_BIT,            0, SQL_C_UTINYINT,  type_code_u8,     sizeof(u8)                  },
	{ SQL_WCHAR,          0, SQL_C_WCHAR,     type_code_string, 0                           },
	{ SQL_WVARCHAR,       0, SQL_C_WCHAR,     type_code_string, 0                           },
	{ SQL_WLONGVARCHAR,   0, SQL_C_WCHAR,     type_code_string, 0                           },
//	{ SQL_GUID,           0, SQL_C_GUID,      type_code_none,   sizeof(GUID)                },
};

//
//
//

const odbc_datatype_t *get_odbc_datatype(SQLSMALLINT fSqlType, long fUnsigned /*, int scale, int precision */)
{
	_ASSERT(fUnsigned == SQL_TRUE || fUnsigned == SQL_FALSE); // these are the only defined ODBC values

	for (unsigned int i = 0; i < DIM(g_odbc_datatypes); i++)
	{
		if (g_odbc_datatypes[i].fSqlType == fSqlType)
		{
			if (g_odbc_datatypes[i].sign == 0)
				return &g_odbc_datatypes[i];
			
			if (g_odbc_datatypes[i].sign == (fUnsigned ? 1 : -1))
				return &g_odbc_datatypes[i];
		}
	}

	return NULL;
}

//
//
//

type_t odbc_to_variant_type(SQLSMALLINT fSqlType, long fUnsigned)
{
	const odbc_datatype_t *inf = get_odbc_datatype(fSqlType, fUnsigned);

	if (inf != NULL)
		return inf->type__;

	return make_type(type_code_none);
}

//	
//	retrieve messages for errors and warnings
//

bool retrieve_messages(HENV henv, HDBC hdbc, HSTMT hstmt, RETCODE rc, ptr_array_t<odbc_msg_t> &queue)
{
	bool status = true;
	
	queue.clear();

	unsigned int count = 0;
	odbc_msg_t *msg = NULL;

	for (;;)
	{
		//
		//  Get next error from ODBC
		//

		odbc_msg_t temp = { 0 };
		
		temp.rc = rc;

		SWORD cbErrMsg;

		// warning C4457: declaration of 'rc' hides function parameter
		
		RETCODE rc = SQLError(henv, hdbc, hstmt, temp.szSqlState, //  warning C6386: Buffer overrun while writing to 'temp.szSqlState':  the writable size is '5' bytes, but '6' bytes might be written
			&temp.fNativeErr, temp.szMsg, sizeof(temp.szMsg), &cbErrMsg);

		_ASSERT(rc != SQL_SUCCESS_WITH_INFO); // buffers should be long enough
		_ASSERT(rc != SQL_INVALID_HANDLE); // should be valid

		if (rc != SQL_SUCCESS && rc != SQL_SUCCESS_WITH_INFO)
			break;

		count++;

#ifdef _DEBUG
		TRACE("ODBC message: %s (%*s)\n", temp.szMsg, SQL_SQLSTATE_SIZE, temp.szSqlState);
#endif

		//
		//
		//

		msg = queue.create_item();

		if (msg != NULL)
		{
			*msg = temp;
		}
		else
		{
			status = false;

			break;
		}
	}

	return status;
}

//
//
//

bool odbc_success(RETCODE rc)
{
	switch (rc)
	{
		case SQL_SUCCESS:
		case SQL_SUCCESS_WITH_INFO:
			return true;

		case SQL_ERROR:
			break;

		case SQL_NO_DATA_FOUND: // FIX-ME
			break;

//		case SQL_NEED_DATA: ???

		default:
			DBG_INVALID_CASE();
			break;
	}

	return false;
}

//
//
//

bool check_result(odbc_environment_t *env, RETCODE rc)
{
	retrieve_messages(env->m_hEnv, NULL, NULL, rc, env->m_message_queue);

	return odbc_success(rc);
}

//
//  ODBC environment handle
//

odbc_environment_t *odbc_env()
{
	odbc_environment_t *env = NULL;
	
	g_odbc_lock.enter();
	
	env = g_odbc_env;
	
	if (env == NULL)
	{
		env = new odbc_environment_t;

		if (env != NULL)
		{
			SQLAllocEnv(&env->m_hEnv);

			if (env->m_hEnv != NULL)
			{
				g_odbc_env = env;
			}
			else
			{
				env->release();

				env = NULL;
			}
		}
	}
	else
	{
		env->retain(); // retain existing environment
	}

	g_odbc_lock.leave();

	return env;
}

odbc_environment_t::~odbc_environment_t()
{
	if (m_hEnv != NULL)
	{
		if (SQLFreeEnv(m_hEnv) != SQL_SUCCESS)
			DBG_BREAK();

		m_hEnv = NULL;
	}
}

bool odbc_environment_init(object_ref_t<odbc_environment_t> &m_environment)
{
 	if (m_environment != NULL)
		return true;
	
	odbc_environment_t *env = odbc_env();
	
	if (env != NULL)
	{
		m_environment = env;

//		env->release();

		return true;
	}

	return false;
}

//
//
//

static bool time_convert_to(const art_time_t &data, DATE_STRUCT &time);
static bool time_convert_to(const art_time_t &data, TIME_STRUCT &time);
static bool time_convert_to(const art_time_t &data, TIMESTAMP_STRUCT &time);
static bool time_convert_to(const art_time_t &data, SQL_INTERVAL_STRUCT &time);

static art_time_t time_create(const DATE_STRUCT &ref);
static art_time_t time_create(const TIME_STRUCT &ref);
static art_time_t time_create(const TIMESTAMP_STRUCT &ref);

//
//
//

#ifndef REMOVE_EXPERIMENTAL

int odbc_field_evaluate(SQLSMALLINT fCType, const void *rgbValue, SQLLEN *pcbValue, data_binding_t value)
{
	DBG_NOT_IMPLEMENTED();
	
	return 0;
}

#endif

variant_t odbc_to_variant(SQLSMALLINT fCType, const void *rgbValue, SQLLEN *pcbValue)
{
	//
	//
	//
	
	if (pcbValue != NULL && *pcbValue == SQL_NULL_DATA)
		return variant_t();

	//
	//
	//
	
	if (rgbValue == NULL)
		return variant_error("odbc_to_variant(): no data to convert");

	//
	//
	//
	
	switch (fCType)
	{
		case SQL_C_CHAR:
		{
			if (pcbValue == NULL)
				return variant_t((const char *)rgbValue);
			else
				return variant_t(string((const char *)rgbValue, *pcbValue));
		}

		case SQL_C_WCHAR:
		{
			if (pcbValue == NULL)
				return variant_t((const wchar_t *)rgbValue);
			else
			{
				_ASSERT((*pcbValue % 2) == 0);
				
				return variant_t(string((const wchar_t *)rgbValue, *pcbValue / 2));
			}
		}

		case SQL_C_SHORT:
		case SQL_C_SSHORT:
//			_ASSERT(pcbValue == NULL || *pcbValue == 0 || *pcbValue == sizeof(short));
			return variant_t(*((const signed short *)rgbValue));

		case SQL_C_USHORT:
//			_ASSERT(pcbValue == NULL || *pcbValue == 0 || *pcbValue == sizeof(short));
			return variant_t(*((const unsigned short *)rgbValue));

		case SQL_C_LONG:
		case SQL_C_SLONG:
//			_ASSERT(pcbValue == NULL || *pcbValue == 0 || *pcbValue == sizeof(long));
			return variant_t(*((const signed long *)rgbValue));

		case SQL_C_ULONG:
//			_ASSERT(pcbValue == NULL || *pcbValue == 0 || *pcbValue == sizeof(long));
			return variant_t(*((const unsigned long *)rgbValue));

		case SQL_C_FLOAT:
//			_ASSERT(pcbValue == NULL || *pcbValue == 0 || *pcbValue == sizeof(float));
			return variant_t(*((const float *)rgbValue));

		case SQL_C_DOUBLE:
//			_ASSERT(pcbValue == NULL || *pcbValue == 0 || *pcbValue == sizeof(double));
			return variant_t(*((const double *)rgbValue));

		case SQL_C_DATE:
//			_ASSERT(pcbValue == NULL || *pcbValue == 0 || *pcbValue == sizeof(DATE_STRUCT));
			return time_create(*((const DATE_STRUCT *)rgbValue));

		case SQL_C_TIME:
//			_ASSERT(pcbValue == NULL || *pcbValue == 0 || *pcbValue == sizeof(TIME_STRUCT));
			return time_create(*((const TIME_STRUCT *)rgbValue));

		case SQL_C_TIMESTAMP:
//			_ASSERT(pcbValue == NULL || *pcbValue == 0 || *pcbValue == sizeof(TIMESTAMP_STRUCT));
			return time_create(*((const TIMESTAMP_STRUCT *)rgbValue));

/*
		case SQL_C_BINARY:
		{
			if (pcbValue != NULL)
			{
				SDWORD cb = *pcbValue;

				if (cb > 0)
				{
					art_snprintf(pszVal, cchValMax, "0x");

					const u8 *prb = (u8*)rgbValue;

					while (cb > 0)
					{
						art_snprintf(pszVal + strlen(pszVal), cchValMax - strlen(pszVal),
							"%02X", *prb);

						prb++;
						cb--;
					}
				}
			}

			break;
		}

*/

		case SQL_C_SBIGINT:
			return variant_t(*((s64 *)rgbValue));

		case SQL_C_UBIGINT:
			return variant_t(*((u64 *)rgbValue));

		case SQL_C_TINYINT:
		case SQL_C_STINYINT:
			return variant_t(*((signed char *)rgbValue));
			
		case SQL_C_UTINYINT:
			return variant_t(*((unsigned char *)rgbValue));

		case SQL_C_NUMERIC:
		{
			const SQL_NUMERIC_STRUCT *ns = (SQL_NUMERIC_STRUCT*)rgbValue;
			
			DBG_NOT_IMPLEMENTED();
			
			break;
		}

		case SQL_C_TYPE_DATE:
		case SQL_C_TYPE_TIME:
		case SQL_C_TYPE_TIMESTAMP:
		case SQL_C_INTERVAL_YEAR:
		case SQL_C_INTERVAL_MONTH:
		case SQL_C_INTERVAL_DAY:
		case SQL_C_INTERVAL_HOUR:
		case SQL_C_INTERVAL_MINUTE:
		case SQL_C_INTERVAL_SECOND:
		case SQL_C_INTERVAL_YEAR_TO_MONTH:
		case SQL_C_INTERVAL_DAY_TO_HOUR:
		case SQL_C_INTERVAL_DAY_TO_MINUTE:
		case SQL_C_INTERVAL_DAY_TO_SECOND:
		case SQL_C_INTERVAL_HOUR_TO_MINUTE:
		case SQL_C_INTERVAL_HOUR_TO_SECOND:
		case SQL_C_INTERVAL_MINUTE_TO_SECOND:
		case SQL_C_BINARY:
		case SQL_C_BIT:
		case SQL_C_GUID:
			DBG_NOT_IMPLEMENTED();
			break;

		default:
			DBG_INVALID_CASE();
			break;
	}

	return variant_error(format_string("odbc_to_variant(): ODBC C datatype %i not supported.",
		fCType));
}

//
//
//

static art_time_t time_create(const DATE_STRUCT &ref)
{
#ifdef _WINDOWS

	SYSTEMTIME st = { 0 };

	st.wYear  = ref.year;
	st.wMonth = ref.month;
	st.wDay   = ref.day;

	FILETIME ft;

	// TODO: indicate that only the date portion is valid
	DBG_NOT_IMPLEMENTED();

	if (SystemTimeToFileTime(&st, &ft))
		return time_data(&ft);

	DBG_BREAK();

	DWORD dwError = GetLastError();

	return time_error(dwError);

#endif
}

//
//
//

static art_time_t time_create(const TIME_STRUCT &ref)
{
	SYSTEMTIME st = { 0 };

	st.wHour   = ref.hour;
	st.wMinute = ref.minute;
	st.wSecond = ref.second;

	FILETIME ft;

	// TODO: indicate that only the time portion is valid
	DBG_NOT_IMPLEMENTED();
	
	if (SystemTimeToFileTime(&st, &ft))
		return time_data(&ft);

	DBG_BREAK();
		
	DWORD dwError = GetLastError();

	return time_error(dwError);
}

//
//
//

static art_time_t time_create(const TIMESTAMP_STRUCT &tss)
{
	SYSTEMTIME st = { 0 };

	st.wYear         = tss.year;
	st.wMonth        = tss.month;
	st.wDay          = tss.day;
	st.wHour         = tss.hour;
	st.wMinute       = tss.minute;
	st.wSecond       = tss.second;
	st.wMilliseconds = (WORD)(tss.fraction / 1000); // XXX: is this right???

	FILETIME ft;

	if (SystemTimeToFileTime(&st, &ft))
		return time_data(&ft);

	DBG_BREAK();
		
	DWORD dwError = GetLastError();

	return time_error(dwError);
}

//
//
//

db_cursor_t db_odbc_drivers()
{
	odbc_cursor_t *driver = new odbc_cursor_t;

	if (driver != NULL)
	{
		driver->m_mode = odbc_cursor_t::MODE_DRIVERS;

		unsigned int ccol = 2;
		
		if (driver->alloc_column_descriptors(ccol))
		{
			driver->col_desc(0).name = "Description";
			driver->col_desc(0).label = driver->col_desc(0).name;

			driver->col_desc(1).name = "Attributes";
			driver->col_desc(1).label = driver->col_desc(1).name;
		}
	}

	return db_cursor_create_and_release(driver, true);
}

//
//
//

db_cursor_t db_odbc_sources()
{
	odbc_cursor_t *driver = new odbc_cursor_t;

	if (driver != NULL)
	{
		driver->m_mode = odbc_cursor_t::MODE_DATA_SOURCES;

		unsigned int ccol = 2;
		
		if (driver->alloc_column_descriptors(ccol))
		{
			driver->col_desc(0).name = "Name";
			driver->col_desc(0).label = driver->col_desc(0).name;

			driver->col_desc(1).name = "Description";
			driver->col_desc(1).label = driver->col_desc(1).name;
		}
	}

	return db_cursor_create_and_release(driver, true);
}

//
//
//

int odbc_cursor_t::fetch_drivers()
{
	_ASSERT(m_mode == MODE_DRIVERS);

	if (odbc_environment_init(m_environment))
	{
		SWORD cbName = 0, cbDesc = 0;
		SQLCHAR szName[256] = "";
		SQLCHAR szDesc[256] = "";

		// SQL_FETCH_FIRST 

		RETCODE rc = SQLDrivers(m_environment->m_hEnv, SQL_FETCH_NEXT,
			szName, DIM(szName), &cbName, szDesc, DIM(szDesc), &cbDesc);

		if (check_result(m_environment, rc))
		{
			m_dsn_name = (const char *)szName;
			m_dsn_desc = (const char *)szDesc;

			return 0;
		}
	}

	return -1;
}

//
//
//

int odbc_cursor_t::fetch_sources()
{
	_ASSERT(m_mode == MODE_DATA_SOURCES);

	if (odbc_environment_init(m_environment))
	{
		SWORD cbName = 0, cbDesc = 0;
		SQLCHAR szName[SQL_MAX_DSN_LENGTH + 1] = "";
		SQLCHAR szDesc[256] = "";

		// SQL_FETCH_FIRST 
		// SQL_FETCH_FIRST_USER
		// SQL_FETCH_FIRST_SYSTEM		
		
		RETCODE rc = SQLDataSources(m_environment->m_hEnv, SQL_FETCH_NEXT,
			szName, DIM(szName), &cbName, szDesc, DIM(szDesc), &cbDesc);

		if (check_result(m_environment, rc))
		{
			m_dsn_name = (const char *)szName;
			m_dsn_desc = (const char *)szDesc;

			return 0;
		}
	}

	return -1;
}

