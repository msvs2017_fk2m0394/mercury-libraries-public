/*------------------------------------------------------------------------------

	platform/windows/pipe.cpp - Windows I/O support

	TODO: support PeekNamedPipe() to get number of bytes available?
	TODO: provide options for buffering pipe I/O
	TODO: flush on close?

------------------------------------------------------------------------------*/

#include "pch.h"
#include <mercury/extensions/io.h>
#include "io.h"

#ifndef LITE_RTL

//
//
//

class windows_pipe_t : public base_io_stream_t
{
public:
	windows_pipe_t();
	~windows_pipe_t();

	// *** base_io_stream_t methods ***

	virtual bool close();
	virtual bool read(void *buffer, u32 count, u32 &count_out);
	virtual bool write(const void *buffer, u32 count, u32 &count_out);
	virtual bool flush();

	HANDLE m_hReadPipe;
	HANDLE m_hWritePipe;

	OBJECT_CLASS_DECLARE();
};

//
//
//

#ifndef REMOVE_EXPERIMENTAL

io_stream_public_t *io_pipe_create__(uint32_t size)
{
	DBG_NOT_IMPLEMENTED();
	
	return NULL;
}

#endif

//
//
//

file_t io_pipe_create()
{
	windows_pipe_t *p = new windows_pipe_t;

	if (p != NULL)
	{
		DWORD nSize = 0; // use default size

		/* "\\Device\\NamedPipe\\Win32Pipes.%08x.%08x" */

		// this function creates a named pipe with a unique name based on
		// the process ID and a sequence number
#if 0

// dwOpenMode values for CreateNamedPipe

#define PIPE_ACCESS_INBOUND         0x00000001
#define PIPE_ACCESS_OUTBOUND        0x00000002
#define PIPE_ACCESS_DUPLEX          0x00000003

#define FILE_FLAG_WRITE_THROUGH         0x80000000
#define FILE_FLAG_OVERLAPPED            0x40000000
#define FILE_FLAG_FIRST_PIPE_INSTANCE   0x00080000

#define WRITE_DAC                       0x00040000
#define WRITE_OWNER                     0x00080000
#define ACCESS_SYSTEM_SECURITY          0x01000000

// dwPipeMode values for CreateNamedPipe

#define PIPE_WAIT                   0x00000000
#define PIPE_NOWAIT                 0x00000001
#define PIPE_READMODE_BYTE          0x00000000
#define PIPE_READMODE_MESSAGE       0x00000002
#define PIPE_TYPE_BYTE              0x00000000
#define PIPE_TYPE_MESSAGE           0x00000004
#define PIPE_ACCEPT_REMOTE_CLIENTS  0x00000000
#define PIPE_REJECT_REMOTE_CLIENTS  0x00000008

#endif

		if (CreatePipe(&p->m_hReadPipe, &p->m_hWritePipe, NULL, nSize))
		{
			return io_stream_create_and_release(p); // TODO: setup io_stream_public_t members: access, status
		}
		else
		{
			p->release();
			
			return io_stream_error(FormatSystemMessage(GetLastError()));
		}
	}

	return io_stream_error/*_out_of_memory*/();
}

HANDLE pipe_read_handle(file_t &file)
{
	windows_pipe_t *device = io_stream_cast<windows_pipe_t>(file);
	
	if (device != NULL)
		return device->m_hReadPipe;

	return NULL;
}

HANDLE pipe_write_handle(file_t &file)
{
	windows_pipe_t *device = io_stream_cast<windows_pipe_t>(file);
	
	if (device != NULL)
		return device->m_hWritePipe;

	return NULL;
}

void pipe_close_read_handle(file_t &file)
{
	windows_pipe_t *device = io_stream_cast<windows_pipe_t>(file);
	
	if (device != NULL)
	{
		if (device->m_hReadPipe != NULL)
		{
			CloseHandle(device->m_hReadPipe);

			device->m_hReadPipe = NULL;
		}
	}
}

void pipe_close_write_handle(file_t &file)
{
	windows_pipe_t *device = io_stream_cast<windows_pipe_t>(file);
	
	if (device != NULL)
	{
		if (device->m_hWritePipe != NULL)
		{
			CloseHandle(device->m_hWritePipe);

			device->m_hWritePipe = NULL;
		}
	}
}

//
//
//

OBJECT_CLASS_BEGIN(windows_pipe_t, base_io_stream_t)
OBJECT_CLASS_END()

windows_pipe_t::windows_pipe_t()
{
}

windows_pipe_t::~windows_pipe_t()
{
	close();
}

bool windows_pipe_t::read(void *buffer, u32 count, u32 &count_out)
{
	DWORD temp = 0;
	
	if (ReadFile(m_hReadPipe, buffer, count, &temp, NULL))
	{
		count_out = temp;

		return true;
	}

	return false;
}

bool windows_pipe_t::write(const void *buffer, u32 count, u32 &count_out)
{
	DWORD temp = 0;
	
	if (WriteFile(m_hWritePipe, buffer, count, &temp, NULL))
	{
		count_out = temp;

		return true;
	}

	return false;
}

bool windows_pipe_t::flush()
{
/*
	if (m_hReadPipe != NULL)
	{
		FlushFileBuffers(m_hReadPipe);
	}
*/

	if (m_hWritePipe != NULL)
	{
		FlushFileBuffers(m_hWritePipe);
	}
	
	return true;
}

bool windows_pipe_t::close()
{
	if (m_hReadPipe != NULL)
	{
		CloseHandle(m_hReadPipe);

		m_hReadPipe = NULL;
	}

	// XXX: should we flush here???
	
	if (m_hWritePipe != NULL)
	{
		CloseHandle(m_hWritePipe);

		m_hWritePipe = NULL;
	}

	return true;
}

#endif

