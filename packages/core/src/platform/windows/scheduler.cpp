/*------------------------------------------------------------------------------

	platform/windows/scheduler.cpp - Windows scheduler support

------------------------------------------------------------------------------*/

#include "pch.h"
#include "core.h"
#include "scheduler.h"

#include "../../lib/core/thread.h"

#ifndef LITE_RTL

//
//
//

typedef ULONG_PTR *PULONG_PTR;
typedef struct _OVERLAPPED_ENTRY OVERLAPPED_ENTRY, *LPOVERLAPPED_ENTRY;

static struct
{
	DWORD   (WINAPI * SleepEx)(DWORD dwMilliseconds, BOOL bAlertable);
	DWORD   (WINAPI * WaitForMultipleObjectsEx)(DWORD nCount, CONST HANDLE *lpHandles, BOOL bWaitAll, DWORD dwMilliseconds, BOOL bAlertable);

#ifdef _DEBUG
	HANDLE  (WINAPI * CreateWaitableTimerA)(LPSECURITY_ATTRIBUTES lpTimerAttributes, BOOL bManualReset, LPCSTR lpTimerName); // requires NT 4.0, or later; not exported from kernel32.dll (4.00.950)
//	HANDLE  (WINAPI * CreateWaitableTimerW)(LPSECURITY_ATTRIBUTES lpTimerAttributes, BOOL bManualReset, LPCWSTR lpTimerName); // requires NT 4.0, or later; not exported from kernel32.dll (4.00.950)
//	BOOL    (WINAPI * SetWaitableTimer)(HANDLE hTimer, const LARGE_INTEGER *lpDueTime, LONG lPeriod, PTIMERAPCROUTINE pfnCompletionRoutine, LPVOID lpArgToCompletionRoutine, BOOL fResume);
//	BOOL    (WINAPI * CancelWaitableTimer)(HANDLE hTimer);

	// NT 4.0 or later (unsupported on Windows 95, and CE)...
	// _WIN32_WINNT >= 0x0400
	DWORD (WINAPI *SignalObjectAndWait)( HANDLE hObjectToSignal, HANDLE hObjectToWaitOn, DWORD dwMilliseconds, BOOL bAlertable);

	// NT 3.5 or later (unsupported on Windows 95, and CE)...

#endif

#ifdef _DEBUG

	HANDLE (WINAPI * CreateIoCompletionPort)(HANDLE FileHandle, HANDLE ExistingCompletionPort, ULONG_PTR CompletionKey, DWORD NumberOfConcurrentThreads );
	BOOL   (WINAPI * GetQueuedCompletionStatus)(HANDLE CompletionPort, LPDWORD lpNumberOfBytesTransferred, PULONG_PTR lpCompletionKey, LPOVERLAPPED *lpOverlapped, DWORD dwMilliseconds );
	BOOL   (WINAPI * GetQueuedCompletionStatusEx)(HANDLE CompletionPort, LPOVERLAPPED_ENTRY lpCompletionPortEntries, ULONG ulCount, PULONG ulNumEntriesRemoved, DWORD dwMilliseconds, BOOL fAlertable ); // _WIN32_WINNT >= 0x0600
	BOOL   (WINAPI * PostQueuedCompletionStatus)(HANDLE CompletionPort, DWORD dwNumberOfBytesTransferred, ULONG_PTR dwCompletionKey, LPOVERLAPPED lpOverlapped );

#endif

} g_kernel32_procs;

static struct
{
	DWORD (WINAPI * MsgWaitForMultipleObjects)(DWORD nCount, LPHANDLE pHandles, BOOL fWaitAll, DWORD dwMilliseconds, DWORD dwWakeMask);
	DWORD (WINAPI * MsgWaitForMultipleObjectsEx)(DWORD nCount, LPHANDLE pHandles, DWORD dwMilliseconds, DWORD dwWakeMask, DWORD dwFlags);
	DWORD (WINAPI * GetQueueStatus)(UINT flags);
} g_user32_procs;

// dynamic binding of DLL functions...

static export_info_t g_export4_ = { &g_kernel32_module, "SleepEx",                  &g_kernel32_procs.SleepEx                  };
static export_info_t g_export3_ = { &g_kernel32_module, "WaitForMultipleObjectsEx", &g_kernel32_procs.WaitForMultipleObjectsEx };

static export_info_t g_export2_ = { &g_user32_module, "MsgWaitForMultipleObjects",   &g_user32_procs.MsgWaitForMultipleObjects   };
static export_info_t g_export1_ = { &g_user32_module, "MsgWaitForMultipleObjectsEx", &g_user32_procs.MsgWaitForMultipleObjectsEx };
static export_info_t g_export5_ = { &g_user32_module, "GetQueueStatus",              &g_user32_procs.GetQueueStatus              };

static DWORD __WaitForMultipleObjectsEx__(DWORD count, CONST HANDLE *handles, BOOL bWaitAll, DWORD dwMilliseconds, BOOL bAlertable);
static DWORD __MsgWaitForMultipleObjectsEx__(DWORD count, HANDLE *handles, DWORD dwMilliseconds, DWORD dwWakeMask, DWORD dwFlags);

//
//
//

#ifndef REMOVE_THIS

// TODO: move into diagnostic module

static void trace_wait_enter(DWORD dwMilliseconds, u32 count, u32 idle_handlers, u32 check_windows_queue)
{
	TRACE("%s: waiting for signals(timeout=%i,handles=%i)...\n",
		g_trace_scheduler.name, dwMilliseconds, count);

	trace_indent();
}

static void trace_wait_result(DWORD result)
{
	trace_unindent();

	TRACE("%s: waiting for signals complete (result=%08X)\n",
		g_trace_scheduler.name, result);
}

static bool trace_idle_processing(u32 idle_count)
{
	if (trace_enabled(g_trace_scheduler, trace_severity_debug))
	{
		TRACE("idle processing (count=%i)\n", idle_count);

		trace_indent();

		return true;
	}
	else
	{
		return false;
	}
}

#endif

//
//
//

static int handle_wait(HANDLE handle, const f32 *timeout)
{
	if (handle != NULL)
	{
 		DWORD result = WaitForSingleObject(handle, millisec(timeout));

		switch (result)
		{
			case WAIT_OBJECT_0:
				return 1;

			case WAIT_TIMEOUT:
				_ASSERT(timeout != NULL);
				return 0;

			default:
				DBG_BREAK();
				break;
		}
	}

	return -1;
}

#endif

//
//
//

sync_handle_t::sync_handle_t()
{
	m_handle = NULL;
}

sync_handle_t::~sync_handle_t()
{
	close();
}

bool sync_handle_t::wait()
{
	return wait2(NULL);
}

bool sync_handle_t::wait(f32 timeout)
{
	return wait2(&timeout);
}

#ifndef LITE_RTL

bool sync_handle_t::wait2(const f32 *timeout)
{
	return (handle_wait(m_handle, timeout) > 0);
}

bool sync_handle_t::close()
{
	if (m_handle != NULL)
	{
		if (CloseHandle(m_handle))
		{
			m_handle = NULL;
			
			return true;
		}

		DBG_BREAK();
	}

	return false;
}

#endif

bool sync_handle_t::is_valid_handle()
{
	return (m_handle != NULL);
}

//
//
//

#ifndef LITE_RTL

bool sync_event_t::create_event(bool manual_reset, bool initial_state)
{
	if (m_handle != NULL)
	{
		VERIFY(CloseHandle(m_handle));

		DBG_BREAK();
	}
	else
	{
	}
	
	m_handle = CreateEvent(NULL, manual_reset, initial_state, NULL);

	if (m_handle != NULL)
		return true;

	DBG_BREAK();

	return false;
}

bool sync_event_t::signal()
{
	if (m_handle == NULL)
	{
		DBG_BREAK();
	}
	
	if (m_handle != NULL)
	{
		if (SetEvent(m_handle))
			return true;

		DBG_BREAK();
	}

	return false;
}

//
//
//

static void build_handle_table(core_thread_state_t *ts, wait_handle_table_t &handles);
static bool add_handle(wait_handle_table_t &handles, HANDLE hObject, UINT index, core_thread_state_t *ts);
static u32  check_message_queue__(core_thread_state_t *ts);
static int  dispatch(core_thread_state_t *ts, wait_handle_table_t &handles, DWORD result);

//
//
//

int sync_dispatch_win32(core_thread_state_t *ts, sync_dispatch_t *args)
{
	_ASSERT(ts != NULL);
	_ASSERT(args != NULL);

	//
	//  Build handle table (TODO: cache this in ts???)
	//

	wait_handle_table_t handles;

	build_handle_table(ts, handles);

	_ASSERT(handles.count <= DIM(handles.handles));

	//
	//
	//

	DWORD timeout = args->timeout_ms;

	unsigned int idle_handlers = idle_handler_count(ts); // any active idle handlers???

	if (idle_handlers != 0)
		timeout = 0;

	// REF: ReadFileEx, QueueUserAPC, CreateWaitableTimer
	
	// e.g. waits for thread's message queue, APCs, DirectInput notification,
	// FindFirstChangeNotification, overlapped I/O events, worker thread
	// handle, process handle, Mercury APC queue, Mercury message queue, etc.

	// XXX: can we use dwMilliseconds to build our own portable timers, and
	// use this timeout value to implement delayed UI updates (e.g. update UI
	// no more than each 100 ms)

	u32 check_windows_queue = check_message_queue__(ts);

	bool x = trace_enabled(g_trace_scheduler, trace_severity_debug);

	if (x)
		trace_wait_enter(timeout, handles.count, idle_handlers, check_windows_queue);

	BOOL bWaitAll = FALSE;
#if !defined(_WIN32_WCE)
	BOOL bAlertable = TRUE;
#else
	BOOL bAlertable = FALSE;
#endif

	DWORD result;
	
	if (check_windows_queue)
	{
#if !defined(_WIN32_WCE)
		DWORD dwWakeMask = QS_ALLINPUT | QS_ALLPOSTMESSAGE;
#else
		DWORD dwWakeMask = QS_ALLINPUT;
#endif
		DWORD dwFlags    = 0;

		if (bAlertable)
		{
#if !defined(_WIN32_WCE)
			dwFlags |= MWMO_ALERTABLE;
#else
			DBG_NOT_IMPLEMENTED();
#endif
		}

		if (bWaitAll)
		{
#if !defined(_WIN32_WCE)
			dwFlags |= MWMO_WAITALL;
#else
			DBG_NOT_IMPLEMENTED2("MWMO_WAITALL not defined on this platform");
#endif
		}

/*

		flag MWMO_INPUTAVAILABLE is not supported on Windows 95 or NT 4.0!!!!

*/

		// first check state of object handles using a timeout of zero
		
		result = __WaitForMultipleObjectsEx__(handles.count, handles.handles, bWaitAll, 0, bAlertable);

		if (result >= WAIT_OBJECT_0 && result < (WAIT_OBJECT_0 + handles.count))
		{
			goto wait_satisfied;
		}
		else
		{
			// check the queue for any existing input
			
			get_addr(g_export5_);

			if (g_user32_procs.GetQueueStatus != NULL)
			{
				DWORD status = g_user32_procs.GetQueueStatus(dwWakeMask);

				WORD qs1 = HIWORD(status); // currently in the queue
				WORD qs2 = LOWORD(status); // added to the queue and that are still in the queue since the last call to the GetQueueStatus, GetMessage, or PeekMessage function.

				if (x)
					TRACE("qs1=%04X,qs2=%04X\n", qs1, qs2);

				if (qs1 != 0)
				{
					result = (WAIT_OBJECT_0 + handles.count);

					goto wait_satisfied;
				}
			}
		}
		
		// NOTE:  The semantics for the Windows message queue are really
		// screwed up.  Once the wait in MsgWait* is satisfied, it is possible
		// that it will not wake up again until after *NEW* events arrive
		// (unless MWMO_INPUTAVAILABLE is specified, which is unavailable on
		// Windows 95 and NT 4.0)

		result = __MsgWaitForMultipleObjectsEx__(handles.count,
			handles.handles, timeout, dwWakeMask, dwFlags);
	}
	else
	{
		// NOTE: the use of these functions assumes your application
		// doesn't create any windows (see SleepEx and related function
		// documentation in MSDN)
		
		result = __WaitForMultipleObjectsEx__(handles.count,
			handles.handles, bWaitAll, timeout, bAlertable);
	}

	//
	//
	//

wait_satisfied:

	if (x)
		trace_wait_result(result);

	//
	//
	//

#ifdef _DEBUG

	if (result == (WAIT_OBJECT_0 + handles.count))
		_ASSERT(check_windows_queue);

	if (result == WAIT_TIMEOUT)
		_ASSERT(timeout != INFINITE);

#if !defined(_WIN32_WCE)

	if (result == WAIT_IO_COMPLETION)
		_ASSERT(bAlertable);

#endif

#endif

	//
	//  Dispatch event
	//

	return dispatch(ts, handles, result);		
}

//
//
//

static void build_handle_table(core_thread_state_t *ts, wait_handle_table_t &handles)
{
	_ASSERT(ts != NULL);

#if 0
	// TODO: store the table with the thread, and rebuild only when needed

	if (ts->handle_table_dirty)
	{
	}
#endif

	// add application-defined objects first
	
	for (u32 i = 0; i < ts->callbacks.length(); i++)
	{
		callback_entry_t *callback = ts->callbacks[i];
		
		if (callback->enabled &&
			callback->type == callback_type_object &&
			callback->hObject != NULL)
		{
			if (add_handle(handles, callback->hObject, i, ts))
			{
			}
			else
			{
				DBG_BREAK();
			}
		}
	}

	// add built-in handles...
	
	if (1)
	{
		add_handle(handles, ts->event.handle(), 0, NULL);
	}

	if (1)
	{
		// TODO: supersede with g_process_general_event???
		add_handle(handles, g_process_exit_event.handle(), 0, NULL);
	}

	if (1)
	{
		add_handle(handles, g_process_general_event.handle(), 0, NULL);
	}

/*
	task_t *task = ts->task.task;

	if (task != NULL)
	{
		add_handle(handles, task->m_event.handle(), 0, NULL);
	}
*/
}

static bool add_handle(wait_handle_table_t &handles, HANDLE hObject, UINT index, core_thread_state_t *ts)
{
	if (hObject != NULL)
	{
		if (handles.count < DIM(handles.handles))
		{
			handles.indices[handles.count] = index;
			handles.handles[handles.count] = hObject;
			handles.callbacks[handles.count] = ts ? ts->callbacks[index] : NULL;

			handles.count++;

			return true;
		}
		else
		{
#ifdef _DEBUG
			TRACE("too many handles specified for sync\n");
#endif
			
			// not enough room in handle buffer!!!
			DBG_NOT_IMPLEMENTED();
		}
	}

	return false;
}

static u32 check_message_queue__(core_thread_state_t *ts)
{
	_ASSERT(ts != NULL);

	// TODO: maintain message queue callback count in ts and eliminate need to
	// look through callback list

/*
	if (ts->queue_handlers != 0)
	{
	}
*/

	// any Windows message queue callbacks registered?
	
	for (u32 i = 0; i < ts->callbacks.length(); i++)
	{
		callback_entry_t *callback = ts->callbacks[i];

		if (callback->enabled)
		{
			if (callback->type == callback_type_message_queue)
			{
				return 1;
			}
		}
	}

	return 0;
}

static int dispatch(core_thread_state_t *ts, wait_handle_table_t &handles, DWORD result)
{
	_ASSERT(ts != NULL);
	
	if (result >= WAIT_OBJECT_0 && result < (WAIT_OBJECT_0 + handles.count))
	{
		// one of the handles we passed has become signaled...
		
		// NOTE: if more than one object became signaled during the
		// call, this is the array index of the signaled object with
		// the smallest index value of all the signaled objects.

		unsigned int index = result - WAIT_OBJECT_0;

		_ASSERT(index < handles.count);
		_ASSERT(index < DIM(handles.indices));

		callback_entry_t *callback = handles.callbacks[index];
		
		bool x = trace_enabled(g_trace_scheduler, trace_severity_debug);

		if (x)
		{
			TRACE("handle signaled: index=%i, handle=%i\n", index, handles.handles[index]);
		}

		if (0)
		{
			if (ts->task != NULL)
			{
//				ts->status = running;
			}

			if (task_id() != 0)
			{
				task_set_status_message("Processing signal");
			}
		}

		if (callback != NULL)
		{
			_ASSERT(callback == ts->callbacks[handles.indices[index]]);

			// be careful to consider side-effects that can occur while
			// calling into the application code (e.g. manipulating the
			// callback list)
			
			invoke_callback(ts, callback);
		}
		else
		{
			if (g_process_exiting)
				return false;
			
			if (task_id() != 0)
			{
				if (!task_continue())
					return false;
			}

			if (handles.handles[index] == ts->event.handle())
			{
				if (ts->command_.buffer.event_data)
				{
#ifdef _DEBUG
					if (0)
					{
						TRACE("thread command buffer signaled\n");
					}
#endif
				}

				if (x)
				{
#ifdef _DEBUG
					TRACE("thread event signaled\n");
#endif
				}
			}
			else
			{
#ifdef _DEBUG
				TRACE("unrecognized handle signaled!\n");
#endif
			}
		}
		
		return true;
	}
	else if (result == (WAIT_OBJECT_0 + handles.count))
	{
		bool x = trace_enabled(g_trace_scheduler, trace_severity_debug);

		if (x)
		{
			TRACE("Windows message queue signaled\n");
		}

		if (0)
		{
			if (ts->task != NULL)
			{
//				ts->task->status = running;
			}
			
			if (task_id() != 0)
			{
				task_set_status_message("Processing signal");
			}
		}

		// new Window message queue message (or messages)

		for (u32 i = 0; i < ts->callbacks.length(); i++)
		{
			callback_entry_t *callback = ts->callbacks[i];

			if (callback->enabled && callback->type == callback_type_message_queue)
			{
				// be careful to consider side-effects that can occur while
				// calling into the application code (e.g. manipulating the
				// callback list)

				invoke_callback(ts, callback);
			}
		}

		return true;
	}
	else if (result >= WAIT_ABANDONED_0 && result < (WAIT_ABANDONED_0 + handles.count))
	{
		unsigned int index = result - WAIT_ABANDONED_0;

		// TODO: callback on abandoned event???

		_ASSERT(index < handles.count);
		_ASSERT(index < DIM(handles.indices));
		_ASSERT(handles.indices[index] < handles.count);
		
		bool x = trace_enabled(g_trace_scheduler, trace_severity_debug);

		if (x)
		{
			TRACE("handle abandoned: index=%i, handle=%i\n", index, handles.handles[index]);
		}

		callback_entry_t *callback = ts->callbacks[handles.indices[index]];

		_ASSERT(callback == handles.callbacks[index]);
	}
	else if (result == WAIT_TIMEOUT)
	{
		unsigned int idle_handlers = idle_handler_count(ts); // any active idle handlers???

		if (idle_handlers != 0)
		{
			if (ts->event_loop != NULL)
				ts->event_loop->idle_count++;

			bool x = trace_idle_processing(ts->event_loop ? ts->event_loop->idle_count : 0);

			profile_instance_t instance = { &g_profile_idle };
			profile_instance_tic(instance);

			// call application-defined idle handlers...

			for (u32 i = 0; i < ts->callbacks.length(); i++)
			{
				callback_entry_t *callback = ts->callbacks[i];

				if (callback->enabled &&
					callback->type == callback_type_idle &&
					callback->idle_continue)
				{
					// be careful to consider side-effects that can occur while
					// calling into the application code (e.g. manipulating the
					// callback list)

					callback->idle_continue = 0;

					invoke_callback(ts, callback);
				}
			}

			profile_instance_toc(instance);

			if (x)
				trace_unindent();

			// warning C4456: declaration of 'idle_handlers' hides previous local declaration
			
			unsigned int idle_handlers = idle_handler_count(ts); // any active idle handlers???

			if (idle_handlers == 0)
				if (ts->event_loop != NULL)
					ts->event_loop->idle_count = 0;
		}

		return true;
	}
	else if (result == WAIT_FAILED)
	{
		DWORD error = GetLastError();
		
		DBG_BREAK();

		return false;
	}
#if !defined(_WIN32_WCE)
	else if (result == WAIT_IO_COMPLETION)
	{
		// one or more APCs were handled

		bool x = trace_enabled(g_trace_scheduler, trace_severity_debug);

		if (x)
		{
			TRACE("APCs called\n");
		}

		return true;
	}
#endif
	else
	{
		DBG_INVALID_CASE();
	}

	return false;
}

// QueueUserAPC doc says "Note that the ReadFileEx, SetWaitableTimer, and
// WriteFileEx functions are implemented using an APC as the completion
// notification callback mechanism."

// NOTE: MsgWaitForMultipleObjectsEx not available on Win95!  this prevents
// us from using ReadFileEx, etc. successfully with our message loop.

/*

	special version of the Win32 function that handles the following cases:

	- degrades call to simpler APIs if possible
	- calls advanced APIs only if needed

*/

static DWORD __WaitForMultipleObjectsEx__(DWORD count, CONST HANDLE *handles,
	BOOL bWaitAll, DWORD dwMilliseconds, BOOL bAlertable)
{
	if (count > 0)
	{
		// NOTE: WaitForMultipleObjectsEx is unsupported on Windows CE

		// NOTE: these functions may also wait on the console input handle
		// (i.e. through the undocumented GetConsoleInputWaitHandle function)

		if (bAlertable)
		{
			get_addr(g_export3_);

			if (g_kernel32_procs.WaitForMultipleObjectsEx != NULL)
			{
				return g_kernel32_procs.WaitForMultipleObjectsEx(
					count, handles, bWaitAll, dwMilliseconds, bAlertable);
			}
			else
			{
				DBG_BREAK();
			}
		}
		else
		{
			// NOTE: based on NT 4.0, WaitForMultipleObjects(...) is
			// equivalent to WaitForMultipleObjectsEx(..., FALSE)
			
			return WaitForMultipleObjects(count, handles, bWaitAll,
				dwMilliseconds);
		}
	}
	else
	{
		// NOTE: SleepEx is unsupported on Windows CE
		
		if (bAlertable)
		{
			get_addr(g_export4_);

			if (g_kernel32_procs.SleepEx != NULL)
			{
				// NOTE: based on NT 4.0, SleepEx is basically a call to
				// the NtDelayExecution
				
				return g_kernel32_procs.SleepEx(dwMilliseconds, bAlertable);
			}
			else
			{
				DBG_BREAK();
			}
		}
		else
		{
			// NOTE: based on NT 4.0, Sleep(dwMilliseconds) is equivalent to
			// SleepEx(dwMilliseconds, FALSE)
			
			Sleep(dwMilliseconds);

			return WAIT_TIMEOUT;
		}
	}

	return WAIT_FAILED;
}

static DWORD __MsgWaitForMultipleObjectsEx__(DWORD count, HANDLE *handles,
	DWORD timeout, DWORD dwWakeMask, DWORD dwFlags)
{
	// NOTE: when this function returns, there could be one (or more!)
	// messages in the Windows queue.  A second wait will not be satisfied
	// until *NEW* input arrives, unless MWMO_INPUTAVAILABLE is specified
	// (which is not available on all the platforms we support)!

#if !defined(_WIN32_WCE)
	if (dwFlags == 0 || dwFlags == MWMO_WAITALL)
	{
		BOOL fWaitAll = ((dwFlags & MWMO_WAITALL) != 0);

		// NOTE: based on NT 4.0 through Windows 2000...
		// MsgWaitForMultipleObjects(...) is implemented as
		// MsgWaitForMultipleObjectsEx(..., fWaitAll ? MWMO_WAITALL : 0)

		get_addr(g_export2_);

		if (g_user32_procs.MsgWaitForMultipleObjects != NULL)
		{
			return g_user32_procs.MsgWaitForMultipleObjects(
				count, handles, fWaitAll, timeout, dwWakeMask);
		}
		else
		{
			DBG_BREAK();
		}
	}
	else
#endif
	{
		get_addr(g_export1_);

		if (g_user32_procs.MsgWaitForMultipleObjectsEx != NULL)
		{
			return g_user32_procs.MsgWaitForMultipleObjectsEx(
				count, handles, timeout, dwWakeMask, dwFlags);
		}
		else
		{
			DBG_BREAK();
		}
	}

	return WAIT_FAILED;
}

//
//
//

static HANDLE copy_event_handle(HANDLE hObject)
{
	if (0)
	{
		HANDLE hCopy;
		
		if (DuplicateHandle(GetCurrentProcess(), hObject, GetCurrentProcess(),
			&hCopy, 0, FALSE, DUPLICATE_SAME_ACCESS))
//			&hCopy, SYNCHRONIZE, FALSE, 0))
		{
			hObject = hCopy;
		}
		else
		{
			DWORD dwError = GetLastError();
		}
	}

	return hObject;
}

u32 event_register_handle_callback(HANDLE hObject, event_signal_callback_t function, void *context)
{
	// TODO: ts->handle_table_dirty = 1;

	if (hObject != NULL)
	{
		callback_entry_t *callback = alloc_callback(function, context);

		if (callback != NULL)
		{
			hObject = copy_event_handle(hObject);
			
			callback->enabled = 1;
			callback->type    = callback_type_object;
			callback->hObject = hObject;

			return callback->id;
		}
	}

	return 0;
}

u32 event_register_handle_delegate__(HANDLE hObject, object_interface_t *object, object_method0_t method)
{
	// TODO: ts->handle_table_dirty = 1;

	if (hObject != NULL)
	{
		callback_entry_t *callback = alloc_callback(object, method);

		if (callback != NULL)
		{
			hObject = copy_event_handle(hObject);

			callback->enabled = 1;
			callback->type    = callback_type_object;
			callback->hObject = hObject;

			return callback->id;
		}
	}

	return 0;
}

//
//
//

u32 event_register_message_queue_callback(event_signal_callback_t function, void *context)
{
	callback_entry_t *callback = alloc_callback(function, context);

	if (callback != NULL)
	{
		callback->enabled = 1;
		callback->type    = callback_type_message_queue;

		return callback->id;
	}

	return 0;
}

//
//
//

u32 event_register_event_callback(sync_event_t *event, event_signal_callback_t function, void *context)
{
	if (event != NULL)
	{
		HANDLE hObject = event->handle();

		return event_register_handle_callback(hObject, function, context);
	}

	return 0;
}

u32 event_register_event_callback4(sync_event_t *event, object_interface_t *object, object_method0_t method)
{
	if (event != NULL)
	{
		HANDLE hObject = event->handle();

		return event_register_handle_delegate__(hObject, object, method);
	}

	return 0;
}

#endif

