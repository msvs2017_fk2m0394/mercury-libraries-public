/*------------------------------------------------------------------------------

	platform/windows/io_mapping.cpp - Windows I/O support
	
	- maps a resource into process address space

	TODO: resolve confusion about use of INVALID_HANDLE_VALUE vs. NULL

------------------------------------------------------------------------------*/

#include "pch.h"
#include "io.h"

#ifndef LITE_RTL

//
//
//

static io_stream_t io_stream_from_mapping(HANDLE hMapping, io_access_t access, u32 size)
{
	io_stream_t stream;

	if (hMapping != NULL)
	{
		_ASSERT(hMapping != INVALID_HANDLE_VALUE);

		// determine access bits

		DWORD dwDesiredAccess = 0;

		if (access.r)
			dwDesiredAccess |= FILE_MAP_READ;
		
		if (access.w)
			dwDesiredAccess |= FILE_MAP_WRITE;
		
		if (access.x)
			dwDesiredAccess |= SECTION_MAP_EXECUTE;

		// map into our address space
		
		LPVOID ptr = MapViewOfFile(hMapping, dwDesiredAccess, 0, 0, size);

		if (ptr != NULL)
		{
			// create our control structure

			win32_io_device_t *obj = new win32_io_device_t(NULL, ptr, "");

			if (obj != NULL)
			{
				stream = io_stream_create_and_release(obj);

				if (obj->m_io_stream != NULL)
				{
					// setup the publicly accessible fields to cover entire
					// mapping
					
					obj->m_io_stream->status    = io_resource_status_open;
					obj->m_io_stream->rbuf.ptr1 = (u8*)ptr;
					obj->m_io_stream->rbuf.ptr2 = (u8*)ptr_offset(ptr, size);

					obj->m_hMapping = hMapping;

					ptr = NULL;
					hMapping = NULL;
				}
			}

			// clean up if no ownership transfer
			
			if (ptr != NULL)
				UnmapViewOfFile(ptr);
		}
		else
		{
			stream = file_last_error();
		}

		// clean up if no ownership transfer

		if (hMapping != NULL)
			CloseHandle(hMapping);
	}
	else
	{
		stream = io_stream_error(NULL);
	}

	return stream;
}

//
//
//

static io_stream_t map_file__(HANDLE hFile)
{
	io_stream_t stream;

	if (hFile != INVALID_HANDLE_VALUE)
	{
		_ASSERT(hFile != NULL);

/*
		SECURITY_DESCRIPTOR sd;

		InitializeSecurityDescriptor(&sd, SECURITY_DESCRIPTOR_REVISION);
		SetSecurityDescriptorDacl(&sd, TRUE, NULL, FALSE);

		SECURITY_ATTRIBUTES sa = { sizeof(sa), &sd, FALSE };
*/

		LPSECURITY_ATTRIBUTES lpFileMappingAttributes = NULL;
		DWORD flProtect = PAGE_READONLY | SEC_COMMIT;
		DWORD dwMaximumSizeHigh = 0;
		DWORD dwMaximumSizeLow = 0;
		LPCTSTR lpName = NULL;

		HANDLE hMapping = CreateFileMapping(hFile, lpFileMappingAttributes,
			flProtect, dwMaximumSizeHigh, dwMaximumSizeLow, lpName);

		if (hMapping == NULL)
			return file_last_error();

		io_access_t access = { 1, 0, 0 }; // read-only access

		DWORD size = GetFileSize(hFile, NULL);

		return io_stream_from_mapping(hMapping, access, size);
	}
	else
	{
		stream = io_stream_error(NULL);
	}

	return stream;
}

// this version of the function maps the entire file and 
// exposes the bits through the public file structure

io_stream_t map_file2(const char *filename)
{
	file_t stream;

	DWORD dwDesiredAccess = 0;
	
	if (filename != NULL && filename[0] != 0)
	{
		HANDLE hFile = CreateFileA(filename, GENERIC_READ, FILE_SHARE_READ,
			NULL, OPEN_EXISTING, 0, 0);

		if (hFile != INVALID_HANDLE_VALUE)
		{
			//
			//
			//

/*
			SECURITY_DESCRIPTOR sd;

			InitializeSecurityDescriptor(&sd, SECURITY_DESCRIPTOR_REVISION);
			SetSecurityDescriptorDacl(&sd, TRUE, NULL, FALSE);

			SECURITY_ATTRIBUTES sa = { sizeof(sa), &sd, FALSE };
*/

			LPSECURITY_ATTRIBUTES lpFileMappingAttributes = NULL;
			DWORD flProtect = PAGE_READONLY | SEC_COMMIT;
			DWORD dwMaximumSizeHigh = 0;
			DWORD dwMaximumSizeLow = 0;
			LPCTSTR lpName = NULL;

			HANDLE hMapping = CreateFileMapping(hFile, lpFileMappingAttributes,
				flProtect, dwMaximumSizeHigh, dwMaximumSizeLow, lpName);

			if (hMapping != NULL)
			{
				//
				//
				//

				dwDesiredAccess = FILE_MAP_READ;
				
				LPVOID ptr = MapViewOfFile(hMapping, dwDesiredAccess, 0, 0, 0);

				if (ptr != NULL)
				{
					DWORD dwSize = GetFileSize(hFile, NULL);

					//
					//
					//

					win32_io_device_t *obj = new win32_io_device_t(hFile, ptr, filename);

					if (obj != NULL)
					{
						stream = io_stream_create_and_release(obj);

						if (obj->m_io_stream != NULL)
						{
							obj->m_io_stream->status    = io_resource_status_open;
							obj->m_io_stream->rbuf.ptr1 = (u8*)ptr;
							obj->m_io_stream->rbuf.ptr2 = (u8*)ptr_offset(ptr, dwSize);
						}

						obj->m_hMapping = hMapping;

						hMapping = NULL;
						ptr = NULL;
						hFile = INVALID_HANDLE_VALUE;
					}

					if (ptr != NULL)
					{
						UnmapViewOfFile(ptr);
						ptr = NULL;
					}
				}
				else
				{
					stream = file_last_error();
				}

				if (hMapping != NULL)
				{
					CloseHandle(hMapping);
					hMapping = NULL;
				}
			}
			else
			{
				stream = file_last_error();
			}
		}
		else
		{
			stream = file_last_error();
		}

		if (hFile != INVALID_HANDLE_VALUE)
		{
			CloseHandle(hFile);
			hFile = INVALID_HANDLE_VALUE;
		}
	}

	return stream;
}

io_stream_t map_file3(file_t &file)
{
	return map_file__(file_handle(file));
}

//
//
//

io_stream_t create_shared_memory(u32 size, const char *name)
{
/*
	SECURITY_DESCRIPTOR sd;

	InitializeSecurityDescriptor(&sd, SECURITY_DESCRIPTOR_REVISION);
	SetSecurityDescriptorDacl(&sd, TRUE, NULL, FALSE);

	SECURITY_ATTRIBUTES sa = { sizeof(sa), &sd, FALSE };
*/

	HANDLE hFile = INVALID_HANDLE_VALUE; // create file mapping backed by system page file
	LPSECURITY_ATTRIBUTES lpFileMappingAttributes = NULL;
	DWORD flProtect = SEC_COMMIT;
	DWORD dwMaximumSizeHigh = 0;
	DWORD dwMaximumSizeLow = size;
	LPCSTR lpName = name;

	io_access_t access = { 1, 1, 0 }; // R/W access

	if (access.r == 1 && access.w == 0 && access.x == 0)
		flProtect |= PAGE_READONLY;
	else if (access.r == 1 && access.w == 1 && access.x == 0)
		flProtect |= PAGE_READWRITE;
	else if (access.r == 0 && access.w == 0 && access.x == 1)
		flProtect |= PAGE_EXECUTE;
	else if (access.r == 1 && access.w == 0 && access.x == 1)
		flProtect |= PAGE_EXECUTE_READ;
	else if (access.r == 1 && access.w == 1 && access.x == 1)
		flProtect |= PAGE_EXECUTE_READWRITE;

	HANDLE hMapping = CreateFileMappingA(hFile, lpFileMappingAttributes,
		flProtect, dwMaximumSizeHigh, dwMaximumSizeLow, lpName);

	if (hMapping == NULL)
		return file_last_error();

	return io_stream_from_mapping(hMapping, access, size);
}

#endif

