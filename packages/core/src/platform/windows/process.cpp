/*------------------------------------------------------------------------------

	platform/windows/process.cpp - Windows process support

------------------------------------------------------------------------------*/

#include "pch.h"
#include <mercury/experimental/var.h>
#include "core.h"
#include "io.h"

#include "../../lib/io/io.h"
#include "../../lib/core/process.h"
#include "../../lib/misc/misc.h"

//
//
//

struct kernel32_procs_t
{
//	BOOL (WINAPI * CreateProcessAsUser)(HANDLE hToken, LPCTSTR lpApplicationName, LPTSTR lpCommandLine, LPSECURITY_ATTRIBUTES lpProcessAttributes, LPSECURITY_ATTRIBUTES lpThreadAttributes, BOOL bInheritHandles, DWORD dwCreationFlags, LPVOID lpEnvironment, LPCTSTR lpCurrentDirectory, LPSTARTUPINFO lpStartupInfo, LPPROCESS_INFORMATION lpProcessInformation);
//	BOOL (WINAPI * CreateProcessWithLogonW)( LPCWSTR lpUsername, LPCWSTR lpDomain, LPCWSTR lpPassword, DWORD dwLogonFlags, LPCWSTR lpApplicationName, LPWSTR lpCommandLine, DWORD dwCreationFlags, LPVOID lpEnvironment, LPCWSTR lpCurrentDirectory, LPSTARTUPINFOW lpStartupInfo, LPPROCESS_INFORMATION lpProcessInfo);
};

#ifndef _MSC_VER
#define _strdup strdup
#endif

//
//
//

#ifndef LITE_RTL

//
//
//

struct windows_process_t : process_object_t
{
	~windows_process_t();

	virtual bool wait(const f32 *timeout);
	virtual bool close();

	void release_handle(); // free Win32 resources, while keeping this windows_process_t state around (useful for exited processes in build.exe)
	void update_status();

	HANDLE       m_hProcess;  // process handle
	process_id_t m_pid;       // process ID (redundant with m_public.id)
	HANDLE       m_hThread;   // main thread handle (needed for resume after create)
	thread_id_t  m_tid;       // main thread ID
	DWORD        m_exit_code; //

	OBJECT_CLASS_DECLARE();
};

#ifndef REMOVE_EXPERIMENTAL
static global_thing_t<windows_process_t> g_current_process;
#endif

//
//
//

OBJECT_CLASS_BEGIN(windows_process_t, process_object_t)
OBJECT_CLASS_END()

//
//
//

windows_process_t::~windows_process_t()
{
	close(); // XXX: should be handled by framework before our destructor is called!
}

//
//
//

bool windows_process_t::wait(const f32 *timeout)
{
	if (m_hProcess != NULL)
	{
		DWORD result = WaitForSingleObject(m_hProcess, millisec(timeout));
		
		if (result == WAIT_OBJECT_0)
		{
			m_public.status = io_task_status_complete;
			
			DWORD exit_code;

			if (GetExitCodeProcess(m_hProcess, &exit_code))
			{
				m_exit_code = exit_code;

				if (exit_code == STILL_ACTIVE)
				{
				}
			}
			else
			{
				DWORD err = GetLastError();
			}

			return true;
		}
		else
		{
		}
	}

	return false;
}

//
//
//

bool windows_process_t::close()
{
	if (m_hProcess != NULL)
	{
		if (m_hThread != NULL)
		{
			CloseHandle(m_hThread);
			m_hThread = NULL;
		}

		if (CloseHandle(m_hProcess))
		{
		}
		else
		{
		}

		m_hProcess = NULL;
		m_public.status = io_task_status_none;

		return true;
	}
	else
	{
		_ASSERT(m_hThread == NULL);
	}

	return false;
}

//
//
//

static windows_process_t *windows_process(process_t process)
{
	if (process.m_object != NULL)
	{
		if (process.m_object->object != NULL)
		{
			windows_process_t *wp = obj_cast<windows_process_t>(process.m_object->object);

			if (wp != NULL)
			{
				debug_validate(wp);

				return wp;
			}
		}
	}

	return NULL;
}

//
//
//

HANDLE process_handle__(process_t process, bool detach)
{
	if (process.m_object != NULL)
	{
		if (process.m_object->object != NULL)
		{
			windows_process_t *wp = windows_process(process);

			if (wp != NULL)
			{
				HANDLE h = wp->m_hProcess;

				if (detach)
				{
					wp->m_hProcess = NULL;
				}

				return h;
			}
		}
	}

	return NULL;
}

//
//
//

HANDLE process_thread_handle(process_t process)
{
	windows_process_t *wp = windows_process(process);

	if (wp != NULL)
		return wp->m_hThread;

	return NULL;
}

thread_id_t process_thread_id(process_t process)
{
	windows_process_t *wp = windows_process(process);

	if (wp != NULL)
		return wp->m_tid;

	return 0;
}

//
//
//

process_t process_from_handle__(HANDLE hProcess, process_id_t pid)
{
	if (hProcess == NULL)
		return process_error();

	if (hProcess == GetCurrentProcess())
	{
		pid = GetCurrentProcessId();
	}
	else
	{
		DBG_NOT_IMPLEMENTED();
	}

	windows_process_t *p = new windows_process_t;

	if (p != NULL)
	{
		p->m_public.id = pid;
		p->m_pid       = pid;
		p->m_hProcess  = hProcess;

		p->update_status();

		process_t result = make_process(p);

		p->release();

		return result;
	}

	return process_error();
}

//
//
//

process_t process_open(process_id_t pid)
{
	if (pid == 0)
		return process_t();

	DWORD access = 0;
	BOOL inherit = FALSE;

	HANDLE hProcess = OpenProcess(access, inherit, pid);

	if (hProcess != NULL)
	{
		windows_process_t *p = new windows_process_t;

		if (p != NULL)
		{
			p->m_pid       = pid;
			p->m_public.id = pid;
			p->m_hProcess  = hProcess;

			p->update_status();

			process_t result = make_process(p);

			p->release();

			return result;
		}
	}
	else
	{
		DWORD err = GetLastError();

		variant_t temp = FormatSystemMessage(err);

		return process_error(temp);
	}
	
	return process_error();
}

//
//
//

static void redirect_helper(file_t *stdio, HANDLE *h1, HANDLE *h2)
{
	HANDLE h1_ = NULL;
	HANDLE h2_ = NULL;
	
	if (stdio != NULL)
	{
		if (h1 != NULL)
		{
			// get an inheritable read handle
			
			HANDLE h = pipe_read_handle(*stdio);

			if (h != NULL)
			{
				HANDLE hDup = NULL;
				
				if (DuplicateHandle(GetCurrentProcess(), h, GetCurrentProcess(), &hDup, 0, TRUE, DUPLICATE_SAME_ACCESS))
				{
					h1_ = hDup;
				}
			}
		}

		if (h2 != NULL)
		{
			// get an inheritable write handle
			
			HANDLE h = pipe_write_handle(*stdio);

			if (h != NULL)
			{
				HANDLE hDup = NULL;
				
				if (DuplicateHandle(GetCurrentProcess(), h, GetCurrentProcess(), &hDup, 0, TRUE, DUPLICATE_SAME_ACCESS))
				{
					h2_ = hDup;
				}
			}
		}
	}

	if (h1 != NULL)
		*h1 = h1_;

	if (h2 != NULL)
		*h2 = h2_;
}

//
//
//

/*

	notes on Windows CreateProcess

	ignoring error handling and other details...

	if lpEnvironment is not NULL, ...
	if lpApplicationName is not NULL, ...

	LdrQueryImageFileExecutionOptions - get debugger command from registry if the process is not
	going to be debugged and the PEB ReadImageFileExecOptions is enabled

	StartupInfo.lpDesktop is not specified, use the caller's

	NtOpenFile()
	NtCreateSection() - if this fails, the library will try to run "cmd /c" with the specified filename

	NtCreateProcess(..., bInheritHandles, ...)

	BasePushProcessParameters() - allocated RTL_USER_PROCESS_PARAMETERS in the new
	process and copies values to it.  Also updates the new process PEB to point to the
	newly allocated block.

	BaseCreateStack()

	BaseInitializeContext()

	NtCreateThread()

	CsrClientCallServer()

*/

/*

RTL_USER_PROCESS_PARAMETERS ProcessParameters

StandardInput
StandardOutput
StandardError
ConsoleHandle

BaseDllInitialize - DLL entry point for kernel32.dll

DLL_PROCESS_ATTACH

DEBUG_PROCESS

XREF: "../tools/console.cpp"

*/

process_object_t *win_process_create(const process_create_t *args)
{
	_ASSERT(args != NULL);

	//
	// parameters for CreateProcess
	//
	
	LPCTSTR               lpApplicationName   = NULL;
	LPTSTR                lpCommandLine       = NULL;
	LPSECURITY_ATTRIBUTES lpProcessAttributes = NULL;
	LPSECURITY_ATTRIBUTES lpThreadAttributes  = NULL;
	BOOL                  bInheritHandles     = FALSE;
	DWORD                 dwCreationFlags     = 0;
	LPVOID                lpEnvironment       = NULL;
	LPCTSTR               lpCurrentDirectory  = NULL;

	LPTSTR cmdline_dup = NULL;

#ifdef UNICODE
	cmdline_dup = wcsdup(args->cmd);
#else
	cmdline_dup = _strdup(args->cmd); // warning C4996: 'strdup' was declared deprecated
#endif

	lpCommandLine = cmdline_dup;

	//
	//
	//
	
	if (args->debug != 0)
	{
		// the kernel32.dll DLL entry point (BaseDllInitialize) will issue a
		// breakpoint when the child process is initializing

		dwCreationFlags |= DEBUG_PROCESS;

		if (args->debug == 1)
			dwCreationFlags |= DEBUG_ONLY_THIS_PROCESS;
	}

	// always create suspended so we can work with child process object before
	// it is scheduled to run

	dwCreationFlags |= CREATE_SUSPENDED;

	//
	//
	//

	if (args->console != 0)
	{
/*

		console control mode

		default: new process inherits its parent's console, if it has one and
		if the child process is a console application

		valid combinations:

		CREATE_NEW_CONSOLE - creates new console, if app is a console app
		CREATE_DETACHED_PROCESS - 
		CREATE_NO_WINDOW - creates a new hidden console, if app is a console app

*/

		if (args->console == 2)
			dwCreationFlags |= DETACHED_PROCESS; // mutually exclusive with CREATE_NEW_CONSOLE
		else if (0)
			dwCreationFlags |= CREATE_NEW_CONSOLE; // mutually exclusive with DETACHED_PROCESS
		else if (args->console == 1)
			dwCreationFlags |= CREATE_NO_WINDOW; // ignored if child is not a console application
	}

#ifdef _DEBUG

	// see if our process has a console
	
	DWORD mode = 0;
	
	if (GetConsoleMode(GetStdHandle(STD_INPUT_HANDLE), &mode))
	{
	}

#endif

	//
	//
	//

	if (0)
	{
//		dwCreationFlags |= CREATE_PROTECTED_PROCESS;
	}

	if (0)
	{
		dwCreationFlags |= CREATE_DEFAULT_ERROR_MODE;
	}

	//
	//
	//
	
	string_t env; // storage for our temporary environment to pass to CreateProcess if needed
	
	if (args->env != NULL)
	{
		static const char sep = '\0';
		
		for (unsigned int i = 0; i < args->env->length(); i++)
			env += (*args->env)[i] + string(&sep, sizeof(sep)); // add null separator

		env += string(&sep, sizeof(sep)); // add null terminator

		if (is_error(env))
		{
			if (cmdline_dup != NULL)
				free(cmdline_dup);

			variant_t temp = args->cmd;
			
			process_error_t *xxx = new process_error_t;
		
			if (xxx != NULL)
			{
				xxx->m_error = temp;
			}

			return xxx;
		}
	
		lpEnvironment = (void*)(const char *)env;
	}

	//
	//
	//
	
	if (args->dir != "")
		lpCurrentDirectory = args->dir;
	else if (is_error(args->dir))
		DBG_NOT_IMPLEMENTED();

	//
	//
	//
	
	STARTUPINFO si = { sizeof(si) }; // initialize and clear all fields

	if (0)
	{
		si.lpTitle = NULL; // TODO:
	}

	//
	//
	//

	HANDLE hDupStdInput = NULL;
	HANDLE hDupStdOutput = NULL;
	HANDLE hDupStdError = NULL;

	if (args->stdio.in != NULL || args->stdio.out != NULL || args->stdio.err != NULL)
	{
		si.dwFlags |= STARTF_USESTDHANDLES;

		si.hStdInput = GetStdHandle(STD_INPUT_HANDLE);
		si.hStdOutput = GetStdHandle(STD_OUTPUT_HANDLE);
		si.hStdError = GetStdHandle(STD_ERROR_HANDLE);

		bInheritHandles = TRUE; // copy *all* inheritable handles into child process ?!?!?!?!?!?!?!?!?

		redirect_helper(args->stdio.in, &hDupStdInput, NULL);
		redirect_helper(args->stdio.out, NULL, &hDupStdOutput);
		redirect_helper(args->stdio.err, NULL, &hDupStdError);

		if (hDupStdInput != NULL)
			si.hStdInput = hDupStdInput;

		if (hDupStdOutput != NULL)
			si.hStdOutput = hDupStdOutput;

		if (hDupStdError != NULL)
			si.hStdError = hDupStdError;
	}

	//
	//
	//
	
	process_object_t *d = NULL;

	PROCESS_INFORMATION pi = { NULL };

	if (CreateProcess(lpApplicationName, lpCommandLine, lpProcessAttributes,
		lpThreadAttributes, bInheritHandles, dwCreationFlags, lpEnvironment,
		lpCurrentDirectory, &si, &pi))
	{
		windows_process_t *xxx = new windows_process_t;

		d = xxx;
		
		if (xxx != NULL)
		{
			xxx->m_public.object = xxx;
			xxx->m_public.id     = pi.dwProcessId;
			xxx->m_pid           = pi.dwProcessId;
			xxx->m_hProcess      = pi.hProcess;
			xxx->m_hThread       = pi.hThread;
			xxx->m_tid           = pi.dwThreadId;

			xxx->update_status();

			if (args->suspended == 0)
			{
				if (ResumeThread(pi.hThread))
				{
				}
				else
				{
					DWORD err = GetLastError();
				}
			}
		}
	}
	else
	{
		DWORD err = GetLastError();
		
		process_error_t *xxx = new process_error_t;
		
		d = xxx;

		if (xxx != NULL)
		{
			// TODO: certain error codes make use of placeholder arguments,
			// such as ERROR_BAD_EXE_FORMAT which refers to the text
			// "%1 is not a valid Win32 application."  Do we have a way to
			// put the placeholder argument here???
			
			xxx->m_error = FormatSystemMessage(err);
		}
	}

	//
	//
	//

	if (cmdline_dup != NULL)
		free(cmdline_dup);

	if (hDupStdInput != NULL)
		CloseHandle(hDupStdInput);

	if (hDupStdOutput != NULL)
		CloseHandle(hDupStdOutput);

	if (hDupStdError != NULL)
		CloseHandle(hDupStdError);

	//
	//
	//

	return d;
}

//
//
//

// NOTE: only for use in resuming a process that was started as suspended, not
// for general use

bool process_resume1(process_t p)
{
	windows_process_t *obj = windows_process(p);

	if (obj != NULL)
	{
		if (obj->m_hThread != NULL)
		{
			if (ResumeThread(obj->m_hThread))
			{
				return true;
			}
		}
	}

	return false;
}

//
//
//

/*

	the CRT function "system" runs the provided command by spawning a shell to
	run it.  the standard handles have their default values, so there's no
	opportunity to capture them here.

	string_t cmdline;

	_tspawnve ( "COMSPEC" /c <command> )

	if (comspec == "")
		comspec = ( _osver & 0x8000 ) ? "command.com" : "cmd.exe";

	cmdline += comspec;
	cmdline += " /c ";
	cmdline += command;

	CreateProcess()

	WaitForSingleObject(hProcess, INFINITE); - optional
    GetExitCodeProcess(hProcess, &retval); - optional
	CloseHandle(hProcess); - optional

	CloseHandle(hThread);

	struct mscrt_reserved_t
	{
		u32  nh; // number of handles
		char file[nh]; // if this entry is zero, there is no handle and the corresponding fhnd[] entry will be INVALID_HANDLE_VALUE
		long fhnd[nh];
	};

	lpReserved2 - points to buffer containing mscrt_reserved_t
	cbReserved2

*/

//
//
//

// get available processors for the current process...

uintptr_t processor_affinity()
{
	DWORD_PTR process = 0;
	DWORD_PTR system = 0;
	
	// this is a kernel call

	if (GetProcessAffinityMask(GetCurrentProcess(), &process, &system))
	{
	}
	else
	{
		process = 1;
	}

	// we're running, so there better be at least one core!

	_ASSERT(process != 0);

	return process;
}

int processor_count()
{
	uintptr_t process = processor_affinity();

	int c;
		
	for (c = 0; process != 0; c++)
		process &= process - 1; // turn off least significant bit

	_ASSERT(c >= 1);
		
	return c;
}

//
//
//

void windows_process_t::update_status()
{
	if (m_hProcess != NULL)
	{
		DWORD dwMilliseconds = 0;

		DWORD result = WaitForSingleObject(m_hProcess, dwMilliseconds);
		
		if (result == WAIT_OBJECT_0)
			m_public.status = io_task_status_complete;
		else if (result == WAIT_TIMEOUT)
			m_public.status = io_task_status_running;
		else
			m_public.status = io_task_status_error;
	}
	else
	{
		m_public.status = io_task_status_none;
	}
}

//
//
//

string_t process_command_line()
{
	return GetCommandLine();
}

#endif

