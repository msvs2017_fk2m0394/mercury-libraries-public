/*------------------------------------------------------------------------------

	platform/windows/io_device.cpp - Windows I/O support

------------------------------------------------------------------------------*/

#include "pch.h"
#include "io.h"
#include "core.h"

// ?!?!?!?!?!?!?!?!?!?!?!?
#include "../../lib/helper.h"
#include "../../lib/io/io_scheme.h"
#include "../../lib/io/io_stream.h"
// ?!?!?!?!?!?!?!?!?!?!?!?

#ifndef LITE_RTL

//
//
//

file_t file_CreateFile(string_t lpFileName, DWORD dwDesiredAccess,
	DWORD dwShareMode, LPSECURITY_ATTRIBUTES lpSecurityAttributes,
	DWORD dwCreationDisposition, DWORD dwFlagsAndAttributes,
    HANDLE hTemplateFile)
{
	// TODO: call create_directory under certain conditions?!?!?

	// TODO: call SetErrorMode(SEM_NOOPENFILEERRORBOX), or set_error_mode() ???!?!?

	//
	//
	//
	
	HANDLE hFile = INVALID_HANDLE_VALUE;

	// CreateFile ultimately sends IRP_MJ_CREATE to the device driver

#ifdef _WIN32_WCE

	// CreateFileA entry point not available under CE

	string_t temp = to_unicode(lpFileName);
	
	if (is_unicode(temp))
	{
		// TODO: dynamically load this KERNEL32.DLL entry point so we will
		// load on non-UNICODE Windows versions (e.g. Windows 95), if these
		// versions don't export CreateFileW
		
		hFile = CreateFileW(temp, dwDesiredAccess, dwShareMode,
			lpSecurityAttributes, dwCreationDisposition,
			dwFlagsAndAttributes, hTemplateFile);
	}
	else
	{
		DBG_BREAK();
	}

#else

	if (is_ansi(lpFileName))
	{
		hFile = CreateFileA(lpFileName, dwDesiredAccess, dwShareMode,
			lpSecurityAttributes, dwCreationDisposition,
			dwFlagsAndAttributes, hTemplateFile);
	}
	else if (is_unicode(lpFileName))
	{
		// TODO: dynamically load this KERNEL32.DLL entry point so we will
		// load on non-UNICODE Windows versions (e.g. Windows 95), if these
		// versions don't export CreateFileW
		
		hFile = CreateFileW(lpFileName, dwDesiredAccess, dwShareMode,
			lpSecurityAttributes, dwCreationDisposition,
			dwFlagsAndAttributes, hTemplateFile);
	}
	else
	{
		DBG_BREAK();
	}

#endif

	//
	//
	//

	if (hFile != INVALID_HANDLE_VALUE)
	{
		if (0)
		{
			file_from_handle(hFile);
		}

		win32_io_device_t *device = new win32_io_device_t(hFile, NULL, lpFileName);

		if (device != NULL)
		{
			if (dwFlagsAndAttributes & FILE_FLAG_OVERLAPPED)
				device->m_overlapped = true;

			return io_stream_create_and_release(device);
		}
		else
		{
			VERIFY(CloseHandle(hFile));

			return io_stream_error();
		}
	}
	else
	{
		DWORD dwError = GetLastError();
		
		variant_t text = FormatSystemMessage(dwError);

		error_log_message(trace_severity_error, text);

		return io_stream_error(text); // , dwError);

/*
		// TODO...
		
		variant_t error = system_error_variant(GetLastError());

		error_log_message(trace_severity_error, error);

		return io_stream_error(error);
*/
	}
}

//
//
//

HANDLE file_handle(file_t &file)
{
	win32_io_device_t *device = io_stream_cast<win32_io_device_t>(file);
	
	if (device != NULL)
		return device->m_hFile;

//	return INVALID_HANDLE_VALUE;
	return NULL;
}

//
//
//

file_t file_from_handle(HANDLE hFile)
{
	if (hFile == NULL)
		return io_stream_error();

	if (hFile == INVALID_HANDLE_VALUE)
		return io_stream_error();

	if (0)
	{
		HANDLE hFile2 = copy_handle(hFile);
	}

//	xxx GetHandleInformation GetFileInformationByHandle GetFileType

	win32_io_device_t *device = new win32_io_device_t(hFile, NULL, "");

	if (device != NULL)
	{
		// TODO: set m_overlapped???
	}
	
	return io_stream_create_and_release(device);
}

//
//
//

win32_io_device_t::win32_io_device_t(HANDLE hFile, LPVOID lpMapBase, string_t filename)
{
	m_hFile     = hFile;
	m_pathname  = filename;
	m_hMapping  = NULL;
	m_lpMapBase = lpMapBase;
}

//
//
//

win32_io_device_t::~win32_io_device_t()
{
	close(); // XXX: should be handled by framework before our destructor is called!
}

//
//
//

bool win32_io_device_t::read(io_read_t &info)
{
	//
	//
	//
	
	LPOVERLAPPED lpOverlapped = NULL;

	win32_io_operation_t *op = NULL;

	if (info.async_id != NULL)
	{
		// TODO: pass io_operation_object_t * in io_read_t???
		
		uint64_t async_id = *info.async_id;

		op = lookup_op(async_id);

		if (op != NULL)
		{
			_ASSERT(op->m_status == io_task_status_none);

			op->m_status = io_task_status_running;
			op->m_overlapped.hEvent = op->m_event.handle();

			lpOverlapped = &op->m_overlapped;
		}
		else
		{
			return false;
		}
	}
#if 0
	else
	{
		// TODO: synthesize an async request if the device is opened as overlapped
	}
#endif

	//
	//
	//

	// NOTE: ReadFile sends IRP_MJ_READ to the underlying driver
	
	// warning C4267: 'argument': conversion from 'size_t' to 'DWORD', possible loss of data

	DWORD len = 0;

	if (ReadFile(m_hFile, info.buf, info.cap, &len, lpOverlapped))
	{
		_ASSERT(len <= info.cap);

		info.len = len;

		if (len == 0)
		{
			// NOTE: for async I/O GetLastError returns ERROR_HANDLE_EOF
			
			// we're at EOF, for synchronous ReadFile operation, or we've
			// received a zero-length message if we're a named piped connection,
			// or ???
		}
		
		if (op != NULL)
		{
			op->m_status = io_task_status_complete;
			op->m_data_transferred = len;
		}

		return true;
	}
	else
	{
		DWORD dwError = GetLastError();

		if (dwError == ERROR_IO_PENDING)
		{
			_ASSERT(op != NULL);
			
			if (info.async_id != NULL)
			{
#ifdef _DEBUG
				if (op != NULL)
				{
//					_ASSERT(op->m_status == io_task_status_running);
				}
#endif
				
				return true;
			}
			else
			{
				DBG_INVALID_CASE();
			}

/*
			if (op != NULL)
			{
				op->m_status = io_task_status_running;
			}
*/
		}
		else
		{
			if (op != NULL)
			{
				op->m_status = io_task_status_error;
				op->m_error = dwError;
			}
		}

		if (dwError == ERROR_BROKEN_PIPE)
		{
			// the write end of the pipe has been closed, we're at EOF on the pipe
		}
		else
		{

/*
		switch (dwError)
		{
			case ERROR_HANDLE_EOF:
			{
				break;
			}

			case ERROR_IO_PENDING:
			{
				break;
			}

			case ERROR_MORE_DATA:
			{
				break;
			}

			case ERROR_BROKEN_PIPE:
			{
				break;
			}
		}
*/

		report_error(dwError, "ReadFile");
		}

		return false;
	}
}

//
//
//

bool win32_io_device_t::write(io_write_t &info)
{
	//
	//
	//
	
	LPOVERLAPPED lpOverlapped = NULL;

	win32_io_operation_t *op = NULL;

	if (info.async_id != NULL)
	{
		// TODO: pass io_operation_object_t * in io_write_t???
		
		uint64_t async_id = *info.async_id;

		op = lookup_op(async_id);

		if (op != NULL)
		{
			_ASSERT(op->m_status == io_task_status_none);
			
			op->m_status = io_task_status_running;
			op->m_overlapped.hEvent = op->m_event.handle();

			lpOverlapped = &op->m_overlapped;
		}
		else
		{
			return false;
		}
	}

#if 0

	else
	{
		// TODO: synthesize an async request if the device is opened as overlapped

		if (m_overlapped)
		{
//			DBG_NOT_IMPLEMENTED();

			if (0)
			{
				io_write_t request = { 0 };

				request.buffer = info.buffer;
				request.count = info.count;
//				request.min_write = ???;
				
/*
				uint64_t async_id = device.async_write(request);

				if (async_id != 0)
				{
				}
*/

	//			io_status_t status = device.async_status(async_id);
			}
		}
	}

#endif

	//
	//
	//
	
	// NOTE: WriteFile sends IRP_MJ_WRITE to the underlying driver
	
	// warning C4267: 'argument': conversion from 'size_t' to 'DWORD', possible loss of data

	DWORD len = 0;

	if (WriteFile(m_hFile, info.buf, info.cap, &len, lpOverlapped))
	{
		_ASSERT(len <= info.cap);

		info.len = len;

		if (op != NULL)
		{
			op->m_status = io_task_status_complete;
			op->m_data_transferred = len;
		}

		return true;
	}
	else
	{
		DWORD dwError = GetLastError();

		if (dwError == ERROR_IO_PENDING)
		{
			_ASSERT(op != NULL);
			
			if (info.async_id != NULL)
			{
				return true;
			}
			else
			{
				DBG_INVALID_CASE();
			}

/*
			if (op != NULL)
			{
				op->m_status = io_task_status_running;
			}
*/
		}
		else
		{
			// record error in async operation
			
			if (op != NULL)
			{
				op->m_status = io_task_status_error;
				op->m_error = dwError;
			}
		}

		report_error(dwError, "WriteFile");

		return false;
	}
}

//
//
//

bool win32_io_device_t::flush()
{
	return error_handler(FlushFileBuffers(m_hFile), "FlushFileBuffers");
}

//
//
//

void win32_io_device_t::get_position(file_pos_t &pos)
{
	LARGE_INTEGER li;

	li.HighPart = 0;

	li.LowPart = SetFilePointer(m_hFile, 0, &li.HighPart, FILE_CURRENT);

	if (li.LowPart != 0xFFFFFFFF || GetLastError() == NO_ERROR)
		pos = file_pos_absolute((u64)li.QuadPart);
}

//
//
//

bool win32_io_device_t::set_position(file_pos_t pos)
{
	if (pos.type == file_pos_type_absolute)
	{
		LARGE_INTEGER li;
		
		li.QuadPart = pos.absolute;
		
		DWORD dwLowPart = SetFilePointer(m_hFile, li.LowPart, &li.HighPart, FILE_BEGIN);

		return (dwLowPart != 0xFFFFFFFF || GetLastError() == NO_ERROR);
	}
	else if (pos.type == file_pos_type_relative)
	{
		LARGE_INTEGER li;
		
		li.QuadPart = pos.absolute;
		
		DWORD dwLowPart = SetFilePointer(m_hFile, li.LowPart, &li.HighPart, FILE_CURRENT);

		return (dwLowPart != 0xFFFFFFFF || GetLastError() == NO_ERROR);
	}

	return false;
}

//
//
//

bool win32_io_device_t::close()
{
	if (m_lpMapBase != NULL)
	{
		UnmapViewOfFile(m_lpMapBase);
		m_lpMapBase = NULL;
	}

	if (m_hMapping != NULL)
	{
		CloseHandle(m_hMapping);
		m_hMapping = NULL;
	}

	if (m_hFile != INVALID_HANDLE_VALUE)
	{
		error_handler(CloseHandle(m_hFile), "CloseHandle");

		m_pathname = "";
		m_hFile = INVALID_HANDLE_VALUE;
		m_overlapped = false;
	}

	return true;
}

//
//
//

void win32_io_device_t::get_length(file_pos_t &pos)
{
	ULARGE_INTEGER li;

	// XXX: should we call SetLastError(NO_ERROR) here, assuming Windows 95
	// probably doesn't set this properly (as evidenced by other APIs)

	li.LowPart = GetFileSize(m_hFile, &li.HighPart);

	if (li.LowPart == 0xFFFFFFFF)
	{
		DWORD dwError = GetLastError();

		if (dwError != NO_ERROR)
			return;
	}

	pos = file_pos_absolute(li.QuadPart);
}

//
//
//

bool win32_io_device_t::set_length(file_pos_t pos)
{
	DBG_NOT_IMPLEMENTED();

//	error_handler(SetEndOfFile(m_hFile), "SetEndOfFile");

	return false;
}

//
//
//

void win32_io_device_t::file_address(file_addr_t &addr)
{
	addr = filesys_name(m_pathname);
}

//
//
//

void win32_io_device_t::info(io_stream_info_t &info)
{
	if (1)
	{
		info.file_address = filesys_name(m_pathname);
	}

	if (1)
	{
/*
		ULARGE_INTEGER li;

		li.LowPart = GetFileSize(m_hFile, &li.HighPart);

		if (li.LowPart == 0xFFFFFFFF)
		{
			DWORD dwError = GetLastError();

			if (dwError != NO_ERROR)
				return false;
		}

		pos = file_pos_absolute(li.QuadPart);

		return true;
*/
	}

	if (1)
	{
/*
		LARGE_INTEGER li;

		if (::get_position(m_hFile, li))
		{
			pos = file_pos_absolute(li.QuadPart);

			return true;
		}

		return false;
*/
	}
}

//
//
//

void win32_io_device_t::status(io_stream_status_t &status)
{
	status = io_resource_status_open;
}

//
//
//

static const io_operation_dispatch_t g_io_operation_dispatch =
{
	static_cast<io_operation_dispatch_t::cancel_method_t>(&base_io_operation_t::cancel),
	static_cast<io_operation_dispatch_t::close_method_t>(&base_io_operation_t::close),
	static_cast<io_operation_dispatch_t::status_method_t>(&base_io_operation_t::status),
	static_cast<io_operation_dispatch_t::wait_method_t>(&base_io_operation_t::wait__),
};

bool win32_io_device_t::allocate_operation_extension(io_operation_object_t *op)
{
	_ASSERT(op != NULL);
	_ASSERT(op->m_dispatch.object == NULL);

	win32_io_operation_t *wop = m_async_ops.create_item();

	if (wop != NULL)
	{
		wop->m_header           = op;
		wop->m_device           = this;
		wop->m_status           = io_task_status_none;
		wop->m_data_transferred = 0;
		
		ZERO_INIT(wop->m_overlapped);

		op->m_dispatch.object = wop;
		op->m_dispatch.table = &g_io_operation_dispatch;

		return true;
	}

	return false;
}

//
//
//

win32_io_operation_t::win32_io_operation_t()
{
}

win32_io_operation_t::~win32_io_operation_t()
{
	if (m_overlapped.hEvent != NULL)
		VERIFY(CloseHandle(m_overlapped.hEvent));
}

void win32_io_operation_t::status(io_operation_status_t &status)
{
	if (m_status == io_task_status_running)
	{
		if (m_device->m_hFile != INVALID_HANDLE_VALUE)
		{
			//
			//
			//
			
#ifndef _WIN32_WCE

			BOOL bWait = FALSE;
			
			if (GetOverlappedResult(m_device->m_hFile, &m_overlapped, &m_data_transferred, bWait))
			{
//				_ASSERT(m_data_transferred <= m_data_to_be_transferred);
				
				m_status = io_task_status_complete;
			}
			else
			{
				DWORD dwError = GetLastError();

				switch (dwError)
				{
					case ERROR_IO_INCOMPLETE:
						break;
					
					case ERROR_HANDLE_EOF:
						DBG_NOT_IMPLEMENTED();
						break;

					default:
						m_status = io_task_status_error;
						m_error = dwError;
						break;
				}
			}
#else

			DBG_NOT_IMPLEMENTED2("win32_io_operation_t::status");

#endif

		}
		else
		{
			// file handle has been closed
			m_status = io_task_status_error;
			m_error = 0; // TODO: determine appropriate error code
		}
	}

	status.status            = m_status;
	status.bytes_transferred = m_data_transferred;
}

#if 1

bool win32_io_operation_t::wait(u32 flags, u32 timeout_msec)
{
	DBG_NOT_IMPLEMENTED();
	
	if (0)
	{
		DWORD result = WaitForSingleObject(m_event.handle(), timeout_msec);
	}

	return false;
}

#endif

bool win32_io_operation_t::cancel()
{
	DBG_NOT_IMPLEMENTED();
	
	return false;
}

//
//
//

win32_io_operation_t *win32_io_device_t::lookup_op(uint64_t async_id)
{
	if (async_id != 0)
	{
		win32_io_operation_t *op = NULL;
		
		for (u32 i = 0; i < m_async_ops.length(); i++)
		{
			win32_io_operation_t *op__ = m_async_ops[i];

			_ASSERT(op__->m_header->m_serial != 0);
			_ASSERT(op__->m_header->m_dispatch.object == op__);
			
			if (op__->m_header->m_serial == async_id)
			{
				op = op__;

				break;
			}
		}

		if (op != NULL)
		{
			if (1)
			{
				//
				//  Create a manual-reset event for signaling asynchronous I/O
				//

				if (op->m_event.handle() == NULL)
				{
					op->m_event.create_manual_reset();

					if (op->m_event.handle() == NULL)
					{
						DWORD dwError = GetLastError();
						
						DBG_BREAK();

						return false;
					}
					else
					{
					}
				}
			}
		}

		return op;
	}
	else
	{
		return NULL;
	}
}

//
//
//

bool device_io_control__(io_stream_t &device, unsigned int code, data_binding_t in, data_binding_t out)
{
	// NOTE: type fields in data_binding_t are ignored
	
	HANDLE handle = file_handle(device);

	if (handle != NULL)
	{
		// warning C4267: 'argument': conversion from 'size_t' to 'DWORD', possible loss of data

		DWORD cbReturned = 0;

		if (DeviceIoControl(handle, code, in.data, in.size, out.data, out.size,
			&cbReturned, NULL))
		{
			_ASSERT(cbReturned <= out.size);

			out.size = cbReturned;

			return true;
		}
		else
		{
			DWORD err = GetLastError();
		}
	}

	return false;
}

#endif

