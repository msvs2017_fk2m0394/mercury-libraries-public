#include "../../lib/core/scheduler.h"

/*
struct win_task_priority_queue_t : task_priority_queue_t
{
	HANDLE ready_event; // signaled while any tasks are on queue
};
*/

struct win_task_processor_group_t : task_processor_group_t
{
//	task_priority_queue_t m_ready_queue; // ready-to-run tasks
	HANDLE ready_event; // signaled while any tasks are on queue
};

struct win_task_manager_t : task_manager_t
{
};

struct win_task_t : task_t
{
//	struct {
//		DWORD  error;
//		LCID   locale;
//		HANDLE token;
//	} m_win32_thread_state;
};

#if defined(_MSC_VER) && defined(_M_IX86)

struct fiber_frame_t;

struct win_fiber_t : fiber_t
{
	NT_TIB tib;
	LPVOID fiber; // OS FIBER
};

#if _MSC_VER <= 1200

// PcTeb is the offset in the structure NT_TIB of the field Self,
// which contains the linear address of the current thread's TEB

// from Windows SDK headers...

#define PcTeb 0x18

#pragma warning (disable:4035) // disable 4035 (function must return something)
__inline struct _TEB * NtCurrentTeb( void ) { __asm mov eax, fs:[PcTeb] }
#pragma warning (default:4035) // re-enable it

#endif

#elif defined(_MSC_VER) && defined(_M_X64)

struct fiber_frame_t;

struct win_fiber_t : fiber_t
{
	NT_TIB tib;
	LPVOID fiber; // OS FIBER
};

#endif

#ifdef __cplusplus
extern "C" {
#endif

u32 event_register_handle_callback(HANDLE hObject,
	event_signal_callback_t callback, void *context);

#ifdef __cplusplus
}
#endif

struct lock_internal_t
{
	lock_internal_t();
	~lock_internal_t();

	u32              serial;
	u32              init;        //
	u32              lock_count;  //
	u32              entry_count; //
	CRITICAL_SECTION cs;          //
};

#ifdef _DEBUG
void debug_validate(const lock_internal_t *data);
#else
inline void debug_validate(const lock_internal_t *) { }
#endif

int sync_dispatch_win32(core_thread_state_t *ts, sync_dispatch_t *args);

struct wait_handle_table_t
{
	inline wait_handle_table_t() { count = 0; }

	DWORD              count;
	UINT               indices[MAXIMUM_WAIT_OBJECTS]; // index into thread's callbacks
	HANDLE             handles[MAXIMUM_WAIT_OBJECTS];
	callback_entry_t * callbacks[MAXIMUM_WAIT_OBJECTS];
};

// represents both threads and fibers

struct task_thread_t : task_processor_t
{
	task_thread_t(task_processor_group_t *pg)
	{
		m_group = pg;
	}

	object_ref_t<task_manager_t> m_tm; //
//	task_processor_group_t *m_processor_group;

	HANDLE   m_thread;
	LPVOID   m_fiber;
	task_t * m_dedicated;
};

