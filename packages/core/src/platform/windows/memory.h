/*------------------------------------------------------------------------------

	platform/windows/memory.h - Windows memory manager support

------------------------------------------------------------------------------*/

//
//	Win32 global memory (i.e. HGLOBAL) utilities.
//

bool MyGlobalFree(HGLOBAL hGlobal);
HGLOBAL MyGlobalDuplicate(HGLOBAL hSrc);
HGLOBAL MyGlobalDuplicate(const void *src, size_t size, unsigned long flags);

//
//
//

// helper class for working with HGLOBALs (used by clipboard and DDE)

struct global_mem_t
{
public:
	global_mem_t();
	~global_mem_t();

	LPBYTE Lock();
	void Unlock();
	bool Realloc(UINT uFlags, DWORD dwBytes);
	inline operator HGLOBAL() const;
	void Free();
	LPBYTE SetHandleAndLock(HGLOBAL hGlobalMemory, BOOL bAllowGrow);
	LPBYTE AllocAndLock(unsigned long nBytes);
	LPBYTE ReallocAndLock(unsigned long nBytes);
	HGLOBAL Detach();
	inline void SetAllocFlags(UINT nAllocFlags);
	inline void SetAllowGrow(BOOL bAllowGrow);

	UINT    m_nAllocFlags;
	HGLOBAL m_hGlobal;
	LPVOID  m_lpVoid;
	BOOL    m_bAllowGrow;
};

inline global_mem_t::operator HGLOBAL() const
{
	return m_hGlobal;
}

inline void global_mem_t::SetAllocFlags(UINT nAllocFlags)
{
	_ASSERT(m_hGlobal == NULL);

	m_nAllocFlags = nAllocFlags;
}

inline void global_mem_t::SetAllowGrow(BOOL bAllowGrow)
{
	_ASSERT(m_hGlobal == NULL);

	m_bAllowGrow = bAllowGrow;
}

//
//
//

struct memory_buffer_t
{
	// XXX: can we use my_buffer2_t to aid this implementation?

	u32              m_nGrowBytes;  // allocation granularity, zero for non-growable
	size_t           m_nBufferSize; // allocated buffer size
	u8 *             m_lpBuffer;    // backing buffer
//	my_buffer2_t<u8> m_buffer__;
	global_mem_t     m_gmem;        // global memory handle
};

