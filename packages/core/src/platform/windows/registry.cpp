/*------------------------------------------------------------------------------

	platform/windows/registry.cpp

	- provides a simple variant-based interface to the Windows registry

	registry_read() always returns a **VALUE** (not a sub-key), for example:

	HKEY_CLASSES_ROOT\.txt  = read the ".txt" **VALUE** under HKEY_CLASSES_ROOT
	HKEY_CLASSES_ROOT\.txt\ = read the unnamed **VALUE** under the path

	NOTE: never use HKEY_CLASSES_ROOT from a service (see the MSDN documentation
	for "HKEY_CLASSES_ROOT" for Windows 2000 and above)

	see also: HKEY_LOCAL_MACHINE\Software\Microsoft\Windows NT\CurrentVersion\IniFileMapping

	todo: implement low-level .INI file manipulation routines for portability

	todo: allow mapping API calls to data stored in registry

	todo: implement low-level .INI file routines to workaround API
		  limitations (no error reporting; no length indicator)

	NOTE: the Windows registry was intended to address the limitations of .ini
	files, but we still have the following issues to address in Mercury:

	* debug and release EXEs end up using 2 different config files since
	  they're in different directories (the registry is in a central location)

	* provide a nice user interface for editing config files (e.g. use a
	  "template" file that explains the settings and shows valid settings
	  where applicable)

	* how to we provide remote administration (the registry is available over
	  the network)

	* where do store user-specific versus common versus machine-specific
	  settings???

	* security..  we need to authorize changes to the common and machine
	  settings

------------------------------------------------------------------------------*/

#include "pch.h"
#include "core.h"
#include "io.h"

#include "../../lib/misc/config.h"
#include "../../lib/misc/misc.h"
#include "../../lib/debug/trace.h"
#include "../../lib/io/io.h"

#ifdef _MBCS
#undef _MBCS
#endif

#include <tchar.h>

//
//
//

#ifndef LITE_RTL

static const struct { const char *name; HKEY khey; } g_entries[] =
{
	{ "HKEY_CLASSES_ROOT",     HKEY_CLASSES_ROOT     }, // alias of HKEY_LOCAL_MACHINE\SOFTWARE\Classes
	{ "HKEY_CURRENT_USER",     HKEY_CURRENT_USER     },
	{ "HKEY_LOCAL_MACHINE",    HKEY_LOCAL_MACHINE    },
	{ "HKEY_USERS",            HKEY_USERS            },
#ifndef _WIN32_WCE
	{ "HKEY_PERFORMANCE_DATA", HKEY_PERFORMANCE_DATA }, // NOTE: NT only
	{ "HKEY_CURRENT_CONFIG",   HKEY_CURRENT_CONFIG   },
	{ "HKEY_DYN_DATA",         HKEY_DYN_DATA         }, // NOTE: 95/98 only
#endif

	// commonly used abbreviations...

	{ "HKCR",                  HKEY_CLASSES_ROOT     },
	{ "HKCU",                  HKEY_CURRENT_USER     },
	{ "HKLM",                  HKEY_LOCAL_MACHINE    },
	{ "HKU",                   HKEY_USERS            },
#ifndef _WIN32_WCE
	{ "HKPD",                  HKEY_PERFORMANCE_DATA },
	{ "HKCC",                  HKEY_CURRENT_CONFIG   },
	{ "HKDD",                  HKEY_DYN_DATA         },
#endif
};

static trace_link_t g_trace_registry = { NULL, "Registry" };

static inline bool query_trace(LONG result)
{
	return trace_enabled(g_trace_registry, result == ERROR_SUCCESS ? trace_severity_debug : trace_severity_error);
}

#endif

//
//
//

bool registry_read(string_t name, u32 &value, u32 default_value)
{
	variant_t var;

	if (registry_read2(name, &var) && variant_evaluate(var, value) > 0)
		return true;

	value = default_value;

	return false;
}

bool registry_read(string_t name, int &value, int default_value)
{
	variant_t var;

	if (registry_read2(name, &var) && variant_evaluate(var, value) > 0)
		return true;

	value = default_value;

	return false;
}

bool registry_read(string_t name, char &value, char default_value)
{
	variant_t var;

	if (registry_read2(name, &var) && variant_evaluate(var, value) > 0)
		return true;

	value = default_value;

	return false;
}

bool registry_read(string_t name, u16 &value, u16 default_value)
{
	variant_t var;

	if (registry_read2(name, &var) && variant_evaluate(var, value) > 0)
		return true;

	value = default_value;

	return false;
}

//
//
//

variant_t registry_read(string_t name)
{
	variant_t value;

	registry_read2(name, &value);

	return value;
}

//
//
//

string_t registry_name__(string_t name)
{
	string_t app = application_name();

	ART_ASSERT(strchr(app, '\\') == NULL);
//	ART_ASSERT(strchr(name, '\\') == NULL);

	return "HKEY_CURRENT_USER\\Software\\HPDD\\Mercury\\Storage\\" + app + "\\" + name;
}

variant_t registry_read__(string_t name)
{
	return registry_read(registry_name__(name));
}

bool registry_write__(string_t name, variant_t value)
{
	return registry_write(registry_name__(name), value);
}

//
//
//

#ifndef LITE_RTL

// this class will open the HKEY on-demand and with only as much security
// as needed to accomplish its job

// XXX: remove this class???  It's only used as a helper for
// registry_subkey_names

struct regkey_t
{
public:
	regkey_t();
	regkey_t(string_t path);
	~regkey_t();

	bool open(string_t path);
	bool GetSubKeyName(DWORD index, string_t &name);
	bool GetValueName(DWORD index, string_t &name);
	void close();

	string_t get_subkey_name(unsigned int index);
	string_array_t enum_subkey_names();

	string_t get_value_name(unsigned int index);

protected:

	bool get_handle(REGSAM regsam);

private:
	string_t m_path;
	HKEY     m_handle;
	REGSAM   m_regsam;
};

// XXX: ANSI/UNICODE build dependent...

static LPCTSTR strip_hive_name(LPCTSTR path, HKEY &hKey);

struct name_info_t
{
	string_t lpMachineName;
	HKEY     hkey;
	string_t pszSubKey;
	string_t pszValueName;
};

// XXX: ANSI/UNICODE build dependent...

static size_t lookup_key_name(LPCTSTR name, HKEY &hkey)
{
	for (unsigned int i = 0; i < DIM(g_entries); i++)
	{
		size_t len = _tcslen(g_entries[i].name);

		if (_tcsnicmp(name, g_entries[i].name, len) == 0)
		{
			hkey = g_entries[i].khey;
			return len;
		}
	}

	return 0;
}

// XXX: ANSI/UNICODE build-dependent

static void parse_name(LPCTSTR name, name_info_t &info);

static LONG registry_write_internal(LPCTSTR name, const variant_t &value);
static HRESULT DeleteRegistryInternal__(HKEY hkey, LPCTSTR pszSubKey, LPCTSTR pszValueName);
static LONG registry_read_internal(LPCTSTR name, variant_t &ans);
static bool next_multisz(LPCTSTR &ptr, LPCTSTR ptr_end, LPCTSTR &start, ptrdiff_t &len);
static variant_t multisz_to_string_array(string_t data);
static variant_t read_value(HKEY hkeyResult, LPCTSTR pszValueName, LONG &result);
static LONG registry_write_internal(LPCTSTR name, const variant_t &value);
static LONG WriteRegistryInternal__(HKEY hkey, LPCTSTR pszSubKey,
	LPCTSTR pszValueName, DWORD dwType, CONST VOID *lpData, DWORD cbData);
static size_t lookup_key_name(LPCTSTR name, HKEY &hkey);

//
//
//

static string_t REG_get_subkey_name(HKEY hKey, DWORD dwIndex);
static string_t REG_get_value_name(HKEY hKey, DWORD dwIndex);

regkey_t::regkey_t()
{
	m_handle = NULL;
	m_regsam = 0;
}

regkey_t::~regkey_t()
{
	close();
}

void regkey_t::close()
{
	if (m_handle != NULL)
	{
		LONG lError = RegCloseKey(m_handle);

		if (lError != ERROR_SUCCESS)
		{
		}

		m_handle = NULL;
		m_regsam = 0;
	}
	else
	{
		_ASSERT(m_regsam == 0);
	}
}

bool regkey_t::open(string_t path)
{
	close();

	m_path = path;

	return (!is_error(m_path));
}

bool regkey_t::GetSubKeyName(DWORD index, string_t &name)
{
	string_t name_temp = get_subkey_name(index);

	if (is_error(name_temp))
		return false;

	name = name_temp;

	return true;
}

string_t regkey_t::get_subkey_name(unsigned int index)
{
	if (!get_handle(KEY_ENUMERATE_SUB_KEYS))
		return string_error(); // TODO: get error string from failed registry call!!

	return REG_get_subkey_name(m_handle, index);
}

string_array_t regkey_t::enum_subkey_names()
{
	string_array_t ans;

	string_t name;

	for (DWORD dwIndex = 0; GetSubKeyName(dwIndex, name); dwIndex++)
		ans.append(name);

	return ans;
}

bool regkey_t::GetValueName(DWORD index, string_t &name)
{
	string_t name_temp = get_value_name(index);

	if (is_error(name_temp))
		return false;

	name = name_temp;

	return true;
}

string_t regkey_t::get_value_name(unsigned int index)
{
	if (!get_handle(KEY_QUERY_VALUE))
		return string_error(); // TODO: get error string from failed registry call!!

	return REG_get_value_name(m_handle, index);
}

bool regkey_t::get_handle(REGSAM regsam)
{
	if (is_empty(m_path))
		return false;

	if ((m_regsam & regsam) == 0)
	{
		//
		//
		//

		HKEY hKey;

		LPCTSTR subkey = strip_hive_name(m_path, hKey);

		if (subkey == NULL)
			return false;

		if (*subkey == 0)
			subkey = NULL;

		//
		//
		//

		HKEY handle;

		LONG lError;

		lError = RegOpenKeyEx(hKey, subkey, 0, regsam, &handle);

		if (lError != ERROR_SUCCESS)
		{
			// TODO: log error

			string_t err = FormatSystemMessage(lError);

			return false;
		}

		close();

		m_handle = handle;
		m_regsam = regsam;
	}
	else
	{
		_ASSERT(m_handle != NULL);
	}

	return (m_handle != NULL);
}

//
//
//

static string_t REG_get_subkey_name(HKEY hKey, DWORD dwIndex)
{
	TCHAR szKeyName[256]; // XXX
	DWORD cbKeyName = DIM(szKeyName);

	LONG result = RegEnumKeyEx(hKey, dwIndex, szKeyName, &cbKeyName,
		NULL, NULL, NULL, NULL);

	if (result == ERROR_SUCCESS)
	{
		_ASSERT(cbKeyName < DIM(szKeyName));

		return string(szKeyName, cbKeyName);
	}

	// TODO: log error

	return FormatSystemMessage(result);
}

static string_t REG_get_value_name(HKEY hKey, DWORD dwIndex)
{
	TCHAR szValueName[256]; // XXX
	DWORD cbValueName = DIM(szValueName);

	LONG result = RegEnumValue(hKey, dwIndex, szValueName, &cbValueName,
		NULL, NULL, NULL, NULL);

	if (result == ERROR_SUCCESS)
	{
		_ASSERT(cbValueName < DIM(szValueName));

		return string(szValueName, cbValueName);
	}

	// TODO: log error

	return FormatSystemMessage(result);
}

//
//
//

bool registry_delete(string_t name/*, bool deep*/)
{
	name_info_t info;

	parse_name(name, info);

	_ASSERT(info.hkey != NULL);

	// remote registry not supported

	if (info.lpMachineName != "")
		return false;

	if (info.hkey == NULL)
		return false;

	if (info.pszSubKey == "")
		return false;

	LONG result = DeleteRegistryInternal__(info.hkey, info.pszSubKey, info.pszValueName);

#ifdef _DEBUG
	if (query_trace(result))
	{
		string_t strResult = FormatSystemMessage(result);

		TRACE("REG: registry_delete_internal(\"%s\") = %s\n",
			get_trace_string(name), get_trace_string(strResult));
	}
#endif

	variant_t temp = variant_system_error(result);

	error_log_message(trace_severity_error, temp);

	return (result == ERROR_SUCCESS);
}

//
//
//

// TODO: recursively delete subkeys (see MFC source AfxDelRegTreeHelper for
// and example of how to do this)

static HRESULT DeleteRegistryInternal__(HKEY hkey, LPCTSTR pszSubKey,
	LPCTSTR pszValueName)
{
	_ASSERT(hkey != NULL);
#ifndef UNICODE
	_ASSERT(pszSubKey != NULL && strlen(pszSubKey) > 0);
	_ASSERT(pszValueName == NULL || strlen(pszValueName) > 0);
#endif

	//
	//  Open existing registry key
	//

	LONG lReg;
	HKEY hkeyResult;

	lReg = RegOpenKeyEx(hkey, pszSubKey, 0, KEY_WRITE, &hkeyResult);

	if (lReg != ERROR_SUCCESS)
		return E_FAIL; // XXX: TODO: return a more meaningful error code (based on Win32 API)

	//
	//
	//

	HRESULT hr = RegDeleteValue(hkeyResult, pszValueName);

	//
	//  Clean-up
	//

	VERIFY(RegCloseKey(hkeyResult) == ERROR_SUCCESS);

	return hr;
}

//
//
//

bool registry_read2(string_t name, variant_t *value)
{
	if (is_error(name))
	{
		if (value != NULL)
			*value = name; // error prop.

		return false;
	}

	variant_t temp;

	LONG result = registry_read_internal(name, temp);

	if (result != ERROR_SUCCESS)
	{
		temp = variant_system_error(result);
	}

	//
	//
	//

#ifdef _DEBUG

	if (0)
	{
		if (query_trace(result))
		{
			string_t strValue = temp.string();

			TRACE("REG: registry_read_internal(\"%s\")=", get_trace_string(name));

			if (result == ERROR_SUCCESS)
			{
				TRACE("\"%s\"\n", get_trace_string(strValue));
			}
			else
			{
				string_t strResult = FormatSystemMessage(result);

				TRACE("%s\n", get_trace_string(strResult));
			}
		}
	}

#endif

	//
	//
	//

	// TODO: write a helper function???

	trace_severity_t trace_level;

	if (result == ERROR_SUCCESS)
		trace_level = trace_severity_debug;
	else
		trace_level = trace_severity_error;

	if (trace_enabled(g_trace_registry, trace_level))
	{
		string_t strValue = temp.string();

		string_t temp = format_string("registry_read_internal(\"%s\")=", (const char *)name); // warning C4456: declaration of 'temp' hides previous local declaration

		if (result == ERROR_SUCCESS)
		{
			temp += "\"";
			temp += get_trace_string(strValue);
			temp += "\"\n";
		}
		else
		{
			string_t strResult = FormatSystemMessage(result);

			temp += get_trace_string(strResult);
			temp += "\n";
		}

		TRACE2V(&g_trace_registry, trace_level, temp, NULL);
	}

	//
	//
	//

	if (value != NULL)
	{
		*value = temp;
	}

	return (result == ERROR_SUCCESS);
}

//
//
//

#ifndef REMOVE_EXPERIMENTAL

int registry_evaluate2(const char *name, data_binding_t value)
{
	DBG_NOT_IMPLEMENTED();

	return 0;
}

int registry_evaluate(string_t name, data_binding_t value)
{
	if (is_error(name))
		return -1;

	return registry_evaluate2(name, value);
}

#endif

//
//
//

static LONG registry_read_internal(LPCTSTR name, variant_t &ans)
{
	//
	//
	//

	name_info_t info;

	parse_name(name, info);

	// NOTE: currently limited to local machine only
	// TODO: return variant_t namespace if name refers to a subkey???

	LONG result;

//	LPCTSTR lpMachineName = info.lpMachineName;
//	HKEY    hkey          = info.hkey;
	LPCTSTR pszSubKey     = info.pszSubKey;
	LPCTSTR pszValueName  = info.pszValueName;

	HKEY hkeyResult = NULL;
	HKEY hkey = NULL;

	//
	//
	//

	HKEY hkeyMachine = NULL;

	if (info.lpMachineName != "")
	{
		// not applicable to remote registry...
		_ASSERT(hkey != HKEY_CLASSES_ROOT);
		_ASSERT(hkey != HKEY_CURRENT_USER);

		static const char fmt[] = "\\\\";

#if !defined(_WIN32_WCE)

		result = RegConnectRegistry(fmt + info.lpMachineName, info.hkey, &hkeyResult);

		if (result != ERROR_SUCCESS)
			return result;
#else

		DBG_NOT_IMPLEMENTED();

		return E_FAIL;

#endif

		hkey = hkeyMachine;
	}
	else
	{
		hkey = info.hkey;
	}

	//
	//
	//

/*
	pszSubKey = name;

	if (hkey == NULL)
	{
		LPCTSTR subkey_temp = strip_hive_name(pszSubKey, hkey);

		if (subkey_temp != NULL)
			pszSubKey = subkey_temp;
	}

	LPCTSTR pszLastSlash = NULL;

	if (pszSubKey != NULL)
	{
		LPCTSTR psz = pszSubKey;

		for (;;)
		{
			if (*psz == '\\')
			{
				pszLastSlash = psz;
			}
			else if (*psz == '\0')
			{
				break;
			}

			psz++;
		}
	}

	string_t path;

	if (pszLastSlash != NULL)
	{
		pszValueName = pszLastSlash + 1;
		path = string_t(pszSubKey, pszLastSlash - pszSubKey);
		pszSubKey = path;
	}
*/

	_ASSERT(hkey != NULL);

	//
	//  Open existing registry key
	//

#if !defined(_WIN32_WCE)

	if (info.hkey == HKEY_PERFORMANCE_DATA)
	{
		// NOTE: special case for NT performance data...  RegOpenKeyEx isn't
		// used to access this data; use RegQueryValueEx directly, in addition,
		// the buffer size cannot be determined by the cbData output parameter

		hkeyResult = hkey;

#ifndef UNICODE
		_ASSERT(pszSubKey == NULL || strlen(pszSubKey) == 0);
#endif

		DWORD cbData = 4 * 1024;

		string_t temp;

		while (true)
		{
			string_t temp; // warning C4456: declaration of 'temp' hides previous local declaration

			void *ptr = alloc_ansi_buffer(temp, cbData);

			DWORD dwType;

			// NOTE: the output parameter cbData isn't used in the usual
			// manner for accessing performance data (since the data is
			// dynamic, the function doesn't return the size of the buffer
			// needed)

			DWORD cbDataDummy = cbData;

			result = RegQueryValueEx(hkeyResult, pszValueName, NULL, &dwType,
				(unsigned char*)ptr, &cbDataDummy);

			if (result == ERROR_SUCCESS)
			{
				ans = temp;

				if (dwType == REG_BINARY)
				{
				}

				break;
			}
			else if (result == ERROR_MORE_DATA)
			{
				cbData += 1024;
			}
			else
			{
				ans = variant_system_error(result);

				break;
			}
		}

		VERIFY(RegCloseKey(hkeyResult) == ERROR_SUCCESS);
	}
	else
#endif
	{
#ifndef UNICODE
		_ASSERT(pszSubKey != NULL && strlen(pszSubKey) > 0);
#endif

		// NOTE: a NULL or empty value name string will return the default
		// value

		result = RegOpenKeyEx(hkey, pszSubKey, 0, KEY_QUERY_VALUE, &hkeyResult);

		if (result == ERROR_SUCCESS)
		{
			//
			//  Read key value
			//

			ans = read_value(hkeyResult, pszValueName, result);

			//
			//  Clean-up
			//

			VERIFY(RegCloseKey(hkeyResult) == ERROR_SUCCESS);
		}
		else
		{
			ans = variant_system_error(result);
		}
	}

	//
	//
	//

	if (hkeyMachine != NULL)
		VERIFY(RegCloseKey(hkeyMachine) == ERROR_SUCCESS);

	return result;
}

//
//
//

// TODO: search for all uses of alloc_ansi_buffer and make sure
// the length is getting set correctly

static void fix_nts_len(string_t &str)
{
	if (is_ansi(str))
	{
		size_t len = strlen((const char *)str);

		if (str.length() != len) // warning C4018: '!=' : signed/unsigned mismatch
		{
			// if this is allowed, further string concatenation will
			// not be correct since the string_t operations use the
			// m_length member of string_object_t

/*
			string_object_t *data = get_string_data(str);

			data->m_length = len;

			_ASSERT(data->m_length < data->m_alloc);
*/

			str = (const char *)str;
		}
	}
	else
	{
	}
}

//
//
//

static variant_t read_value(HKEY hkeyResult, LPCTSTR pszValueName, LONG &result)
{
	//
	//
	//

	DWORD dwType;
	UCHAR buffer[64];
	DWORD cbData = sizeof(buffer);

	// NOTE: REG_SZ, REG_MULTI_SZ and REG_EXPAND_SZ values might not be properly
	// null-terminated (see MSDN Library online "RegQueryValueEx")

	result = RegQueryValueEx(hkeyResult, pszValueName, NULL, &dwType,
		buffer, &cbData);

	if (result == ERROR_SUCCESS || result == ERROR_MORE_DATA)
	{
		switch (dwType)
		{
			//
			//
			//

			case REG_NONE:
				return variant_t();

			//
			//
			//

			case REG_SZ:
			case REG_EXPAND_SZ:
			{
				if (cbData == 0)
					return "";

				string_t ans;

				_ASSERT(cbData > 0);

				char *psz = alloc_ansi_buffer(ans, cbData - 1);

				if (psz != NULL)
				{
					if (result == ERROR_MORE_DATA)
					{
						result = RegQueryValueEx(hkeyResult, pszValueName, NULL,
							&dwType, (LPBYTE)psz, &cbData);
					}
					else
					{
						_ASSERT(result == ERROR_SUCCESS);

						memcpy(psz, buffer, cbData);
					}

					if (result != ERROR_SUCCESS)
						return variant_system_error(result);

					// WINBUG???: ...

					// NOTE: this assertion fails when reading "HKEY_CLASSES_ROOT\DevCpp.cpp\DefaultIcon\"
					// the string returned is "C:\Program Files\Microsoft Visual Studio\Common\MSDev98\Bin\msdev.exe,2"
					// (71 chars), but cbData is 130

//					_ASSERT(psz[cbData - 1] == 0);
					_ASSERT(cbData - 1 == ans.length());

					fix_nts_len(ans);
				}

				return ans;
			}

			//
			//
			//

			case REG_BINARY:
			{
#if 0
				variant_t ans;

				void *ptr = variant_create_blob(ans, cbData);

				if (ptr != NULL)
				{
					if (result == ERROR_MORE_DATA)
					{
						result = RegQueryValueEx(hkeyResult, pszValueName, NULL,
							&dwType, (LPBYTE)ptr, &cbData);
					}
					else
					{
						memcpy(ptr, buffer, cbData);
					}

					if (result != ERROR_SUCCESS)
						return variant_system_error(result);
				}

				return ans;
#else
				DBG_NOT_IMPLEMENTED();

				return variant_error();
#endif
			}

			//
			//
			//

			case REG_DWORD_LITTLE_ENDIAN:
			case REG_DWORD_BIG_ENDIAN:
			{
				_ASSERT(cbData == sizeof(DWORD));

				DWORD dwValue;

				if (result == ERROR_MORE_DATA) // ?!?!?!?!?!?!?!?!?
				{
					DBG_INVALID_CASE();
					
					result = RegQueryValueEx(hkeyResult, pszValueName, NULL,
						&dwType, (LPBYTE)&dwValue, &cbData);
				}
				else
				{
					memcpy(&dwValue, buffer, cbData);
				}

				if (result == ERROR_SUCCESS)
				{
					if ((host_byte_order() == byte_order_big) ^ (dwType == REG_DWORD_BIG_ENDIAN))
						dwValue = byte_swap(dwValue);

					return dwValue;
				}

				break;
			}

			//
			//
			//

			case REG_LINK:
			{
				DBG_NOT_IMPLEMENTED();

				return variant_error();
			}

			//
			//
			//

			case REG_MULTI_SZ:
			{
				if (cbData == 0)
					return string_array_t();

				string_t temp;

				_ASSERT(cbData > 0);

				char *psz = alloc_ansi_buffer(temp, cbData - 1);

				if (psz != NULL)
				{
					if (result == ERROR_MORE_DATA)
					{
						result = RegQueryValueEx(hkeyResult, pszValueName, NULL,
							&dwType, (LPBYTE)psz, &cbData);
					}
					else
					{
						memcpy(psz, buffer, cbData);
					}

					if (result == ERROR_SUCCESS)
					{
						_ASSERT(psz[cbData - 1] == 0);
						_ASSERT(cbData - 1 == temp.length());

						return multisz_to_string_array(temp);
					}
					else
					{
						return variant_system_error(result);
					}
				}

				return temp;
			}

			//
			//
			//

			case REG_RESOURCE_LIST:
			{
				DBG_NOT_IMPLEMENTED();

				return variant_error();
			}

			//
			//
			//

			case REG_FULL_RESOURCE_DESCRIPTOR:
			{
				DBG_NOT_IMPLEMENTED();

				return variant_error();
			}

			//
			//
			//

			case REG_RESOURCE_REQUIREMENTS_LIST:
			{
				DBG_NOT_IMPLEMENTED();

				return variant_error();
			}

			//
			//
			//

#ifdef REG_QWORD_LITTLE_ENDIAN
#ifdef REG_QWORD_BIG_ENDIAN
			case REG_QWORD_LITTLE_ENDIAN:
			case REG_QWORD_BIG_ENDIAN:
			{
				DBG_NOT_IMPLEMENTED();

				return variant_error();
			}
#endif
#endif

			default:
			{
				DBG_INVALID_CASE();

				return variant_error();
			}
		}
	}

	return variant_system_error(result);
}

//
//
//

static variant_t multisz_to_string_array(string_t data)
{
	if (is_error(data))
		return variant_error(data);

	string_array_t array;

	LPCTSTR ptr_end = ((LPCTSTR)data) + data.length();
	LPCTSTR ptr = data;
	LPCTSTR start;
	ptrdiff_t len;

	while (next_multisz(ptr, ptr_end, start, len))
		array += string(start, len);

	return array;
}

//
//
//

// XXX: similar function in multi-select file dialog

static bool next_multisz(LPCTSTR &ptr, LPCTSTR ptr_end, LPCTSTR &start, ptrdiff_t &len)
{
	start = NULL;
	len = 0;

	if (ptr != NULL)
	{
		if (*ptr != 0)
		{
			start = ptr;

			while (ptr < ptr_end)
			{
				if (*ptr == 0)
				{
					len = ptr_diff(start, ptr);
					ptr++;
					return true;
				}

				ptr++;
			}

			_ASSERT(start == NULL);
		}
	}

	return false;
}

//
//
//

static LONG WriteRegistryInternal__(HKEY hkey, LPCTSTR pszSubKey,
	LPCTSTR pszValueName, DWORD dwType, CONST VOID *lpData, DWORD cbData)
{
	_ASSERT(hkey != NULL);
#ifndef UNICODE
	_ASSERT(pszSubKey != NULL && strlen(pszSubKey) > 0);
//	_ASSERT(pszValueName == NULL || strlen(pszValueName) > 0);
#endif
	_ASSERT(lpData != NULL);
//	_ASSERT(cbData > 0);

	// XXX: if win95, then dwType must be REG_SZ when pszValueName is NULL or ""

#ifdef _DEBUG

	if (!ART_IsPlatformNT())
	{
		if (pszValueName == NULL || pszValueName[0] == 0)
		{
			_ASSERT(dwType == REG_SZ);
		}
	}

#endif

	//
	//  Open or create registry key
	//

	LONG lReg;
	HKEY hkeyResult;

	lReg = RegCreateKeyEx(hkey, pszSubKey, 0, NULL,
		REG_OPTION_NON_VOLATILE, KEY_WRITE, NULL,
		&hkeyResult, NULL);

	if (lReg != ERROR_SUCCESS)
		return E_FAIL; // XXX: TODO: return a more meaningful error code (based on Win32 API)

	//
	//
	//

	HRESULT hr = RegSetValueEx(hkeyResult, pszValueName, 0, dwType,
		(LPBYTE)lpData, cbData);

	//
	//
	//

	VERIFY(RegCloseKey(hkeyResult) == ERROR_SUCCESS);

	return hr;
}

//
//
//

bool registry_write(string_t name, variant_t value)
{
	//
	//
	//

	if (is_error(name))
		return false;

	if (is_error(value))
		return false;

	//
	//
	//

	LONG result = registry_write_internal(name, value);

	//
	//
	//

#ifdef _DEBUG
	if (query_trace(result))
	{
		string_t strValue = value.string();
		string_t strResult = FormatSystemMessage(result);

		TRACE("REG: registry_write_internal(\"%s\", \"%s\") = %s\n",
			name, get_trace_string(strValue), get_trace_string(strResult));
	}
#endif

	return (result == ERROR_SUCCESS);
}

//
//
//

static LONG registry_write_internal(LPCTSTR name, const variant_t &value)
{
	name_info_t info;

	parse_name(name, info);

	_ASSERT(info.hkey != NULL);

	// remote registry not supported

	HKEY hkeyMachine = NULL;

	if (info.lpMachineName != "")
	{
		DBG_NOT_IMPLEMENTED();

		return false;
	}

	if (info.hkey == NULL)
		return false;

	if (info.pszSubKey == "")
		return false;

	DWORD l;
	string_t temp;

	DWORD dwType = REG_NONE;
	CONST VOID *lpData = NULL;
	size_t cbData = 0;

	if (variant_type__(value) == type_code_string)
	{
		temp = value.string();

		dwType = REG_SZ;
		lpData = (LPCTSTR)temp;
		cbData = temp.length() + 1; // according to docs, size ** IN BYTES INCLUDING NULL TERMINATOR **
	}
	else if (variant_type__(value) == type_code_psz)
	{
		dwType = REG_SZ;
		lpData = value.m_data.text.value.text_psz;
		cbData = strlen(value.m_data.text.value.text_psz) + 1; // according to docs, size ** IN BYTES INCLUDING NULL TERMINATOR **
	}
	else if (variant_evaluate(value, l) > 0)
	{
		dwType = REG_DWORD;
		lpData = &l;
		cbData = sizeof(l);
	}
	else
	{
		DBG_INVALID_CASE();

		return E_FAIL;
	}

	LONG result = WriteRegistryInternal__(info.hkey, info.pszSubKey,
		info.pszValueName, dwType, lpData, cbData);

	//
	//
	//

	if (hkeyMachine != NULL)
		VERIFY(RegCloseKey(hkeyMachine) == ERROR_SUCCESS);

	return result;

#if 0
	LPCTSTR lpMachineName = NULL;
	HKEY    hkey          = NULL;
	LPCTSTR pszSubKey     = NULL;
	LPCTSTR pszValueName  = NULL;

	_ASSERT(hkey != NULL);
	_ASSERT(pszSubKey != NULL && strlen(pszSubKey) > 0);
	_ASSERT(pszValueName == NULL || strlen(pszValueName) > 0);

	LONG result;
	HKEY hkeyResult = NULL;
	HKEY hkeyMachine = NULL;

	//
	//
	//

	if (lpMachineName != NULL)
	{
		result = RegConnectRegistry(lpMachineName, hkey, &hkeyMachine);

		if (result != ERROR_SUCCESS)
			return result;

		hkey = hkeyMachine;
	}

	//
	//  Open or create registry key
	//

	result = RegCreateKeyEx(hkey, pszSubKey, 0, NULL, REG_OPTION_NON_VOLATILE,
		KEY_WRITE, NULL, &hkeyResult, NULL);

	if (result == ERROR_SUCCESS)
	{
//		RegDisconnect();
	}
	else
	{
		return result;
	}

	//
	//
	//

	DWORD dwType;
	DWORD cbData;
	LPVOID lpData;

	result = RegSetValueEx(hkeyResult, pszValueName, 0, dwType,
		(LPBYTE)lpData, cbData);

	//
	//  Clean-up
	//

	VERIFY(RegCloseKey(hkeyResult) == ERROR_SUCCESS);
	VERIFY(RegCloseKey(hkeyMachine) == ERROR_SUCCESS);

	return result;
#endif
}

//
//
//

string_array_t registry_subkey_names(string_t name)
{
	string_array_t ans;

	regkey_t regkey;

	if (regkey.open(name))
	{
		string_t subkey;

		for (DWORD dwIndex = 0; regkey.GetSubKeyName(dwIndex, subkey); dwIndex++)
		{
			ans += subkey;
		}
	}

	return ans;
}

//
//
//

string_array_t registry_value_names(string_t name)
{
	string_array_t ans;

	regkey_t regkey;

	if (regkey.open(name))
	{
		string_t subkey;

		for (DWORD dwIndex = 0; regkey.GetValueName(dwIndex, subkey); dwIndex++)
		{
			ans += subkey;
		}
	}

	return ans;
}

//
//
//

// BUG: "HKEY_PERFORMANCE_DATA\4" yields "4" as a subkey instead of a value?!?!

// XXX: ANSI/UNICODE build dependent...

static void parse_name(LPCTSTR name, name_info_t &info)
{
	//
	//
	//

	info.lpMachineName = "";
	info.hkey          = NULL;
	info.pszSubKey     = "";
	info.pszValueName  = "";

	//
	//  Machine name
	//

	// XXX: ANSI/UNICODE build-dependent
	LPCTSTR ptr = name;
	LPCTSTR temp;
	size_t len;

	if (path_scan_unc(ptr, temp, len))
		info.lpMachineName = string(temp, len);

	// XXX: ANSI/UNICODE build-dependent
	LPCTSTR subkey_temp = strip_hive_name(ptr, info.hkey);

	if (subkey_temp != NULL)
		ptr = subkey_temp;

	LPCTSTR last_slash = _tcsrchr(ptr, '\\');

	if (last_slash != NULL)
	{
		info.pszSubKey = string(ptr, ptr_diff(ptr, last_slash));
		info.pszValueName = last_slash + 1;
	}
	else
	{
		info.pszValueName = ptr;
	}
}

//
//
//

// XXX: ANSI/UNICODE build dependent...

static LPCTSTR strip_hive_name(LPCTSTR m_path, HKEY &hKey)
{
	LPCTSTR psz = m_path;

	size_t length = lookup_key_name(m_path, hKey);

	if (length != 0)
	{
		psz += length;

		if (*psz == '\\')
			return ++psz;
		else if (*psz == 0)
			return psz;
	}

	return NULL;
}

//
//
//

// XXX: ANSI/UNICODE build dependent...

static string_t REG_read_string_internal(HKEY hkey, LPCTSTR subkey, LPCTSTR pszValueName)
{
#if 1
	//
	//
	//

	if (hkey == NULL)
	{
		LPCTSTR subkey_temp = strip_hive_name(subkey, hkey);

		if (subkey_temp != NULL)
			subkey = subkey_temp;
	}

	_ASSERT(hkey != NULL);

	//
	//  Open existing registry key
	//

	LONG lReg;
	HKEY hkeyResult;

	lReg = RegOpenKeyEx(hkey, subkey, 0, KEY_QUERY_VALUE, &hkeyResult);

	if (lReg != ERROR_SUCCESS)
		return FormatSystemMessage(lReg);

	//
	//  Read key value
	//

	string_t ans;

	DWORD dwType;
	DWORD cbData;

	HRESULT hr = RegQueryValueEx(hkeyResult, pszValueName, NULL, &dwType,
		NULL, &cbData);

	if (hr == ERROR_SUCCESS)
	{
		_ASSERT(dwType == REG_SZ);

		char *data = alloc_ansi_buffer(ans, cbData - 1);

		if (data != NULL)
		{
			hr = RegQueryValueEx(hkeyResult, pszValueName, NULL, &dwType,
				(LPBYTE)data, &cbData);

			if (hr != ERROR_SUCCESS)
				ans = FormatSystemMessage(hr);
			else
				_ASSERT(data[cbData - 1] == 0);
		}
	}
	else
	{
		ans = FormatSystemMessage(hr);
	}

	//
	//  Clean-up
	//

	VERIFY(RegCloseKey(hkeyResult) == ERROR_SUCCESS);

	return ans;
#else
	static LONG registry_read_internal(LPCTSTR name, variant_t &ans);
#endif
}

//
//
//

// XXX: startup code
static lock_t g_ini_lock;

//
//
//

static bool get_profile_string_internal(const char *filename, const char *section, const char *key, string_t &value);
static bool put_profile_string_internal(const char *filename, const char *section, const char *key, const char *value);

//
//
//

bool get_profile_string2(const char *filename, const char *section,
	const char *key, string_t &value)
{
	bool result = get_profile_string_internal(filename, section, key, value);

	// TODO: this type of diagnostic tracing should be handled by the debugger, not here
	
	if (trace_enabled(g_trace_config))
	{
		trace_severity_t severity = trace_severity_unspecified;

		if (is_error(value))
			severity = trace_severity_error;

		TRACE2(&g_trace_config, severity,
			"get_profile_string(\"%s\", \"%s\", \"%s\")=%s\n",
			__file_name(filename), section, key, get_trace_string(value));
	}

	return result;
}

//
//
//

static bool get_profile_string_internal(const char *filename, LPCSTR section, const char *key, string_t &value)
{
#if !defined(_WIN32_WCE)

	if (section == NULL)
		return false; // otherwise, all section names are enumerated

	if (key == NULL)
		return false; // otherwise, all key names are enumerated

	// XXX: hard-coded buffer size

	char local_buf[1024];

	static const char default_value[] = "___!!!___"; // a very unlikely value

	g_ini_lock.enter();

	DWORD dw = GetPrivateProfileStringA(section, key, default_value,
		local_buf, DIM(local_buf), filename);

	g_ini_lock.leave();

	_ASSERT(dw < DIM(local_buf) - 1); // XXX: we don't handle truncation here

	// NOTE: Win32 API function doesn't distinguish between empty values and
	// non-existent values, so we use the following...

	if (strcmp(local_buf, default_value) == 0)
		return false;

	value = local_buf;

	return true;

#else

	DBG_NOT_IMPLEMENTED2("get_profile_string_internal()");

	return false;

#endif
}

//
//
//

bool put_profile_string(const char *filename, const char *section, const char *key, const char *value)
{
	bool result = put_profile_string_internal(filename, section, key, value);

	if (trace_enabled(g_trace_config, trace_severity_debug))
	{
		TRACE("put_profile_string_internal(\"%s\", \"%s\", \"%s\", \"%s\")=%i\n",
			filename, section, key, value, result);
	}

	return result;
}

bool put_profile_string_if_changed(const char *filename, const char *section, const char *key, const char *value)
{
	bool result = false;

	bool write = false;

	string_t value_current;

	// NOTE: we are calling the internal version of this function to avoid the
	// TRACE output if nothing has changed

#if 0
	if (get_profile_string2(filename, section, key, value_current))
#else
	if (get_profile_string_internal(filename, section, key, value_current))
#endif
	{
		write = (value_current != value);
	}
	else
	{
		write = true;
	}

	if (write)
	{
		if (put_profile_string(filename, section, key, value))
		{
			result = true;
		}
		else
		{
		}
	}
	else
	{
		result = true;
	}

	return result;
}

// TODO: static-link???

bool put_profile_string_if_changed2(const char *filename, const char *section, const char *key, string_t value)
{
	if (is_error(value))
		return false;

	if (is_ansi(value) || is_empty(value))
		return put_profile_string_if_changed(filename, section, key, value);

	DBG_BREAK();

	return false;
}

static bool put_profile_string_internal(const char *filename, LPCSTR section, LPCSTR key, LPCSTR value)
{
#if !defined(_WIN32_WCE)

	if (key == NULL)
		return false; // otherwise, the section is deleted

	if (value == NULL)
		return false; // otherwise, the key is deleted

	// TODO: we need some way to escape/unescape these values

	_ASSERT(strchr(value, 10) == NULL);
	_ASSERT(strchr(value, 13) == NULL);

	g_ini_lock.enter();

	bool result = WritePrivateProfileStringA(section, key, value, filename);

	g_ini_lock.leave();

	return result;

#else

	DBG_NOT_IMPLEMENTED2("put_profile_string_internal");

	return false;

#endif
}

//
//
//

bool delete_profile_string(const char *filename, const char *section, const char *key)
{
#if !defined(_WIN32_WCE)

	const char *value = NULL;

	if (key == NULL)
		return false; // otherwise, the section is deleted

	g_ini_lock.enter();

	bool result = WritePrivateProfileStringA(section, key, value, filename);

	g_ini_lock.leave();

	return result;

#else

	DBG_NOT_IMPLEMENTED2("delete_profile_string");

	return false;

#endif
}

bool delete_profile_string__(string_t filename, string_t section, string_t key)
{
	if (is_error(filename))
		return false;

	if (is_error(section))
		return false;

	if (is_error(key))
		return false;

#if !defined(_WIN32_WCE)

	const char *value = NULL;

/*
	if (key == NULL)
		return false; // otherwise, the section is deleted
*/

	bool result = false;

	g_ini_lock.enter();

	// XXX: UNICODE not supported

	if (is_ansi(filename) && is_ansi(section) && is_ansi(key))
		result = WritePrivateProfileStringA(section, key, value, filename);
	else
		DBG_NOT_IMPLEMENTED();

	g_ini_lock.leave();

	return result;

#else

	DBG_NOT_IMPLEMENTED2("delete_profile_string__");

	return false;

#endif
}

//
//
//

bool delete_profile_section(const char *filename, const char *section)
{
#if !defined(_WIN32_WCE)

	const char *key = NULL;
	const char *value = NULL;

	g_ini_lock.enter();

	bool result = WritePrivateProfileStringA(section, key, value, filename);

	g_ini_lock.leave();

	return result;

#else

	DBG_NOT_IMPLEMENTED2("delete_profile_section");

	return false;

#endif

}

bool delete_profile_section__(string_t filename, string_t section)
{
	if (is_error(filename))
		return false;

	if (is_error(section))
		return false;

#if !defined(_WIN32_WCE)

	const char *key = NULL;
	const char *value = NULL;

	bool result = false;

	g_ini_lock.enter();

	// XXX: UNICODE not supported

	if (is_ansi(filename) && is_ansi(section) && is_ansi(key))
		result = WritePrivateProfileStringA(section, key, value, filename);
	else
		DBG_NOT_IMPLEMENTED();

	g_ini_lock.leave();

	return result;

#else

	DBG_NOT_IMPLEMENTED2("delete_profile_section__");

	return false;

#endif

}

//
//
//

variant_t get_profile_section_names(string_t filename)
{
	DBG_NOT_IMPLEMENTED();

	return variant_error();
}

/*

	Win32 API Functions

	UINT WINAPI GetProfileIntA( LPCSTR lpAppName, LPCSTR lpKeyName, INT nDefault );
	UINT WINAPI GetProfileIntW( LPCWSTR lpAppName, LPCWSTR lpKeyName, INT nDefault );
	DWORD WINAPI GetProfileStringA( LPCSTR lpAppName, LPCSTR lpKeyName, LPCSTR lpDefault, LPSTR lpReturnedString, DWORD nSize );
	DWORD WINAPI GetProfileStringW( LPCWSTR lpAppName, LPCWSTR lpKeyName, LPCWSTR lpDefault, LPWSTR lpReturnedString, DWORD nSize );
	BOOL WINAPI WriteProfileStringA( LPCSTR lpAppName, LPCSTR lpKeyName, LPCSTR lpString );
	BOOL WINAPI WriteProfileStringW( LPCWSTR lpAppName, LPCWSTR lpKeyName, LPCWSTR lpString );
	DWORD WINAPI GetProfileSectionA( LPCSTR lpAppName, LPSTR lpReturnedString, DWORD nSize );
	DWORD WINAPI GetProfileSectionW( LPCWSTR lpAppName, LPWSTR lpReturnedString, DWORD nSize );
	BOOL WINAPI WriteProfileSectionA( LPCSTR lpAppName, LPCSTR lpString );
	BOOL WINAPI WriteProfileSectionW( LPCWSTR lpAppName, LPCWSTR lpString );
	UINT WINAPI GetPrivateProfileIntA( LPCSTR lpAppName, LPCSTR lpKeyName, INT nDefault, LPCSTR lpFileName );
	UINT WINAPI GetPrivateProfileIntW( LPCWSTR lpAppName, LPCWSTR lpKeyName, INT nDefault, LPCWSTR lpFileName );
	DWORD WINAPI GetPrivateProfileStringA( LPCSTR lpAppName, LPCSTR lpKeyName, LPCSTR lpDefault, LPSTR lpReturnedString, DWORD nSize, LPCSTR lpFileName );
	DWORD WINAPI GetPrivateProfileStringW( LPCWSTR lpAppName, LPCWSTR lpKeyName, LPCWSTR lpDefault, LPWSTR lpReturnedString, DWORD nSize, LPCWSTR lpFileName );
	BOOL WINAPI WritePrivateProfileStringA( LPCSTR lpAppName, LPCSTR lpKeyName, LPCSTR lpString, LPCSTR lpFileName );
	BOOL WINAPI WritePrivateProfileStringW( LPCWSTR lpAppName, LPCWSTR lpKeyName, LPCWSTR lpString, LPCWSTR lpFileName );
	DWORD WINAPI GetPrivateProfileSectionA( LPCSTR lpAppName, LPSTR lpReturnedString, DWORD nSize, LPCSTR lpFileName );
	DWORD WINAPI GetPrivateProfileSectionW( LPCWSTR lpAppName, LPWSTR lpReturnedString, DWORD nSize, LPCWSTR lpFileName );
	BOOL WINAPI WritePrivateProfileSectionA( LPCSTR lpAppName, LPCSTR lpString, LPCSTR lpFileName );
	BOOL WINAPI WritePrivateProfileSectionW( LPCWSTR lpAppName, LPCWSTR lpString, LPCWSTR lpFileName );
	DWORD WINAPI GetPrivateProfileSectionNamesA( LPSTR lpszReturnBuffer, DWORD nSize, LPCSTR lpFileName );
	DWORD WINAPI GetPrivateProfileSectionNamesW( LPWSTR lpszReturnBuffer, DWORD nSize, LPCWSTR lpFileName );
	BOOL WINAPI GetPrivateProfileStructA( LPCSTR lpszSection, LPCSTR lpszKey, LPVOID   lpStruct, UINT     uSizeStruct, LPCSTR szFile );
	BOOL WINAPI GetPrivateProfileStructW( LPCWSTR lpszSection, LPCWSTR lpszKey, LPVOID   lpStruct, UINT     uSizeStruct, LPCWSTR szFile );
	BOOL WINAPI WritePrivateProfileStructA( LPCSTR lpszSection, LPCSTR lpszKey, LPVOID   lpStruct, UINT     uSizeStruct, LPCSTR szFile );
	BOOL WINAPI WritePrivateProfileStructW( LPCWSTR lpszSection, LPCWSTR lpszKey, LPVOID   lpStruct, UINT     uSizeStruct, LPCWSTR szFile );

	from MSDN...

	The Win32 profile functions (Get/WriteProfile*, Get/WritePrivateProfile*)
	use the following steps to locate initialization information:

	Look in the registry for the name of the initialization file, say
	MYFILE.INI, under IniFileMapping:

	HKEY_LOCAL_MACHINE\Software\Microsoft\Windows NT\CurrentVersion\IniFileMapping\myfile.ini

	Look for the section name specified by lpAppName. This will be a named
	value under myfile.ini, or a subkey of myfile.ini, or will not exist.

	If the section name specified by lpAppName is a named value under
	myfile.ini, then that value specifies where in the registry you will find
	the keys for the section.

	If the section name specified by lpAppName is a subkey of myfile.ini, then
	named values under that subkey specify where in the registry you will find
	the keys for the section. If the key you are looking for does not exist as
	a named value, then there will be an unnamed value (shown as <No Name>)
	that specifies the default location in the registry where you will find
	the key.

	If the section name specified by lpAppName does not exist as a named value
	or as a subkey under myfile.ini, then there will be an unnamed value
	(shown as <No Name>) under myfile.ini that specifies the default location
	in the registry where you will find the keys for the section.

	If there is no subkey for MYFILE.INI, or if there is no entry for the
	section name, then look for the actual MYFILE.INI on the disk and read its
	contents.

	When looking at values in the registry that specify other registry
	locations, there are several prefixes that change the behavior of the .INI
	file mapping:

	! - this character forces all writes to go both to the registry and to the .INI file on disk.

	# - this character causes the registry value to be set to the value in the Windows 3.1 .INI file when a new user logs in for the first time after setup.

	@ - this character prevents any reads from going to the .INI file on disk if the requested data is not found in the registry.

	USR: - this prefix stands for HKEY_CURRENT_USER, and the text after the prefix is relative to that key.

	SYS: - this prefix stands for HKEY_LOCAL_MACHINE\SOFTWARE, and the text after the prefix is relative to that key.

	[HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\IniFileMapping]

	[HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\IniFileMapping\Clock.ini]
	@="#USR:Software\\Microsoft\\Clock"

	[HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\IniFileMapping\KeyboardLayout.ini\Keyboard Layout]
	@="\\Registry\\Machine\\System\\CurrentControlSet\\Control\\Keyboard Layout"
	"Active"="USR:Keyboard Layout"

*/

#endif

string_t get_profile_string(const char *filename, const char *section, const char *key)
{
	string_t value;

	if (get_profile_string2(filename, section, key, value))
		return value;

	return string_error();
}

variant_t get_profile_value(const char *filename, const char *section, const char *key)
{
	variant_t value;

	if (get_profile_value(filename, section, key, value))
		return value;

	return variant_error();
}

bool get_profile_value(const char *filename, const char *section, const char *key, variant_t &value)
{
	string_t string;

	if (!get_profile_string2(filename, section, key, string))
		return false;

	value = string;

	return true;
}

bool put_profile_value(const char *filename, const char *section, const char *key, variant_t value)
{
	string_t str_value;

	if (variant_evaluate(value, str_value) != varconv_exact)
		return false;

	_ASSERT(!is_error(str_value));

	return put_profile_string(filename, section, key, str_value);
}

bool put_profile_value_if_changed(const char *filename, const char *section, const char *key, variant_t value)
{
	string_t str_value;

	if (variant_evaluate(value, str_value) != varconv_exact)
		return false;

	_ASSERT(!is_error(str_value));

	return put_profile_string_if_changed(filename, section, key, str_value);
}

#ifndef REMOVE_EXPERIMENTAL

int profile_evaluate(const char *filename, const char *section, const char *key, data_binding_t value)
{
#if 0
	string_t string;

	if (!get_profile_string2(filename, section, key, string))
		return false;

	value = string;

	return true;
#endif

	DBG_NOT_IMPLEMENTED();

	return 0;
}

int profile_write(const char *filename, const char *section, const char *key, data_binding_t value)
{
#if 0
	string_t str_value;

	if (variant_evaluate(value, str_value) != varconv_exact)
		return false;

	_ASSERT(!is_error(str_value));

	return put_profile_string(filename, section, key, str_value);
#endif

	DBG_NOT_IMPLEMENTED();

	return 0;
}

#endif

//
//
//

#ifndef LITE_RTL

bool ini_exchange2_t::exchange()
{
	_ASSERT(m_context != NULL);
	_ASSERT(m_context->status() != -1);

	const char *section = m_binding.section;

	if (m_context->m_section_override != NULL)
	{
		_ASSERT(section == NULL);

		section = m_context->m_section_override;
	}

	if (mode() == dbdx_mode_load)
	{
		// transfer the value from the application to the .ini file...

		string_t text;

		if (variant_evaluate(m_value, text) > 0)
		{
			// TODO: do processing from ini_set_value here!!!

			if (m_binding.flags & 0x01)
			{
				text = ini_escape_text(text);
			}

			if (put_profile_string_if_changed(m_context->m_filename, section, m_binding.key, text))
			{
				return true;
			}
		}
		else
		{
			variant_t err = "Unable to convert data to text format needed .ini data exchange";

			set_error(&err);

			return false;
		}
	}
	else if (mode() == dbdx_mode_validate)
	{
	}
	else if (mode() == dbdx_mode_save)
	{
		// transfer the value from the .ini file to the application...

		string_t text;

		if (get_profile_string2(m_context->m_filename, section, m_binding.key, text))
		{
			if (m_binding.flags & 0x01)
			{
				text = ini_unescape_text(text);
			}

/*
			variant_t value = text;

			if (__convert_from_variant(value))
			{
				return true;
			}
			else
			{
				dbdx_log_error(*this, 0, "");

				DBG_BREAK();
			}
*/

			m_value = text;

			return true;
		}
		else
		{
			// not specified...  leave unmodified

			return true;
		}
	}

	set_error(NULL);

	return false;
}

#endif

