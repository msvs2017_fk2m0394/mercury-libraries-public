/*------------------------------------------------------------------------------

	memory.cpp - Windows memory support
	
	- address space queries
	- HGLOBAL support

	TODO: VLM address query support

------------------------------------------------------------------------------*/

#include "pch.h"
#include "core.h"

#include "../../lib/core/memory.h"

#ifdef _DEBUG
static void debug_validate(const MEMORY_BASIC_INFORMATION &mbi);
#endif

//
//
//

#ifndef LITE_RTL

#define R_BITS (PAGE_READONLY | PAGE_READWRITE | PAGE_WRITECOPY | PAGE_EXECUTE_READ | PAGE_EXECUTE_READWRITE | PAGE_EXECUTE_WRITECOPY)
#define W_BITS (                PAGE_READWRITE | PAGE_WRITECOPY |                     PAGE_EXECUTE_READWRITE | PAGE_EXECUTE_WRITECOPY)
#define X_BITS (PAGE_EXECUTE |                                    PAGE_EXECUTE_READ | PAGE_EXECUTE_READWRITE | PAGE_EXECUTE_WRITECOPY)

static const mm_info_t::type_t g_mm_type_image = { "image" };
static const mm_info_t::type_t g_mm_type_section = { "section" };

//
//
//

mm_info_t mm_query_region__(const void *address)
{
	//
	//
	//

	MEMORY_BASIC_INFORMATION mbi = { NULL };

	size_t len = VirtualQuery((void*)address, &mbi, sizeof(mbi));

	if (len != 0)
	{
		_ASSERT(len == sizeof(mbi));

		return make_mm_info(mbi, 1);
	}

	//
	//  invalid address - return dummy descriptor
	//
	
	mm_info_t info = { 0 };

	return info;
}

mm_info_t mm_query_allocation__(const void *address)
{
	//
	//
	//

	MEMORY_BASIC_INFORMATION mbi = { NULL };

	size_t len = VirtualQuery((void*)address, &mbi, sizeof(mbi));

	if (len != 0)
	{
		_ASSERT(len == sizeof(mbi));

		return make_mm_info(mbi, 2);
	}

	//
	//  invalid address - return dummy descriptor
	//
	
	mm_info_t info = { 0 };

	return info;
}

//
//
//

mm_info_t make_mm_info(const MEMORY_BASIC_INFORMATION &mbi, int alloc)
{
	mm_info_t info = { 0 };

#ifdef _DEBUG

	SYSTEM_INFO si = { 0 };

	GetSystemInfo(&si);

	debug_validate(mbi);

#endif

#ifndef REMOVE_THIS
	info.State   = mbi.State;
	info.Protect = mbi.Protect;
	info.Type    = mbi.Type;
#endif

	switch (mbi.State)
	{
		case MEM_FREE:
		{
			// NOTE: AllocationBase, AllocationProtect, Protect, and Type
			// members are undefined

			break;
		}

		case MEM_RESERVE:
		{
			// NOTE: Protect is undefined 

			break;
		}

		case MEM_COMMIT:
		{
			break;
		}

		default:
			DBG_INVALID_CASE();
			break;
	}

	u32 Protect = (alloc == 2) ? mbi.AllocationProtect : mbi.Protect;

	if (mbi.State & MEM_RESERVE)
	{
		// NOTE: Protect is undefined 
	}
	else
	{
		if (Protect == 0)
			Protect = mbi.AllocationProtect; // e.g. stack
	}

	info.base = (alloc == 2) ? (u64)mbi.AllocationBase : (u64)mbi.BaseAddress;
	info.size = mbi.RegionSize;

	info.access.r = ((Protect & R_BITS) != 0);
	info.access.w = ((Protect & W_BITS) != 0);
	info.access.x = ((Protect & X_BITS) != 0);
	
	if (mbi.Type & MEM_IMAGE)
		info.type = &g_mm_type_image;
	else if (mbi.Type & MEM_MAPPED)
		info.type = &g_mm_type_section;

	return info;
}

#ifdef _DEBUG

static void debug_validate(const MEMORY_BASIC_INFORMATION &mbi)
{
	//
	//  Validate some assumptions about the address space range
	//
	
	SYSTEM_INFO si;

	GetSystemInfo(&si);

	// verify correctness of typecasts below...
	C_ASSERT(sizeof(mbi.BaseAddress) == sizeof(uintptr_t));
	C_ASSERT(sizeof(mbi.AllocationBase) == sizeof(uintptr_t));

	_ASSERT(((uintptr_t)mbi.BaseAddress % si.dwPageSize) == 0);
	_ASSERT(((uintptr_t)mbi.AllocationBase % si.dwPageSize) == 0);
//	_ASSERT(((uintptr_t)mbi.AllocationBase % si.dwAllocationGranularity) == 0); // NOTE: this assertion fails for TEB and PEB blocks
	_ASSERT((mbi.RegionSize % si.dwPageSize) == 0);

	switch (mbi.State)
	{
		case MEM_FREE:
		{
			// NOTE: AllocationBase, AllocationProtect, Protect, and Type
			// members are undefined

			break;
		}

		case MEM_RESERVE:
		{
			_ASSERT(mbi.Type == MEM_IMAGE ||
				mbi.Type == MEM_MAPPED ||
				mbi.Type == MEM_PRIVATE);

			// NOTE: Protect is undefined 

			break;
		}

		case MEM_COMMIT:
		{
			_ASSERT(mbi.Type == MEM_IMAGE ||
				mbi.Type == MEM_MAPPED ||
				mbi.Type == MEM_PRIVATE);

			switch (mbi.Protect & ~(PAGE_GUARD | PAGE_NOCACHE | PAGE_WRITECOMBINE))
			{
				case PAGE_NOACCESS:
				case PAGE_READONLY:
				case PAGE_READWRITE:
				case PAGE_WRITECOPY:
				case PAGE_EXECUTE:
				case PAGE_EXECUTE_READ:
				case PAGE_EXECUTE_READWRITE:
				case PAGE_EXECUTE_WRITECOPY:
					break;

				default:
					DBG_INVALID_CASE();
					break;
			}

			break;
		}

		default:
		{
			DBG_INVALID_CASE();
			break;
		}
	}

	if (mbi.Type == MEM_IMAGE)
	{
		_ASSERT(mbi.State == MEM_RESERVE || mbi.State == MEM_COMMIT);
	}
}

#endif

/*----------------------------------------------------------------------------

	routines for manipulating HGLOBAL memory

	HGLOBAL memory objects are movable memory that's shared across process
	boundaries.  The actual bytes are accessed through a lock/unlock 
	mechanism.  This mechanism has its origins in 16-bit Windows.

	HGLOBAL memory has very limited use in Win32 applications.  It is needed
	for USER32 clipboard operations (i.e. OLE2 clipboard hides this from you)
	and for DDE operations.  DDE operations are still used to communicate
	with the shell, so it's not dead yet.

----------------------------------------------------------------------------*/

static LPVOID MyGlobalLock(HGLOBAL hGlobal);
static void MyGlobalUnlock(HGLOBAL hGlobal);
static HGLOBAL MyGlobalReAlloc(HGLOBAL hMem, DWORD dwBytes, UINT uFlags);
static HGLOBAL MyGlobalAlloc(UINT uFlags, DWORD dwBytes);

//
//
//

global_mem_t::global_mem_t()
{
	m_nAllocFlags = 0;
	m_hGlobal     = NULL;
	m_lpVoid      = NULL;
	m_bAllowGrow  = FALSE;
}

//
//
//

global_mem_t::~global_mem_t()
{
	Free();
}

//
//
//

LPBYTE global_mem_t::Lock()
{
	if (m_hGlobal != NULL)
	{
		LPBYTE data = (LPBYTE)MyGlobalLock(m_hGlobal);

		if (data == NULL)
		{
		}

		_ASSERT(m_lpVoid == NULL || data == m_lpVoid);

		m_lpVoid = data;

		return data;
	}
	else
	{
		return NULL;
	}
}

//
//
//

void global_mem_t::Unlock()
{
	_ASSERT(m_lpVoid != NULL);

	if (m_lpVoid != NULL)
	{
		if (m_hGlobal != NULL)
			MyGlobalUnlock(m_hGlobal);

		m_lpVoid = NULL;
	}
}

//
//
//

bool global_mem_t::Realloc(UINT uFlags, DWORD dwBytes)
{
	if (m_hGlobal != NULL)
	{
		_ASSERT(m_lpVoid == NULL);
		
		HGLOBAL hGlobal = MyGlobalReAlloc(m_hGlobal, dwBytes, uFlags);

		if (hGlobal != NULL)
		{
			m_hGlobal = hGlobal;

			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
}

//
//
//

HGLOBAL global_mem_t::Detach()
{
	_ASSERT(m_hGlobal != NULL);

	if (m_lpVoid != NULL)
		Unlock();

#ifdef _DEBUG
	TRACE("MEM: Detach(%08X)\n", m_hGlobal);
#endif

	HGLOBAL hMem = m_hGlobal;

	m_hGlobal = NULL; // detach from global handle
	m_lpVoid  = NULL;

	return hMem;
}

//
//
//

void global_mem_t::Free()
{
	if (m_hGlobal != NULL)
	{
		if (m_lpVoid != NULL)
		{
			MyGlobalUnlock(m_hGlobal);

			m_lpVoid = NULL;
		}

		if (MyGlobalFree(m_hGlobal))
		{
			m_hGlobal = NULL;
		}

/*
		// XXX: shouldn't this be here???

		m_nAllocFlags = 0;
		m_bAllowGrow  = FALSE;
*/

	}
	else
	{
		_ASSERT(m_lpVoid == NULL);
	}
}

//
//
//

LPBYTE global_mem_t::SetHandleAndLock(HGLOBAL hGlobal, BOOL bAllowGrow)
{
	Free();

	m_hGlobal = hGlobal;

	m_bAllowGrow = bAllowGrow;

	return Lock();
}

//
//
//

unsigned char* global_mem_t::AllocAndLock(unsigned long nBytes)
{
	_ASSERT(m_hGlobal == NULL); // do once only
	
	Free();

	m_hGlobal = MyGlobalAlloc(m_nAllocFlags, nBytes);
	
	if (m_hGlobal == NULL)
		return NULL;
	
	return Lock();
}

//
//
//

LPBYTE global_mem_t::ReallocAndLock(unsigned long nBytes)
{
	if (m_bAllowGrow)
	{
		_ASSERT(m_hGlobal != NULL);

		if (m_hGlobal == NULL)
			return AllocAndLock(nBytes);

		Unlock();

		if (::GlobalSize(m_hGlobal) == nBytes)
			return Lock();

		HGLOBAL hNew = MyGlobalReAlloc(m_hGlobal, nBytes, m_nAllocFlags);

		if (hNew != NULL)
		{
			m_hGlobal = hNew;

			return Lock();
		}
	}

	return NULL;
}

//
//
//

static HGLOBAL MyGlobalAlloc(UINT uFlags, DWORD dwBytes)
{
	HGLOBAL hGlobal = GlobalAlloc(uFlags, dwBytes);

#ifdef _DEBUG
	TRACE("MEM: GlobalAlloc(uFlags=%08X, dwBytes=%08X)=%08X\n",
		uFlags, dwBytes, hGlobal);
#endif

	return hGlobal;
}

//
//
//

static HGLOBAL MyGlobalReAlloc(HGLOBAL hMem, DWORD dwBytes, UINT uFlags)
{
	HGLOBAL hMem2 = GlobalReAlloc(hMem, dwBytes, uFlags);

#ifdef _DEBUG
	TRACE("MEM: GlobalReAlloc(hMem=%08X, dwBytes=%08X, uFlags=%08X)=%08X\n",
		hMem, dwBytes, uFlags, hMem2);
#endif
	
	return hMem2;
}

//
//
//

// TODO: use our debugger to do this type of tracing at a more generalized level

static LPVOID MyGlobalLock(HGLOBAL hGlobal)
{
	LPVOID data = GlobalLock(hGlobal);

#ifdef _DEBUG
#ifndef _WIN32_WCE
	TRACE("MEM: GlobalLock(%08X)=%08X, %i lock(s)\n",
		hGlobal, data, GlobalFlags(hGlobal) & GMEM_LOCKCOUNT);
#endif
#endif

	return data;
}

//
//
//

// TODO: use our debugger to do this type of tracing at a more generalized level

static void MyGlobalUnlock(HGLOBAL hGlobal)
{
#ifdef _DEBUG
	// stupid Win95...
	SetLastError(0);
#endif
	
	BOOL f = GlobalUnlock(hGlobal);
	
#ifdef _DEBUG
#ifndef _WIN32_WCE
	TRACE("MEM: GlobalUnlock(%08X)=%08X, %i lock(s) remaining\n",
		hGlobal, f, GlobalFlags(hGlobal) & GMEM_LOCKCOUNT);
#endif
#endif

	_ASSERT(f || GetLastError() == NO_ERROR);
}

//
//
//

bool MyGlobalFree(HGLOBAL hGlobal)
{
	HGLOBAL temp = GlobalFree(hGlobal);

#ifdef _DEBUG
	TRACE("MEM: GlobalFree(%08X)=%08X\n", hGlobal, temp);
#endif

	_ASSERT(temp == NULL);
	
	return (temp == NULL);
}

//
//
//

HGLOBAL MyGlobalDuplicate(HGLOBAL hSrc)
{
	HGLOBAL hDest = NULL;
	
	const void *src = MyGlobalLock(hSrc);
	
	if (src != NULL)
	{
		hDest = MyGlobalDuplicate(src, GlobalSize(hSrc), GlobalFlags(hSrc));

		MyGlobalUnlock(hDest);
	}

	return hDest;
}

//
//
//

HGLOBAL MyGlobalDuplicate(const void *src, size_t size, unsigned long flags)
{
	HGLOBAL hDest = MyGlobalAlloc(flags, size);

	if (hDest != NULL)
	{
		void *dest = MyGlobalLock(hDest);

		if (dest != NULL)
		{
			memcpy(dest, src, size);

			MyGlobalUnlock(hDest);
		}
		else
		{
			MyGlobalFree(hDest);
		}
	}

	return hDest;
}

//
//
//

#ifndef REMOVE_EXPERIMENTAL

static void *mem_win_heap_alloc(const mm2_t *ctx, void *old, size_t size, int clear)
{
	HANDLE hHeap = GetProcessHeap();

#ifdef _DEBUG

	if (heap_validate())
		HeapValidate(hHeap, 0, NULL);

#endif

	if (old == NULL)
	{
		if (clear != 0)
		{
			return HeapAlloc(hHeap, HEAP_ZERO_MEMORY, size);
		}
		else
		{
			return HeapAlloc(hHeap, 0, size);
		}
	}
	else
	{
		_ASSERT(clear == 0);
		
		return HeapReAlloc(hHeap, 0, old, size);
	}
}

static void mem_win_heap_free(const mm2_t *ctx, void *ptr)
{
	HANDLE hHeap = GetProcessHeap();

#ifdef _DEBUG

	if (heap_validate())
		HeapValidate(hHeap, 0, NULL);

#endif

	if (HeapFree(hHeap, 0, ptr))
	{
	}
	else
	{
		DBG_BREAK();

		DWORD dwError = GetLastError();
	}
}

const mm2_t g_mm_win_heap =
{
	mem_win_heap_alloc,
	mem_win_heap_free,
	NULL,
	NULL
};

#endif

#endif

#ifndef REMOVE_THIS

mm_info_t mm_query(const void *address)
{
	return mm_query_region__(address);
}

#endif

mm_info_t mm_query_region(const void *address)
{
	return mm_query_region__(address);
}

mm_info_t mm_query_allocation(const void *address)
{
	return mm_query_allocation__(address);
}

