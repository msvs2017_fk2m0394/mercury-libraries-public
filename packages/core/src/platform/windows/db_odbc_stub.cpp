/*------------------------------------------------------------------------------

	db_odbc_stub.cpp

------------------------------------------------------------------------------*/

#ifdef _WINDOWS
#define STRICT
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#endif

#include <mercury/core.h>
#include <mercury/odbc.h>

//
//
//

struct ext_procs_t
{
	db_connection_t (ART_API_CALLTYPE *db_odbc_connect)(const char *odbc_conn);
	db_cursor_t     (ART_API_CALLTYPE *db_odbc_sources)();
	db_cursor_t     (ART_API_CALLTYPE *db_odbc_drivers)();
};

//
//
//

static ext_procs_t g_ext_procs;

static export_module_t g_module_ext_db_odbc = { "ext_db_odbc.dll"  };

//
//
//

db_connection_t db_odbc_connect(const char *odbc_conn)
{
	static export_info_t export__ = { &g_module_ext_db_odbc, "db_odbc_connect", &g_ext_procs.db_odbc_connect };

	get_addr_ext(export__);

	if (g_ext_procs.db_odbc_connect != NULL)
		return g_ext_procs.db_odbc_connect(odbc_conn);

	string_t message = error_message(export__);
		
	return db_connection_error(message);
}

db_cursor_t db_odbc_sources()
{
	static export_info_t export__ = { &g_module_ext_db_odbc, "db_odbc_sources", &g_ext_procs.db_odbc_sources };

	get_addr_ext(export__);

	if (g_ext_procs.db_odbc_sources != NULL)
		return g_ext_procs.db_odbc_sources();

	string_t message = error_message(export__);
		
	return db_cursor_error(message);
}

db_cursor_t db_odbc_drivers()
{
	static export_info_t export__ = { &g_module_ext_db_odbc, "db_odbc_drivers", &g_ext_procs.db_odbc_drivers };

	get_addr_ext(export__);

	if (g_ext_procs.db_odbc_drivers != NULL)
		return g_ext_procs.db_odbc_drivers();

	string_t message = error_message(export__);
		
	return db_cursor_error(message);
}

