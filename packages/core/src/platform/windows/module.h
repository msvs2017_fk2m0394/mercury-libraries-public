/*------------------------------------------------------------------------------

	platform/windows/module.h

	XREF: "./pe.h"

------------------------------------------------------------------------------*/

#include <mercury/experimental/var.h>

#include "../../lib/core/module.h"

//
//
//

struct windows_module_t : module_object_t
{
	windows_module_t();

	// *** module_object_t methods ***
	virtual void close();

	void resolve_filename();

	string_t m_filename; // used for "lazy" module loading
	HMODULE  m_hModule;  // Win32 module handle

	OBJECT_CLASS_DECLARE();
};

#ifndef REMOVE_THIS

// static-link state for each executable module using Mercury libraries...

struct rtl_module_state_t : base_rtl_module_state_t
{
	rtl_module_state_t();

	global_thing_t<windows_module_t> module____;
};

#endif

module_t module_from_handle_internal(HMODULE hModule, windows_module_t *data);
string_t module_filename__(HMODULE hModule);

extern "C" {

HMODULE      hmodule_from_address(const void *addr);
const void * hmodule_base_address(HMODULE hModule);

bool    module_info(const module_object_t *module, const void *&base, string_t &filename, HMODULE &hModule);
HMODULE module_handle__(module_public_t *p);

void init_module_info(HMODULE hModule, windows_module_t &info);

}

#ifndef REMOVE_THIS

#ifdef _DEBUG
void debug_validate(HMODULE hModule);
#else
inline void debug_validate(HMODULE) { }
#endif

#endif

string_t short_module_name(module_public_t *module);

