/*------------------------------------------------------------------------------

	db_odbc_connection.cpp - ODBC connection

	TODO: parameter binding

------------------------------------------------------------------------------*/

#include "pch.h"
#include "db_odbc.h"

//
//
//

static const db_connection_dispatch_t g_db_connection_dispatch = DB_CONNECTION_DISPATCH_INITIALIZER
(
	&odbc_connection_t::cancel,
	&odbc_connection_t::close,
	&odbc_connection_t::status,
	&odbc_connection_t::attributes,
	&odbc_connection_t::catalog,
	&odbc_connection_t::execute
);

OBJECT_CLASS_BEGIN(odbc_connection_t, base_db_connection_t)
OBJECT_CLASS_END()

//
//
//

static const struct { const char *name; SQLINTEGER Attribute; type_t type;} g_odbc_conn_attr[] = 
{
	{ "SQL_ATTR_ACCESS_MODE",        SQL_ATTR_ACCESS_MODE,        type_code_u32    },
	{ "SQL_ATTR_ASYNC_ENABLE",       SQL_ATTR_ASYNC_ENABLE,       type_code_u32    },
	{ "SQL_ATTR_AUTO_IPD",           SQL_ATTR_AUTO_IPD,           type_code_u32    },
	{ "SQL_ATTR_AUTOCOMMIT",         SQL_ATTR_AUTOCOMMIT,         type_code_u32    },
	{ "SQL_ATTR_CONNECTION_DEAD",    SQL_ATTR_CONNECTION_DEAD,    type_code_u32    },
	{ "SQL_ATTR_CONNECTION_TIMEOUT", SQL_ATTR_CONNECTION_TIMEOUT, type_code_u32    },
	{ "SQL_ATTR_CURRENT_CATALOG",    SQL_ATTR_CURRENT_CATALOG,    type_code_string },
	{ "SQL_ATTR_LOGIN_TIMEOUT",      SQL_ATTR_LOGIN_TIMEOUT,      type_code_u32    },
	{ "SQL_ATTR_METADATA_ID",        SQL_ATTR_METADATA_ID,        type_code_u32    },
	{ "SQL_ATTR_ODBC_CURSORS",       SQL_ATTR_ODBC_CURSORS,       type_code_u32    },
	{ "SQL_ATTR_PACKET_SIZE",        SQL_ATTR_PACKET_SIZE,        type_code_u32    },
	{ "SQL_ATTR_QUIET_MODE",         SQL_ATTR_QUIET_MODE,         type_code_u32    },
	{ "SQL_ATTR_TRACE",              SQL_ATTR_TRACE,              type_code_u32    },
	{ "SQL_ATTR_TRACEFILE",          SQL_ATTR_TRACEFILE,          type_code_string },
	{ "SQL_ATTR_TRANSLATE_LIB",      SQL_ATTR_TRANSLATE_LIB,      type_code_string },
	{ "SQL_ATTR_TRANSLATE_OPTION",   SQL_ATTR_TRANSLATE_OPTION,   type_code_u32    },
	{ "SQL_ATTR_TXN_ISOLATION",      SQL_ATTR_TXN_ISOLATION,      type_code_u32    },
};

//
//
//

static inline size_t strlen(const SQLCHAR *str)
{
	return strlen((char*)str);
}

//
//
//

db_connection_t db_odbc_connect(const char *connstr)
{
	odbc_connection_t *conn = new odbc_connection_t;

	if (conn != NULL)
	{
		bool open = false;

		if (odbc_environment_init(conn->m_environment))
		{
			RETCODE rc;
			
			rc = SQLAllocConnect(conn->m_environment->m_hEnv, &conn->m_hdbc);
			
			if (check_result(conn->m_environment, rc))
			{
				SQLSMALLINT cbConn = 0;
				
				// XXX: tossing away const
				
				// TODO: do tracing and profiling around this call

				rc = SQLDriverConnect(conn->m_hdbc, NULL,
					const_cast<SQLCHAR*>((const SQLCHAR*)connstr),
					strlen(connstr), NULL, 0, &cbConn, SQL_DRIVER_COMPLETE);
				
				if (check_result(conn, rc))
				{
#ifdef _DEBUG
					if (0)
					{
					// dump connection attributes
					
					for (int i = 0; i < DIM(g_odbc_conn_attr); i++)
					{
						variant_t value;

						char buffer[256] = { 0 };

						SQLINTEGER len = 0;
				
						SQLRETURN rc = SQLGetConnectAttr(conn->m_hdbc, g_odbc_conn_attr[i].Attribute,
							buffer, sizeof(buffer), &len);

						if (rc == SQL_NO_DATA)
							value = variant_t();

						if (rc == SQL_SUCCESS)
						{
							switch (type_code(g_odbc_conn_attr[i].type))
							{
								case type_code_u32:
//									_ASSERT(len == sizeof(u32));
									value = *((u32*)buffer);
									break;

								case type_code_string:
									value = string(buffer, len);
									break;
							}
						}

						trace("ODBC connection attribute: name=", g_odbc_conn_attr[i].name, " value=", value, "\n");
					}

					}
#endif

					open = true;
				}
			}
		}

//		return db_connection_create_and_release(conn, &odbc_connection_t::g_db_connection_dispatch, open);
		return db_connection_create_and_release(conn, open);
	}

	return db_connection_error();
}

//
//
//

bool odbc_connection_t::close()
{
	if (m_hdbc != NULL)
	{
		if (check_result(this, SQLDisconnect(m_hdbc)))
		{
			// TODO: xxx = io_status_closed;
			
			if (check_result(this, SQLFreeConnect(m_hdbc)))
			{
			}
			else
			{
				DBG_BREAK();
			}

			m_hdbc = NULL;

			return true;
		}
	}

	return false;
}

//
//
//

#ifndef REMOVE_THIS

variant_t odbc_connection_t::evaluate_field_connection_attribute(long i, int m_metadata_irow)
{
	_ASSERT(m_metadata_irow >= 1 && m_metadata_irow <= DIM(g_odbc_conn_attr));

	switch (i)
	{
		case 0: return g_odbc_conn_attr[m_metadata_irow - 1].name;

		case 1:
		{
			char buffer[256];

			SQLINTEGER len = SQL_NTS;
			
			SQLRETURN rc = SQLGetConnectAttr(m_hdbc, g_odbc_conn_attr[m_metadata_irow - 1].Attribute,
				buffer, sizeof(buffer), &len);

			if (rc == SQL_NO_DATA)
				return variant_t();
			
			if (rc == SQL_SUCCESS)
			{
				switch (type_code(g_odbc_conn_attr[m_metadata_irow - 1].type))
				{
					case type_code_u32:
//						_ASSERT(len == sizeof(u32));
						return *((u32*)buffer);

					default:
					{
					if (g_odbc_conn_attr[m_metadata_irow - 1].type == type_code_string)
					{
						if (len >= 0)
							return string(buffer, len);
					}
					break;
					}
				}
			}
			else
			{
				// TODO: pass text info to variant_error();
			}
		}
	}

	return variant_error();
}

#endif

//
//
//

void odbc_connection_t::attributes(db_cursor_t &cursor)
{
	odbc_cursor_t *driver = new odbc_cursor_t;

	if (driver != NULL)
	{
		cursor = db_cursor_create_and_release(driver);

		driver->m_mode = odbc_cursor_t::MODE_CONNECTION_ATTRIBUTES;
		driver->m_metadata_irow = 0;
		driver->m_connection = this;

		unsigned int ccol = 2;
		
		if (driver->alloc_column_descriptors(ccol))
		{
			driver->col_desc(0).name = "Name";
			driver->col_desc(0).label = driver->col_desc(0).name;

			driver->col_desc(1).name = "Label";
			driver->col_desc(1).label = driver->col_desc(1).name;

			return;
		}
	}

	cursor = db_cursor_error();
}

//
//
//

void odbc_connection_t::catalog(int index, db_cursor_t &cursor)
{
	odbc_cursor_t *driver = new odbc_cursor_t;

	if (driver != NULL)
	{
		cursor = db_cursor_create_and_release(driver);

		driver->m_mode = odbc_cursor_t::MODE_CATALOG;
		driver->m_catalog = index;
		driver->m_connection = this;

		HSTMT hstmt = SQL_NULL_HSTMT;

		RETCODE rc;
		
		//
		//  Allocate an ODBC statement handle on our connection handle
		//

		rc = SQLAllocStmt(m_hdbc, &hstmt);
		
		driver->m_hstmt = hstmt;
		
		if (check_result(this, rc))
		{
			//
			//
			//
			
			SQLCHAR szCatalog[] = SQL_ALL_CATALOGS;
			SQLCHAR szSchema[] = SQL_ALL_SCHEMAS;
			SQLCHAR szTableName[] = "";
			SQLCHAR szTableType[] = SQL_ALL_TABLE_TYPES;
			SQLCHAR szColumnName[] = "";
			SQLCHAR szFkCatalog[] = "";
			SQLCHAR szFkSchema[] = "";
			SQLCHAR szFkTable[] = "";

			if (index == 0)
			{
				rc = SQLTables(hstmt,
					szCatalog, strlen(szCatalog),
					szSchema, strlen(szSchema),
					szTableName, strlen(szTableName),
					szTableType, strlen(szTableType));
			}
			else if (index == 1)
			{
				rc = SQLColumns(hstmt,
					szCatalog, strlen(szCatalog),
					szSchema, strlen(szSchema),
					szTableName, strlen(szTableName),
					szColumnName, strlen(szColumnName));
			}
			else if (index == 2)
			{
				rc = SQLColumnPrivileges(hstmt,
					szCatalog, strlen(szCatalog),
					szSchema, strlen(szSchema),
					szTableName, strlen(szTableName),
					szColumnName, strlen(szColumnName));
			}
			else if (index == 3)
			{
				rc = SQLForeignKeys(hstmt,
					szCatalog, strlen(szCatalog),
					szSchema, strlen(szSchema),
					szTableName, strlen(szTableName),
					szFkCatalog, strlen(szFkCatalog),
					szFkSchema, strlen(szFkSchema),
					szFkTable, strlen(szFkTable));
			}
			else if (index == 4)
			{
				rc = SQLGetTypeInfo(hstmt, SQL_ALL_TYPES);
			}
			else if (index == 5)
			{
				rc = SQLPrimaryKeys(hstmt,
					szCatalog, strlen(szCatalog),
					szSchema, strlen(szSchema),
					szTableName, strlen(szTableName));
			}
			else if (index == 6)
			{
				rc = SQLProcedureColumns(hstmt,
					szCatalog, strlen(szCatalog),
					szSchema, strlen(szSchema),
					szTableName, strlen(szTableName),
					szColumnName, strlen(szColumnName));
			}
			else if (index == 7)
			{
				rc = SQLProcedures(hstmt,
					szCatalog, strlen(szCatalog),
					szSchema, strlen(szSchema),
					szTableName, strlen(szTableName));
			}
			else if (index == 8)
			{
/*
				rc = SQLSpecialColumns(hstmt,
					szCatalog, strlen(szCatalog),
					szSchema, strlen(szSchema),
					szTableName, strlen(szTableName),
					szColumnName, strlen(szColumnName));
*/
			}
			else if (index == 9)
			{
/*
				rc = SQLStatistics(hstmt,
					szCatalog, strlen(szCatalog),
					szSchema, strlen(szSchema),
					szTableName, strlen(szTableName),
					szColumnName, strlen(szColumnName));
*/
			}
			else if (index == 10)
			{
				rc = SQLTablePrivileges(hstmt,
					szCatalog, strlen(szCatalog),
					szSchema, strlen(szSchema),
					szTableName, strlen(szTableName));
			}
			else
			{
				rc = SQLFreeStmt(hstmt, SQL_DROP);

				cursor = db_cursor_error();

				return;
			}

			if (check_result(driver, rc))
			{
				//
				//  Unbind parameter bindings
				//

				rc = SQLFreeStmt(hstmt, SQL_UNBIND);
				
				check_result(driver, rc);

				//
				//
				//

				driver->get_col_desc();

				return;
			}
			else
			{
				rc = SQLFreeStmt(hstmt, SQL_DROP);

//				check_result(driver, rc);

				driver->m_hstmt = SQL_NULL_HSTMT;
			}
		}
	}

	cursor = db_cursor_error();
}

//
//
//

void odbc_connection_t::execute(db_execute_t &cmd, db_cursor_t &cursor)
{
	odbc_cursor_t *driver = new odbc_cursor_t;

	if (driver != NULL)
	{
		driver->m_connection = this;

		RETCODE rc;
		
		//
		//  Allocate an ODBC statement handle on our connection handle
		//

		HSTMT hstmt = SQL_NULL_HSTMT;

		rc = SQLAllocStmt(m_hdbc, &hstmt);
		
		driver->m_hstmt = hstmt;

		if (check_result(this, rc))
		{
			// TODO: SQLSetStmtOption(hstmt, SQL_CURSOR_TYPE, SQL_CURSOR_FORWARD_ONLY)

#if 0
			if (pParam != NULL && cParam > 0)
			{
				for (SQLUSMALLINT ipar = 1; ipar <= cParam; ipar++)
				{
					const DB_STMT_PARAMETER &par = pParam[ipar - 1];

					rc = SQLBindParameter(hstmt, ipar, par.fParamType,
						par.fCType, par.fSqlType, par.cbColDef, par.ibScale,
						par.rgbValue, par.cbValueMax, par.pcbValue);

					hr = StmtRc(hstmt, rc, m_pMsgSink);

					if (FAILED(hr))
						break;
				}
			}

			if (FAILED(hr))
			{
				DropHstmt(hstmt, m_pMsgSink);

				hstmt = NULL;

				return false;
			}
#endif

			//
			//
			//
		
			rc = SQLExecDirect(hstmt, (SQLCHAR*)cmd.sql, strlen(cmd.sql));

			if (check_result(driver, rc))
			{
				//
				//  Unbind parameter bindings
				//

				rc = SQLFreeStmt(hstmt, SQL_UNBIND);
			
				check_result(driver, rc);

				//
				//
				//

				driver->get_col_desc();

				cursor = db_cursor_create_and_release(driver);

				return;
			}
			else
			{
				rc = SQLFreeStmt(hstmt, SQL_DROP);

//				check_result(driver, rc);

				driver->m_hstmt = SQL_NULL_HSTMT;
			}
		}
		else
		{
//			driver->m_status = io_status_error;
//			driver->m_is_error = true;

			// TODO: propagate error value

			db_cursor_t err = db_cursor_error();

//			cursor = err;
		}
	}

	cursor = db_cursor_error();
}

//
//
//

db_cursor_t odbc_call__(db_connection_t &conn, const char *name, int argc, const call_arg0_t * const *argv)
{
	odbc_connection_t *obj = db_connection_cast<odbc_connection_t>(conn);

	if (obj != NULL)
	{
	}

	DBG_NOT_IMPLEMENTED();

	variant_t error;
	
	if (is_error(conn, error))
		return db_cursor_error(error);

	string_t sql;

	sql += "{?=call ";
	sql += name;
	sql += "(";
	for (int i = 0; i < argc; i++)
	{
		if (i > 0)
			sql += ", ";

		sql += "?";
	}
	sql += ")";

#if 0

/*
	const DB_STMT_PARAMETER rgPar[] = {
		{ SQL_PARAM_OUTPUT, SQL_C_SLONG, SQL_INTEGER, 0, 0, &m_dwRetValue,    0,                   &m_cbRetValue },
		{ SQL_PARAM_INPUT,  SQL_C_ULONG, SQL_INTEGER, 0, 0, &m_loc.iType,     0,                   &_cbTypeId    },
		{ SQL_PARAM_INPUT,  SQL_C_ULONG, SQL_INTEGER, 0, 0, &m_loc.rgnId[0],  0,                   &_cbObjId     },
		{ SQL_PARAM_INPUT,  SQL_C_ULONG, SQL_INTEGER, 0, 0, &m_loc.rgnId[1],  0,                   &_cbObjId     },
		{ SQL_PARAM_INPUT,  SQL_C_ULONG, SQL_INTEGER, 0, 0, &m_loc.rgnId[2],  0,                   &_cbObjId     },
		{ SQL_PARAM_INPUT,  SQL_C_ULONG, SQL_INTEGER, 0, 0, &m_dwBrowseFlags, 0,                   NULL          },
		{ SQL_PARAM_OUTPUT, SQL_C_CHAR,  SQL_VARCHAR, 0, 0, &m_szCaption,     sizeof(m_szCaption), &m_cbCaption  },
	};
*/

	return execute(conn, sql);
}

#endif

	return db_cursor_error();
}

#define _ARGS_1 &arg1.arg
#define _ARGS_2 _ARGS_1, &arg2.arg
#define _ARGS_3 _ARGS_2, &arg3.arg
#define _ARGS_4 _ARGS_3, &arg4.arg
#define _ARGS_5 _ARGS_4, &arg5.arg
#define _ARGS_6 _ARGS_5, &arg6.arg
#define _ARGS_7 _ARGS_6, &arg7.arg
#define _ARGS_8 _ARGS_7, &arg8.arg
#define _ARGS_9 _ARGS_8, &arg9.arg
#define _ARGS_10 _ARGS_9, &arg10.arg

db_cursor_t odbc_call(db_connection_t &conn, const char *name)
	{ return odbc_call__(conn, name, 0, NULL); }

db_cursor_t odbc_call(db_connection_t &conn, const char *name, ARGS_1(call_arg1_t))
	{ const call_arg0_t *argv[] = { _ARGS_1 }; return odbc_call__(conn, name, DIM(argv), argv); }

db_cursor_t odbc_call(db_connection_t &conn, const char *name, ARGS_2(call_arg1_t))
	{ const call_arg0_t *argv[] = { _ARGS_2 }; return odbc_call__(conn, name, DIM(argv), argv); }

db_cursor_t odbc_call(db_connection_t &conn, const char *name, ARGS_3(call_arg1_t))
	{ const call_arg0_t *argv[] = { _ARGS_3 }; return odbc_call__(conn, name, DIM(argv), argv); }

db_cursor_t odbc_call(db_connection_t &conn, const char *name, ARGS_4(call_arg1_t))
	{ const call_arg0_t *argv[] = { _ARGS_4 }; return odbc_call__(conn, name, DIM(argv), argv); }

db_cursor_t odbc_call(db_connection_t &conn, const char *name, ARGS_5(call_arg1_t))
	{ const call_arg0_t *argv[] = { _ARGS_5 }; return odbc_call__(conn, name, DIM(argv), argv); }

//
//
//

bool db_odbc_message(db_connection_t &conn, string_t &text, int32_t *fNativeError)
{
	C_ASSERT(sizeof(SDWORD) == sizeof(int32_t));

	odbc_connection_t *obj = db_connection_cast<odbc_connection_t>(conn);

	if (obj != NULL)
	{
		odbc_msg_t *msg = obj->m_message_queue.unlink_head();

		if (msg != NULL)
		{
			C_ASSERT(sizeof(char) == sizeof(msg->szMsg[0]));
			
			text = (char*)msg->szMsg;

			if (fNativeError != NULL)
				*fNativeError = msg->fNativeErr;

			delete msg;

			return true;
		}
	}

	return false;
}

bool check_result(odbc_connection_t *conn, RETCODE rc)
{
	retrieve_messages(NULL, conn->m_hdbc, NULL, rc, conn->m_message_queue);

	return odbc_success(rc);
}

unsigned int odbc_connection_t::connection_attribute_count()
{
	return DIM(g_odbc_conn_attr);
}

