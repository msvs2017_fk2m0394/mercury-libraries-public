/*------------------------------------------------------------------------------

	platform/windows/core.h - Windows-specific declarations

	- process
	- thread
	- module
	- I/O
	- memory
	- message

------------------------------------------------------------------------------*/

#ifndef __WIN_CORE_H__
#define __WIN_CORE_H__

#include "../../lib/core/thread.h"
#include "../../lib/core/clock.h"

#include "module.h"
#include "memory.h"
#include "message.h"

#ifdef _WIN32_WCE
extern export_module_t g_coredll_module;
#else
extern export_module_t g_kernel32_module;
extern export_module_t g_user32_module;
#endif

/*

	Win32 handles, all represented with by the HANDLE type...

	1.  real kernel handle (e.g. process, thread)
	2.  INVALID_HANDLE_VALUE
	3.  NULL
	4.  console stream handle
	5.  FindFirst handle
	6.  STD_INPUT_HANDLE, etc.

	For example, ReadFile has to deal with this by checking the lower
	two bits of the handle value to determine if its a kernel handle, or
	a console stream handle.

*/

extern "C" {
HANDLE ART_API_CALLTYPE process_thread_handle(process_t process);
thread_id_t ART_API_CALLTYPE process_thread_id(process_t process);
}

//
//
//

inline u64 filetime_to_u64(FILETIME filetime)
{
	ULARGE_INTEGER x;

	x.LowPart  = filetime.dwLowDateTime;
	x.HighPart = filetime.dwHighDateTime;

	return x.QuadPart;
}

inline FILETIME u64_to_filetime(u64 value)
{
	ULARGE_INTEGER value2;

	value2.QuadPart = value;

	FILETIME filetime;

	filetime.dwLowDateTime  = value2.LowPart;
	filetime.dwHighDateTime = value2.HighPart;

	return filetime;
}

//
//
//

struct win_core_thread_state_t : core_thread_state_t
{
	thread_id_t thread_id_host; // Host OS thread ID
	HANDLE      thread_handle;  // Host OS thread handle

	OBJECT_CLASS_DECLARE();
};

//
//
//

#ifdef __cplusplus
extern "C" {
#endif

const SYSTEM_INFO &ART_SystemInfo();
const OSVERSIONINFO &ART_OSVersion();
bool ART_IsPlatformNT();

mm_info_t mm_query_region__(const void *address);
mm_info_t mm_query_allocation__(const void *address);
mm_info_t make_mm_info(const MEMORY_BASIC_INFORMATION &mbi, int alloc);

HANDLE process_handle__(process_t process, bool detach = false);

extern const time_type_t g_time_type_perf;

bool db_drives_internal(db_cursor_t &cursor);
bool db_find_file_internal(string_t filespec, unsigned int max_depth, db_cursor_t &cursor);
bool io_system_query_internal(const io_system_query_t *query, db_cursor_t &cursor);

#ifdef __cplusplus
}
#endif

process_t process_from_handle__(HANDLE hProcess, process_id_t pid);

//
//
//

#ifndef REMOVE_THIS

// XREF: clock_info_t

struct timer_info_t
{
	u64 base_time;   // base system time, in FILETIME units
	s64 base_counts; // base system time, in hires timer counts
	f64 period;      // hires timer period, in sec
	f64 factor__;    // conversion coefficient - hires to FILETIME
	f64 factor2__;   // conversion coefficient - milliseconds to hires
};

struct core_global_data_t
{
	int          timer_status; // 0 = uninitialized, 1 = initialized, -1 = error
	timer_info_t timer_info;   //
//	clock_info_t clock_info__;
};

extern core_global_data_t g_core_global_data;

void timer_info2();

static inline bool timer_info()
{
	if (g_core_global_data.timer_status == 0)
		timer_info2();

	return (g_core_global_data.timer_status > 0);
}

#endif

extern "C" HMODULE module_handle_current();

//
//
//

process_object_t *win_process_create(const process_create_t *args);
void win_debug_break();
void win_debug_output_string(const char *text);
io_status_t win_io_delete(string_t filename);
io_status_t win_io_exists(string_t filename);
io_system_t win_io_system();

//
//
//

inline DWORD millisec(const f32 *timeout)
{
	if (timeout == NULL)
		return INFINITE;

	return (DWORD)(*timeout * 1000);
}

//
//
//

HANDLE copy_handle(HANDLE);

#endif // __WIN_CORE_H__

