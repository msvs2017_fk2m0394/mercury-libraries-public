/*------------------------------------------------------------------------------

	io_system.cpp - Windows I/O support

------------------------------------------------------------------------------*/

#include "pch.h"
#include <winioctl.h>
#include "io.h"
#include "core.h"

#include <stdio.h>
#include <io.h>
//#include <fcntl.h>

#undef _MBCS
#include <tchar.h>

#include <mercury/experimental/core_crt_stdio.h>
#include <mercury/experimental/db.h>

#include "../../lib/io/io.h"
#include "../../lib/elements/text.h"
#include "../../lib/misc/misc.h"
#include "../../lib/misc/print.h"

string_t get_sid_user_name(PSID sid);

static string_t fixup_find_filename(const string_t &filespec, const string_t &filename);

#ifndef FSCTL_QUERY_USN_JOURNAL

// from ntifs.h...
#define FSCTL_QUERY_USN_JOURNAL         CTL_CODE(FILE_DEVICE_FILE_SYSTEM, 61, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define FSCTL_ENUM_USN_DATA             CTL_CODE(FILE_DEVICE_FILE_SYSTEM, 44,  METHOD_NEITHER, FILE_READ_DATA)
#define FSCTL_READ_USN_JOURNAL          CTL_CODE(FILE_DEVICE_FILE_SYSTEM, 46,  METHOD_NEITHER, FILE_READ_DATA)

#endif

#ifndef LITE_RTL

struct kernel32_procs_t
{
	BOOL    (WINAPI * GetDiskFreeSpaceExA)(LPCSTR lpDirectoryName, PULARGE_INTEGER lpFreeBytesAvailableToCaller, PULARGE_INTEGER lpTotalNumberOfBytes, PULARGE_INTEGER lpTotalNumberOfFreeBytes);
	BOOL    (WINAPI * GetDiskFreeSpaceExW)(LPCWSTR lpDirectoryName, PULARGE_INTEGER lpFreeBytesAvailableToCaller, PULARGE_INTEGER lpTotalNumberOfBytes, PULARGE_INTEGER lpTotalNumberOfFreeBytes);

#if 0

	// Windows Vista...

	//_WIN32_WINNT >= 0x0600
#ifdef _DEBUG
	BOOL (WINAPI *SetFileCompletionNotificationModes)(HANDLE FileHandle, UCHAR Flags );
	BOOL (WINAPI *SetFileIoOverlappedRange)(HANDLE FileHandle, PUCHAR OverlappedRangeStart, ULONG Length );
#endif
#endif

};

static kernel32_procs_t g_kernel32_procs;

static export_info_t g_kernel32_exports[] = 
{
	{ &g_kernel32_module, "GetDiskFreeSpaceExA",    &g_kernel32_procs.GetDiskFreeSpaceExA    },
	{ &g_kernel32_module, "GetDiskFreeSpaceExW",    &g_kernel32_procs.GetDiskFreeSpaceExW    },
//	{ &g_kernel32_module, "GetCompressedFileSizeA", &g_kernel32_procs.GetCompressedFileSizeA },
};

#endif

//
//
//

#ifndef REMOVE_THIS

// XREF: uri_t
// XREF: file_addr_t

struct filename_t
{
	filename_t(string_t psz);

	operator string_t() const
	{
		string_t ans;

		ans = root;
		
		ans += path;
		
		if (file != "")
		{
			ans += "\\";
			ans += file;
			ans += ext;
		}

		return ans;
	}

	bool append_dir(string_t psz);
	
	string_t root; // "C:"
	string_t path; // "/projects"
	string_t file; // "foobar"
	string_t ext;  // "txt"
};

#endif

// XREF: file_info_t

struct file_attr_t
{
	bool is_directory()
	{
		return ((m_gfa != 0xFFFFFFFF) && (m_gfa & FILE_ATTRIBUTE_DIRECTORY));
	}
		
	bool  m_gfa_valid;
	DWORD m_gfa;
/*
	LPFILETIME lpCreationTime,    // address of creation time
	LPFILETIME lpLastAccessTime,  // address of last access time
	LPFILETIME lpLastWriteTime    // address of last write time
*/
 
};

struct dir_info_t
{
	u64 file_bytes;
	u64 file_count;
	u64 dir_count;
};

#ifndef LITE_RTL

/*----------------------------------------------------------------------------

	todo: _searchenv function equivalent
	todo: build complete function lib for file system path manipulation (add, sub, etc.)

	see also: ./path_util.cpp
	see also: db_findfile.cpp

	XREF: uri_t
	XREF: file_addr_t

	REF: _tfullpath (MSCRT source)

	REF: "HOWTO: Getting Floppy Drive Type Information", Article ID: Q163920

----------------------------------------------------------------------------*/

bool is_dir_sep(const char *ptr);

//
//
//

string_t path(path_id_t id, string_t rel)
{
	if (is_error(rel))
		return rel;
	
	string_t base = path_internal(id);

	if (is_error(base))
		return base;
	
	return file_path_append(base, rel);
}

string_t path(path_id_t id)
{
	return path_internal(id);
}

//
//
//

io_status_t file_last_write_date_utc(string_t filename, art_time_t &t)
{
	WIN32_FIND_DATA info;

	HANDLE hFind = FindFirstFile(filename, &info);

	if (hFind == INVALID_HANDLE_VALUE)
		return io_status_t(false, GetLastError());

	FindClose(hFind);

	t = time_from_filetime_utc(info.ftLastWriteTime);

	return true;
}

//
//
//

io_status_t io_directory_create(string_t filename)
{
	string_t base;
	string_t path2;

	string_t unc_machine;
	string_t unc_share;
	string_t unc_remainder;
	
	if (path_scan_unc(filename, unc_machine, unc_share, unc_remainder))
	{
		base = "\\\\" + unc_machine + "\\" + unc_share;
		path2 = "\\" + unc_remainder;
	}
	else
	{
#if defined(_MSC_VER) && !defined(_WIN32_WCE)

		char drive[_MAX_DRIVE] = "";
		char dir[_MAX_DIR] = "";

		_splitpath(filename, drive, dir, NULL, NULL); // warning C4996: '_splitpath' was declared deprecated

		if (drive[0] == 0)
			return false;

		if (dir[0] == 0)
			return false;

		base = drive;
		path2 = dir;

#else

		DBG_NOT_IMPLEMENTED2("io_directory_create");

		return false;

#endif
	}

	//
	//  Start with base designation (drive or UNC machine name and sharename)
	//

	string_t path = base;
	
	LPCSTR ptr = path2;

	for (;;)
	{
		//
		//  Add next path component (begins with directory separator)
		//
		
		int dir_count = 1;

		for (; *ptr != 0; ptr++)
		{
			if (is_dir_sep(ptr))
			{
				int temp = dir_count;
				
				dir_count--;
				
				if (temp == 0)
					break;
			}

			path += *ptr;
		}

		//
		//  Any more components added?
		//

//		TRACE("dir_count=%i, path=%s\n", dir_count, (const char *)path);
		
		if (dir_count == 0)
			break;
		
		//
		//  Check if directory already exists
		//
		
		if (!is_directory(path))
		{
			// NOTE: no provision for security descriptor in function prototype
			LPSECURITY_ATTRIBUTES security = NULL;

			if (!CreateDirectory(path, security))
				return io_status_t(false, GetLastError());
		}
	}

	return true;

#if 0

	// NOTE: no provision for security descriptor in function prototype
	LPSECURITY_ATTRIBUTES security = NULL;
	
	//
	//  Pull apart given filename/pathname
	//
	
	char drive[_MAX_DRIVE] = "";
	char dir[_MAX_DIR] = "";

	_splitpath(filename, drive, dir, NULL, NULL);

	if (drive[0] == 0)
		return false;

	if (dir[0] == 0)
		return false;

	//
	//  Start with drive designation
	//
	
	char path[_MAX_PATH];

	strcpy(path, drive);

	int path_len = art_strlen(path);
	const char *ptr = dir;

	for (;;)
	{
		//
		//  Add next path component (begins with backslash)
		//
		
		int dir_count = 1;

		for (; *ptr != 0; ptr++)
		{
			if ((ptr[0] == '\\' || ptr[0] == '/') && dir_count-- == 0)
				break;

			_ASSERT(path_len < DIM(path));
			
			path[path_len++] = *ptr;
		}

		path[path_len] = 0;

		//
		//  Any more components added?
		//
		
		if (dir_count == 0)
			break;
		
		//
		//  Check if directory already exists
		//
		
		if (!is_directory(path))
		{
			if (!CreateDirectory(path, security))
				return io_status_t(false, GetLastError());
		}
	}

	return true;

#endif

}

//
//
//

io_status_t win_io_exists(string_t filename)
{
	_ASSERT(filename != "");

	u32 dwAttr;

	io_status_t ret = file_attributes(filename, dwAttr);

	if (ret)
	{
		_ASSERT((dwAttr & FILE_ATTRIBUTE_DIRECTORY) == 0);
	}

	return ret;
}

//
//
//

io_status_t file_attributes(string_t filename, u32 &attr)
{
	// otherwise, the system might return information about the
	// current directory if the error string proxy "" is passed
	
	if (is_error(filename))
	{
		return false;
	}

	DWORD dwAttr;

#if 1
	dwAttr = GetFileAttributes(filename);
#else

	// XXX: similar processing should occur everywhere we are passing
	// string_t to a Windows API that has ANSI and UNICODE versions...

#ifdef UNICODE

	if (is_unicode(filename))
	{
		dwAttr = GetFileAttributesW(filename);
	}
	else if (is_ansi(filename))
	{
		// NOTE: symbol not available on CE...
		dwAttr = GetFileAttributesA(filename);
	}
	else
	{
		return io_status_t(false, GetLastError());
	}

#else

	// NOTE: symbol not available on CE...
	dwAttr = GetFileAttributesA(filename);

#endif

#endif

	if (dwAttr == 0xFFFFFFFF)
	{
		return io_status_t(false, GetLastError());
	}
	else
	{
		attr = dwAttr;
		
		return true;
	}
}

//
//
//

io_status_t is_directory(string_t filename)
{
	u32 dwAttr;
	
	io_status_t ret = file_attributes(filename, dwAttr);
	
	if (ret)
		return ((dwAttr & FILE_ATTRIBUTE_DIRECTORY) != 0);

	return ret;
}

//
//
//

string_t io_short_pathname(string_t filename)
{
	if (is_error(filename))
		return filename;

#if !defined(_WIN32_WCE)

	// TODO: use FindFirstFile and cAlternateFileName to make porting easier???

	TCHAR szShortPath[_MAX_PATH];
	
	DWORD dwLen = GetShortPathName(filename, szShortPath, DIM(szShortPath));

	if (dwLen == 0)
		return FormatSystemMessage(GetLastError());

	_ASSERT(dwLen < DIM(szShortPath));

	if (dwLen >= DIM(szShortPath))
		return string_error();

#ifdef UNICODE
	_ASSERT(wcslen(szShortPath) == dwLen);
#else
	_ASSERT(art_strlen(szShortPath) == dwLen);
#endif

	return string(szShortPath, dwLen);

#else
	DBG_NOT_IMPLEMENTED2("io_short_pathname()");

	return string_error("io_short_pathname() not implemented");
#endif

}

//
//
//

string_t io_long_pathname(string_t filename)
{
	//
	//
	//
	
	if (is_error(filename))
		return filename;
	
	//
	//
	//
	
	WIN32_FIND_DATA data;
	
	// NOTE: FindFirstFile/FindNextFile return only the name of the file
	// within the containing directory
	
	HANDLE hFindFile = FindFirstFile(filename, &data);

	if (hFindFile != INVALID_HANDLE_VALUE)
	{
		string_t result = fixup_find_filename(filename, data.cFileName);
		
		FindClose(hFindFile);

		return result;
	}
	else
	{
		return FormatSystemMessage(GetLastError());
	}
}

//
//
//

io_status_t win_io_delete(string_t filename)
{
	_ASSERT(filename != "");

	if (is_directory(filename))
	{
		if (RemoveDirectory(filename))
			return true;
	}
	else
	{
		if (DeleteFile(filename))
			return true;
	}
	
	DWORD dwError = GetLastError();

	variant_t temp = variant_system_error(dwError);
	
	error_log_message(trace_severity_error, temp);
	
	return io_status_t(false, dwError);
}

//
//
//

static bool get_dir_info(LPCSTR path, u64 *total_bytes, u64 *total_files, u64 *total_directories, bool recurse)
{
	DBG_NOT_IMPLEMENTED();
	
	if (total_directories != NULL)
		*total_directories = 0;
	
	if (total_files != NULL)
		*total_files = 0;
	
	if (total_bytes != NULL)
		*total_bytes = 0;

	WIN32_FIND_DATAA info;

	HANDLE hFind = FindFirstFileA(path, &info);

	if (hFind != INVALID_HANDLE_VALUE)
	{
		do
		{
			if (info.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			{
				if (total_directories != NULL)
				{
					(*total_directories)++;
				}

				if (recurse)
				{
					u64 local_bytes, local_files, local_directories;

					string_t subpath = file_path_append(path, info.cFileName);
					
					if (get_dir_info(subpath, &local_bytes, &local_files, &local_directories, recurse))
					{
					}
				}
			}
			else
			{
				if (total_files != NULL)
				{
					(*total_files)++;
				}

				if (total_bytes != NULL)
				{
					LARGE_INTEGER li;

					li.LowPart = info.nFileSizeLow;
					li.HighPart = info.nFileSizeHigh;
					
					*total_bytes += li.QuadPart;
				}
			}
		}
		while (FindNextFileA(hFind, &info));
		
		FindClose(hFind);
	}
	else
	{
		return false;
	}
}

//
//
//

bool is_dir_sep(const char *ptr)
{
	// file system specific...
	
	return (ptr != NULL && (ptr[0] == '\\' || ptr[0] == '/'));
}

//
//
//

bool path_scan_unc(const char *&path, const char *&name, size_t &len)
{
	const char *psz = path;

	if (psz[0] == '\\' && psz[1] == '\\')
	{
		psz += 2;

		name = psz;
		
		while (psz[0] != 0 && psz[0] != '\\')
			psz++;

		len  = psz - name;

		if (psz[0] == '\\')
			psz++;

		path = psz;

		return true;
	}

	return false;
}

bool path_scan_unc(const wchar_t *&path, const wchar_t *&name, size_t &len)
{
	const wchar_t *psz = path;

	if (psz[0] == '\\' && psz[1] == '\\')
	{
		psz += 2;

		name = psz;
		
		while (psz[0] != 0 && psz[0] != '\\')
			psz++;

		len  = psz - name;

		if (psz[0] == '\\')
			psz++;

		path = psz;

		return true;
	}

	return false;
}

bool path_scan_unc(const char *path, string_t &machine, string_t &share, string_t &remainder)
{
	const char *psz = path;

	if (psz[0] == '\\' && psz[1] == '\\')
	{
		psz += 2;

		machine = "";
		share = "";
		remainder = "";

		while (psz[0] != 0 && psz[0] != '\\')
			machine += *psz++;

		if (psz[0] == '\\')
			psz++;

		while (psz[0] != 0 && psz[0] != '\\')
			share += *psz++;

		if (psz[0] == '\\')
			psz++;

		remainder = psz;

		return true;
	}

	return false;
}

//
//
//

static string_t fixup_find_filename(const string_t &filespec_, const string_t &filename_)
{
	if (is_ansi(filespec_))
	{
		const char *filespec = filespec_;
		
		string_t filename2 = to_ansi(filename_);
		
		const char *filename = filename2;

		_ASSERT(art_strchr(filename, '\\') == NULL);

		const char *last_slash = art_strrchr(filespec, '\\');

		string_t result;
		
		if (last_slash != NULL)
		{
			result = string(filespec, last_slash - filespec + 1) + filename;
		}
		else
		{
			DBG_BREAK();

			result = filename;
		}

		return result;
	}
#ifdef _MSC_VER

	else if (is_unicode(filespec_))
	{
		const wchar_t *filespec = filespec_;
		
		string_t filename2 = to_unicode(filename_);
		
		const wchar_t *filename = filename2;

		_ASSERT(wcschr(filename, '\\') == NULL);

		const wchar_t *last_slash = wcsrchr(filespec, '\\');

		string_t result;
		
		if (last_slash != NULL)
		{
			result = string(filespec, last_slash - filespec + 1) + filename;
		}
		else
		{
			DBG_BREAK();

			result = filename;
		}

		return result;
	}
	
#endif

	return string_error();
}

/*----------------------------------------------------------------------------

	TODO: support FindFirstFileEx on NT 4.0 machines???
	TODO: GetCompressedFileSize
	TODO: GetDiskFreeSpace, GetDiskFreeSpaceEx
	TODO: support reparse points (e.g. avoid infinite recursion)
	TODO: grab and decode security descriptors
	TODO: read volume journal records (FSCTL_QUERY_USN_JOURNAL)
	TODO: extension mechanism for searching file contents

	REF: "HOWTO: Getting Floppy Drive Type Information", Article ID: Q163920

----------------------------------------------------------------------------*/

//
//
//

class find_file_driver_t;

static bool fetch_file(find_file_driver_t *driver, unsigned int max_depth);

//
//
//

STRUCT_DB_VIEW_BEGIN(find_file_driver_t, db_find_file_columns)

/*
	DB_COLUMN_NULL("dwFileAttributes")
	DB_COLUMN_NULL("ftCreationTime")
	DB_COLUMN_NULL("ftLastAccessTime")
	DB_COLUMN_NULL("ftLastWriteTime")
	DB_COLUMN_NULL("file size")
*/
	DB_COLUMN_NULL("Attributes")  // m_file_info.m_find_data.dwFileAttributes
/*
		FIELD_ENUM(FILE_ATTRIBUTE_READONLY, "R")
		FIELD_ENUM(FILE_ATTRIBUTE_HIDDEN, "H")
		FIELD_ENUM(FILE_ATTRIBUTE_SYSTEM, "S")
		FIELD_ENUM(FILE_ATTRIBUTE_DIRECTORY, "")
		FIELD_ENUM(FILE_ATTRIBUTE_ARCHIVE, "A")
		FIELD_ENUM(FILE_ATTRIBUTE_ENCRYPTED, "E")
		FIELD_ENUM(FILE_ATTRIBUTE_COMPRESSED, "")
*/
	DB_COLUMN_NULL("Date Created") // m_file_info.m_find_data.ftCreationTime
	DB_COLUMN_NULL("Date Accessed") // m_file_info.m_find_data.ftLastAccessTime
	DB_COLUMN_NULL("Date Modified") // m_file_info.m_find_data.ftLastWriteTime
	DB_COLUMN_NULL("Size")

	DB_COLUMN_NULL("cFileName") // m_file_info.m_find_data.cFileName
//	DB_COLUMN_NULL("cAlternateFileName")  // m_file_info.m_find_data.cAlternateFileName
	DB_COLUMN_NULL("Name (alternate)") // m_file_info.m_find_data.cAlternateFileName

	DB_COLUMN_NULL("security_descriptor")
	DB_COLUMN_NULL("full pathname")

	DB_COLUMN_NULL("MP3 ID3 Title")

	DB_COLUMN_NULL("file version")
//	DB_COLUMN_NULL("file size (on disk)")
	DB_COLUMN_NULL("Size (on disk)")
	DB_COLUMN_NULL("security_descriptor (owner)")
	DB_COLUMN_NULL("security_descriptor (group)")

/*

	// Columns from Windows Explorer (on XP)...

	DB_COLUMN_NULL("Name")
	DB_COLUMN_NULL("Size")
	DB_COLUMN_NULL("Type")
	DB_COLUMN_NULL("Date Modified")
	DB_COLUMN_NULL("Date Created")
	DB_COLUMN_NULL("Date Accessed")
	DB_COLUMN_NULL("Attributes")
	DB_COLUMN_NULL("Status")
	DB_COLUMN_NULL("Owner")
	DB_COLUMN_NULL("Author")
	DB_COLUMN_NULL("Title")
	DB_COLUMN_NULL("Subject")
	DB_COLUMN_NULL("Category")
	DB_COLUMN_NULL("Pages")
	DB_COLUMN_NULL("Comments")
	DB_COLUMN_NULL("Artist")
	DB_COLUMN_NULL("Album Title")
	DB_COLUMN_NULL("Year")
	DB_COLUMN_NULL("Track Number")
	DB_COLUMN_NULL("Genre")
	DB_COLUMN_NULL("Duration")
	DB_COLUMN_NULL("Bit Rate")
	DB_COLUMN_NULL("Protected")
	DB_COLUMN_NULL("Camera Model")
	DB_COLUMN_NULL("Date Picture Taken")
	DB_COLUMN_NULL("Dimensions")
	DB_COLUMN_NULL("Episode Name")
	DB_COLUMN_NULL("Program Description")
	DB_COLUMN_NULL("Audio sample size")
	DB_COLUMN_NULL("Audio sample rate")
	DB_COLUMN_NULL("Channels")
	DB_COLUMN_NULL("Company")
	DB_COLUMN_NULL("Description")
	DB_COLUMN_NULL("File Version")
	DB_COLUMN_NULL("Product Name")
	DB_COLUMN_NULL("Product Version")
	DB_COLUMN_NULL("Keywords")

	// Columns from Windows Media Player 11...

	DB_COLUMN_NULL("Album Art")
	DB_COLUMN_NULL("Track Number")
	DB_COLUMN_NULL("Title")
	DB_COLUMN_NULL("Length")
	DB_COLUMN_NULL("Rating")
	DB_COLUMN_NULL("Contributing Artist")
	DB_COLUMN_NULL("Album")
	DB_COLUMN_NULL("Genre")
	DB_COLUMN_NULL("Release Year")
	DB_COLUMN_NULL("Composer")
	DB_COLUMN_NULL("Size")
	DB_COLUMN_NULL("Parental Rating")
	DB_COLUMN_NULL("Album Artist")
	DB_COLUMN_NULL("Action")
	DB_COLUMN_NULL("Play Count")
	DB_COLUMN_NULL("Date Last Played")
	DB_COLUMN_NULL("Type")
	DB_COLUMN_NULL("Bit Rate")
	DB_COLUMN_NULL("Date Added")
	DB_COLUMN_NULL("Content Provider")
	DB_COLUMN_NULL("Mood")
	DB_COLUMN_NULL("Keywords")
	DB_COLUMN_NULL("Subtitle")
	DB_COLUMN_NULL("Period")
	DB_COLUMN_NULL("Protected")
	DB_COLUMN_NULL("File Name")
	DB_COLUMN_NULL("File Path")
	DB_COLUMN_NULL("Media Info")
	DB_COLUMN_NULL("Publisher")
	DB_COLUMN_NULL("Conductor")
	DB_COLUMN_NULL("Writer")
	DB_COLUMN_NULL("Key")
	DB_COLUMN_NULL("Language")
	DB_COLUMN_NULL("Copyright")
	DB_COLUMN_NULL("Subgenre")
	DB_COLUMN_NULL("Date Recorded")
	DB_COLUMN_NULL("Date Created")
	DB_COLUMN_NULL("Custom 1")
	DB_COLUMN_NULL("Custom 2")
	DB_COLUMN_NULL("Data Provider")

*/

STRUCT_DB_VIEW_END()

STRUCT_DB_VIEW_BEGIN(drives_driver_t, db_drives_columns)

	DB_COLUMN_NULL("root path")
	DB_COLUMN_NULL("drive type")
	DB_COLUMN_NULL("volume name")
	DB_COLUMN_NULL("volume serial number")
	DB_COLUMN_NULL("file system name")
	DB_COLUMN_NULL("free space (available)")
	DB_COLUMN_NULL("total space (available)")
	DB_COLUMN_NULL("free space")
	DB_COLUMN_NULL("geometry")
	DB_COLUMN_NULL("layout")
	DB_COLUMN_NULL("performance")

STRUCT_DB_VIEW_END()

//
//
//

#ifdef _WIN32_WCE
static DWORD GetLogicalDrives() { return 0; }
#endif

bool db_drives_internal(db_cursor_t &cursor)
{
	drives_driver_t *driver = new drives_driver_t;

	cursor = db_cursor_create_and_release(driver);

	if (driver != NULL)
	{
		DWORD dwDrives = GetLogicalDrives();

		driver->m_dwDrives = dwDrives;

		return true;
	}
	else
	{
		return false;
	}
}

// XXX: UNICODE not supported!!!

bool db_find_file_internal(string_t filespec, unsigned int max_depth, db_cursor_t &cursor)
{
	find_file_driver_t *driver = new find_file_driver_t;
	
	cursor = db_cursor_create_and_release(driver);

	if (driver != NULL)
	{
		driver->m_max_depth = max_depth;

		return driver->open(filespec);
	}
	else
	{
		return false;
	}
}

bool io_system_query_internal(const io_system_query_t *query, db_cursor_t &cursor)
{
	find_file_driver_t *driver = new find_file_driver_t;
	
	cursor = db_cursor_create_and_release(driver);

	if (driver != NULL)
	{
		return driver->open(query);
	}
	else
	{
		return false;
	}
}

//
//
//

#ifndef REMOVE_THIS

drives_driver_t::drives_driver_t()
{
	m_pos = -1;
}

#endif

//
//
//

bool find_file_driver_t::open(string_t filespec)
{
	close();

	file_entry_t *entry = m_file_stack.create_item();

	if (entry != NULL)
	{
		entry->m_filespec = filespec;

		return true;
	}

	return false;
}

//
//
//

bool find_file_driver_t::open(const io_system_query_t *query)
{
	DBG_NOT_IMPLEMENTED();
	
	close();

	return false;
}

//
//
//

static bool is_self_or_parent_dir(LPCSTR filename)
{
	if (filename[0] == '.')
	{
		if (filename[1] == 0)
			return true;

		if (filename[1] == '.')
		{
			if (filename[2] == 0)
				return true;
		}
	}

	return false;
}

//
//
//

int find_file_driver_t::fetch()
{
	for (;;)
	{
		if (fetch_file(this, m_max_depth))
		{
			// skip the "." and ".." directory entries (otherwise, we will get
			// in an infinite loop)
			
			if (m_file_info.m_find_data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			{
				if (is_self_or_parent_dir(m_file_info.m_find_data.cFileName))
				{
					continue;
				}
			}

			return 0;
		}
		else
		{
			break;
		}
	}

	return 1;
}

//
//
//

int drives_driver_t::fetch()
{
	// TODO: use GetLogicalDriveStrings or QueryDosDevice instead???

	int bits = sizeof(m_dwDrives) * 8;
	
	while (m_pos < bits)
	{
		m_pos++;

		if (m_pos < bits && m_dwDrives & (1 << m_pos))
		{
			//
			//
			//
			
			TCHAR szDrive[] = TEXT("?:\\");

			szDrive[0] = TEXT('A') + m_pos;

			//
			//
			//
			
#ifndef _WIN32_WCE

			// XXX: assumes structure can be zero-initialized (i.e. there are
			// no embedded string_t or other similar classes)
			
			ZERO_INIT(m_volume_info);

			m_geometry = "";
			m_layout = "";
			m_performance = "";

			m_volume_info.uType = GetDriveType(szDrive);

			bool check_drive = false;

			switch (m_volume_info.uType)
			{
				case DRIVE_UNKNOWN:
					break;
				
				case DRIVE_NO_ROOT_DIR:
					break;
				
				case DRIVE_REMOVABLE:
					break;
				
				case DRIVE_FIXED:
					check_drive = true;
					break;
				
				case DRIVE_REMOTE:
					break;
				
				case DRIVE_CDROM:
					break;
				
				case DRIVE_RAMDISK:
					break;

				default:
					DBG_INVALID_CASE();
					break;
			}

			//
			//
			//

			if (check_drive)
			{
				// TODO: do all of this only on-demand (when the values are actually
				// needed)

				io_volume_info(szDrive, 0x0000001F, m_volume_info);

				if (m_volume_info.disk_geometry_valid)
				{
					m_geometry += "bytes/sector=";
					m_geometry += size_to_string(m_volume_info.disk_geometry.BytesPerSector);
					m_geometry += ", sector/track=";
					m_geometry += size_to_string(m_volume_info.disk_geometry.SectorsPerTrack);
					m_geometry += ", track/cylinder=";
					m_geometry += size_to_string(m_volume_info.disk_geometry.TracksPerCylinder);
					m_geometry += ", cylinders=";
					m_geometry += variant_t(m_volume_info.disk_geometry.Cylinders.QuadPart).string();
				}
				else
				{
					m_geometry = FormatSystemMessage(m_volume_info.disk_geometry_error);
				}

				if (m_volume_info.disk_performance_valid)
				{
					m_performance = format_string("reads=%i",
						m_volume_info.disk_performance.ReadCount);
				}
				else
				{
					m_performance = FormatSystemMessage(m_volume_info.disk_performance_error);
				}

				if (m_volume_info.drive_layout_valid)
				{
					m_layout = format_string("partitions=%i",
						m_volume_info.drive_layout.info.PartitionCount);
				}
				else
				{
					m_layout = FormatSystemMessage(m_volume_info.drive_layout_error);
				}
			}

			return 0;

#endif

		}
	}

	return 1;
}

//
//
//

void io_volume_info(string_t szDrive, u32 flags, io_volume_info_t &info)
{
#if !defined(_WIN32_WCE)

	//
	//
	//

	string_t dir = szDrive;
	
	if (!ends_with(dir, "\\"))
		dir += "\\"; // put root directory backslash onboard

	//
	//
	//
	
	info.volinfo_valid = false;
	info.volinfo_error = 0;

	if (flags & 0x00000001)
	{
		// call Win32 API...
		
		if (GetVolumeInformation(dir, info.volume_name, DIM(info.volume_name),
			&info.volume_serial, &info.fs_max_comp_len, &info.fs_flags,
			info.fs_name, DIM(info.fs_name)))
		{
			info.volinfo_valid = true;
		}
		else
		{
			info.volinfo_error = GetLastError();
		}
	}

	//
	//
	//

	info.dfs_valid = false;
	info.dfs_error = 0;
	info.dfs_a = 0;
	info.dfs_b = 0;
	info.dfs_c = 0;

	if (flags & 0x00000002)
	{
		// REF: Article ID: Q137230

		// NOTE: not supported on Windows 95 before OSR 2

		get_addr(g_kernel32_exports[0]);

		if (g_kernel32_procs.GetDiskFreeSpaceExA != NULL)
		{
			ULARGE_INTEGER a, b, c;
			
			if (GetDiskFreeSpaceExA(dir, &a, &b, &c))
			{
				info.dfs_valid = true;
				info.dfs_a = a.QuadPart;
				info.dfs_b = b.QuadPart;
				info.dfs_c = c.QuadPart;
			}
			else
			{
				info.dfs_error = GetLastError();
			}
		}

		if (0)
		{
			// NOTE: On Windows 95, the existing Win32 function,
			// GetDiskFreeSpace, may obtain incorrect values for
			// volumes that are larger than 2 GB;

			// NOTE: Windows 95: lack of ability to accept UNC paths

			DWORD temp[4];
			
			if (GetDiskFreeSpace(dir, &temp[0], &temp[1], &temp[2], &temp[3]))
			{
				ULARGE_INTEGER bytes_per_cluster;

				bytes_per_cluster.QuadPart = temp[0];
				bytes_per_cluster.QuadPart *= temp[1];

				ULARGE_INTEGER capacity;
				
				capacity.QuadPart = bytes_per_cluster.QuadPart * temp[3];
				
				ULARGE_INTEGER freebytes;

				freebytes.QuadPart = bytes_per_cluster.QuadPart * temp[2];
			}
		}
	}

	//
	//
	//

	info.disk_geometry_valid = false;
	info.disk_performance_valid = false;
	info.drive_layout_valid = false;

	if (flags & 0x0000001C)
	{
		string_t device_name = string_t("\\\\.\\") + szDrive;
		
		HANDLE hDrive = CreateFile(device_name, 0, FILE_SHARE_WRITE | FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);

		if (hDrive != INVALID_HANDLE_VALUE)
		{
			DWORD cbReturned;
			
			//
			//  Get geometry information
			//
			
			if (flags & 0x00000004)
			{
				if (DeviceIoControl(hDrive, IOCTL_DISK_GET_DRIVE_GEOMETRY,
					NULL, 0, &info.disk_geometry,
					sizeof(info.disk_geometry), &cbReturned, NULL))
				{
					_ASSERT(cbReturned == sizeof(info.disk_geometry));
					
					if (cbReturned > 0)
					{
						info.disk_geometry_valid = true;
					}
				}
				else
				{
					info.disk_geometry_error = GetLastError();
				}
			}

			//
			//  Get partition layout
			//

			if (flags & 0x00000008)
			{
				if (DeviceIoControl(hDrive, IOCTL_DISK_GET_DRIVE_LAYOUT,
					NULL, 0, &info.drive_layout,
					sizeof(info.drive_layout), &cbReturned, NULL))
				{
					if (cbReturned > 0)
					{
						info.drive_layout_valid = true;
					}
				}
				else
				{
					info.drive_layout_error = GetLastError();
				}
			}

			//
			//  Get performance information
			//

			if (0x00000010)
			{
			// REF: http://msdn2.microsoft.com/en-us/library/aa363991.aspx

				struct DISK_PERFORMANCE__
				{
					LARGE_INTEGER BytesRead;
					LARGE_INTEGER BytesWritten;
					LARGE_INTEGER ReadTime;
					LARGE_INTEGER WriteTime;
					LARGE_INTEGER IdleTime;
					DWORD ReadCount;
					DWORD WriteCount;
					DWORD QueueDepth;

					// additional fields (XP???)...
					
					DWORD SplitCount;
					LARGE_INTEGER QueryTime;
					DWORD StorageDeviceNumber;
					WCHAR StorageManagerName[8];
				};

				union buffer_xxx_t
				{
					DISK_PERFORMANCE   perf1;
					DISK_PERFORMANCE__ perf2;
				};
				
				buffer_xxx_t buffer = { 0 };

				if (DeviceIoControl(hDrive, IOCTL_DISK_PERFORMANCE,
					NULL, 0, &buffer,
					sizeof(buffer), &cbReturned, NULL))
				{
//					_ASSERT(cbReturned == sizeof(info.disk_performance));

					DISK_PERFORMANCE__ perf2 = buffer.perf2;

					if (cbReturned > 0)
					{
						info.disk_performance_valid = true;

						info.disk_performance = buffer.perf1;
					}
				}
				else
				{
					// NOTE: XP will return an insufficient buffer error if
					// we pass the older version of the structure
					
					info.disk_performance_error = GetLastError();
				}
			}

			//
			//
			//

#if 0

			if (DeviceIoControl(hDrive, FSCTL_QUERY_USN_JOURNAL,
				NULL, 0, &xxx,
				sizeof(xxx), &cbReturned, NULL))


			if (DeviceIoControl(hDrive, FSCTL_READ_USN_JOURNAL,
				NULL, 0, &xxx,
				sizeof(xxx), &cbReturned, NULL))

			FSCTL_ENUM_USN_DATA

			READ_USN_JOURNAL_DATA 
			USN_JOURNAL_DATA 
			USN_RECORD 

#if 0

#define IOCTL_VOLUME_BASE   ((DWORD) 'V')

#define IOCTL_VOLUME_GET_VOLUME_DISK_EXTENTS    CTL_CODE(IOCTL_VOLUME_BASE, 0, METHOD_BUFFERED, FILE_ANY_ACCESS)

typedef struct _DISK_EXTENT {
    DWORD           DiskNumber;
    LARGE_INTEGER   StartingOffset;
    LARGE_INTEGER   ExtentLength;
} DISK_EXTENT, *PDISK_EXTENT;

typedef struct _VOLUME_DISK_EXTENTS {
    DWORD       NumberOfDiskExtents;
    DISK_EXTENT Extents[1];
} VOLUME_DISK_EXTENTS, *PVOLUME_DISK_EXTENTS;

#endif

#if 0

//		DISK_GEOMETRY dg = { 0 };

		{
			struct { DRIVE_LAYOUT_INFORMATION dli; PARTITION_INFORMATION extra[256]; } dli = { 0 };

			data_binding_t in = { type_code_none, 0, NULL };
			data_binding_t out = { type_code_none, sizeof(dli), &dli };

			if (device_io_control(info.device, IOCTL_DISK_GET_DRIVE_LAYOUT, in, out))
			{
				trace("IOCTL_DISK_GET_DRIVE_LAYOUT succeeded...\n");

//				hex_dump(out.data, out.size);
			}
		}

#endif

static bool device_io_control(io_stream_t &device, unsigned int code, data_binding_t in, data_binding_t out)
{
	// NOTE: type fields in data_binding_t are ignored
	
	HANDLE handle = file_handle(device);

	if (handle != NULL)
	{
		DWORD cbReturned = 0;
		
		if (DeviceIoControl(handle, code, in.data, in.size, out.data, out.size,
			&cbReturned, NULL))
		{
			_ASSERT(cbReturned <= out.size);

			out.size = cbReturned;

			return true;
		}
		else
		{
			DWORD err = GetLastError();
		}
	}

	return false;
}

	//
	//
	//
	
	string_t device_name = "\\\\.\\" + info.device_name;

	DWORD dwShareMode = FILE_SHARE_READ | FILE_SHARE_WRITE;

	device = file_CreateFile(device_name, GENERIC_READ, dwShareMode,
		NULL, OPEN_EXISTING, 0, NULL);


#endif

			//
			//
			//

			CloseHandle(hDrive);
		}
		else
		{
			info.disk_geometry_error = GetLastError();
			info.drive_layout_error = GetLastError();
			info.disk_performance_error = GetLastError();
		}
	}
#else
	DBG_NOT_IMPLEMENTED2("io_volume_info");
#endif
}

//
//
//

// depth-first traversal of filesystem...

static bool fetch_file(find_file_driver_t *driver, unsigned int max_depth)
{
	//
	//
	//
	
	file_entry_t *entry = driver->m_file_stack.tail();

	if (entry != NULL)
	{
		// skip the "." and ".." directory entries, to avoid infinite loop

		if (driver->m_file_info.m_find_data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			if (!is_self_or_parent_dir(driver->m_file_info.m_find_data.cFileName))
			{
				entry = NULL;
			}
		}
	}

	//
	//
	//
	
	if (entry == NULL)
	{
		const file_entry_t *prev = driver->m_file_stack.tail();

		if (max_depth == 0)
		{
			entry = driver->m_file_stack.create_item();
		}
		else
		{
			if (driver->m_file_stack.length() < max_depth)
			{
				entry = driver->m_file_stack.create_item();
			}
			else
			{
				entry = driver->m_file_stack.tail();
			}
		}
	
		if (entry != NULL)
		{
			if (prev == NULL)
			{
			}
			else
			{
				entry->m_filespec = fixup_find_filename(prev->m_filespec, driver->m_file_info.m_find_data.cFileName);

				// BUG: F:\*.txt only gets the .txt files in the root directory since directory
				// entries are skipped by the file spec

#if 0
				const char *last_slash = strrchr(prev->m_filespec, '\\');

				if (last_slash != NULL)
				{
					entry->m_filespec += last_slash;
				}
				else
				{
					DBG_BREAK();

					// not sure about this...

					entry->m_filespec += "\\*.*";
				}
#else

				entry->m_filespec += "\\*.*";

				// TODO: entry->m_filespec = file_path_append(entry->m_filespec, "*.*");
#endif

			}

//			entry->m_filespec = fixup_find_filename(entry->m_filespec, "*.*");
		}
	}

	//
	//
	//

	if (entry != NULL)
	{
		if (entry->m_handle == INVALID_HANDLE_VALUE)
		{
			entry->m_handle = FindFirstFileA(entry->m_filespec, &driver->m_file_info.m_find_data);

			if (entry->m_handle != INVALID_HANDLE_VALUE)
				return true;

			driver->m_file_stack.remove_item(entry);

			if (driver->m_file_stack.tail() != NULL)
				return fetch_file(driver, max_depth);
		}
		else
		{
			if (FindNextFileA(entry->m_handle, &driver->m_file_info.m_find_data))
				return true;

			DWORD dwError = GetLastError();
				
			if (dwError == ERROR_NO_MORE_FILES)
			{
			}
				
			driver->m_file_stack.remove_item(entry);

			if (driver->m_file_stack.tail() != NULL)
				return fetch_file(driver, max_depth);
		}
	}

	return false;
}

//
//
//

static variant_t convert_time(const FILETIME &ref)
{
	// NOTE:  FindFirstFile/FindNextFile set the FILETIME members to zero if
	// the file system containing the file does not support this time member
	
	if (ref.dwLowDateTime == 0 && ref.dwHighDateTime == 0)
		return variant_t();

	return time_from_filetime_utc(ref);
}

//
//
//

static variant_t convert_size(DWORD nFileSizeLow, DWORD nFileSizeHigh)
{
	LARGE_INTEGER li;

	li.LowPart  = nFileSizeLow;
	li.HighPart = nFileSizeHigh;

	if (0)
	{
		return li.QuadPart;
	}
	else
	{
#ifdef _DEBUG
		return size_to_string(li.QuadPart);
#endif

		return li.QuadPart;
	}
}

static variant_t file_security_description(string_t filename)
{
#ifdef _DEBUG

#ifdef _MSC_VER

#ifndef _WIN32_WCE

	if (0)
	{
		char desc[256];
		
		DWORD needed = sizeof(desc);

		string_t ans;

		if (InitializeSecurityDescriptor((PSECURITY_DESCRIPTOR*)desc,
			SECURITY_DESCRIPTOR_REVISION))
		{
			if (GetFileSecurity(filename, OWNER_SECURITY_INFORMATION, desc, sizeof(desc), &needed))
			{
				if (needed == 0)
				{
					PSID psidOwner;
					
					BOOL b;

					if (GetSecurityDescriptorOwner((PSECURITY_DESCRIPTOR*)desc, &psidOwner, &b))
					{
						string_t owner = get_sid_user_name(psidOwner);

						ans += "owner: " + owner;
					}
				}
			}

			needed = sizeof(desc);
			
			if (GetFileSecurity(filename, GROUP_SECURITY_INFORMATION, desc, sizeof(desc), &needed))
			{
				if (needed == 0)
				{
					PSID psidGroup;
					
					BOOL b;
					
#ifdef _MSC_VER
//					if (GetSecurityDescriptorGroup((PSECURITY_DESCRIPTOR*)desc, &psidGroup, &b))
					if (GetSecurityDescriptorGroup((SECURITY_DESCRIPTOR*)desc, &psidGroup, &b))
#else
					if (GetSecurityDescriptorGroup((SECURITY_DESCRIPTOR*)desc, &psidGroup, &b))
#endif
					{
						string_t group = get_sid_user_name(psidGroup);

						ans += "group: " + group;
					}
				}
			}

			char acl[256];

			InitializeAcl((ACL*)acl, sizeof(acl), ACL_REVISION);
			
			BOOL b;
			
			if (GetSecurityDescriptorSacl((PSECURITY_DESCRIPTOR*)desc, &b, (ACL**)acl, &b))
			{
			}
		}

		return ans;
	}
	else
#endif
	{
		return variant_error("Not Supported");
	}

#endif
	
#else

	return variant_error("Not Supported");

#endif
}

//
//
//

#ifndef REMOVE_THIS

variant_t find_file_driver_t::evaluate_field(long i)
{
	if (i == 0)
	{
		if (0)
		{
			return m_file_info.m_find_data.dwFileAttributes;
		}
		else
		{
			string_t result;

			DWORD dw = m_file_info.m_find_data.dwFileAttributes;

			if (dw & FILE_ATTRIBUTE_READONLY)
				result += "R";

			if (dw & FILE_ATTRIBUTE_HIDDEN)
				result += "H";

			if (dw & FILE_ATTRIBUTE_SYSTEM)
				result += "S";

			if (dw & FILE_ATTRIBUTE_DIRECTORY)
				result += "";

			if (dw & FILE_ATTRIBUTE_ARCHIVE)
				result += "A";

			if (dw & FILE_ATTRIBUTE_ENCRYPTED)
				result += "E";

			if (dw & FILE_ATTRIBUTE_COMPRESSED)
				result += "";

			return result;
		}
	}
	
	if (i == 1)
		return convert_time(m_file_info.m_find_data.ftCreationTime);
	
	if (i == 2)
		return convert_time(m_file_info.m_find_data.ftLastAccessTime);
	
	if (i == 3)
		return convert_time(m_file_info.m_find_data.ftLastWriteTime);
	
	if (i == 4)
		return convert_size(m_file_info.m_find_data.nFileSizeLow,
			m_file_info.m_find_data.nFileSizeHigh);
	
	if (i == 5)
		return m_file_info.m_find_data.cFileName;
	
	if (i == 6)
	{
#ifndef _WIN32_WCE
		return m_file_info.m_find_data.cAlternateFileName;
#endif
	}
	
	if (i == 7)
	{
		string_t filespec;
		
		const file_entry_t *entry = m_file_stack.tail();

		if (entry != NULL)
		{
			filespec = entry->m_filespec;
		}
		
		string_t filename = fixup_find_filename(filespec, m_file_info.m_find_data.cFileName);

		return file_security_description(filename);
	}
	
	if (i == 8)
	{
		string_t filespec;
		
		const file_entry_t *entry = m_file_stack.tail();

		if (entry != NULL)
		{
			filespec = entry->m_filespec;
		}

		return fixup_find_filename(filespec, m_file_info.m_find_data.cFileName);
	}

#ifdef _DEBUG

	if (i == 9)
	{
		if (match_file_extension(m_file_info.m_find_data.cFileName, "mp3"))
		{
			return "???";
		}
		else
		{
			return variant_t();
		}
	}

#endif

	return base_db_cursor_t::evaluate_field(i);
}

#endif

//
//
//

#ifndef REMOVE_THIS

variant_t drives_driver_t::evaluate_field(long i)
{
#ifndef _WIN32_WCE

	if (i == 0)
	{
#if 0
		TCHAR szDrive[] = TEXT("?:\\");

		szDrive[0] = TEXT('A') + m_pos;

		return szDrive;
#else
		return print_to_string((char)('A' + m_pos), ":\\");
#endif
	}
	else if (i == 1)
	{
		switch (m_volume_info.uType)
		{
			case DRIVE_UNKNOWN:     return "DRIVE_UNKNOWN";
			case DRIVE_NO_ROOT_DIR: return "DRIVE_NO_ROOT_DIR";
			case DRIVE_REMOVABLE:   return "DRIVE_REMOVABLE";
			case DRIVE_FIXED:       return "DRIVE_FIXED";
			case DRIVE_REMOTE:      return "DRIVE_REMOTE";
			case DRIVE_CDROM:       return "DRIVE_CDROM";
			case DRIVE_RAMDISK:     return "DRIVE_RAMDISK";
		}

		return m_volume_info.uType;
	}
	else if (i == 2)
	{
		if (m_volume_info.volinfo_valid)
		{
			return m_volume_info.volume_name;
		}
		else
		{
		}
	}
	else if (i == 3)
	{
		if (m_volume_info.volinfo_valid)
		{
			if (0)
			{
				return m_volume_info.volume_serial;
			}
			else
			{
				return format_string("%04X-%04X",
					HIWORD(m_volume_info.volume_serial),
					LOWORD(m_volume_info.volume_serial));
			}
		}
		else
		{
		}
	}
	else if (i == 4)
	{
		if (m_volume_info.volinfo_valid)
		{
			return m_volume_info.fs_name;
		}
		else
		{
		}
	}
	else if (i == 5)
	{
		if (m_volume_info.dfs_valid)
		{
			return m_volume_info.dfs_a;
		}
	}
	else if (i == 6)
	{
		if (m_volume_info.dfs_valid)
		{
			return m_volume_info.dfs_b;
		}
	}
	else if (i == 7)
	{
		if (m_volume_info.dfs_valid)
		{
			return m_volume_info.dfs_c;
		}
	}
	else if (i == 8)
	{
		return m_geometry;
	}
	else if (i == 9)
	{
		return m_layout;
	}
	else if (i == 10)
	{
		return m_performance;
	}

#endif

	return base_db_cursor_t::evaluate_field(i);
}

#endif

//
//
//

bool find_file_driver_t::close()
{
	ZERO_INIT(m_file_info.m_find_data);
//	ZERO_INIT(m_file_info.m_file_version);

//	m_file_info.m_file_version_valid = false;
	
	m_file_stack.clear();

	return true;
}

//
//
//

bool drives_driver_t::close()
{
	return true;
}

//
//
//

file_entry_t::~file_entry_t()
{
	if (m_handle != INVALID_HANDLE_VALUE)
		VERIFY(FindClose(m_handle));
}

//
//
//

const db_column_binding_info_t *find_file_driver_t::get_table_info() const
{
	return &db_find_file_columns()->cols;
}

//
//
//

const db_column_binding_info_t *drives_driver_t::get_table_info() const
{
	return &db_drives_columns()->cols;
}

//
//
//

bool get_win32_find_data(db_cursor_t &cursor, WIN32_FIND_DATAA &data)
{
	find_file_driver_t *driver = db_cursor_cast<find_file_driver_t>(cursor);

	if (driver != NULL)
	{
		const file_entry_t *entry = driver->m_file_stack.tail();

		if (entry != NULL)
		{
			string_t filename = fixup_find_filename(entry->m_filespec, driver->m_file_info.m_find_data.cFileName);

			data = driver->m_file_info.m_find_data;

			art_snprintf(data.cFileName, DIM(data.cFileName), "%s", (const char *)filename);

			return true;
		}
	}

	return false;
}

WIN32_FIND_DATA win32_find_data(db_cursor_t cursor)
{
	WIN32_FIND_DATA data = { 0 };

	get_win32_find_data(cursor, data);

	return data;
}

#ifndef REMOVE_EXPERIMENTAL

io_system_t win_io_system()
{
	DBG_NOT_IMPLEMENTED();

	return io_system_error();
}

#endif

#endif

//
//
//

string_t io_current_directory()
	{ return path(path_id_current); }

string_t io_directory_current()
	{ return path(path_id_current); }

//
//
//

io_status_t file_last_write_date_local(string_t filename, art_time_t &t)
{
	art_time_t temp;
	
	io_status_t ret = file_last_write_date_utc(filename, temp);

	if (!ret)
		return ret;

	t = time_local(temp);

	return true;
}

//
//
//

db_cursor_t db_drives()
{
	db_cursor_t cursor;
	
	if (db_drives_internal(cursor))
		return cursor;

	return db_cursor_error();
}

db_cursor_t db_find_file(string_t filespec, unsigned int max_depth)
{
	db_cursor_t cursor;
	
	if (db_find_file_internal(filespec, max_depth, cursor))
		return cursor;

	return db_cursor_error();
}

