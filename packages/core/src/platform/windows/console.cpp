/*------------------------------------------------------------------------------

	platform/windows/console.cpp - Windows console support routines

------------------------------------------------------------------------------*/

#include "pch.h"
#include <signal.h>

//
//
//

static volatile struct { int flag; int sig; DWORD dwCtrlType; sync_event_t *event; } g_signal_context;

//
//
//

#if !defined(_WIN32_WCE)

static BOOL WINAPI ctrl_handler(DWORD dwCtrlType)
{
	// NOTE: initially, the console handler chain for a process contains only a
	// default handler that calls ExitProcess

	BOOL status = FALSE; // call next handler

	switch (dwCtrlType)
	{
		case CTRL_C_EVENT:
		{
			g_signal_context.flag = 1;
			g_signal_context.dwCtrlType = dwCtrlType;

			if (g_signal_context.event != NULL)
				g_signal_context.event->signal();

			status = TRUE; // don't call next handler

			break;
		}

		case CTRL_BREAK_EVENT:
		{
			break;
		}

		case CTRL_CLOSE_EVENT:
		{
			g_signal_context.flag = 1;
			g_signal_context.dwCtrlType = dwCtrlType;

			if (g_signal_context.event != NULL)
				g_signal_context.event->signal();

			status = TRUE; // don't call next handler

			break;
		}

		case CTRL_LOGOFF_EVENT:
		{
			break;
		}

		case CTRL_SHUTDOWN_EVENT:
		{
			break;
		}

		default:
		{
#ifdef _DEBUG
			message("unrecognized console control type ", dwCtrlType, "\n");
#endif
			break;
		}
	}

	return status;
}

#endif

//
//
//

// BUG: beware of sharing stdio between this process and the command shell,
// since, if the application image is for the Windows subsystem, cmd.exe will
// not wait for the process to exit before returning to its command prompt

// defined in wincon.h

static struct
{
	BOOL (WINAPI * AttachConsole)(DWORD dwProcessId);
} g_kernel32_procs;

#ifndef ATTACH_PARENT_PROCESS
#define ATTACH_PARENT_PROCESS ((DWORD)-1)
#endif

int console_setup()
{
	int status = 0;
	
	HANDLE hStdIn = GetStdHandle(STD_INPUT_HANDLE);
	HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	HANDLE hStdErr = GetStdHandle(STD_ERROR_HANDLE);

	if (hStdIn != NULL)
	{
	}

	if (hStdIn == INVALID_HANDLE_VALUE)
	{
		DWORD error = GetLastError();
	}

	// beware if the handle is synchronous and has an outstanding operation
	// (e.g. it's shared stdin and another process is blocking with a pending
	// read operation), GetFileType will block!

	DWORD type = GetFileType(hStdOut);

	if (type == FILE_TYPE_UNKNOWN)
	{
		// output not connected

		DWORD error = GetLastError();

		if (error == NO_ERROR)
		{
		}
		else
		{
		}
	}
	else
	{
		// output already connected to something

		return 1;
	}
		
	if (0)
	{
		DWORD mode = 0;

		if (GetConsoleMode(hStdOut, &mode))
		{
		}
	}

	g_kernel32_procs.AttachConsole = (BOOL(WINAPI*)(DWORD))(
		GetProcAddress(GetModuleHandle(TEXT("kernel32")), "AttachConsole"));

	if (g_kernel32_procs.AttachConsole != NULL)
	{
		if (g_kernel32_procs.AttachConsole(ATTACH_PARENT_PROCESS))
		{
#ifdef _DEBUG
			TRACE("attached to parent console\n");
#endif

			goto setup_stdio;
		}
		else
		{
			DWORD error = GetLastError();
		}
	}

	if (AllocConsole())
	{
#ifdef _DEBUG
		TRACE("AllocConsole: succeeded\n");
#endif
	}
	else
	{
#ifdef _DEBUG
		TRACE("AllocConsole: failed\n");
#endif

		return 0;
	}

setup_stdio:

#if !defined(_WIN32_WCE)
//	SetConsoleCtrlHandler(console_control_handler, TRUE);
#endif

	return 1;
}

//
//
//

#ifndef REMOVE_EXPERIMENTAL

#ifndef LITE_RTL

int console_line__(int *set)
{
	HANDLE h = GetStdHandle(STD_ERROR_HANDLE);

	CONSOLE_SCREEN_BUFFER_INFO info = { 0 };

	// according to docs, all coordinates are expressed relative to the
	// upper-left character cell, defined as 0,0
	
	if (GetConsoleScreenBufferInfo(h, &info))
	{
		_ASSERT(info.dwCursorPosition.Y >= 0);

		if (set != NULL && *set > 0)
		{
			COORD dwCursorPosition = { 0, *set - 1 };
		
			if (SetConsoleCursorPosition(h, dwCursorPosition))
			{
			}
			else
			{
				// error is silently ignored
			}
		}

		return info.dwCursorPosition.Y + 1;
	}

	return 0;
}

void console_line_clear(int l1, int l2)
{
	_ASSERT(l1 > 0);
	_ASSERT(l2 >= l1);
	
	HANDLE h = GetStdHandle(STD_ERROR_HANDLE);

	CONSOLE_SCREEN_BUFFER_INFO info = { 0 };

	if (GetConsoleScreenBufferInfo(h, &info))
	{
		DWORD out;
		COORD coord = { 0, l1 - 1 };
		DWORD length = info.dwSize.X * (1 + l2 - l1);
		FillConsoleOutputCharacter(h, ' ', length, coord, &out);
		FillConsoleOutputAttribute(h, info.wAttributes, length, coord, &out);
	}
}

#endif

#endif

//
//
//

static void ART_CALLTYPE_CDECL signal_handler(int sig)
{
	g_signal_context.flag = 1;
	g_signal_context.sig = sig;

	if (g_signal_context.event != NULL)
		g_signal_context.event->signal();
}

void console_init()
{
	SetConsoleCtrlHandler(ctrl_handler, TRUE);
}

void console_signal(int sig)
{
	signal(sig, signal_handler);
}

int console_stat()
{
	return g_signal_context.flag;
}

