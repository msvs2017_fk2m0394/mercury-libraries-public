/*------------------------------------------------------------------------------

	platform/windows/io_stream.cpp - Windows I/O support

------------------------------------------------------------------------------*/

#include "pch.h"
#include <mercury/experimental/var.h>
#include "../../lib/io/io_address.h"
#include "io.h"

#ifndef LITE_RTL

static global_thing_t<win32_io_device_t> g_win_stdin;
static global_thing_t<win32_io_device_t> g_win_stdout;
static global_thing_t<win32_io_device_t> g_win_stderr;

static file_t helper3(global_thing_t<win32_io_device_t> &obj, HANDLE handle);

// NOTE: these are here so we can get access to the standard I/O handles in
// *binary* mode, since the CRT does CRLF translation on standard handles

// BUG: the standard handles could be NULL (instead of INVALID_HANDLE_VALUE),
// which causes an error in CloseHandle during cleanup

// NOTE: Windows allows STD_INPUT_HANDLE, etc. to be used in place of a file
// handle in the file APIs.  If we want to alias the process standard handles
// then we could set the m_hFile to STD_INPUT_HANDLE, etc.

file_t io_stdin_win()
{
	HANDLE handle = GetStdHandle(STD_INPUT_HANDLE);
//	HANDLE handle = STD_INPUT_HANDLE;

	return helper3(g_win_stdin, handle);
}

file_t io_stdout_win()
{
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
//	HANDLE handle = STD_OUTPUT_HANDLE;

	return helper3(g_win_stdout, handle);
}

file_t io_stderr_win()
{
	HANDLE handle = GetStdHandle(STD_ERROR_HANDLE);
//	HANDLE handle = STD_ERROR_HANDLE;

	return helper3(g_win_stderr, handle);
}

//
//
//

static file_t helper3(global_thing_t<win32_io_device_t> &obj, HANDLE handle)
{
	// XXX: not thread-safe

	if (obj.pointer == NULL)
	{
		if (handle != NULL)
		{
			_ASSERT(handle != INVALID_HANDLE_VALUE);

			DWORD dwFileType = GetFileType(handle);

			DWORD dwConsoleMode = -1;

			if (!GetConsoleMode(handle, &dwConsoleMode))
				dwConsoleMode = -1;

#ifdef _DEBUG
			trace("io_std*_win: handle=", format_string("%p", handle),
				", GetFileType=", dwFileType, ", GetConsoleMode=", format_string("%08X", dwConsoleMode),
				"\n");
#endif

			if (dwFileType == FILE_TYPE_UNKNOWN)
			{
/*
				e.g. if a GUI app is started from a console app without any
				redirection of stdio, then we can get handle=00000003 (zeroth console handle), GetFileType=0

				with redirection we can get a valid handle value, where GetFileType
				returns FILE_TYPE_DISK, FILE_TYPE_CHAR, or FILE_TYPE_PIPE

				if a GUI app is started from the explorer, then we can get
				handle=00000000

*/
//				return NULL;
			}
			else
			{
				obj.pointer = new win32_io_device_t(handle, NULL, "");
			}
		}
		else
		{
#ifdef _DEBUG
			trace("io_std*_win: handle=", format_string("%p", handle), "\n");
#endif

//			return NULL;
		}
	}
	
	win32_io_device_t *device = obj.pointer;
	
	if (device != NULL)
	{
/*
		if (device->m_io_stream != NULL)
		{
			return file_t(device->m_io_stream);
		}
*/

		file_t file = io_stream_create_and_release(device);

		if (is_valid(file))
		{
			_ASSERT(file.m_object == device->m_io_stream);

			device->m_io_stream->status = io_resource_status_open;
			const char *temp = strrchr(device->m_pathname, '\\');
			temp = temp ? temp + 1 : NULL;
//			device->m_io_stream->filename = temp; // N.B. this creates an alias into the m_pathname buffer
//			device->m_io_stream->address->m_object->type = NULL;
//			device->m_io_stream->address = NULL;

			io_address_object_t *addr = NULL;

			if (addr != NULL)
			{
				addr->m_type = NULL;
			}
		}

		return file;
	}
	else
	{
		return io_stream_error();
	}
}

#endif

