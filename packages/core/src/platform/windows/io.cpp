/*------------------------------------------------------------------------------

	platform/windows/io.cpp - Windows I/O support

	- file system
	- stream
	- standard I/O handles

------------------------------------------------------------------------------*/

#include "pch.h"

#include <winioctl.h>

#include "core.h"

#include "../../lib/io/io.h"

#include "io.h"

//
//
//

#ifndef LITE_RTL

//
//
//

void report_error(DWORD dwError, LPCSTR pszProcName)
{
#ifdef _DEBUG
	if (pszProcName == NULL)
		pszProcName = "[unspecified procedure]";

	string_t err_msg = FormatSystemMessage(dwError);

	TRACE("I/O: ERROR: %s failed, error code %08X (%s)\n",
		pszProcName, dwError, get_error_text(err_msg));
#endif
}

//
//
//

file_t file_last_error()
{
	DWORD dwError = GetLastError();
	
	variant_t text = FormatSystemMessage(dwError);

	error_log_message(trace_severity_error, text);

	return io_stream_error(text);
}

//
//
//

static string_t last_error()
{
	return FormatSystemMessage(GetLastError());
}

#ifndef _MAX_PATH
#define _MAX_PATH MAX_PATH
#endif

string_t path_internal(path_id_t id)
{
	// XXX: depends in UNICODE precompiler symbol!!!
	TCHAR szBuffer[_MAX_PATH];

	switch (id)
	{
		case path_id_current:
		{
#ifndef _WIN32_WCE
			if (GetCurrentDirectory(DIM(szBuffer), szBuffer))
				return szBuffer;
			else
				return last_error();
#endif

			break;
		}

		default:
		{
			break;
		}
	}

	return string_error();
}

//
//
//

string_t io_full_path(string_t str)
{
#if !defined(_WIN32_WCE)

#if 1
	if (is_error(str))
		return str;
	
	TCHAR szBuffer[_MAX_PATH];
	LPTSTR lpFilePart;
	
	// merges the name with the current working directory
	
	if (GetFullPathName(str, DIM(szBuffer), szBuffer, &lpFilePart) == 0)
		return FormatSystemMessage(GetLastError());

	if (str != szBuffer)
		return string_t(szBuffer); // make a copy of the buffer
	else
		return str;
#else
	if (is_ansi(str))
	{
		TCHAR szBuffer[_MAX_PATH];
		LPTSTR lpFilePart;
		
		if (GetFullPathNameA(str, DIM(szBuffer), szBuffer, &lpFilePart) == 0)
			return FormatSystemMessage(GetLastError());

		if (str != szBuffer)
			return string_t(szBuffer); // make a copy of the buffer
	}
	else if (is_unicode(str))
	{
		WCHAR szBuffer[_MAX_PATH];
		LPWSTR lpFilePart;
		
		if (GetFullPathNameW(str, DIM(szBuffer), szBuffer, &lpFilePart) == 0)
			return FormatSystemMessage(GetLastError());

		if (str != szBuffer)
			return string_t(szBuffer); // make a copy of the buffer
	}

	return str;
#endif

#else

	return str;

#endif
}

//
//
//

string_t file_path(string_t filename)
{
	if (is_error(filename))
		return filename;

#if !defined(_WIN32_WCE)

	// XXX: depends in UNICODE precompiler symbol
	
	TCHAR szBuffer[_MAX_PATH];
	LPTSTR lpFilePart;
	
	// GetFullPathName merges the current drive and directory with the
	// specified filename to determine the full path and filename of the
	// specified file.

	if (GetFullPathName(filename, DIM(szBuffer), szBuffer, &lpFilePart) == 0)
		return FormatSystemMessage(GetLastError());

	u32 attributes;
	
	io_status_t ret = file_attributes(szBuffer, attributes);
	
	if (ret)
	{
		if (attributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			// return directory since lpFilePart will strip off the last directory component otherwise...
			
			return string_t(szBuffer);
		}
	}

	if (lpFilePart != NULL)
	{
		_ASSERT(lpFilePart > szBuffer && lpFilePart < szBuffer + DIM(szBuffer));

		_ASSERT(lpFilePart[-1] == '\\');
		
		return string(szBuffer, lpFilePart - szBuffer);
	}

	return string_t(szBuffer);

#else

	u32 attributes;
	
	io_status_t ret = file_attributes(filename, attributes);
	
	if (ret)
	{
		if (attributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			return filename;
		}
	}
	
	if (is_ansi(filename))
	{
		const char *lpFilePart = strrchr(filename, '\\');
		
		if (lpFilePart == NULL)
			lpFilePart = strrchr(filename, '/');
		
		if (lpFilePart != NULL)
		{
			return left(filename, lpFilePart - (const char *)filename);
		}
	}
	else if (is_unicode(filename))
	{
		const wchar_t *lpFilePart = wcsrchr(filename, L'\\');
		
		if (lpFilePart == NULL)
			lpFilePart = wcsrchr(filename, L'/');
		
		if (lpFilePart != NULL)
		{
			return left(filename, lpFilePart - (const wchar_t *)filename);
		}
	}

	return filename;

#endif
}

//
//
//

static lock_t g_full_path_lock;

// XXX: this only works for cases where the filenames are valid names within
// the file system and is not useful for deriving canonical paths for files
// that don't yet exist

string_t full_path_name(string_t filename, string_t base)
{
	if (is_error(filename) || is_empty(filename))
		return filename;

	// inherently not thread-safe (uses per-process state), but we can at least
	// make this function safe to use if nobody's setting the current directory
	// elsewhere

	g_full_path_lock.enter();

	TCHAR prev[MAX_PATH] = TEXT("");

	GetCurrentDirectory(DIM(prev), prev);

	if (base != "")
		SetCurrentDirectory(file_path(base));

	string_t filename2 = io_full_path(filename);

	if (base != "")
		SetCurrentDirectory(prev);

	g_full_path_lock.leave();

	return filename2;
}

#endif

