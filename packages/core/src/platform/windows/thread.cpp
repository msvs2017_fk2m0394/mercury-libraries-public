/*------------------------------------------------------------------------------

	thread.cpp - Windows thread support

------------------------------------------------------------------------------*/

#include "pch.h"
#include "core.h"

#if defined(_MSC_VER) && !defined(_WIN32_WCE)
#include <process.h> // threads
#endif

#include "../../crt/crt.h"

void string_exit_dump();
void array_exit_dump();
void object_exit_dump();

#ifndef LITE_RTL

#ifndef REMOVE_THIS
static lock_data_t g_tls_lock_data;
static int         g_tls_index_status;
static DWORD       g_tls_index;
static ref_count_t g_tls_index_usage;
#endif

static u32 g_thread_id_core; // last issued ID

void cleanup_external_ts(core_thread_state_t *thread);
bool trace_task_thread_begin();
void trace_task_thread_end(bool x);
void run(core_thread_state_t *thread);
void free_task_thread(win_core_thread_state_t *thread);

// XREF: g_debug_trace_dllmain

#ifdef _DEBUG
static bool g_debug_trace_ts = false;
static bool g_debug_trace_tls = false; // can't use this (currently causes stack overflow)
#endif

static int g_in_core_term_process;

//
//
//

OBJECT_CLASS_BEGIN(win_core_thread_state_t, core_thread_state_t)
OBJECT_CLASS_END()

//
//
//

static core_thread_state_t *create_core_thread_state(DWORD dwTlsIndex);
static void ART_CALLTYPE_CDECL task_thread_proc(void *data);

core_thread_state_t *core_thread_state2(bool create)
{
	core_thread_state_t *thread;

	//
	//
	//

	// TODO: support Fls* functions for fibers???

/*
#ifdef _DEBUG
#ifndef _WIN32_WCE

	if (0)
	{
		// TODO: figure out how to assert if we're running on a fiber since
		// we don't support fibers!!!

		PVOID fiber = GetCurrentFiber();

		if (0)
		{
			PVOID data = GetFiberData(); // this will crash if we're not a fiber
		}

		get_addr(g_export_fiber);

		// XXX: Requires Vista or Windows Server 2003

		if (g_kernel32_procs.FlsGetValue != NULL)
		{
			void *data = g_kernel32_procs.FlsGetValue(0);
		}
	}

#endif
#endif
*/
	//
	//
	//

	// allocate TLS index...

	int tls_index_status;
	DWORD dwTlsIndex;

	if (!create)
	{
		dwTlsIndex = g_tls_index; // warning C28112: A variable (g_tls_index) which is accessed via an Interlocked function must always be accessed via an Interlocked function
		tls_index_status = g_tls_index_status;
	}
	else
	{
		// XREF: wire_thread_data()

		// FIXME: this is likely to be a HUGE bottleneck!!!!!!!!!!!!!!!!!

		lock_enter(g_tls_lock_data);

		if (g_tls_index_status == 0)
		{
			g_tls_index = TlsAlloc(); // warning C28112: A variable (g_tls_index) which is accessed via an Interlocked function must always be accessed via an Interlocked function

#ifdef _DEBUG
			if (g_debug_trace_tls)
			{
				TRACE("TlsAlloc()=%i\n", g_tls_index); // warning C28112: A variable (g_tls_index) which is accessed via an Interlocked function must always be accessed via an Interlocked function
			}
#endif

#if !defined(_WIN32_WCE)

			if (g_tls_index == TLS_OUT_OF_INDEXES) // warning C28112: A variable (g_tls_index) which is accessed via an Interlocked function must always be accessed via an Interlocked function
			{
				g_tls_index_status = -1;
			}
			else
#endif

			{
				g_tls_index_status = 1;
			}
		}

		dwTlsIndex = g_tls_index; // warning C28112: A variable (g_tls_index) which is accessed via an Interlocked function must always be accessed via an Interlocked function
		tls_index_status = g_tls_index_status;

		lock_leave(&g_tls_lock_data);

		if (0)
		{
			// the Windows CE runtime does something like this instead...

#define INVALID_TLS_INDEX ((DWORD)0xFFFFFFFF)

			if (g_tls_index == INVALID_TLS_INDEX) // warning C28112: A variable (g_tls_index) which is accessed via an Interlocked function must always be accessed via an Interlocked function
			{
				dwTlsIndex = TlsAlloc();

#if (WINVER >= 0x0500)
				if (InterlockedCompareExchange((LPLONG)&g_tls_index, (LONG)dwTlsIndex, (LONG)INVALID_TLS_INDEX) != INVALID_TLS_INDEX)
				{
					TlsFree(dwTlsIndex);
				}
#endif
			}

			dwTlsIndex = g_tls_index; // warning C28112: A variable (g_tls_index) which is accessed via an Interlocked function must always be accessed via an Interlocked function
		}
	}

	//
	//
	//

	if (tls_index_status == 1)
	{
		thread = (core_thread_state_t*)TlsGetValue(dwTlsIndex);

		if (thread == NULL && create)
		{
			thread = create_core_thread_state(dwTlsIndex);
		}

		return thread;
	}

	return NULL;
}

//
//
//

static core_thread_state_t *create_core_thread_state(DWORD dwTlsIndex)
{
#if !defined(_WIN32_WCE)
	_ASSERT(dwTlsIndex != TLS_OUT_OF_INDEXES);
#else
	DBG_NOT_IMPLEMENTED();
#endif

	win_core_thread_state_t *thread = new win_core_thread_state_t;

	if (thread != NULL)
	{
		if (TlsSetValue(dwTlsIndex, thread))
		{
			xinc(g_tls_index_usage);

			thread->thread_id_host = current_thread_id();

			thread->thread_id_core = xinc(g_thread_id_core);

#ifdef _DEBUG
			if (0)
			{
				// can cause deadlock?!?!?!?

				TRACE("core thread state allocated: tls=%i, thread=%i, data=%p\n",
					dwTlsIndex, thread->thread_id_host, thread);
			}
#endif
		}
		else
		{
			DWORD dwError = GetLastError();

			DBG_BREAK();

			thread->release();

			thread = NULL;
		}
	}

	return thread;
}

//
//
//

bool wire_thread_data(win_core_thread_state_t *thread)
{
	_ASSERT(thread != NULL);

	//
	//
	//

	int tls_index_status;
	DWORD dwTlsIndex;

	// FIXME: this is likely to be a HUGE bottleneck!!!!!!!!!!!!!!!!!

	lock_enter(g_tls_lock_data);

	if (g_tls_index_status == 0)
	{
		g_tls_index = TlsAlloc();

#ifdef _DEBUG
		if (g_debug_trace_tls)
		{
			TRACE("TlsAlloc()=%i\n", g_tls_index);
		}
#endif

#if !defined(_WIN32_WCE)

		if (g_tls_index == TLS_OUT_OF_INDEXES)
		{
			g_tls_index_status = -1;
		}
		else
#else

		DBG_NOT_IMPLEMENTED();

#endif

		{
			g_tls_index_status = 1;
		}
	}

	dwTlsIndex = g_tls_index;
	tls_index_status = g_tls_index_status;

	lock_leave(&g_tls_lock_data);

	//
	//
	//

	if (tls_index_status == 1)
	{
#if !defined(_WIN32_WCE)
		_ASSERT(dwTlsIndex != TLS_OUT_OF_INDEXES);
#endif

		if (0)
		{
			_ASSERT(TlsGetValue(dwTlsIndex) == NULL);
		}

		if (TlsSetValue(dwTlsIndex, thread))
		{
			xinc(g_tls_index_usage);

			thread->thread_id_host = current_thread_id();

			thread->thread_id_core = xinc(g_thread_id_core);

#ifdef _DEBUG
			TRACE("core thread state wired: tls=%i, thread=%i, data=%p\n",
				dwTlsIndex, thread->thread_id_host, thread);
#endif

			return true;
		}
		else
		{
			DWORD dwError = GetLastError();

			DBG_BREAK();
		}
	}

	return false;
}

static DWORD WINAPI ThreadStart(LPVOID lpParameter)
{
	task_thread_proc(lpParameter);

	return 0;
}

bool create_thread(win_core_thread_state_t *thread)
{
	_ASSERT(thread != NULL);

#if defined(_MSC_VER) && !defined(_WIN32_WCE)

	uintptr_t h = _beginthread(task_thread_proc, 0, thread);

	if (h != -1)
	{
		thread->thread_handle = (HANDLE)h;

		return true;
	}

#else

	DWORD dwIDThread = 0;

	HANDLE hThread = CreateThread(NULL, 0, ThreadStart, thread, CREATE_SUSPENDED, &dwIDThread);

	if (hThread != NULL)
	{
		thread->thread_handle = hThread;
		thread->thread_id_host = dwIDThread;
		thread->thread_id_core = xinc(g_thread_id_core);

		ResumeThread(hThread);

		return true;
	}

#endif

	return false;
}

//
//
//

static void ART_CALLTYPE_CDECL task_thread_proc(void *data)
{
	win_core_thread_state_t *thread = (win_core_thread_state_t*)data;

	_ASSERT(thread != NULL);

	if (wire_thread_data(thread))
	{
		//
		//
		//

		bool x = trace_task_thread_begin();

		run(thread);

		trace_task_thread_end(x);

		//
		//
		//

/*
		if (thread->m_queue != NULL)
		{
			thread->m_queue->lock();

			thread->m_queue->m_threads.remove_item(thread);

			thread->m_queue->unlock();
		}
		else
*/
		{
			free_task_thread(thread);
		}
	}
	else
	{
		DBG_NOT_IMPLEMENTED();
	}

	//
	//
	//

	// NOTE: this call is placed here, just before the thread returns control
	// to the OS (or to the CRT), instead of handling this only when
	// processing the DLL_THREAD_DETACH notification, so that we can work when
	// we're static-linked into an EXE (where there are no DLL notifications).

	core_term_thread();
}

//
//
//

/*

	example call graph of an exe linked with the mercury DLL:

	mainCRTStartup (in exe)
	  __crt_startup
	  main
	  __crt_shutdown
		ExitProcess
		  _DllMainCRTStartup (in DLL)
			DllMain
			  core_term_process
			__crt_shutdown2

*/

void core_term_process()
{
	_ASSERT(g_in_core_term_process == 0);
	
	g_in_core_term_process++;
	
	// XXX: beware!  we are called from within DllMain, and therefore
	// within the OS loader lock

	// NOTE: DLL_THREAD_DETACH isn't sent for the main thread, so we clean
	// that up here

	core_term_thread();

	// TODO: loop through all thread structures and cleanup any
	// that were left undone???

	if (g_tls_index_usage == 0)
	{
		if (g_tls_index_status == 1)
		{
			TlsFree(g_tls_index);

			g_tls_index_status = 0;
		}
	}
	else
	{
#ifdef _DEBUG

		if (0)
		{
			// NOTE: this can be reached when a non-terminated task is still
			// running (e.g. Mercury Test) and will cause a stack overflow since
			// DBG_BREAK() -> assertion_failed -> TRACE -> core_thread_state
			// --> assertion_failed ...

			DBG_BREAK();
		}
#endif

	}

	// XXX: at this point, we've just killed our own thread structure
	// and possibly tore down our TLS support, and we're now going to
	// build that stuff back up from within our trace calls !!!!
	
	if (1)
	{
#ifdef _DEBUG
		trace("core_term_process: dumping stats...\n");
		trace_indent();
#endif
		string_exit_dump();
		array_exit_dump();
		object_exit_dump();
#ifdef _DEBUG
		trace_unindent();
		trace("core_term_process: complete\n");
#endif
	}
}

//
//
//

void core_term_thread()
{
	// XXX: beware!  we are called from within DllMain, and therefore
	// within the OS loader lock

	core_thread_state_t *thread = core_thread_state(false);

	if (thread != NULL)
	{
#ifdef _DEBUG

		bool do_trace = g_debug_trace_ts;

		if (do_trace)
		{
			TRACE("core_term_thread(): ts=%p, id=%i\n",
				thread, thread ? obj_cast<win_core_thread_state_t>(thread)->thread_id_host : 0);

			trace_indent();
		}

#endif

		cleanup_external_ts(thread);

#ifdef _DEBUG

		if (do_trace)
		{
			trace_unindent();
		}

#endif

		//
		//
		//

#ifdef CORE_ENABLE_TS_CACHE

		thread_id_t tid = current_thread_id();

		core_thread_state_cache_entry_t &cache = g_core_thread_state_cache[
			tid % DIM_g_core_thread_state_cache];

		_ASSERT(tid != 0); // we assume zero is not a valid thread ID here

		thread_id_t owner = cmpxchg(&cache.owner, tid, 0);

#endif

		//
		//
		//

		_ASSERT(g_tls_index_status == 1);

		if (TlsSetValue(g_tls_index, NULL))
		{
			xdec(g_tls_index_usage);
		}
		else
		{
			DWORD dwError = GetLastError();

			DBG_BREAK();
		}

		thread->release();
	}
}

#ifndef REMOVE_EXPERIMENTAL

static int g_in_mercury_dll_post_term;

static void mercury_dll_post_term()
{
	// XXX: beware!  we are called from within DllMain, and therefore
	// within the OS loader lock

	g_in_mercury_dll_post_term++;

	if (0)
	{
		string_exit_dump();
		array_exit_dump();
		object_exit_dump();
	}

	g_in_mercury_dll_post_term++;
}

static void after_dtors_xxx()
{
	// XXX: beware!  we are called from within DllMain, and therefore
	// within the OS loader lock

	g_in_mercury_dll_post_term++;

	if (0)
	{
		string_exit_dump();
		array_exit_dump();
		object_exit_dump();
	}

	g_in_mercury_dll_post_term++;
}

#endif

void core_init_process()
{
	// XXX: beware!  we are called from within DllMain, and therefore
	// within the OS loader lock

	dll_post_term_routine = mercury_dll_post_term;

	set_after_dtors_callback(after_dtors_xxx);
}

#endif

thread_id_t current_thread_id()
{
	return GetCurrentThreadId();
}

