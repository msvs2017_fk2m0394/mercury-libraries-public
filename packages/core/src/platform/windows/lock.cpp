/*------------------------------------------------------------------------------

	platform/windows/lock.cpp - Windows scheduler support

	BUG: multiple initialization of critical sections is occurring
	
	This happens in the following example:  g_atomic_lock_data is initialized to
	point to g_atomic_lock in the executable image.  However, the constructor to
	g_atomic_lock is called after g_atomic_lock_data has already been used in a
	call to atomic_lock_acquire.

------------------------------------------------------------------------------*/

#include "pch.h"
#include "core.h"
#include "scheduler.h"

#include "../../lib/core/thread.h"

#ifndef LITE_RTL

#ifndef REMOVE_EXPERIMENTAL
lock_internal_t g_default_mm_lock; // XXX: startup code
#endif

lock_internal_t g_atomic_lock; // XXX: startup code

#ifdef _DEBUG
static u32 g_lock_debug_1;
#endif

static u32 g_lock_serial;

//
//
//

lock_internal_t::lock_internal_t()
{
	serial = 0;
	init = 0;
	lock_count = 0;
	entry_count = 0;

	ZERO_INIT(cs);

	debug_validate(this);
}

//
//
//

lock_internal_t::~lock_internal_t()
{
	debug_validate(this);

#ifndef _WIN32_WCE
	_ASSERT(cs.OwningThread == NULL);
	_ASSERT(cs.RecursionCount == 0);
#endif
	
	if (init)
	{
		DeleteCriticalSection(&cs);
	}
}

//
//
//

int lock_enter(lock_data_t *lock, lock_mode_t mode, const f32 *timeout__)
{
	debug_validate(lock);

	DWORD timeout = millisec(timeout__);

	_ASSERT(mode == lock_mode_exclusive);
	_ASSERT(timeout == INFINITE);

	bool result = false;

	//
	//  Get internal data associated with lock
	//
	
	lock_internal_t *data = lock->m_data; // XXX: is this (non-atomic) read safe???

	if (data == NULL)
	{
		data = new lock_internal_t;

		if (data != NULL)
		{
			lock_internal_t *last = cmpxchg_ptr<lock_internal_t>(lock->m_data, NULL, data);

			if (last == NULL)
			{
			}
			else
			{
				delete data;

				data = last;
			}
		}
		else
		{
			DBG_NOT_IMPLEMENTED();
		}
	}

	//
	//
	//

	// BUG: app verifier reveals that g_profile_link_lock is being initialized more than once!!!
	
	if (data != NULL)
	{
		//
		//  Do one-time initialization
		//
		
		u32 init = cmpxchg(&data->init, 0, 1);
		
		if (init == 0)
		{
			_ASSERT(data->lock_count == 0);
			_ASSERT(data->serial == 0);

#ifdef _DEBUG
			u32 serial0 = data->serial;
#endif

			u32 serial = xinc(g_lock_serial);

			data->serial = serial;

#ifdef _DEBUG
			if (1)
			{
				string_t buf = format_string("initializing lock @ %p, data=%p, init=%i, serial0=%i, serial=%i\n",
					lock, data, init, serial0, data->serial);

				OutputDebugString(buf);
			}
#endif

			//
			//
			//
			
			InitializeCriticalSection(&data->cs);

			debug_validate(data);
			
#ifdef _DEBUG
			
#ifndef _WIN32_WCE

			_ASSERT(data->cs.OwningThread == NULL);
			_ASSERT(data->cs.RecursionCount == 0);

			// NOTE: in Windows 10, the DebugInfo pointer is initialized to -1
			// instead of zero as in earlier OS versions, so an additional check
			// is required
		
			if (data->cs.DebugInfo != NULL && data->cs.DebugInfo != (PRTL_CRITICAL_SECTION_DEBUG)-1)
			{
				_ASSERT(data->cs.DebugInfo->EntryCount == 0);
				_ASSERT(data->cs.DebugInfo->ContentionCount == 0);
			}

#endif

#endif

			//
			//
			//

			xinc(data->lock_count);
		}
		else
		{
			while (data->lock_count == 0)
			{
#ifdef _DEBUG
				if (1)
				{
					string_t buf = format_string("waiting on lock initialization @ %p, data=%p, init=%i, serial=%i\n", lock, data, init, data->serial);

					OutputDebugString(buf);
				}
#endif
				// Wait until other thread sets lock_count to 1.  This ensures that
				// InitializeCriticalSection has completed execution.

				// NOTE: if we're running in a multi-processor environment, we
				// could spin instead of yielding our time quantum

#ifdef _DEBUG
				xinc(g_lock_debug_1);
#endif

				Sleep(0);
			}
		}

		//
		//
		//
		
		EnterCriticalSection(&data->cs);

		result = false;

		//
		//
		//

		data->entry_count++;

#if 0 

		if (0)
		{
			if (data->owner == NULL)
			{
				_ASSERT(data->lock_count == 0);
				_ASSERT(data->prev == NULL);

				if (1)
				{
					data->frame = current_stack_frame();
				}

				core_thread_state_t *thread = core_thread_state(true);

				if (thread != NULL)
				{
					data->prev = thread->locks;
					data->owner = thread;

					thread->locks = data;
				}
			}
			else
			{
				_ASSERT(data->lock_count != 0);
			}
		}

#endif

		data->lock_count++;

		_ASSERT(data->lock_count > 0); // wrap-around not supported
	}

	return result;
}

//
//
//

void lock_leave(lock_data_t *lock)
{
	debug_validate(lock);
	
	lock_internal_t *data = lock->m_data;

	if (data != NULL)
	{
		_ASSERT(!data->init || data->lock_count > 1);
		
		if (data->init)
		{
			if (data->lock_count > 1)
			{
#ifndef _WIN32_WCE
				_ASSERT(data->cs.OwningThread == (HANDLE)GetCurrentThreadId());
				_ASSERT(data->cs.RecursionCount > 0);
#endif

				LeaveCriticalSection(&data->cs);

#if 0

				if (0)
				{
					core_thread_state_t *ts = core_thread_state(false);

					_ASSERT(data->owner == ts);

					if (ts != NULL)
					{
						lock_internal_t *link = ts->locks;

						while (link != NULL)
						{
							if (link == data)
							{
								break;
							}

							link = link->prev;
						}
					}
				}

#endif

				if (xdec(data->lock_count) == 0)
				{
//					data->frame = NULL;
//					data->owner = NULL;
				}
			}
			else
			{
				DBG_BREAK();
			}
		}
		else
		{
			DBG_BREAK();
		}
	}
	else
	{
		DBG_BREAK();
	}
}

//
//
//

void lock_destroy(lock_data_t *lock)
{
	if (lock != NULL)
	{
		lock_internal_t *data = (lock_internal_t*)xchg((uintptr_t*)&lock->m_data, NULL);
//		lock_internal_t *data = xchg<lock_internal_t*>(&lock->m_data, NULL);
//		lock_internal_t *data = xchg_ptr<lock_internal_t>(lock->m_data, NULL);
		
		delete data;

		debug_shred(lock, sizeof(*lock)); // overwrite object in debug builds
	}
}

//
//
//

#ifdef _DEBUG

void debug_validate(const lock_data_t *data)
{
	_ASSERT(data != NULL);
//	_ASSERT(object_type(data) == g_type_lock__);

	const lock_internal_t *x = data->m_data;

	if (x != NULL)
		debug_validate(x);
}

void debug_validate(const lock_internal_t *data)
{
	_ASSERT(data != NULL);

	if (data->lock_count)
	{
	}
	else
	{
//		_ASSERT(data->frame == NULL);
	}

	const CRITICAL_SECTION &cs = data->cs;

#if !defined(_WIN32_WCE)

	// NOTE: in Windows 10, the DebugInfo pointer is initialized to -1, instead
	// of zero as in earlier OS versions, so an additional check is required

	if (cs.DebugInfo != NULL && cs.DebugInfo != (PRTL_CRITICAL_SECTION_DEBUG)-1)
	{
#ifdef RTL_CRITSECT_TYPE
		_ASSERT(cs.DebugInfo->Type == RTL_CRITSECT_TYPE);
#endif
		_ASSERT(cs.DebugInfo->CriticalSection == &cs);
		
		const LIST_ENTRY *list = &cs.DebugInfo->ProcessLocksList;

		const RTL_CRITICAL_SECTION_DEBUG *dbg = (RTL_CRITICAL_SECTION_DEBUG *)
			((uintptr_t)list->Flink - (offsetof(RTL_CRITICAL_SECTION_DEBUG, ProcessLocksList)));

		dbg = CONTAINING_RECORD(list->Flink, RTL_CRITICAL_SECTION_DEBUG, ProcessLocksList);
	}

#endif
}

#endif

#endif

