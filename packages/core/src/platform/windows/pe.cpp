/*------------------------------------------------------------------------------

	platform/windows/pe.cpp - Windows Portable Executable (PE) utilities

	- headers
	- sections (COFF)
	- symbols (COFF)
	- PE data directory (import/export, resource, TLS, COM+/.NET, etc.)
	- parses Win32 Portable Executable (PE) resource directories

	REF: "Microsoft Portable Executable and Common Object File Format
	Specification Revision 6.0 February 1999"

	TODO: do we need to examine pe_sections[i].Characteristics for the 
	IMAGE_SCN_CNT_UNINITIALIZED_DATA flag (WINE does this in its icon
	extraction function)??

	REF: MSDN "The Portable Executable File Format from Top to Bottom"

	REF: MSDN sample SDKTOOLS\WINNT\WALKER\PEFILE.C

	todo: gather COFF functions into a coff.cpp module???

	Basic structure of a COFF file with resources:

	IMAGE_DOS_HEADER.e_lfanew -->
	IMAGE_NT_HEADERS.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_RESOURCE].VirtualAddress -->
	IMAGE_RESOURCE_DIRECTORY
	IMAGE_RESOURCE_DIRECTORY_ENTRY
	IMAGE_RESOURCE_DATA_ENTRY

	see also: "../core/module.cpp"

	REF: ImageHelp API

	TODO: decode binding info

------------------------------------------------------------------------------*/

#include "pch.h"
#include <mercury/extensions/io.h>
#include <mercury/x86.h>
#include "core.h"
#include "pe.h"

#include "../../lib/debug/trace.h"

#include "../../../include/mercury/_detail_/exe-segment.h"

#ifdef _DEBUG
#ifndef _WIN32_WCE
#include <imagehlp.h>
#endif
#endif

#ifndef LITE_RTL

const IMAGE_DOS_HEADER *hmodule_dos_header(HMODULE hModule)
{
	return hmodule_dos_header__(hmodule_base_address(hModule));
}

#if 0

static const void *module_nt_header2(HMODULE hModule)
{
	return module_nt_header2__(hmodule_base_address(hModule));
}

#endif

const IMAGE_NT_HEADERS64 *hmodule_nt_headers64(HMODULE hModule)
{
	return hmodule_nt_headers64__(hmodule_base_address(hModule));
}

const IMAGE_NT_HEADERS32 *hmodule_nt_headers32(HMODULE hModule)
{
	return hmodule_nt_headers32__(hmodule_base_address(hModule));
}

#if 0

static const IMAGE_NT_HEADERS *module_nt_header(HMODULE hModule)
{
	return module_nt_header__(hmodule_base_address(hModule));
}

#endif

#if 0

static const IMAGE_SECTION_HEADER *find_section(const void *base, uintptr_t rva)
{
	return find_section__(base, rva);
}

#endif

const void *hmodule_rva_to_va(HMODULE hModule, uintptr_t rva)
{
	const void *base = hmodule_base_address(hModule);
	bool image = !LDR_IS_DATAFILE(hModule); // mapped as data file, not as a loader image
	return hmodule_rva_to_va__(base, image, rva);
}

const IMAGE_SECTION_HEADER *hmodule_rva_to_section(HMODULE hModule, uintptr_t rva)
{
	return hmodule_rva_to_section__(hmodule_base_address(hModule), rva);
}

const IMAGE_FILE_HEADER *hmodule_file_header(HMODULE hModule)
{
	return hmodule_file_header__(hmodule_base_address(hModule));
}

const void *hmodule_data_directory(HMODULE hModule, unsigned int index)
{
	const void *base = hmodule_base_address(hModule);
	bool image = !LDR_IS_DATAFILE(hModule); // mapped as data file, not as a loader image
	return hmodule_data_directory__(base, image, index);
}

const IMAGE_DATA_DIRECTORY *hmodule_data_directory2(HMODULE hModule, unsigned int index)
{
	return hmodule_data_directory2__(hmodule_base_address(hModule), index);
}

const void *module_rva_to_va(module_t module, uintptr_t rva)
{
	return hmodule_rva_to_va(module_handle(module), rva);
}

static const IMAGE_SECTION_HEADER *find_section__(module_t module, const char *name)
{
	const IMAGE_NT_HEADERS32 *nt = hmodule_nt_headers32(module_handle(module));

	if (nt != NULL)
	{
		u32 i;
		
		for (i = 0; i < nt->FileHeader.NumberOfSections; i++)
		{
			const IMAGE_SECTION_HEADER *section = section_header(&nt->FileHeader, i);

			if (section != NULL)
			{
				if (strncmp((char*)section->Name, name, 8) == 0)
					return section;
			}
		}
	}

	return NULL;
}

static const void *find_section(module_t module, const char *name)
{
	const IMAGE_SECTION_HEADER *section = find_section__(module, name);

	if (section != NULL)
	{
		if (section->PointerToRawData != 0)
		{
			const void *base = module_rva_to_va(module, section->VirtualAddress);
			const void *end  = ptr_offset(base, section->SizeOfRawData);

			return base;
		}
	}

	return NULL;
}

static const IMAGE_SECTION_HEADER *find_section__(module_public_t *module, const char *name)
{
	const IMAGE_NT_HEADERS32 *nt = hmodule_nt_headers32(module_handle__(module));

	if (nt != NULL)
	{
		u32 i;
		
		for (i = 0; i < nt->FileHeader.NumberOfSections; i++)
		{
			const IMAGE_SECTION_HEADER *section = section_header(&nt->FileHeader, i);

			if (section != NULL)
			{
				if (strncmp((char*)section->Name, name, 8) == 0)
					return section;
			}
		}
	}

	return NULL;
}

static const void *module_rva_to_va(module_public_t *module, uintptr_t rva)
{
	return hmodule_rva_to_va(module_handle__(module), rva);
}

static const mercury_section_record_t *mercury_section_record(module_public_t *module, const char *name)
{
	const IMAGE_SECTION_HEADER *section = find_section__(module, ".mercury");

	if (section != NULL)
	{
		if (section->PointerToRawData != 0)
		{
			const void *base = module_rva_to_va(module, section->VirtualAddress);
			const void *end = ptr_offset(base, section->Misc.VirtualSize);

			return mercury_section_record2(base, end, name);
		}
	}

	return NULL;
}

const mercury_section_record_t *mercury_section_record(module_public_t *module, unsigned int index)
{
	const IMAGE_SECTION_HEADER *section = find_section__(module, ".mercury");

	if (section != NULL)
	{
		if (section->PointerToRawData != 0)
		{
			const void *base = module_rva_to_va(module, section->VirtualAddress);
			const void *end = ptr_offset(base, section->Misc.VirtualSize);

			return mercury_section_record2(base, end, index);
		}
	}

	return NULL;
}

string_t bldinfo_text(module_t module)
{
	string_t text;

	const mercury_section_record_t *rec = mercury_section_record(module.m_object, "build info");

	if (rec != NULL)
		return text += (const char *)rec->addr;

	if (1)
	{
		const void *base = find_section(module, ".bldinfo");

		if (base != NULL)
			text += trim((const char *)base);
	}

	return text;
}

#endif

extern "C" HMODULE pointer_to_hmodule(const void *base)
{
	C_ASSERT(sizeof(base) == sizeof(uintptr_t));

#ifndef _WIN32_WCE

	_ASSERT(!LDR_IS_DATAFILE(base)); // low bit must not already be set

	if (base != NULL)
	{
		// NOTE: setting the low bit of a module handle indicates the module
		// handle is a mapped file, not one loaded by the OS loader (this is
		// an undocumented Windows convention)

		return (HMODULE)((uintptr_t)base | 1); // XREF: LDR_IS_DATAFILE
	}

#endif

	return NULL;
}

