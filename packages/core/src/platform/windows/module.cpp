/*------------------------------------------------------------------------------

	platform/windows/module.cpp - Windows module support

	BUG: static allocated module objects do not have object headers
	BUG: static allocated module objects do not have filename member

------------------------------------------------------------------------------*/

#include "pch.h"
#include "core.h"
#include "pe.h"
#include "io.h"

#include "../../lib/core/object.h"
#include "../../lib/elements/time.h"
#include "../../crt/crt.h"

#ifndef LITE_RTL

// NOTE: the EXE module information can safely be cached since its module will
// always be the same while this process is running.  This is not necessarily
// the case for DLL modules in this process, except for those explicitly loaded
// with the EXE at startup.

static windows_module_t g_exe_module; // XXX: startup code

#ifdef _DEBUG
struct windows_module_with_object_header_t { object_header_t header; windows_module_t module; };
static global_thing_t<windows_module_with_object_header_t> g_exe_module__;
#endif

static lock_t g_error_mode_lock; // XXX: startup code

static string_t g_extensions_dirname = "extensions"; // XXX: startup code

#ifdef _WIN32_WCE
export_module_t g_coredll_module = { "coredll.dll" };
#else
export_module_t g_kernel32_module = { "kernel32.dll" };
export_module_t g_user32_module = { "user32.dll" };
#endif

static struct {
	BOOL    (WINAPI * GetModuleHandleExA)(DWORD dwFlags, LPCSTR lpModuleName, HMODULE* phModule);
//	BOOL    (WINAPI * GetModuleHandleExW)(DWORD dwFlags, LPCWSTR lpModuleName, HMODULE* phModule);
} g_kernel32_procs;

static export_info_t g_kernel32_exports[] = 
{
	{ &g_kernel32_module, "GetModuleHandleExA", &g_kernel32_procs.GetModuleHandleExA },
};

//
//
//

static const void *get_addr__(export_info_t &proc, int mode);

// export is a keyword in newer C++
#define export export__

// NOTE: GetProcAddress() is a function on Win32, but is a macro on CE, where
// GetProcAddressW() and GetProcAddressA() are functions

extern "C" FARPROC OS_GetProcAddressA(HMODULE hModule, LPCSTR lpProcName);
extern "C" FARPROC OS_GetProcAddressW(HMODULE hModule, LPCWSTR lpProcName);

bool OS_LoadLibraryExA(LPCSTR lpLibFileName, DWORD dwFlags, HMODULE &hModule, DWORD &error);

#ifdef _WIN32_WCE

#ifdef __cplusplus
extern "C" {
#endif

DWORD WINAPI GetModuleFileNameA(HMODULE hModule, LPCH lpFilename, DWORD nSize);
HMODULE WINAPI LoadLibraryExA(LPCSTR lpLibFileName, HANDLE hFile, DWORD dwFlags);
HMODULE WINAPI GetModuleHandleA(LPCSTR lpModuleName);

#ifdef __cplusplus
}
#endif

#endif

//
//
//

#ifdef _DEBUG

void debug_validate(HMODULE hModule)
{
	if (hModule != NULL)
	{
		// NOTE: LoadLibraryEx returns the low bit on if loaded as a data file
		// (i.e. file is loaded simply as a single memory mapped file, without
		// any section-related adjustments)
		
		if (LDR_IS_RESOURCE(hModule))
		{
			// loaded as an image file, or as a simple file mapping
		}
		else
		{
			if (0)
			{
				// XXX: no longer using this check since it grabs the loader lock (see notes in string.cpp)

				// verify that this really is a Win32 module handle

				TCHAR szFilename[MAX_PATH];
				
				DWORD dwLen = GetModuleFileName(hModule, szFilename,
					DIM(szFilename));

				_ASSERT(dwLen != 0);
			}
		}
	}
}

#endif

//
//
//

static windows_module_t *windows_module(module_object_t *obj)
{
// !?!?!?!?!?!?!?!?!?!?!?
	if (obj == &g_exe_module)
	{
		// obj_cast won't work since object doesn't have a header!!!
	}
// !?!?!?!?!?!?!?!?!?!?!?

	return obj_cast<windows_module_t>(obj);
}

//
//
//

module_t module_load(string_t filename, u32 flags)
{
	module_t module = module_from_filename(filename);
	
	module_load__(module_object(module.m_object), filename, flags);

	return module;
}

bool module_load__(module_object_t *obj__, string_t filename, u32 dwFlags)
{
	if (obj__ == NULL)
		return false;

	if (is_error(filename))
		return false;
	
	if (is_empty(filename))
		return false;

	windows_module_t *obj = windows_module(obj__);
	
	if (obj == NULL)
		return false;

	if (obj->m_hModule != NULL)
		return true;

	if (is_ansi(filename))
	{
		LPCSTR pszFileName = filename;

		DWORD error;

		_ASSERT(obj->m_hModule == NULL);
		
//		if (dwFlags & (LOAD_LIBRARY_AS_DATAFILE | DONT_RESOLVE_DLL_REFERENCES) == 0)
//		{
//		}

		// XXX: should search our own paths e.g. "module", "extensions" ?!?!?!?...
		
		if (OS_LoadLibraryExA(pszFileName, dwFlags, obj->m_hModule, error))
		{
			obj->m_public.status = io_resource_status_open;

			return true;
		}
		else
		{
			DBG_NOT_IMPLEMENTED();
//			obj->m_error = variant_system_error(error);
			obj->m_public.status = io_resource_status_error;

			return false;
		}

		// unreachable...

		return false;
	}
	
	if (is_unicode(filename))
	{
		DBG_NOT_IMPLEMENTED();

		return false;
	}

	DBG_INVALID_CASE();

	return false;
}

//
//
//

const void *get_addr(export_info_t &proc)
{
	return get_addr__(proc, 0);
}

const void *get_addr_sys(export_info_t &proc)
{
	return get_addr__(proc, 1);
}

const void *get_addr_ext(export_info_t &proc)
{
	return get_addr__(proc, 2);
}

//
//
//

static const void *get_addr__(export_info_t &proc, int mode)
{
	_ASSERT(&proc != NULL);
	
	// TODO: use atomic operations???

	if (proc.state == 0)
	{
		_ASSERT(proc.proc == NULL);
		
		HMODULE hModule = module_load2__(proc.module, mode, NULL);

		if (hModule != NULL)
		{
			proc.proc = (void*)OS_GetProcAddressA(hModule, proc.name);

			if (proc.proc != NULL)
			{
				proc.state = 1;
			}
			else
			{
				proc.state = -2;
				proc.dwErrorCode = GetLastError();
			}
				
			if (proc.proc_ptr != NULL)
			{
				*(void**)proc.proc_ptr = proc.proc;
			}
		}
		else
		{
			proc.state = -1;
			proc.dwErrorCode = proc.module->dwLoadError;
		}
	}

	return proc.proc;
}

//
//
//

static bool find_module(string_t base, string_array_t path, string_t &filename)
{
	for (unsigned int i = 0; i < path.length(); i++)
	{
		string_t temp = base;
		
		temp = file_path_append(temp, path[i]);
		temp = file_path_append(temp, filename);

		if (file_exists(temp))
		{
			filename = temp;

			return true;
		}
	}

	return false;
}

//
//
//

string_t find_extension_module(string_t name)
{
	string_t buffer = name;

	string_array_t path;

	path += "";
	path += g_extensions_dirname;
	
	if (find_module(file_path(module_filename()), path, buffer))
		return buffer;

	return string_error();
}

module_t module_load_extension(string_t name)
{
	return module_load(find_extension_module(name));
}

// TODO: make thread-safe

HMODULE module_load2__(export_module_t *module, int mode, const void *caller)
{
	if (module != NULL)
	{
		if (module->state == 0)
		{
			//
			//  Save/set process error mode
			//
			
			// NOTE: prevent annoying modal dialog box

			// XXX: race condition (error mode is process global)...

#ifndef _WIN32_WCE

			UINT uMode = SetErrorMode(SEM_FAILCRITICALERRORS | SEM_NOOPENFILEERRORBOX);

			SetErrorMode(uMode | SEM_FAILCRITICALERRORS | SEM_NOOPENFILEERRORBOX);

#endif

			//
			//
			//

			LPCSTR lpLibFileName = module->filename;
			DWORD dwFlags = 0;

			string_t buffer = module->filename;

			string_array_t path;

			string_t base1 = file_path(module_filename(module_current()));
			string_t base2 = file_path(module_filename(module_exe()));
			string_t base3;

			if (caller != NULL)
			{
				HMODULE hModuleCaller = hmodule_from_address(caller);

				if (hModuleCaller != NULL)
				{
					if (hModuleCaller != module_handle(module_current()) &&
						hModuleCaller != module_handle(module_exe()))
					{
						base3 = file_path(module_filename__(hModuleCaller));

						if (base3 == base1 ||
							base3 == base2)
						{
							base3 = "";
						}
					}
				}
			}

			if (base2 == base1)
				base2 == "";

			path += "";
			path += g_extensions_dirname;
			
			if (find_module(base1, path, buffer) ||
				(base2 != "" && find_module(base2, path, buffer)) ||
				(base3 != "" && find_module(base3, path, buffer)))
			{
				buffer = to_ansi(buffer);
				
				lpLibFileName = buffer;
			}
			
			if (module->hModule == NULL)
			{
				OS_LoadLibraryExA(lpLibFileName, dwFlags, module->hModule, module->dwLoadError);
			}
			else
			{
				// XXX: with media.exe (creating two UI threads), we're currently hitting
				// this on g_module_gl...

				if (0)
				{
					DBG_BREAK();
				}
			}

			//
			//
			//

			if (module->hModule != NULL)
			{
				module->state = 1;
			}
			else
			{
				module->state = -1;
			}

			//
			//  Restore process error mode
			//
			
#ifndef _WIN32_WCE
			SetErrorMode(uMode);
#endif
		}

		return module->hModule;
	}

	return NULL;
}

static UINT set_error_mode()
{
#if !defined(_WIN32_WCE)

	// global objects destroyed?
	_ASSERT(crt_status() != dll_state_terminated);
	
	g_error_mode_lock.enter();

	UINT uMode = SetErrorMode(SEM_FAILCRITICALERRORS | SEM_NOOPENFILEERRORBOX);

	SetErrorMode(uMode | SEM_FAILCRITICALERRORS | SEM_NOOPENFILEERRORBOX);

	g_error_mode_lock.leave();

	return uMode;

#else

	return 0;

#endif

}

//
//
//

/*

	Windows loader file search order (if DLL is specified without a path)...

	1.  application load directory (standard search strategy begins here,
	    alternate search strategy uses the directory of the executable module
	    LoadLibraryEx is using)

	2.  current directory (if SafeDllSearchMode is set, this step becomes #4)

	3.  system directory (including 16-bit system directory)

	4.  windows directory

	5.  PATH environment variable

*/

bool OS_LoadLibraryExA(LPCSTR lpLibFileName, DWORD dwFlags, HMODULE &hModule, DWORD &error)
{
	// NOTE: filename must not include forward slashes (see API doc)

//	TRACE_WARNING_IF2(strchr(lpLibFileName, '/') == NULL, "DLL path shouldn't contain forward slashes");

	HANDLE hFile = NULL; // reserved parameter

	//
	//
	//
	
	bool t = trace_enabled(g_trace_module, trace_severity_info);
	
	if (crt_status() != dll_state_initialized)
		t = false;

#ifndef _WIN32_WCE
	DWORD heaps = 0;
	HANDLE dummy[1];
#endif
	DWORD tls = 0;

	if (t)
	{
		TRACE("DLL: LoadLibraryEx(\"%s\", 0x%08X, 0x%08X)...\n",
			lpLibFileName, hFile, dwFlags);

		trace_indent();

#ifndef _WIN32_WCE
		heaps = GetProcessHeaps(DIM(dummy), dummy);
#endif
		tls = TlsAlloc();
		TlsFree(tls);
	
		tic();
	}

	// prevent annoying modal dialog box

	set_error_mode();

	hModule = LoadLibraryExA(lpLibFileName, hFile, dwFlags);

	if (hModule == NULL)
	{
		error = GetLastError();

		string_t temp = FormatSystemMessage(error);

		error_log_message(temp);
	}

	double dt = 0.0;
	
	if (t)
	{
		dt = toc();
	}

	if (hModule == NULL)
	{
		// TODO: use variant_system_error

		if (trace_enabled(g_trace_module, trace_severity_warning))
		{
			string_t temp = FormatSystemMessage(error);

			TRACE("DLL: LoadLibraryEx(\"%s\", 0x%08X, 0x%08X): %s\n",
				lpLibFileName, hFile, dwFlags, (const char *)get_error_text(temp));
		}

		if (t)
		{
			trace_unindent();
		}

		return false;
	}
	else
	{
		error = 0;

		if (t)
		{
			TRACE("DLL: LoadLibraryEx(\"%s\", 0x%08X, 0x%08X)=0x%08X\n",
				lpLibFileName, hFile, dwFlags, hModule);

			TRACE("%.3f ms load time\n", dt * 1000.0);

#ifndef _WIN32_WCE

			DWORD heaps2 = GetProcessHeaps(DIM(dummy), dummy);
			
			if (heaps2 != heaps)
			{
				TRACE("%i heap(s) created\n", heaps2-heaps);
			}

#endif

			DWORD tls2 = TlsAlloc();
			TlsFree(tls2);

			if (tls2 != tls)
			{
				TRACE("TLS alloced\n");
			}

			if (1)
			{
				string_t filename = module_filename__(hModule);

//				_ASSERT(filename == find_module_file(lpLibFileName));

				if (filename != lpLibFileName)
				{
					TRACE("full filename = %s\n", get_trace_string(filename));
				}
			}
		}

		if (t)
		{
			trace_unindent();
		}

		return true;
	}
}

extern "C" FARPROC OS_GetProcAddressA(HMODULE hModule, LPCSTR lpProcName)
{
	FARPROC proc1 = NULL;

#ifdef _DEBUG

#ifndef _WIN32_WCE

	bool p = (crt_status() == dll_state_initialized);

	if (p)
		tic();
	
	// brute-force...
	
	unsigned int index = 0;

	module_symbol_t symbol;

	while (image_enum_exports__(hModule, index, symbol))
	{
		if (ISINTRESOURCE(lpProcName))
		{
			if (symbol.name.ordinal == INTRESOURCE(lpProcName))
			{
				if (symbol.forwarder != NULL)
				{
					DBG_NOT_IMPLEMENTED();
				}
				else
				{
					proc1 = (FARPROC)hmodule_rva_to_va(hModule, (uintptr_t)symbol.rva);
				}

				break;
			}
		}
		else
		{
			if (symbol.name.text != NULL && strcmp(symbol.name.text, lpProcName) == 0)
			{
				if (symbol.forwarder != NULL)
				{
					DBG_NOT_IMPLEMENTED();
				}
				else
				{
					proc1 = (FARPROC)hmodule_rva_to_va(hModule, (uintptr_t)symbol.rva);
				}

				break;
			}
		}

		index++;
	}

	double t1;
	
	if (p)
	{
		t1 = toc();

		tic();
	}

#endif
#endif

	FARPROC proc;

#ifndef _WIN32_WCE
	proc = GetProcAddress(hModule, lpProcName);
#else
	proc = GetProcAddressA(hModule, lpProcName);
#endif

#ifdef _DEBUG
#ifndef _WIN32_WCE

	DWORD err = GetLastError();

	if (p)
	{
		double t2 = toc();
	}

	if (proc != NULL)
	{
		if (0)
		{
			// XXX: this assertion is not valid if the application is running
			// in compatibility mode (e.g. our proc1 is the address of
			// RegisterClassA in USER32.DLL, while proc is the address of
			// NS_EmulateUser::APIHook_RegisterClassA in aclayers.dll)

			if (hModule == hmodule_from_address((void*)proc))
			{
				_ASSERT(proc1 == proc);
			}
			else
			{
			}
		}
	}
	else
	{
		_ASSERT(proc1 == proc);
	}

	SetLastError(err); // restore error code as it was just after GetProcAddress

#endif
#endif

	return proc;
}

#ifdef _WIN32_WCE

extern "C" FARPROC OS_GetProcAddressW(HMODULE hModule, LPCWSTR lpProcName)
{
	FARPROC proc;

	proc = GetProcAddressW(hModule, lpProcName);

	return proc;
}

#endif

//
//
//

void *module_address(module_t module, const char *name)
{
	//
	//
	//
	
	module_object_t *obj = module_object(module.m_object);

	if (obj != NULL)
	{
		//
		//
		//
		
		windows_module_t *win = windows_module(obj);
		
		if (win != NULL)
		{
			//
			//
			//

			if (win->m_hModule == NULL)
			{
				// demand-load...
				
				if (win->m_filename != "")
				{
					module_load__(obj, win->m_filename, 0);
				}
			}

			//
			//
			//
			
			if (name == NULL)
			{
				return const_cast<void*>(hmodule_base_address(win->m_hModule));
			}

			//
			//
			//

			if (win->m_hModule != NULL)
			{
				FARPROC proc = OS_GetProcAddressA(win->m_hModule, name);

				if (proc == NULL)
				{
					DWORD dwError = GetLastError();
				}

#if 1
				// this type of tracing is probably better suited to part of our
				// debugger, instead of the library, making available for more
				// general use

				if (trace_enabled(g_trace_module, trace_severity_info))
				{
					TRACE("module: module_address(0x%08X, \"%s\")=0x%08X\n",
						win->m_hModule, name, proc);
				}
#endif

				return (void*)proc;
			}
		}
	}

	return NULL;
}

//
//
//

string_t module_filename____(module_public_t *pub)
{
	module_object_t *obj = module_object(pub);

	if (obj == NULL)
		return string_error();

	windows_module_t *win = windows_module(obj);

	if (win == NULL)
		return string_error();

	win->resolve_filename();

	return win->m_filename;
}

//
//
//

/*

	We would ideally like to abstract the module handle in a way that the
	signature for window_module() would return module_t instead of HMODULE.

	If module_from_handle() is called twice with the same hModule, the caller
	will get back two distinct module_t objects, both referring to the same
	HMODULE.

*/

module_t module_from_handle(HMODULE hModule)
{
	// global objects destroyed?
	_ASSERT(crt_status() != dll_state_terminated);
	
	// TODO: look in a global table of all modules we know about, so we don't
	// keep reallocating a module data structure for the same module???

	windows_module_t *obj = new windows_module_t;

	if (obj == NULL)
		return module_error();

	obj->m_hModule = hModule;

	obj->m_public.status = io_resource_status_open;

	module_t result = module_create/*_and_release*/(obj);

	obj->release();

	if (0)
	{
		_ASSERT(module_handle(result) == hModule);
	}

	return result;
}

module_t module_from_handle_internal(HMODULE hModule, windows_module_t *obj)
{
#ifndef _WIN32_WCE
	_ASSERT(hModule != NULL);
#else
	if (hModule == NULL)
		return module_t();
#endif

	// global objects destroyed?
//	_ASSERT(crt_status() != dll_state_terminated);
	
	// TODO: look in a global table of all modules we know about, so we don't
	// keep reallocating a module data structure for the same module???

	_ASSERT(obj != NULL);
	
	if (obj == NULL)
		return module_error();

	init_module_info(hModule, *obj);

	module_t result = module_create/*_and_release*/(obj);

	obj->release();

	return result;
}

//
//
//

#ifndef MOVE_THIS_ELSEWHERE

string_t module_file_name_from_address(const void *addr)
{
	HMODULE hModule = hmodule_from_address(addr);

	if (hModule != NULL)
		return module_filename__(hModule);

	return "";
}

#endif

//
//
//

void init_module_info(HMODULE hModule, windows_module_t &info)
{
#ifndef _WIN32_WCE
	// under Win Mobile, hModule can be a pseudo-handle
	_ASSERT(hmodule_from_address(hModule) == hModule);
#endif

	info.m_hModule = hModule;

	info.m_public.status = io_resource_status_open;
}

//
//
//

HMODULE module_handle__(module_public_t *pub)
{
	module_object_t *obj = module_object(pub);

	if (obj != NULL)
	{
		// TODO: demand-load module here if indicated
		
		HMODULE hModule;
		
		windows_module_t *win = windows_module(obj);
		
		if (win != NULL)
		{
			hModule = win->m_hModule;

			debug_validate(hModule);
			
			return hModule;
		}
	}

	return NULL;
}

static art_time_t module_timestamp__(HMODULE hModule)
{
	// read timestamp from image header
	
	const IMAGE_FILE_HEADER *header = hmodule_file_header(hModule);

	if (header != NULL)
		return time_from_time_t(header->TimeDateStamp);

	string_t filename = module_filename__(hModule);

	art_time_t timestamp;
	
	file_last_write_date_utc(filename, timestamp);

	return timestamp;
}

time_data_t module_timestamp(module_t module)
{
	return module_timestamp__(module_handle(module));
}

HMODULE module_handle(module_t module)
{
	return module_handle__(module.m_object);
}

//
//
//

#ifndef REMOVE_THIS

module_t module_current_helper(rtl_module_state_t *state, size_t module_data_size)
{
	_ASSERT(state != NULL);

	global_thing_t<windows_module_t> &module = state->module____;

	// have global objects have already been destroyed (i.e. we are being
	// called during CRT shutdown, such as from within trace output)?
	
	if (crt_status() != dll_state_terminated)
	{
		lock_enter(state->lock);

		if (state->module____.pointer == NULL)
		{
			if (module_data_size >= sizeof(windows_module_t))
			{
				// optimization: uses reserved static data storage to
				// to avoid incurring malloc() overhead

				windows_module_t *data = mercury::construct1a<windows_module_t>
					(state->module____.storage, sizeof(state->module____.storage));

				if (data != NULL)
				{
					HMODULE hModule = hmodule_from_address(state);

					module_t temp = module_from_handle_internal(hModule, data);
					
					state->module____.pointer = data;
					
					state->module = temp;
				}
			}
		}

		// BUG: state->module__ module_object_t m_object_header is NULL!!!
		// BUG: state->module__ module_public_t filename is NULL!!!

		_ASSERT(state->module____.pointer != NULL);

		if (module_handle(state->module) == NULL)
		{
			HMODULE hModule = hmodule_from_address(state);

			if (state->module____.pointer == NULL)
			{
				if (module_data_size >= sizeof(windows_module_t))
				{
					// optimization: uses reserved static data storage to
					// to avoid incurring malloc() overhead

					windows_module_t *data = mercury::construct1a<windows_module_t>
						(state->module____.storage, sizeof(state->module____.storage));

					if (data != NULL)
					{
						module_t temp = module_from_handle_internal(hModule, data);
						
						state->module____.pointer = data;
						
						state->module = temp;
					}
				}
				else
				{
					state->module = module_from_handle(hModule); // XREF: init_module_info
				}
			}
			else
			{
				state->module = module_from_handle(hModule); // XREF: init_module_info
			}

#ifndef _WIN32_WCE
			_ASSERT(module_handle(state->module) != NULL);
#endif
			_ASSERT(module_handle(state->module) == hModule);
		}

		lock_leave(state->lock);

		return state->module;
	}
	else
	{
// 		DBG_BREAK();
	}

	return module_t();
}

#endif

//
//
//

#ifdef _WIN32_WCE

// stubs to help us with porting to Windows CE

#ifdef __cplusplus
extern "C" {
#endif

DWORD
WINAPI
GetModuleFileNameA(
    HMODULE hModule,
    LPCH lpFilename,
    DWORD nSize
    )
{
	WCHAR buf[_MAX_PATH];
	
	buf[0] = 0;
	
	DWORD result = GetModuleFileNameW(hModule, buf, DIM(buf));
	
	string_t temp = to_ansi(buf);
	
	strncpy(lpFilename, temp, nSize);
	
	return result;
}

HMODULE
WINAPI
LoadLibraryExA(
    LPCSTR lpLibFileName,
    HANDLE hFile,
    DWORD dwFlags
    )
{
	if (lpLibFileName != NULL)
	{
		string_t temp = to_unicode(lpLibFileName);
		
		HMODULE hModule = LoadLibraryExW(temp, hFile, dwFlags);
		
		return hModule;
	}
	else
	{
		HMODULE hModule = LoadLibraryExW(NULL, hFile, dwFlags);
		
		return hModule;
	}
}

HMODULE
WINAPI
GetModuleHandleA(
    LPCSTR lpModuleName
    )
{
#ifdef _WIN32_WCE
	if (lpModuleName == NULL)
	{
		return GetModuleHandleW(NULL);
	}
#endif
	
	string_t temp = to_unicode(lpModuleName);
	
	return GetModuleHandleW(temp);
}

#ifdef __cplusplus
}
#endif

#endif

//
//
//

OBJECT_CLASS_BEGIN(windows_module_t, module_object_t)
OBJECT_CLASS_END()

windows_module_t::windows_module_t()
{
	if (this == &g_exe_module)
	{
#ifdef _DEBUG
		HMODULE hModuleExe = GetModuleHandle(NULL);

		init_module_info(hModuleExe, g_exe_module);
#endif
	}
#ifdef _DEBUG
	else if (this == (windows_module_t*)g_exe_module__.storage)
	{
#ifdef _DEBUG
		HMODULE hModuleExe = GetModuleHandle(NULL);

		init_module_info(hModuleExe, g_exe_module);
#endif
	}
#endif
}

void windows_module_t::close()
{
	// don't modify static data

	if (m_hModule != NULL)
	{
		// TODO: use our debugger here instead!
		
		if (trace_enabled(g_trace_module, trace_severity_info))
		{
			TRACE("DLL: FreeLibrary(0x%08X)\n", m_hModule);
		}

		VERIFY(FreeLibrary(m_hModule));

		m_public.status = io_resource_status_closed;
		m_public.filename = NULL;
		m_hModule  = NULL;
		m_filename = "";
	}
}

void windows_module_t::resolve_filename()
{
	// load module on-demand...
	
	// Get full pathname (e.g. if m_filename is "foo.dll")
	
	if (m_hModule == NULL)
	{
#ifdef _DEBUG
		TRACE("loading module on-demand to resolve filename!\n");
#endif

		module_load__(this, m_filename, 0);
	}

	// NOTE: otherwise, Win32 GetModuleFileName(NULL) returns the EXE module

	if (m_hModule != NULL)
	{
		if (m_filename == "")
		{
			m_filename = module_filename__(m_hModule);
		}
	}
}

//
//
//

bool module_info(const module_object_t *module, const void *&base, string_t &filename, HMODULE &hModule)
{
	if (module != NULL)
	{
#if _MSC_VER < 1400
		__try
#endif
		{
			debug_validate(module);

			windows_module_t *win = obj_cast<windows_module_t>(const_cast<module_object_t*>(module));
			base = NULL;
			filename = win ? win->m_filename : "";
			hModule = win ? win->m_hModule : NULL;
			
			return true;
		}
#if _MSC_VER < 1400
		__except (GetExceptionCode() == STATUS_ACCESS_VIOLATION)
		{
		}
#endif
	}

	return false;
}

//
//
//

module_t module_from_address(const void *addr)
{
	HMODULE hModule = hmodule_from_address(addr);

	if (hModule != NULL)
		return module_from_handle(hModule);

	module_t module;

	return module;
}

//
//
//

module_t module_exe()
{
	// global objects destroyed?
	_ASSERT(crt_status() != dll_state_terminated);

	// NOTE: under NT, this information is accessed in user mode directly from
	// the process environment block (PEB), without a kernel call, so this is
	// relatively fast.
	
	// NOTE: under Mobile, GetModuleHandle(NULL) returns a pseudo handle, not the
	// module base address!!!

#ifdef _DEBUG
	HMODULE hModule = g_exe_module__->module.m_hModule;

	if (0)
	{
		module_t temp;

		temp.m_object = &g_exe_module__->module.m_public;

		return temp;
	}
#endif

#if 1

	if (g_exe_module.m_public.status == io_resource_status_none)
	{
		HMODULE hModuleExe = GetModuleHandle(NULL);
		
		init_module_info(hModuleExe, g_exe_module);
	}

	return module_create(&g_exe_module);

#else

	if (g_exe_module__->m_status == 0)
	{
		HMODULE hModuleExe = GetModuleHandle(NULL);
		
		init_module_info(hModuleExe, *g_exe_module__);
	}

	return module_create(g_exe_module__);

#endif

}

//
//
//

module_object_t *module_from_filename__(string_t filename)
{
	_ASSERT(filename != "");
	
	windows_module_t *data = new windows_module_t;

	if (data != NULL)
	{
		// NOTE: we're storing the filename, as given, without checking the path
		// or current directory for the validity of this filename.  If the module
		// is later demand-loaded, we simply assume the DLL search path and
		// current directory haven't changed since this call, if the filename is
		// not an absolute path.

		data->m_filename = filename;
	}

	return data;
}

//
//
//

module_object_t *module_map__(string_t filename)
{
	_ASSERT(filename != "");

	windows_module_t *data = new windows_module_t;

	if (data != NULL)
	{
#if 0
		// we'll map it ourselves...

		data->m_filename = filename;
		data->m_base     = map_file(filename, NULL);

		if (data->m_base == NULL)
			data->m_error = variant_system_error(GetLastError());

#else

		module_load__(data, filename, DONT_RESOLVE_DLL_REFERENCES | LOAD_LIBRARY_AS_DATAFILE);

#endif

	}

	return data;
}

static const void *module_rva_to_va(module_public_t *module, uintptr_t rva)
{
	return hmodule_rva_to_va(module_handle__(module), rva);
}

//
//
//

#ifndef REMOVE_THIS

HMODULE hmodule_from_address(const void *addr)
{
	HMODULE hModule = NULL;

	// PORT: using undocumented techniques here, based on the way the Windows
	// OS maps executable modules into process address space (doesn't apply to
	// Windows CE!)

#ifndef _WIN32_WCE

	mm_info_t mbi = mm_query_allocation(addr);

	if (mbi.Type == MEM_IMAGE)
	{
		if (mbi.State == MEM_RESERVE ||
			mbi.State == MEM_COMMIT)
		{
			hModule = (HMODULE)mbi.base;
		}
	}

#else

	if (hProcess == GetCurrentProcess())
	{
		MODULEINFO info = { 0 };
		
		HMODULE hModuleExe = GetModuleHandle(NULL);

		if (GetModuleInformation(hProcess, hModuleExe, &info, sizeof(info)))
		{
			if (addr >= info.lpBaseOfDll && addr < ptr_offset(info.lpBaseOfDll, info.SizeOfImage))
			{
				return hModuleExe;
			}
		}
	}
#endif

#ifdef _DEBUG

	if (0)
	{
		// XXX: no longer using this check since it grabs the loader lock
		// (see notes in string.cpp)

		get_addr(g_kernel32_exports[0]);

		if (g_kernel32_procs.GetModuleHandleExA != NULL)
		{
			HMODULE hModule2 = NULL;
			
			if (g_kernel32_procs.GetModuleHandleExA(
				GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS |
				GET_MODULE_HANDLE_EX_FLAG_UNCHANGED_REFCOUNT,
				(const char *)addr, &hModule2))
			{
				_ASSERT(hModule2 == hModule);
			}
		}
	}

#endif

	debug_validate(hModule);

	return hModule;
}

#endif

// NOTE: (from MSDN Library) Windows 95: The GetModuleFilename function will
// return long filenames when an application's version number is greater than
// or equal to 4.00 and the long filename is available. Otherwise, it returns
// only 8.3 format filenames.

// XREF: process_module_filename(DWORD dwProcessId, HMODULE hModule) in
// dbg/module.cpp

string_t module_filename__(HMODULE hModule)
{
	// NOTE: special case - Windows API GetModuleFileName(NULL) returns EXE
	// module filename

	TCHAR szFileName[MAX_PATH];

	// NOTE: if we're compiled without UNICODE defined, this function
	// (GetModuleFileNameA) will use RtlAllocateHeap, GetModuleFileNameW,
	// RtlUnicodeStringToAnsiString, etc.
	
	if (!GetModuleFileName(hModule, szFileName, DIM(szFileName)))
		return FormatSystemMessage(GetLastError()); // XXX: GetLastError always returns 0!?!?!

	// NOTE: when an application is started by a shell link that has (uses??)
	// a short (8.3) filename, then the module file name is retrieved as a
	// short name!
	
	if (0)
	{
		return io_long_pathname(szFileName);
	}

	return szFileName;
}

const void *hmodule_base_address(HMODULE hModule)
{
#ifndef _WIN32_WCE

	if (hModule != NULL)
	{
		// NOTE: LoadLibraryEx returns the low bit on if loaded as a data file
		// (i.e. file is loaded simply as a single memory mapped file, without
		// any section-related adjustments)
		
		if (LDR_IS_DATAFILE(hModule))
		{
			hModule = (HMODULE)((uintptr_t)hModule & ~0x03);
		}

		return (void*)hModule;
	}

#else

	MODULEINFO info = { 0 };
	
	if (GetModuleInformation(GetCurrentProcess(), hModule, &info, sizeof(info)))
	{
		return info.lpBaseOfDll;
	}

#endif

	return NULL;
}

//
//
//

static string_t get_exe_module_name()
{
	return file_name(module_filename(module_exe()));
}

static string_t get_current_module_name()
{
	return file_name(module_filename(module_current()));
}

//
//
//

string_t short_module_name(module_public_t *module)
{
	HMODULE hModule = module_handle__(module);

	// NOTE: the module handle can be NULL, if we're being called during
	// ART_CORE module shutdown which can potentially call into other modules
	// that have already been shutdown!

	if (hModule != NULL)
	{
		// NOTE: in this optimized case, the full module name is not returned
		// in the supplied buffer

		module_t temp = module_exe();

		if (hModule == module_handle(temp))
			return get_exe_module_name();

		if (1)
		{
			module_t temp = module_current(); // warning C4456: declaration of 'temp' hides previous local declaration

			if (hModule == module_handle(temp))
				return get_current_module_name();
		}

		// do the (relatively) expensive lookup...

		return file_name(module_filename__(hModule));
	}

	return "";
}

#endif

//
//
//

string_t error_message(export_info_t &export__)
{
	string_t message;
	
	if (export__.module->hModule == NULL)
	{
		message = print_to_string(
			"Unable to load module ", export__.module->filename, ": ",
			export__.dwErrorCode, " (", FormatSystemMessage(export__.dwErrorCode), ")");
	}
	else
	{
		message = print_to_string(
			"Unable to load procedure ", export__.name, ": ",
			export__.dwErrorCode, " (", FormatSystemMessage(export__.dwErrorCode), ")");
	}

	return message;
}

