/*------------------------------------------------------------------------------

	platform/windows/timer.cpp

	timers in Win32

	- SetTimer: low-priority timer based on message queue
	- timeSetEvent: high-precision timer, runs on separate thread (uses CreateWaitableTimer in NT)
	- Wait*: timeout values when waiting for an object to become signaled

------------------------------------------------------------------------------*/

#include "pch.h"
#include <mmsystem.h>
#include "../../lib/elements/time.h"
#include "core.h"

//
//
//

#ifndef LITE_RTL

//
//
//

#ifndef REMOVE_THIS

bool delay(double time)
{
	//
	//  Thanks MageX...
	//

	// TODO: use get_timer_period()
	// TODO: use Joe's "nice" timer (i.e. call Sleep if there's enough time)
	// TODO: use CreateWaitableTimer when running under NT???

#if 1

	//
	//
	//
	
	if (time > 0.0)
	{
		// TODO: use bool time_current(art_time_t &current)

		if (0)
		{
			art_time_t start;
			
			if (time_current__(start))
			{
				art_time_t current;
				
				while (time_current__(current))
				{
					double delta;

					if (time_delta_seconds(start, current, delta))
					{
					}
				}
			}
		}

		LARGE_INTEGER nFreq, nStartTime, nCurrTime, /*nTargTime, */xxx, yyy;
		
		if (QueryPerformanceFrequency(&nFreq))
		{
			_ASSERT(nFreq.QuadPart > 0);

			if (QueryPerformanceCounter(&nStartTime))
			{
//				nTargTime.QuadPart = nStartTime.QuadPart + nFreq.QuadPart * time;

				// XXX: conversion from 'double' to '__int64', possible loss of data
				xxx.QuadPart = nFreq.QuadPart * time;

				// TODO: protect from counter wrap-around!!!

				while (QueryPerformanceCounter(&nCurrTime))
				{
					yyy.QuadPart = nCurrTime.QuadPart - nStartTime.QuadPart;
					
					if (yyy.QuadPart > xxx.QuadPart)
						return true;

//					if (nCurrTime.QuadPart >= nTargTime.QuadPart)
//						break;

					// got time to nap???
					
					if (1)
					{
						DWORD dwMilliseconds = 0;

						DWORD sleep_threshold     = 1; // milliseconds
						DWORD sleep_threshold_max = 2000;
						DWORD sleep_factor        = 2;

						if (yyy.QuadPart >= sleep_threshold * nFreq.QuadPart / 1000)
						{
							// warning C4244: '=' : conversion from '__int64' to 'unsigned long', possible loss of data
							dwMilliseconds = 1000 * yyy.QuadPart / nFreq.QuadPart / sleep_factor;

							if (dwMilliseconds > sleep_threshold_max) // warning C4018: '>' : signed/unsigned mismatch
							{
								dwMilliseconds = sleep_threshold_max;
							}
						}
						
//						if (nCurrTime.QuadPart + 2 * nFreq.QuadPart * time / 1000 < nTargTime.QuadPart)
//							Sleep(sleep_threshold);

						Sleep(dwMilliseconds);
					}
				}
			}
		}
	}

	return false;


/*

	// TODO...
	
	art_time_t start = time_hires();

	art_time_t target = time_add_sec(start, time);

	for (;;)
	{
		if (time_hires() < target)
		{
		}
	}

*/


#else

#if 0
	MMRESULT res = timeBeginPeriod(uPeriod);
#endif

	DWORD dwStartTime = timeGetTime();

	for (;;)
	{
		DWORD dwElapsedTime = timeGetTime() - dwStartTime;

		if (dwElapsedTime > 5)
			break;
	}

#if 0
	if (res == TIMERR_NOERROR)
		timeEndPeriod(uPeriod);
#endif

#endif
}

#endif

//
//
//

#ifndef REMOVE_THIS

f32 delay2(f32 timeout)
{
	art_time_t t1 = time_hires();

	for (;;)
	{
		art_time_t t2 = time_hires();

		f64 elapsed;
		
		if (time_delta_seconds(t1, t2, elapsed))
		{
			if (elapsed >= timeout)
				return elapsed;

			if (0)
			{
				Sleep(0);
			}
		}
		else
		{
			break;
		}
	}

	return 0.0;
}

#endif

#endif

//
//
//

// XREF: time_check_interval

f32 wait__(f64 &t2, f32 freq)
{
	f64 t4 = time_monotonic();

	f32 sec = 0.0;

	f32 t5 = 0.0;

	if (freq != 0.0)
		t5 = 1.0 / freq;
	
	if (t2 == 0)
		sec = 1.0 / freq;
	else if (t2 + t5 > t4)
		sec = ((t2 + t5) - t4);
	
	// simple way to wait (using a timer may result in less jitter)
	
	Sleep(sec * 1000);

	f64 t1 = time_monotonic();

	t2 = t1;

	return sec;
}

//
//
//

void timer_dtor(run_at_freq_t *timer);

run_at_freq_t::run_at_freq_t()
{
	freq = 0;
	timer = 0;
}

run_at_freq_t::~run_at_freq_t()
{
	timer_dtor(this);
}

#ifndef LITE_RTL

void timer_dtor(run_at_freq_t *timer)
{
	if (timer->timer != 0)
	{
		// NOTE: the system will make a call to timeEndPeriod() on our behalf
		timeKillEvent(timer->timer);
		timer->timer = 0;
	}

	timer->event.close();
}

static bool run_at_freq_init(run_at_freq_t &t, f32 hertz)
{
	// NOTE: the timer structure is not designed to be shared across threads

	_ASSERT(t.freq != hertz);

	t.freq = 0;

	if (t.timer != 0)
	{
		// NOTE: the system will make a call to timeEndPeriod() on our behalf
		timeKillEvent(t.timer);
		t.timer = 0;
	}

	if (t.event.handle() == NULL)
		t.event.create_auto_reset();

#ifdef _DEBUG
	TIMECAPS TimeCaps = { 0 };

	MMRESULT result = timeGetDevCaps(&TimeCaps, sizeof(TimeCaps));
#endif

	UINT wDelay = 1000 / hertz; // delay, in milliseconds
	UINT wResolution = 1000 / hertz; // in milliseconds

	// NOTE: the system will make a call to timeBeginPeriod() on our behalf

	t.timer = timeSetEvent(wDelay, wResolution,
		(LPTIMECALLBACK)t.event.handle(), 0,
		TIME_PERIODIC | TIME_CALLBACK_EVENT_SET);

	if (t.event.handle() != NULL && t.timer != 0)
	{
		t.freq = hertz;
		return true;
	}

	return false;
}

bool run_at_freq(run_at_freq_t &t, f32 hertz)
{
	// NOTE: the timer structure is not designed to be shared across threads

	if (hertz >= 0)
	{
		if (t.freq == hertz || run_at_freq_init(t, hertz))
		{
			_ASSERT(t.event.handle() != NULL);

#if 0
			task_t *task = NULL;

			if (task != NULL)
			{
				if (task->m_event.is_valid_handle())
				{
				}
			}
#endif

#if 0
// TODO...

			return wait(t.event.wait(), task->m_event()) == 1;

if (handle != NULL)
{
 	DWORD result = WaitForMultipleObjects(handle, millisec(timeout));

	switch (result)
	{
		case WAIT_OBJECT_0:
			return 1;

		case WAIT_TIMEOUT:
			_ASSERT(timeout != NULL);
			return 0;

		default:
			DBG_BREAK();
			break;
	}
}

#endif

			return t.event.wait();
		}
	}

	return false;
}

#endif

//
//
//

bool reset_timer(timer_id_t &uIDEvent, UINT uElapse, TIMERPROC lpTimerProc)
{
	if (uIDEvent != 0)
	{
		VERIFY(KillTimer((HWND)NULL, uIDEvent));
	}

	if (lpTimerProc != NULL)
	{
		uIDEvent = SetTimer((HWND)NULL, 0, uElapse, lpTimerProc);

		return (uIDEvent != 0);
	}
	else
	{
		uIDEvent = 0;

		return true;
	}

	return false;
}

