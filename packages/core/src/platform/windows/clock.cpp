/*------------------------------------------------------------------------------

	platform/windows/clock.cpp

------------------------------------------------------------------------------*/

#include "pch.h"
#include <time.h>
#include "core.h"

#include "../../lib/elements/time.h"
#include "../../lib/misc/misc.h"

//
//
//

#ifndef LITE_RTL

#ifndef REMOVE_THIS
core_global_data_t g_core_global_data;
#endif

#ifdef _DEBUG
static bool g_debug_trace_timer = false;
#endif

// XREF: delay()
// XREF: __host_timer_info2()

#ifndef REMOVE_THIS
static int g_get_time_select = 1; // use QPC, GetTickCount, or GetSystemTimeAsFileTime
#endif

// beware of jumping QPC...  http://support.microsoft.com/kb/274323

static clock_info_t g_time_qpc; // QueryPerformanceCounter
static clock_info_t g_time_gtc; // GetTickCount
static clock_info_t g_time_gst; // GetSystemTimeAsFileTime

static double get_time_qpc()
{
	LARGE_INTEGER t;

	if (g_time_qpc.period == 0)
	{
		if (QueryPerformanceFrequency(&t))
		{
			_ASSERT(t.QuadPart != 0);

			g_time_qpc.period = 1.0 / t.QuadPart;
		}
		else
		{
			g_time_qpc.period = float_qnan();
		}
	}

#ifdef _DEBUG
	if (QueryPerformanceFrequency(&t))
		_ASSERT(g_time_qpc.period == 1.0 / t.QuadPart);
#endif
	
	if (QueryPerformanceCounter(&t))
	{
		// TODO: CMPXCHG8B

		if (g_time_qpc.base == 0)
		{
#ifdef _DEBUG
			// in debug builds, add a bias to verify we can tolerate large
			// integer values without experiencing unacceptable loss of floating
			// point numeric precision

			g_time_qpc.base = t.QuadPart - (1 << 22) / g_time_qpc.period;
#else
			g_time_qpc.base = t.QuadPart;
#endif
		}

#ifdef _DEBUG
		if (g_time_qpc.last != 0)
		{
			if (t.QuadPart < g_time_qpc.last)
			{
				xinc(g_time_qpc.retrograde);

#ifdef _DEBUG
				trace("QPC moved backwards from ", g_time_qpc.last, " to ", t.QuadPart, "\n");
#endif
			}
		}

		g_time_qpc.last = t.QuadPart;
#endif

		return (t.QuadPart - g_time_qpc.base) * g_time_qpc.period;
	}

	return float_qnan();
}

static double get_time_gtc()
{
	DWORD t = GetTickCount(); // milliseconds since boot
		
	if (g_time_gtc.period == 0)
		g_time_gtc.period = 1.0 / 1000.0;
	
	if (g_time_gtc.base == 0)
		g_time_gtc.base = t;

	return (t - g_time_gtc.base) * g_time_gtc.period;
}

static double get_time_gst()
{
	FILETIME t = { 0, 0 };

	GetSystemTimeAsFileTime(&t);

	LARGE_INTEGER t_;

	t_.LowPart = t.dwLowDateTime;
	t_.HighPart = t.dwHighDateTime;

	if (g_time_gst.period == 0)
		g_time_gst.period = 1.0 / 10000000.0; // resolution is 100 ns
	
	if (g_time_gst.base == 0)
		g_time_gst.base = t_.QuadPart;

	return (t_.QuadPart - g_time_gst.base) * g_time_gst.period;
}

#ifdef _DEBUG

static const clock_source_t g_clock_sources[] =
{
	{ NULL, NULL, get_time_qpc, &g_time_qpc },
	{ NULL, NULL, get_time_gtc, &g_time_gtc },
	{ NULL, NULL, get_time_gst, &g_time_gst }
};

#endif

//
//
//

static double get_time__(int select);

#ifdef _DEBUG

// XXX: this code is essentially duplicated in posix/clock.cpp and should be
// factored into core/clock.cpp

static int g_test_get_time;

static void test_get_time()
{
	for (int select = 1; select <= 3; select++)
	{
		f64 t1 = get_time__(select);
		f64 t2 = t1;

		// wait for change
		
		for (;;)
		{
			t2 = get_time__(select);

			if (t2 != t1)
				break;
		}

		// wait for change

		t1 = get_time__(select);

		for (;;)
		{
			t2 = get_time__(select);

			if (t2 != t1)
				break;
		}

		trace("test_get_time(): select=", select, ", resolution=~", (t2 - t1) * 1000.0, " msec\n");
	}
}

#endif

//
//
//

static double get_time__(int select)
{
	//
	//
	//

#ifdef _DEBUG
	if (g_test_get_time == 0)
	{
		g_test_get_time = 1;

		test_get_time();
	}
#endif

	//
	//
	//
	
	switch (select)
	{
		case 1: return get_time_qpc();
		case 2: return get_time_gtc();
		case 3: return get_time_gst();
	}

	//
	//
	//

	return float_qnan();
}

//
//
//

double time_monotonic()
{
	return get_time__(1);
}

//
//
//

double time_realtime()
{
// ?!?!?!?!?!?!?!?!?!?!?!?!?

	// TODO: we need a real high res clock to query, but GetSystemTimeAsFileTime
	// updates only every ~15.6 msec

	double t = time_monotonic(); // local high-resolution time, in seconds

	// adjust the bias to put zero at the UNIX epoch

	static time_t bias = time(NULL);
	
	t += bias;

	return t;

// ?!?!?!?!?!?!?!?!?!?!?!?!?

}

#endif

//
//
//

uint64_t posix_timestamp()
{
	double t = time_realtime();
	
	return (uint64_t)(t * 1000000); // seconds to microseconds
}

#ifndef LITE_RTL

//
//
//

#ifndef REMOVE_THIS

void timer_info2()
{
	// XXX: we may have some memory barrier troubles here (i.e. we need to
	// guarantee that status is written last)!!!
	
	_ASSERT(g_core_global_data.timer_status == 0);

	__host_timer_info2();

	//
	//
	//

#ifdef _DEBUG

	if (g_core_global_data.timer_status == 1)
	{
		// NOTE: the TRACE output must occur *AFTER* setting the timer status,
		// so we don't infinitely recurse by re-entering this function.

		if (g_debug_trace_timer)
		{
			TRACE("TIME: high resolution timer frequency is %f MHz\n",
				(1.0 / g_core_global_data.timer_info.period) / 1000000.0);

			// establish performance overhead for profile functions (loop
			// 100000 times or 1/10th second, whichever comes first)

			timer_thing_t thing;

			int i;
			
			for (i = 0; i < 100000 && next_iteration(thing, 0.1); i++)
			{
			}

			if (i != 0)
			{
				double delta = time_delta_seconds(thing.start_time, thing.last);

				TRACE("TIME: approximate timer overhead...  ");

				TRACE("%i calls in %f seconds (%f �sec/call)\n",
					i, delta, 1000000.0 * delta / i);
			}
		}
	}

#endif

	_ASSERT(g_core_global_data.timer_status != 0);
}

#endif

static bool time_convert_to_100ns(art_time_t &t);

//
//
//

art_time_t time_system_utc()
{
	return __host_time_system_utc();
}

//
//
//

art_time_t time_hires()
{
	return __host_time_hires();
}

//
//
//

art_time_t time_current()
{
	art_time_t t;
	
	if (time_current__(t))
		return t;

	return time_error();
}

//
//
//

// NOTE: currently limited to 100ns resolution, regardless of high
// performance timer resolution !!!

bool time_current__(art_time_t &current)
{
	// XXX: force base timer counts to be established before getting
	// current counts through time_hires()
	
	if (timer_info())
	{
		art_time_t t = time_hires();
		
		if (time_convert_to_100ns(t))
		{
			current = t;

			return true;
		}
	}

	return false;
}

//
//
//

// TODO: provide high-resolution counts from time_current() and
// use this conversion function to get the FILETIME compatible values
// instead of loosing the resolution in time_current().  Or use
// time_hires() instead.

static bool time_convert_to_100ns(art_time_t &t)
{
	if (t.type == &g_time_type_perf)
	{
		u64 counts = t.value;
		
		if (timer_info())
		{
			_ASSERT(counts >= g_core_global_data.timer_info.base_counts); // XXX: signed/unsigned mismatch

			// offset from base hires time counts, convert to FILETIME units,
			// and add base FILETIME time...

			// TODO: handle timer wrap-around here!!!?!?

			// convert performance counter units into FILETIME units (note
			// that this step limits our resolution to 100ns, regardless of
			// how fast the high performance counter runs)
			
			// TODO: use integer math here, instead, if possible???
			
			// NOTE: (s64) used here since unsigned 64-bit integer math is
			// not implemented by VC++ 6.0

			s64 y = counts - g_core_global_data.timer_info.base_counts;

			// XXX: conversion from 'double' to '__int64', possible loss of data
			s64 z = y * g_core_global_data.timer_info.factor__; // hires-to-FILETIME
			
			s64 x = g_core_global_data.timer_info.base_time + z;

			t = time_from_filetime_utc(u64_to_filetime(x));

			return true;
		}
	}

	return false;
}

//
//
//

// XXX: use time_delta_seconds() calls instead???

#ifndef REMOVE_THIS

double get_timer_period()
{
	if (timer_info())
	{
		return g_core_global_data.timer_info.period;
	}
	else
	{
		return 0.0;
	}
}

#endif

//
//
//

#if defined(_WIN32_WCE)

static VOID GetSystemTimeAsFileTime(LPFILETIME lpSystemTimeAsFileTime)
{
	// NOTE: this limits time resolution to milliseconds, per SYSTEMTIME,
	// instead of the 100ns resolution expressed in FILETIME
	
	SYSTEMTIME st;
	
	GetSystemTime(&st);

	FILETIME ft = { 0, 0 };
	
	if (SystemTimeToFileTime(&st, &ft))
	{
		*lpSystemTimeAsFileTime = ft;
	}
	else
	{
		*lpSystemTimeAsFileTime = ft;
	}
}

#endif

//
//
//

#ifndef REMOVE_THIS

void __host_timer_info2()
{
	//
	//  Establish a time basis.  We use the relatively low resolution system
	//  time to get a baseline time, then get the high resolution timer counts
	//  so we can map them to FILETIME.
	//

	// TODO: use time_system_utc()???

	FILETIME base_time;
	LARGE_INTEGER base_counts;
	LARGE_INTEGER frequency;

	GetSystemTimeAsFileTime(&base_time);

	g_core_global_data.timer_info.base_time = filetime_to_u64(base_time);

	if (QueryPerformanceCounter(&base_counts))
	{
		g_core_global_data.timer_info.base_counts = base_counts.QuadPart;

		//
		//  Determine high resolution timer frequency and pre-calculate
		//  frequently needed conversions factors.
		//
		
		if (QueryPerformanceFrequency(&frequency))
		{
			if (frequency.QuadPart != 0)
			{
				_ASSERT(frequency.QuadPart > 0);

				g_core_global_data.timer_info.period = 1.0 / frequency.QuadPart;

				g_core_global_data.timer_info.factor__ = 10000000.0 / frequency.QuadPart; // hires-to-FILETIME

				g_core_global_data.timer_info.factor2__ = frequency.QuadPart / 1000.0; // ms-to-hires
			}

			g_core_global_data.timer_status = 1;
		}
		else
		{
			g_core_global_data.timer_status = -1;
		}
	}
	else
	{
		g_core_global_data.timer_status = -1;
	}
}

#endif

//
//
//

time_data_t __host_time_system_utc()
{
	// Get current system time in UTC format

	FILETIME filetime;

	GetSystemTimeAsFileTime(&filetime);

	// TODO: indicate that this is *system* time, accurate only to the clock
	// interrupt rate, not necessarily the 100ns resolution implied by
	// art_time_type_100ns

	return time_from_filetime_utc(filetime);
}

time_data_t __host_time_hires()
{
	// XXX: replace with time_current (can't yet since that function
	// converts the value from hires counter units to 100ns units!!!)
	
	LARGE_INTEGER time;

	if (QueryPerformanceCounter(&time))
		return time_data(&g_time_type_perf, time.QuadPart);

	// The installed hardware does not support a high-performance counter, no
	// additional error information is available.

	return time_error();
}

#endif

