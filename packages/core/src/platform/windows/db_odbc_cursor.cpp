/*------------------------------------------------------------------------------

	db_odbc_cursor.cpp - ODBC cursor support

	TODO: SQL_ATTR_ROW_STATUS_PTR
	TODO: SQL_ATTR_ROW_ARRAY_SIZE

------------------------------------------------------------------------------*/

#include "pch.h"
#include <stdlib.h>
#include "db_odbc.h"

//
//
//

static inline size_t Align32(size_t ul)
{
	return ((ul + 3L) & ~3L);
}

//
//
//

#ifndef REMOVE_EXPERIMENTAL

static const struct { const char *name; SQLINTEGER Attribute; type_t type; } g_odbc_stmt_attr[] = 
{
	{ "SQL_ATTR_APP_PARAM_DESC",        SQL_ATTR_APP_PARAM_DESC,         type_code_u32 },
	{ "SQL_ATTR_APP_ROW_DESC",          SQL_ATTR_APP_ROW_DESC,           type_code_u32 },
	{ "SQL_ATTR_ASYNC_ENABLE",          SQL_ATTR_ASYNC_ENABLE,           type_code_u32 },
	{ "SQL_ATTR_CONCURRENCY",           SQL_ATTR_CONCURRENCY,            type_code_u32 },
	{ "SQL_ATTR_CURSOR_SCROLLABLE",     SQL_ATTR_CURSOR_SCROLLABLE,      type_code_u32 },
	{ "SQL_ATTR_CURSOR_SENSITIVITY",    SQL_ATTR_CURSOR_SENSITIVITY,     type_code_u32 },
	{ "SQL_ATTR_CURSOR_TYPE",           SQL_ATTR_CURSOR_TYPE,            type_code_u32 },
	{ "SQL_ATTR_ENABLE_AUTO_IPD",       SQL_ATTR_ENABLE_AUTO_IPD,        type_code_u32 },
	{ "SQL_ATTR_FETCH_BOOKMARK_PTR",    SQL_ATTR_FETCH_BOOKMARK_PTR,     type_code_u32 },
	{ "SQL_ATTR_IMP_PARAM_DESC",        SQL_ATTR_IMP_PARAM_DESC,         type_code_u32 },
	{ "SQL_ATTR_IMP_ROW_DESC",          SQL_ATTR_IMP_ROW_DESC,           type_code_u32 },
	{ "SQL_ATTR_KEYSET_SIZE",           SQL_ATTR_KEYSET_SIZE,            type_code_u32 },
	{ "SQL_ATTR_MAX_LENGTH",            SQL_ATTR_MAX_LENGTH,             type_code_u32 },
	{ "SQL_ATTR_MAX_ROWS",              SQL_ATTR_MAX_ROWS,               type_code_u32 },
	{ "SQL_ATTR_METADATA_ID",           SQL_ATTR_METADATA_ID,            type_code_u32 },
	{ "SQL_ATTR_NOSCAN",                SQL_ATTR_NOSCAN,                 type_code_u32 },
	{ "SQL_ATTR_PARAM_BIND_OFFSET_PTR", SQL_ATTR_PARAM_BIND_OFFSET_PTR,  type_code_u32 },
	{ "SQL_ATTR_PARAM_BIND_OFFSET_PTR", SQL_ATTR_PARAM_BIND_OFFSET_PTR,  type_code_u32 },
	{ "SQL_ATTR_PARAM_BIND_TYPE",       SQL_ATTR_PARAM_BIND_TYPE,        type_code_u32 },
	{ "SQL_ATTR_PARAM_OPERATION_PTR",   SQL_ATTR_PARAM_OPERATION_PTR,    type_code_u32 },
	{ "SQL_ATTR_PARAM_STATUS_PTR",      SQL_ATTR_PARAM_STATUS_PTR,       type_code_u32 },
	{ "SQL_ATTR_PARAMS_PROCESSED_PTR",  SQL_ATTR_PARAMS_PROCESSED_PTR,   type_code_u32 },
	{ "SQL_ATTR_PARAMSET_SIZE",         SQL_ATTR_PARAMSET_SIZE,          type_code_u32 },
	{ "SQL_ATTR_QUERY_TIMEOUT",         SQL_ATTR_QUERY_TIMEOUT,          type_code_u32 },
	{ "SQL_ATTR_RETRIEVE_DATA",         SQL_ATTR_RETRIEVE_DATA,          type_code_u32 },
	{ "SQL_ATTR_ROW_ARRAY_SIZE",        SQL_ATTR_ROW_ARRAY_SIZE,         type_code_u32 },
	{ "SQL_ATTR_ROW_BIND_OFFSET_PTR",   SQL_ATTR_ROW_BIND_OFFSET_PTR,    type_code_u32 },
	{ "SQL_ATTR_ROW_BIND_TYPE",         SQL_ATTR_ROW_BIND_TYPE,          type_code_u32 },
	{ "SQL_ATTR_ROW_NUMBER",            SQL_ATTR_ROW_NUMBER,             type_code_u32 },
	{ "SQL_ATTR_ROW_OPERATION_PTR",     SQL_ATTR_ROW_OPERATION_PTR,      type_code_u32 },
	{ "SQL_ATTR_ROW_STATUS_PTR",        SQL_ATTR_ROW_STATUS_PTR,         type_code_u32 },
	{ "SQL_ATTR_ROWS_FETCHED_PTR",      SQL_ATTR_ROWS_FETCHED_PTR,       type_code_u32 },
	{ "SQL_ATTR_SIMULATE_CURSOR",       SQL_ATTR_SIMULATE_CURSOR,        type_code_u32 },
	{ "SQL_ATTR_USE_BOOKMARKS",         SQL_ATTR_USE_BOOKMARKS,          type_code_u32 },
};

static const struct { SQLINTEGER Attribute; SQLINTEGER Value; } g_rgStmtAttrValues[] =
{
	{ SQL_ATTR_ASYNC_ENABLE,       SQL_ASYNC_ENABLE_OFF },
	{ SQL_ATTR_ASYNC_ENABLE,       SQL_ASYNC_ENABLE_ON  },

	{ SQL_ATTR_CONCURRENCY,        SQL_CONCUR_READ_ONLY },
	{ SQL_ATTR_CONCURRENCY,        SQL_CONCUR_LOCK },
	{ SQL_ATTR_CONCURRENCY,        SQL_CONCUR_ROWVER},
	{ SQL_ATTR_CONCURRENCY,        SQL_CONCUR_VALUES },

	{ SQL_ATTR_CURSOR_SCROLLABLE,  SQL_NONSCROLLABLE}, // -1, 0
	{ SQL_ATTR_CURSOR_SCROLLABLE,  SQL_SCROLLABLE }, // -1, 1

	{ SQL_ATTR_CURSOR_SENSITIVITY, SQL_UNSPECIFIED},
	{ SQL_ATTR_CURSOR_SENSITIVITY, SQL_INSENSITIVE},
	{ SQL_ATTR_CURSOR_SENSITIVITY, SQL_SENSITIVE},

	{ SQL_ATTR_CURSOR_TYPE,        SQL_CURSOR_FORWARD_ONLY },
	{ SQL_ATTR_CURSOR_TYPE,        SQL_CURSOR_STATIC },
	{ SQL_ATTR_CURSOR_TYPE,        SQL_CURSOR_KEYSET_DRIVEN },
	{ SQL_ATTR_CURSOR_TYPE,        SQL_CURSOR_DYNAMIC },

	{ SQL_ATTR_ENABLE_AUTO_IPD,    SQL_TRUE},
	{ SQL_ATTR_ENABLE_AUTO_IPD,    SQL_FALSE},

	{ SQL_ATTR_CURSOR_TYPE,        },

	{ SQL_NOSCAN,                  SQL_NOSCAN_OFF},
	{ SQL_NOSCAN,                  SQL_NOSCAN_ON},

	{ SQL_ATTR_ASYNC_ENABLE,       SQL_ASYNC_ENABLE_OFF},
	{ SQL_ATTR_ASYNC_ENABLE,       SQL_ASYNC_ENABLE_ON},

	{ SQL_CONCURRENCY,             SQL_CONCUR_READ_ONLY},
	{ SQL_CONCURRENCY,             SQL_CONCUR_LOCK},
	{ SQL_CONCURRENCY,             SQL_CONCUR_ROWVER},
	{ SQL_CONCURRENCY,             SQL_CONCUR_VALUES},

	{ SQL_CURSOR_TYPE,             SQL_CURSOR_FORWARD_ONLY},
	{ SQL_CURSOR_TYPE,             SQL_CURSOR_KEYSET_DRIVEN},
	{ SQL_CURSOR_TYPE,             SQL_CURSOR_DYNAMIC},
	{ SQL_CURSOR_TYPE,             SQL_CURSOR_STATIC},

	{ SQL_SIMULATE_CURSOR,         SQL_SC_NON_UNIQUE},
	{ SQL_SIMULATE_CURSOR,         SQL_SC_TRY_UNIQUE},
	{ SQL_SIMULATE_CURSOR,         SQL_SC_UNIQUE},

	{ SQL_RETRIEVE_DATA,           SQL_RD_OFF},
	{ SQL_RETRIEVE_DATA,           SQL_RD_ON},

	{ SQL_USE_BOOKMARKS,           SQL_UB_OFF},
	{ SQL_USE_BOOKMARKS,           SQL_UB_FIXED},
	{ SQL_USE_BOOKMARKS,           SQL_UB_VARIABLE},
};

#endif

//
//
//

OBJECT_CLASS_BEGIN(odbc_cursor_t, base_db_cursor_t)
OBJECT_CLASS_END()

//
//
//

odbc_cursor_t::~odbc_cursor_t()
{
	if (m_row_buffer.ptr != NULL)
		free(m_row_buffer.ptr);

	m_columns.header.len = 0;
	delete [] m_columns.array;
	m_columns.array = NULL;
}

//
//
//

bool odbc_cursor_t::close()
{
	switch (m_mode)
	{
		case MODE_DATA_SOURCES:
		{
			break;
		}

		case MODE_DRIVERS:
		{
			break;
		}

		case MODE_NORMAL:
		{
			break;
		}
		
		case MODE_CATALOG:
		{
			break;
		}

		case MODE_CONNECTION_ATTRIBUTES:
		{
			break;
		}

		case MODE_CURSOR_ATTRIBUTES:
		{
			break;
		}

		case MODE_METADATA:
		{
			break;
		}

		default:
		{
			DBG_INVALID_CASE();
			break;
		}
	}
	
	if (m_mode == MODE_DATA_SOURCES)
	{
		m_environment = NULL;

		return true;
	}

	if (m_mode == MODE_DRIVERS)
	{
		m_environment = NULL;

		return true;
	}

	if (m_mode == MODE_NORMAL)
	{
		_ASSERT(m_hstmt != NULL);

		if (SQLFreeStmt(m_hstmt, SQL_DROP) == SQL_SUCCESS)
		{
		}
		else
		{
			DBG_BREAK();
		}

		m_hstmt = SQL_NULL_HSTMT;

//		m_params.clear();

//		reset();

		return true;
	}

	return false;
}

//
//
//

int odbc_cursor_t::fetch()
{
	//
	//
	//
	
	if (m_mode == MODE_DATA_SOURCES)
		return fetch_sources();

	//
	//
	//
	
	if (m_mode == MODE_DRIVERS)
		return fetch_drivers();

	//
	//
	//
	
	if (m_mode == MODE_METADATA)
	{
		if (m_metadata_irow < m_columns.header.len)
		{
			m_metadata_irow++;

			return 0;
		}
	}

	//
	//
	//
	
	if (m_mode == MODE_CONNECTION_ATTRIBUTES)
	{
		unsigned int count = m_connection->connection_attribute_count();

		if (m_metadata_irow < count)
		{
			m_metadata_irow++;

			return 0;
		}
	}

	//
	//
	//
	
	if (m_mode == MODE_CURSOR_ATTRIBUTES)
	{
		if (m_metadata_irow < DIM(g_odbc_stmt_attr))
		{
			m_metadata_irow++;

			return 0;
		}
	}

	//
	//
	//

	if (m_mode == MODE_NORMAL || m_mode == MODE_CATALOG)
	{
		RETCODE rc = SQLFetch(m_hstmt);

		if (check_result(this, rc))
			return 0;
	}

	return -1;
}

//
//
//

#if 0

int odbc_cursor_t::field_evaluate__(unsigned int index, data_binding_t value)
{
	// NOTE: column ordinal is 1-based, not 0-based
	
	if (index > 0 && index <= m_columns.header.len)
	{
		const odbc_column_t &col = m_columns.array[index - 1];

		return odbc_field_evaluate(col.fCType, col.rgbValue, col.pcbValue, value);
	}

	return 0;
}

#endif

int odbc_cursor_t::field_evaluate(int index, data_binding_t value)
{
	return base_db_cursor_t::field_evaluate(index, value);
}

#ifndef REMOVE_THIS

variant_t odbc_cursor_t::evaluate_field(long i)
{
	//
	//
	//
	
	switch (m_mode)
	{
		case MODE_DATA_SOURCES:
			break;
	}

	//
	//
	//

	if (m_mode == MODE_DATA_SOURCES)
	{
		if (i == 0)
		{
			return m_dsn_name;
		}

		if (i == 1)
		{
			return m_dsn_desc;
		}

		return variant_error();
	}

	if (m_mode == MODE_DRIVERS)
	{
		if (i == 0)
		{
			return m_dsn_name;
		}

		if (i == 1)
		{
			return m_dsn_desc;
		}

		return variant_error();
	}

	if (m_mode == MODE_METADATA)
	{
		_ASSERT(m_metadata_irow >= 1 && m_metadata_irow <= m_columns.header.len);

		switch (i)
		{
			case 0: return col_desc(m_metadata_irow - 1).name;
			case 1: return col_desc(m_metadata_irow - 1).label;
			case 2: return col_desc(m_metadata_irow - 1).maxlength;
			case 3: return col_desc(m_metadata_irow - 1).precision;
			case 4: return col_desc(m_metadata_irow - 1).scale;
			case 5: return col_desc(m_metadata_irow - 1).nullable;
			case 6: return col_desc(m_metadata_irow - 1).fDisplaySize;
			case 7: return col_desc(m_metadata_irow - 1).fUnsigned;
//			case 8: return col_desc(m_metadata_irow - 1).fUpdatable;
			case 9: return col_desc(m_metadata_irow - 1).binding.type.init;
			case 10: return col_desc(m_metadata_irow - 1).fSqlType;
			case 11: return col_desc(m_metadata_irow - 1).fCType;
		}

		return variant_error();
	}

	if (m_mode == MODE_CONNECTION_ATTRIBUTES)
	{
		return m_connection->evaluate_field_connection_attribute(i, m_metadata_irow);
	}

	if (m_mode == MODE_CURSOR_ATTRIBUTES)
	{
		_ASSERT(m_metadata_irow >= 1 && m_metadata_irow <= DIM(g_odbc_stmt_attr));

		switch (i)
		{
			case 0: return g_odbc_stmt_attr[m_metadata_irow - 1].name;

			case 1:
			{
				DBG_NOT_IMPLEMENTED();
				
				return "XXX";
			}
		}

		return variant_error();
	}

	if (m_mode == MODE_NORMAL || m_mode == MODE_CATALOG)
	{
		const odbc_column_t &col = col_desc(i);

		// special case for large values
		
		if (col.fSqlType == SQL_LONGVARCHAR ||
			col.fSqlType == SQL_VARBINARY ||
			col.fSqlType == SQL_LONGVARBINARY ||
			col.fSqlType == SQL_WLONGVARCHAR)
		{
			// TODO: return a file object that allows access to the field

			if (0)
			{
				SQLRETURN rc = SQLSetPos(m_hstmt, 1, SQL_POSITION, SQL_LOCK_NO_CHANGE);
			}

			string_t result;

			int ucs2 = (col.fSqlType == SQL_WLONGVARCHAR);

			SQLSMALLINT fCType = SQL_C_BINARY;
				
			int iteration = 0;
				
			union { u8 data[1024]; u16 ucs2[1024/2]; } buffer;
			size_t buflen = sizeof(buffer);

			for (;;)
			{
				iteration++;
					
				SQLLEN len = 0;
				
				SQLRETURN rc = SQLGetData(m_hstmt, i + 1, fCType,
					&buffer, buflen, &len); // conversion from 'long' to 'SQLUSMALLINT', possible loss of data

				if (rc == SQL_NO_DATA)
				{
					_ASSERT(iteration == 1);
					_ASSERT(result.length() == 0);
					return variant_t(); // NULL value
				}
				else if (rc == SQL_SUCCESS)
				{
					if (ucs2 == 0)
					{
						result += string((char*)buffer.data, len);
					}
					else
					{
						_ASSERT((len % 2) == 0);
						
						result += string((unicode_char_t*)buffer.ucs2, len / 2);
					}

					return result;
				}
				else if (rc == SQL_SUCCESS_WITH_INFO)
				{
					// in this case, len is the number of bytes remaining before
					// the call (NOTE: we skip calling retrieve_messages in this
					// case, otherwise we will retrieve "String data, right
					// truncation" warnings, which we handle here, locally)

					if (0)
						retrieve_messages(NULL, NULL, m_hstmt, rc, m_message_queue);

					if (ucs2 == 0)
					{
						result += string((char*)buffer.data, buflen);
					}
					else
					{
						_ASSERT((buflen % 2) == 0);
						
						result += string((unicode_char_t*)buffer.ucs2, buflen / 2);
					}
				}
				else
				{
					retrieve_messages(NULL, NULL, m_hstmt, rc, m_message_queue);

					return variant_error();
				}
			}

			return result;
		}

		return odbc_to_variant(col.fCType, col.rgbValue, col.pcbValue);
	}

	return variant_error();
}

#endif

//
//
//

odbc_column_t &odbc_cursor_t::col_desc(unsigned int i) const
{
	_ASSERT(i >= 0 && i < m_columns.header.len);

	return m_columns.array[i];
}
//
//
//

long odbc_cursor_t::col_count()
{
	if (m_mode == MODE_METADATA)
	{
		return 12;
	}

	if (m_mode == MODE_CONNECTION_ATTRIBUTES)
	{
//		return DIM(g_odbc_conn_attr);
	}

	return m_columns.header.len;
}

//
//
//

string_t odbc_cursor_t::col_name(long i)
{
	if (m_mode == MODE_METADATA)
	{
		// TODO: create column entries and eliminate special cases here
		
		switch (i)
		{
			case 0: return "name";
			case 1: return "label";
			case 2: return "maxlength";
			case 3: return "precision";
			case 4: return "scale";
			case 5: return "nullable";
			case 6: return "fDisplaySize";
			case 7: return "fUnsigned";
			case 8: return "fUpdatable";
			case 9: return "variant_type";
			case 10: return "fSqlType";
			case 11: return "fCType";
		}
	}

	if (m_mode == MODE_CONNECTION_ATTRIBUTES)
	{
//		return g_odbc_conn_attr[i].m_name;
	}

	if (i >= 0 && i < m_columns.header.len)
		return m_columns.array[i].name;
	else
		return string_error();
}

//
//
//

string_t odbc_cursor_t::col_label(long i)
{
	if (m_mode == MODE_METADATA)
	{
		return col_name(i);
	}
	
	if (m_mode == MODE_CONNECTION_ATTRIBUTES)
	{
		return col_name(i);
	}

	if (i >= 0 && i < m_columns.header.len)
		return m_columns.array[i].label;
	else
		return string_error();
}

//
//
//

void odbc_cursor_t::metadata(db_cursor_t &cursor)
{
	odbc_cursor_t *driver = new odbc_cursor_t;

	if (driver != NULL)
	{
		cursor = db_cursor_create_and_release(driver, true);

		driver->m_mode = MODE_METADATA;
		driver->m_metadata_irow = 0; // driver->m_metadata_for = this;
		
		unsigned int ccol = col_count();
		
		if (driver->alloc_column_descriptors(ccol))
		{
			for (unsigned int i = 0; i < ccol; i++)
				driver->col_desc(i) = col_desc(i);
		}
	}
}

//
//
//

void odbc_cursor_t::attributes(db_cursor_t &cursor)
{
	odbc_cursor_t *driver = new odbc_cursor_t;

	if (driver != NULL)
	{
		cursor = db_cursor_create_and_release(driver, true);

		driver->m_mode = MODE_CURSOR_ATTRIBUTES;
		driver->m_metadata_irow = 0; // driver->m_metadata_for = this;

		unsigned int ccol = 2;
		
		if (driver->alloc_column_descriptors(ccol))
		{
			driver->col_desc(0).name = "Name";
			driver->col_desc(0).label = driver->col_desc(0).name;

			driver->col_desc(1).name = "Value";
			driver->col_desc(1).label = driver->col_desc(1).name;
		}
	}
}

//
//
//

bool odbc_cursor_t::more_results()
{
	if (m_mode == MODE_NORMAL)
	{
		RETCODE rc = SQLMoreResults(m_hstmt);
		
		if (!check_result(this, rc))
			return false;

		// next result set could have different columns, so rebind them

		m_colinfo_valid = false;

		get_col_desc();

		return true;
	}

	return false;
}

//
//
//

bool odbc_cursor_t::cancel()
{
	if (m_mode == MODE_NORMAL)
	{
		RETCODE rc = SQLCancel(m_hstmt);

		return check_result(this, rc);
	}

	return false;
}

//
//
//

bool odbc_cursor_t::get_col_desc()
{
	_ASSERT(m_hstmt != NULL);

#ifdef _DEBUG
	if (0)
	{
	
	// dump statement attributes
					
	for (int i = 0; i < DIM(g_odbc_stmt_attr); i++)
	{
		variant_t value;

		char buffer[256] = { 0 };

		SQLINTEGER len = 0;
				
		SQLRETURN rc = SQLGetStmtAttr(m_hstmt, g_odbc_stmt_attr[i].Attribute,
			buffer, sizeof(buffer), &len);

		if (rc == SQL_NO_DATA)
			value = variant_t();

		if (rc == SQL_SUCCESS)
		{
			switch (type_code(g_odbc_stmt_attr[i].type))
			{
				case type_code_u32:
//					_ASSERT(len == sizeof(u32));
					value = *((u32*)buffer);
					break;

				case type_code_string:
					value = string(buffer, len);
					break;
			}
		}

		trace("ODBC statement attribute: name=", g_odbc_stmt_attr[i].name, " value=", value, "\n");
	}

	}
#endif

	bool status = false;

	if (!get_column_info())
		return false;

	unsigned int ccol = m_columns.header.len;

	//
	//  allocate row buffer for raw data
	//

	void *rowbuf = NULL;

	for (int pass = 1; pass <= 2; pass++)
	{
		for (unsigned int i = 0; i < ccol; i++)
		{
			odbc_column_t &col = col_desc(i);
			
			SQLLEN cbCol = 0;

			const odbc_datatype_t *inf = get_odbc_datatype(col.fSqlType, col.fUnsigned);

			if (inf != NULL)
			{
				col.fCType = inf->fCType;
				col.binding.type = inf->type__;
				col.binding.size = inf->size;
	
				cbCol = inf->size;
			}
			else
			{
			}

			switch (col.fSqlType)
			{
				case SQL_INTEGER:
				case SQL_SMALLINT:
				case SQL_FLOAT:
				case SQL_REAL:
				case SQL_DOUBLE:
				case SQL_BIGINT:
				case SQL_TINYINT:
				case SQL_BIT:
					break;

				case SQL_CHAR:
				case SQL_VARCHAR:
					cbCol = sizeof(char) * col.fDisplaySize + 1;
					break;

				case SQL_LONGVARCHAR:
				case SQL_LONGVARBINARY:
				case SQL_WLONGVARCHAR:
//				case SQL_BINARY:
				case SQL_VARBINARY:
					cbCol = 0;
					break;					

				case SQL_WCHAR:
				case SQL_WVARCHAR:
					cbCol = sizeof(unsigned short) * col.fDisplaySize + 1;
					break;

#if 0
				case SQL_DECIMAL:
				case SQL_NUMERIC:
				case SQL_INTEGER:
				{
					if (col.scale == 0 && col.precision <= 5)
					{
						col.fCType = col.fUnsigned ? SQL_C_USHORT : SQL_C_SSHORT;
						col.type__ = make_type(col.fUnsigned ? type_code_u16 : type_code_s16);
						cbCol = sizeof(short);
						break;
					}

					if (col.scale == 0 && col.precision <= 10)
					{
						col.fCType = col.fUnsigned ? SQL_C_ULONG : SQL_C_SLONG;
						col.type__ = make_type(col.fUnsigned ? type_code_u32 : type_code_s32);
						cbCol = sizeof(long);
						break;
					}
				}
#endif

				default:
#ifdef _DEBUG
					TRACE2(NULL, trace_severity_warning, "odbc_cursor_t::get_col_desc():  unsupported ODBC SQL type %i in column '%s'.  col.fDisplaySize=%i, Binding as SQL_C_CHAR\n",
						col.fSqlType, (const char *)col.label, col.fDisplaySize);
#endif
					col.fCType = SQL_C_CHAR;
					col.binding.type = make_type(type_code_string);
					cbCol = sizeof(char) * col.fDisplaySize + 1;
					break;
			}

			if (cbCol != 0)
			{
				cbCol = Align32(cbCol); // TODO: align 64-bit values on 64-bit boundaries?!?!?!?

				col.rgbValue = rowbuf;

				rowbuf = ptr_offset(rowbuf, cbCol);

				col.pcbValue = (SQLLEN*)rowbuf;

				rowbuf = ptr_offset(rowbuf, sizeof(SQLLEN));

				if (pass == 2)
				{
					if (col.fCType != SQL_TYPE_NULL)
					{
						// NOTE: columns in SQLBindCol are one-based
						
						RETCODE rc = SQLBindCol(m_hstmt, i + 1, col.fCType, col.rgbValue,
							cbCol, col.pcbValue);
						
						if (!check_result(this, rc))
							return false;
					}
				}
			}
		}

		// accumulated buffer size is now known, allocate a row buffer

		if (pass == 1)
		{
			size_t cbrowbuf = (size_t)(rowbuf);

			rowbuf = alloc_rowbuf(1, cbrowbuf);

			if (rowbuf == NULL)
				return false;
		}
	}

#if 0
	//
	//  Tell ODBC how big our rows are (in bytes), and how many rows are in
	//  our rowset.
	//
	
	if (SUCCEEDED(hr))
	{
		rc = SQLSetStmtOption(m_hstmt, SQL_BIND_TYPE, cbRow);

		hr = StmtRc(rc);

		if (SUCCEEDED(hr))
		{
			rc = SQLSetStmtOption(m_hstmt, SQL_ROWSET_SIZE, crowset);
				
			hr = StmtRc(rc);
		}
	}

	//
	//
	//
	
	if (FAILED(hr))
		SQLFreeStmt(m_hstmt, SQL_UNBIND);

	return SUCCEEDED(hr);
#endif

	return true;
}

//
//
//

bool odbc_cursor_t::get_column_info()
{
	if (!m_colinfo_valid)
	{
		_ASSERT(m_hstmt != NULL);

		//  NOTE:  Access ODBC driver 7.0 will answer 0 for prepared procedures.
		//  Executed procedures behave normally.

		SQLSMALLINT ccol;

		if (!check_result(this, SQLNumResultCols(m_hstmt, &ccol)))
			return false;

		if (ccol < 0)
			return false;

		//
		//
		//

		if (!alloc_column_descriptors(ccol))
			return false;

		//
		//
		//

		for (unsigned int icol = 1; icol <= col_count(); icol++)
		{
			if (!load_col_desc(icol))
				return false;
		}

		m_colinfo_valid = true;
	}

	return true;
}

//
//
//

bool odbc_cursor_t::alloc_column_descriptors(unsigned int ccol)
{
	//
	//
	//
	
	m_columns.header.len = 0;
	delete [] m_columns.array;
	m_columns.array = NULL;

	if (m_row_buffer.ptr != NULL)
	{
		free(m_row_buffer.ptr);
		m_row_buffer.ptr = NULL;
	}

	m_row_buffer.count = 0;

	//
	//
	//
	
	m_columns.array = new odbc_column_t[ccol];

	if (m_columns.array == NULL)
		return false;

	m_columns.header.len = ccol;

	return true;
}

//
//
//

bool odbc_cursor_t::load_col_desc(unsigned int icol)
{
	// TODO: use SqlColAttribute???

	RETCODE rc;

	struct ODBC_STMT_COLUMN
	{
		SQLLEN fSqlType;
		SQLLEN fDisplaySize;
		SQLLEN fPrecision;
		SQLLEN fScale;
		SQLLEN cbLength;
		SQLLEN fUnsigned;
		SQLLEN fNullable;
		TCHAR szName[256]; // ?!?!?!?!?!?!?!?!?!?!?
		TCHAR szLabel[256]; // ?!?!?!?!?!?!?!?!?!?!?
	};

	ODBC_STMT_COLUMN temp = { 0 };
	
	struct X
	{
		UWORD fDescType;
		PTR rgbDesc;
		SQLLEN cbDescMax;
		SQLLEN *pfDesc;
	};

	#define SDWE(t,m) t, NULL, 0, &m
	#define PTRE(t,m) t, m, sizeof(m), NULL

	const X rgx[] = {
		PTRE(SQL_COLUMN_NAME,         temp.szName),
		SDWE(SQL_COLUMN_TYPE,         temp.fSqlType),
		SDWE(SQL_COLUMN_LENGTH,       temp.cbLength),
		SDWE(SQL_COLUMN_PRECISION,    temp.fPrecision),
		SDWE(SQL_COLUMN_SCALE,        temp.fScale),
		SDWE(SQL_COLUMN_DISPLAY_SIZE, temp.fDisplaySize),
		SDWE(SQL_COLUMN_NULLABLE,     temp.fNullable),
		SDWE(SQL_COLUMN_UNSIGNED,     temp.fUnsigned),
		PTRE(SQL_COLUMN_LABEL,        temp.szLabel),
	};

	#undef SDWE
	#undef PTRE

	for (int i = 0; i < DIM(rgx); i++)
	{
		SQLSMALLINT cbDesc;

		rc = SQLColAttributes(m_hstmt, icol, rgx[i].fDescType,
			rgx[i].rgbDesc, rgx[i].cbDescMax, &cbDesc, rgx[i].pfDesc); // conversion from 'const SQLINTEGER' to 'SQLSMALLINT', possible loss of data

		if (!check_result(this, rc))
			return false;
	}

	// these are the only ODBC-defined values
	
	_ASSERT(temp.fUnsigned == SQL_TRUE || temp.fUnsigned == SQL_FALSE);
	
	_ASSERT(temp.fNullable == SQL_NO_NULLS || temp.fNullable == SQL_NULLABLE ||
		temp.fNullable == SQL_NULLABLE_UNKNOWN);

	odbc_column_t &col = col_desc(icol - 1);

	col.name         = temp.szName;
	col.label        = temp.szLabel;
	col.fSqlType     = temp.fSqlType; // conversion from 'SQLINTEGER' to 'SQLSMALLINT', possible loss of data
	col.maxlength    = temp.cbLength;
	col.precision    = temp.fPrecision;
	col.scale        = temp.fScale;
	col.fDisplaySize = temp.fDisplaySize;
	col.fUnsigned    = temp.fUnsigned;

	col.nullable     = (temp.fNullable == SQL_NULLABLE);

	col.binding.type = odbc_to_variant_type(temp.fSqlType, temp.fUnsigned); // conversion from 'SQLINTEGER' to 'SQLSMALLINT', possible loss of data
	col.binding.size = 0;
	col.binding.data = NULL;
	col.fCType       = 0;
	col.rgbValue     = NULL;
	col.pcbValue     = NULL;

	if (is_error(col.name))
		return false;

	if (is_error(col.label))
		return false;

	return true;
}

//
//
//

void *odbc_cursor_t::alloc_rowbuf(unsigned int row_max, size_t cbrow)
{
	_ASSERT(m_row_buffer.ptr == NULL);
	
	m_row_buffer.count = row_max;

	m_row_buffer.ptr = malloc(cbrow * m_row_buffer.count);

	return m_row_buffer.ptr;
}

//
//
//

bool db_odbc_message(db_cursor_t &cursor, string_t &text, int32_t *fNativeError)
{
	C_ASSERT(sizeof(SDWORD) == sizeof(int32_t));

	odbc_cursor_t *obj = db_cursor_cast<odbc_cursor_t>(cursor);

	if (obj != NULL)
	{
		if (obj->m_message_queue.length() > 0)
		{
			odbc_msg_t *msg = obj->m_message_queue.unlink_head();

			C_ASSERT(sizeof(char) == sizeof(msg->szMsg[0]));
			
			text = (char*)msg->szMsg;

			if (fNativeError != NULL)
				*fNativeError = msg->fNativeErr;

			delete msg;

			return true;
		}
	}

	return false;
}

//
//
//

bool check_result(odbc_cursor_t *csr, RETCODE rc)
{
	retrieve_messages(NULL, NULL, csr->m_hstmt, rc, csr->m_message_queue);

	return odbc_success(rc);
}

