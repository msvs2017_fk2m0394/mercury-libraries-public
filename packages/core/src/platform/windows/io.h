/*------------------------------------------------------------------------------

	platform/windows/io.h - Windows I/O support

------------------------------------------------------------------------------*/

#ifndef __WIN_IO_H__
#define __WIN_IO_H__

#include <mercury/extensions/io.h>
#include <mercury/extensions/db.h>

#include "core.h"

class win32_io_operation_t;

//
//
//

struct windows_file_system_t : public base_io_system_t
{
	virtual db_cursor_t query(const io_system_query_t &query);
	virtual file_t      open(string_t filename, u32 flags);
	virtual bool        create(string_t filename);
	virtual bool        remove(string_t filename);
};

//
//
//

class win32_io_device_t : public base_io_stream_t
{
public:
	win32_io_device_t(HANDLE hFile, LPVOID lpMapBase, string_t filename);

	// *** base_io_stream_t methods ***
	
	virtual ~win32_io_device_t();

	virtual void status(io_stream_status_t &status);
	virtual bool close();
	virtual void info(io_stream_info_t &info);
	virtual bool read(io_read_t &data);
	virtual bool write(io_write_t &data);
	virtual bool flush();
	virtual void get_position(file_pos_t &pos);
	virtual bool set_position(file_pos_t pos);
	virtual void get_length(file_pos_t &pos);
	virtual bool set_length(file_pos_t pos);
	virtual void file_address(file_addr_t &addr);
	virtual bool allocate_operation_extension(io_operation_object_t *op);

	win32_io_operation_t *lookup_op(uint64_t async_id);

	string_t                          m_pathname;   //
	HANDLE                            m_hFile;      // file handle
	HANDLE                            m_hMapping;   // memory-mapped files only
	LPVOID                            m_lpMapBase;  // base address (memory-mapped files only)
	bool                              m_overlapped; // file was opened for overlapped operation
	ptr_array_t<win32_io_operation_t> m_async_ops;  //
};

//
//
//

// XREF: io_helper_t

class win32_io_operation_t : public base_io_operation_t
{
public:

	win32_io_operation_t();
	~win32_io_operation_t();

	// *** base_io_operation_t methods ***
	virtual bool cancel();
	virtual bool close() { return cancel(); }
	virtual void status(io_operation_status_t &status);
	virtual bool wait(u32 flags, u32 timeout_msec);

	io_operation_object_t *         m_header;           // back-pointer
	win32_io_device_t *             m_device;           //
	sync_event_t                    m_event;            //
	OVERLAPPED                      m_overlapped;       //
	io_status_enum_t                m_status;           //
	DWORD                           m_data_transferred; //
	DWORD                           m_error;            //
};

//
//
//

#ifdef _WINIOCTL_

#ifndef _WIN32_WCE

struct DRIVE_LAYOUT
{
	DRIVE_LAYOUT_INFORMATION info;     //
	PARTITION_INFORMATION    more[31]; //
};

// XXX: ANSI/UNICODE build dependent...

// NOTE: must currently be ZERO_INIT() safe...

struct io_volume_info_t
{
	UINT             uType;                  //

	bool             volinfo_valid;          // if true, the following five fields are valid
	TCHAR            volume_name[MAX_PATH];  //
	DWORD            volume_serial;          //
	DWORD            fs_max_comp_len;        // maximum component length
	DWORD            fs_flags;               // FS_*, FILE_SUPPORTS_*, FILE_VOLUME_QUOTAS
	TCHAR            fs_name[MAX_PATH];      //
	DWORD            volinfo_error;          //

	bool             dfs_valid;              // if true, the following three fields are valid
	u64              dfs_a;                  // number of free bytes available to caller
	u64              dfs_b;                  // number of total bytes available to caller
	u64              dfs_c;                  // number of free bytes
	DWORD            dfs_error;              //

	bool             disk_geometry_valid;    //
	DWORD            disk_geometry_error;    //
	DISK_GEOMETRY    disk_geometry;          //

	bool             drive_layout_valid;     //
	DWORD            drive_layout_error;     //
	DRIVE_LAYOUT     drive_layout;           //

	bool             disk_performance_valid; //
	DWORD            disk_performance_error; //
	DISK_PERFORMANCE disk_performance;       //
};

void io_volume_info(string_t szDrive, u32 flags, io_volume_info_t &info);

#endif

#endif

//
//
//

extern "C" {
HANDLE file_handle(file_t &file);
HANDLE pipe_read_handle(file_t &file);
HANDLE pipe_write_handle(file_t &file);
void pipe_close_read_handle(file_t &file);
void pipe_close_write_handle(file_t &file);
bool device_io_control__(io_stream_t &device, unsigned int code, data_binding_t in, data_binding_t out);
}

file_t file_from_handle(HANDLE hFile);

//
//
//

struct file_entry_t
{
	file_entry_t()
	{
		m_handle = INVALID_HANDLE_VALUE;
	}
	
	~file_entry_t();

	HANDLE   m_handle;   // handle returned by Win32 API FindFirstFile
	string_t m_filespec; //
};

//
//
//

// XREF: similar structure in Mercury setup library

struct file_info_t // : base_object2_t
{
	file_info_t()
	{
		ZERO_INIT(m_find_data);

//		m_file_version_valid = false;
	}

	WIN32_FIND_DATAA m_find_data;          //
//	bool             m_file_version_valid; //
//	module_version_t m_file_version;       //
//	string_t         m_file_type_name;     // cached value
};

//
//
//

// breadth-first traversal

class find_file_driver_t : public base_db_cursor_t
{
public:
	bool open(string_t filespec);
	bool open(const io_system_query_t *query);

	// *** base_db_cursor_t methods ***
	virtual int       fetch();
	virtual variant_t evaluate_field(long i);
	virtual bool      close();
	virtual const db_column_binding_info_t *get_table_info() const;

	unsigned int              m_max_depth;  // maximum allowed directory recursion depth (0=none)
	ptr_array_t<file_entry_t> m_file_stack; // directory recursion stack
	file_info_t               m_file_info;  // current file info
};

#ifndef REMOVE_EXPERIMENTAL

class find_file_driver2_t : public base_object_t
{
public:
	bool open(string_t filespec);
	bool open(const io_system_query_t *query);

	int  fetch();
	bool close();

	string_array_t include_patterns;
	string_array_t exclude_patterns;

	unsigned int              m_max_depth;  // maximum allowed directory recursion depth (0=none)
	ptr_array_t<file_entry_t> m_file_stack; // directory recursion stack
	file_info_t               m_file_info;  // current file info
};

#endif

//
//
//

#ifdef _WINIOCTL_

class drives_driver_t : public base_db_cursor_t
{
public:
	drives_driver_t();

	bool open(string_t filespec);

	// *** base_db_cursor_t methods ***
	virtual int       fetch();
	virtual variant_t evaluate_field(long i);
	virtual bool      close();
	virtual const db_column_binding_info_t *get_table_info() const;

	void get_drive_info(LPCSTR szDrive);

	DWORD            m_dwDrives;    // bitmask of valid drives
	int              m_pos;         // bit position within m_dwDrives
#ifndef _WIN32_WCE
	io_volume_info_t m_volume_info; // current drive info
#endif
	string_t         m_geometry;    //
	string_t         m_layout;      //
	string_t         m_performance; //
};

#endif

string_t io_short_pathname(string_t filename);
string_t io_long_pathname(string_t filename);

//
//
//

void report_error(DWORD dwError, LPCSTR pszProcName);

static inline bool error_handler(BOOL fStatus, LPCSTR pszProcName)
{
	if (fStatus)
		return true;

	report_error(GetLastError(), pszProcName);

	return false;
}

file_t file_last_error();

bool path_scan_unc(const char *&path, const char *&name, size_t &len);
bool path_scan_unc(const wchar_t *&path, const wchar_t *&name, size_t &len);
bool path_scan_unc(const char *path, string_t &machine, string_t &share, string_t &remainder);

#endif // __WIN_IO_H__

