/*------------------------------------------------------------------------------
------------------------------------------------------------------------------*/

#include "pch.h"
#include "pe.h"
#include "module.h"

#include "../../lib/extra.h"
#include "../../lib/debug/trace.h"

#ifndef LITE_RTL

//
//
//

static void read_import_descriptor2__(const void *base, const mercury::ImgDelayDescr2 *pidd, mercury::ImgDelayDescr2 &idd)
{
	if ((pidd->grAttrs & mercury::dlattrRva) == 0)
	{
		const mercury::ImgDelayDescr1 *pidd1 = (mercury::ImgDelayDescr1*)pidd;
		
		idd.grAttrs      = pidd1->grAttrs;
		idd.rvaDLLName   = ptr_diff(base, pidd1->szName);
		idd.rvaHmod      = ptr_diff(base, pidd1->phmod);
		idd.rvaIAT       = ptr_diff(base, pidd1->pIAT);
		idd.rvaINT       = ptr_diff(base, pidd1->pINT);
		idd.rvaBoundIAT  = ptr_diff(base, pidd1->pBoundIAT);
		idd.rvaUnloadIAT = ptr_diff(base, pidd1->pUnloadIAT);
		idd.dwTimeStamp  = pidd1->dwTimeStamp;
	}
	else
	{
		const mercury::ImgDelayDescr2 *pidd2 = (mercury::ImgDelayDescr2*)pidd;

		idd = *pidd2;
	}
}

//
//
//

bool image_enum_exports2__(const void *base, bool image, unsigned int index, module_symbol_t &symbol)
{
	//
	//  Get image export directory
	//

	const mercury::IMAGE_EXPORT_DIRECTORY *directory = (mercury::IMAGE_EXPORT_DIRECTORY*)
		hmodule_data_directory__(base, image, IMAGE_DIRECTORY_ENTRY_EXPORT);

	if (directory != NULL)
	{
		TRACE_WARNING_IF2(directory->Characteristics == 0, "reserved field is not NULL");
		TRACE_WARNING_IF2(directory->MajorVersion    == 0, "reserved field is not NULL");
		TRACE_WARNING_IF2(directory->MinorVersion    == 0, "reserved field is not NULL");

		_ASSERT(directory->NumberOfNames <= directory->NumberOfFunctions);

		if (index < directory->NumberOfFunctions) // XXX: signed/unsigned mismatch
		{
			const char *module_name = (char *)
				hmodule_rva_to_va__(base, image, directory->Name.rva);

			_ASSERT(module_name == directory->Name.va(base));

			const u32 *fn_addr = (u32*)
				hmodule_rva_to_va__(base, image, directory->AddressOfFunctions.rva);

			_ASSERT(fn_addr == directory->AddressOfFunctions.va(base));

			u32 ordinal = directory->Base + index;
			
			u32 rva = fn_addr[index];

			//
			//  Find exported symbol name, if any
			//

			const char *name = NULL;

			if (1)
			{
				const WORD *fn_ord = (WORD*)
					hmodule_rva_to_va__(base, image, directory->AddressOfNameOrdinals.rva);

				_ASSERT(fn_ord == directory->AddressOfNameOrdinals.va(base));

				// XXX: isn't there a direct algorithm for this?!?!?!
				
				for (unsigned int i = 0; i < directory->NumberOfNames; i++)
				{
					if (ordinal == directory->Base + fn_ord[i])
					{
						//
						//  Symbol is exported by name
						//
						
						const u32 *fn_names = (u32*)
							hmodule_rva_to_va__(base, image, directory->AddressOfNames.rva);

						_ASSERT(fn_names == directory->AddressOfNames.va(base));

						_ASSERT(name == NULL);

						name = (const char *)hmodule_rva_to_va__(base, image, fn_names[i]);

						break;
					}
				}
			}

			//
			//  If symbol address falls within the export directory itself, it
			//  is a forwarder, by definition.
			//

			const char *forwarder = NULL;

			const IMAGE_DATA_DIRECTORY *dir = hmodule_data_directory2__(base, IMAGE_DIRECTORY_ENTRY_EXPORT);

			if (dir != NULL)
			{
				if (rva >= dir->VirtualAddress && rva < dir->VirtualAddress + dir->Size)
				{
					forwarder = (const char *)hmodule_rva_to_va__(base, image, fn_addr[index]);
					rva = 0;
				}
			}

			//
			//
			//

			symbol.name.text    = name;
			symbol.name.ordinal = ordinal;
			symbol.rva          = (void*)rva; // warning C4312: 'type cast': conversion from 'u32' to 'void *' of greater size
			symbol.module       = module_name;
			symbol.forwarder    = forwarder;

			return true;
		}
	}

	return false;
}

//
//
//

void image_enum_import_helper32__(const void *base, bool image, const mercury::IMAGE_IMPORT_DESCRIPTOR *import, const mercury::IMAGE_THUNK_DATA32 *thunk, module_symbol_t &symbol)
{
	_ASSERT(base != NULL);
	_ASSERT(import != NULL);
	_ASSERT(thunk != NULL);

	if (IMAGE_SNAP_BY_ORDINAL32(thunk->Ordinal))
	{
		// symbol imported by ordinal
		
		symbol.module       = import->Name.va(base, image);
		symbol.name.text    = NULL;
		symbol.name.ordinal = IMAGE_ORDINAL32(thunk->Ordinal);
		symbol.timestamp    = import->TimeDateStamp;
		symbol.hint         = 0;
		symbol.address      = 0; // not implemented!
		symbol.delay_load   = 0;
	}
	else
	{
		// symbol imported by name

		const mercury::IMAGE_IMPORT_BY_NAME *x = thunk->AddressOfData.va(base, image);

		symbol.module       = import->Name.va(base, image);
		symbol.name.text    = x->Name;
		symbol.name.ordinal = 0;
		symbol.timestamp    = import->TimeDateStamp;
		symbol.hint         = x->Hint;
		symbol.address      = 0; // not implemented!
		symbol.delay_load   = 0;
	}
}

void image_enum_import_helper64__(const void *base, bool image, const mercury::IMAGE_IMPORT_DESCRIPTOR *import, const mercury::IMAGE_THUNK_DATA64 *thunk, module_symbol_t &symbol)
{
	_ASSERT(base != NULL);
	_ASSERT(import != NULL);
	_ASSERT(thunk != NULL);

	const char *module_name = (char *)hmodule_rva_to_va__(base, image, 
		import->Name.rva);

	_ASSERT(module_name == import->Name.va(base, image));

#ifdef _WIN64
	if (IMAGE_SNAP_BY_ORDINAL64(thunk->Ordinal))
#else
	if (IMAGE_SNAP_BY_ORDINAL32(thunk->Ordinal))
#endif
	{
		//
		//  Symbol is imported by ordinal
		//
		
		symbol.module       = module_name;
		symbol.name.text    = NULL;
		symbol.name.ordinal = IMAGE_ORDINAL64(thunk->Ordinal);
		symbol.timestamp    = import->TimeDateStamp;
		symbol.hint         = 0;
		symbol.address      = 0; // not implemented!
		symbol.delay_load   = 0;
	}
	else
	{
		//
		//  Symbol is imported by name
		//

		const IMAGE_IMPORT_BY_NAME *x = (IMAGE_IMPORT_BY_NAME*)
			hmodule_rva_to_va__(base, image, (uintptr_t)thunk->AddressOfData);

		const char *name = (char*)x->Name;

		symbol.module       = module_name;
		symbol.name.text    = name;
		symbol.name.ordinal = 0;
		symbol.timestamp    = import->TimeDateStamp;
		symbol.hint         = x->Hint;
		symbol.address      = 0; // not implemented!
		symbol.delay_load   = 0;
	}
}

bool image_enum_imports2__(const void *base, bool image, unsigned int index, module_symbol_t &symbol)
{
	// get image import directory
	
	const mercury::IMAGE_IMPORT_DESCRIPTOR *import = (mercury::IMAGE_IMPORT_DESCRIPTOR *)
		hmodule_data_directory__(base, image, IMAGE_DIRECTORY_ENTRY_IMPORT);

	if (import != NULL)
	{
		const IMAGE_NT_HEADERS32 *hdr32 = hmodule_nt_headers32__(base);

		if (hdr32 != NULL)
		{
			// loop through each module listed in the directory
			
//			const mercury::IMAGE_THUNK_DATA32 *thunk0 = (mercury::IMAGE_THUNK_DATA32 *)import->OriginalFirstThunk.va(base, image);

			while (import->OriginalFirstThunk.rva != 0)
			{
				// loop through each imported symbol

				const mercury::IMAGE_THUNK_DATA32 *thunk = (mercury::IMAGE_THUNK_DATA32*)
					import->OriginalFirstThunk.va(base, image);

				_ASSERT(thunk != NULL);

				while (thunk->Ordinal != 0)
				{
					if (index == 0)
					{
						image_enum_import_helper32__(base, image, import, thunk, symbol);

						return true;
					}

					index--;

					thunk++;
				}

				import++;
			}
		}

		const IMAGE_NT_HEADERS64 *hdr64 = hmodule_nt_headers64__(base);

		if (hdr64 != NULL)
		{
			// loop through each module listed in the directory
			
			while (import->OriginalFirstThunk.rva != 0)
			{
				// loop through each imported symbol

				const mercury::IMAGE_THUNK_DATA64 *thunk = (mercury::IMAGE_THUNK_DATA64*)
					import->OriginalFirstThunk.va(base, image);

				while (thunk->Ordinal != 0)
				{
					if (index == 0)
					{
						image_enum_import_helper64__(base, image, import, thunk, symbol);

						return true;
					}

					index--;

					thunk++;
				}

				import++;
			}
		}
	}

	//
	//  Show delay imports
	//
	
	if (0)
	{
		const mercury::ImgDelayDescr2 *pidd = (mercury::ImgDelayDescr2*)
			hmodule_data_directory__(base, image, IMAGE_DIRECTORY_ENTRY_DELAY_IMPORT);

		if (pidd != NULL)
		{
			for (;;)
			{
				if (pidd->rvaDLLName.rva == 0) // end of list
					break;

				mercury::ImgDelayDescr2 idd;
				
				read_import_descriptor2__(base, pidd, idd);

				PCImgThunkData thunk  = idd.rvaINT.va(base, image);
				PCImgThunkData thunk2 = idd.rvaIAT.va(base, image);

				for (;;)
				{
					if (thunk->u1.Ordinal == 0)
						break;

					if (index == 0)
					{
						symbol.module= idd.rvaDLLName.va(base, image);

						if (IMAGE_SNAP_BY_ORDINAL(thunk->u1.Ordinal))
						{
							symbol.name.text    = NULL;
							symbol.name.ordinal = IMAGE_ORDINAL64(thunk->u1.Ordinal);
							symbol.hint         = 0;
						}
						else
						{
							const IMAGE_IMPORT_BY_NAME *p;

							p = (IMAGE_IMPORT_BY_NAME*)hmodule_rva_to_va__(base, image, (uintptr_t)thunk->u1.AddressOfData);

							symbol.name.text    = (LPCSTR)p->Name;
							symbol.name.ordinal = 0;
							symbol.hint         = p->Hint;
						}

						symbol.timestamp  = import->TimeDateStamp;
						symbol.address    = (void*)thunk2->u1.Function;
						symbol.delay_load = 1;

						return true;
					}

					thunk++;
					thunk2++;
					
					index--;
				}

				pidd++;
			}
		}
	}

	return false;
}

//
//
//

void read_import_descriptor(const mercury::ImgDelayDescr2 *pidd, mercury::ImgDelayDescr1 &idd)
{
	if ((pidd->grAttrs & mercury::dlattrRva) == 0)
	{
		const mercury::ImgDelayDescr1 *pidd1 = (mercury::ImgDelayDescr1*)pidd;
		
		idd = *pidd1;
	}
	else
	{
		const mercury::ImgDelayDescr2 *pidd2 = (mercury::ImgDelayDescr2*)pidd;

		// NOTE: VC++ 2005 uses the linker-defined variable __ImageBase instead

		HMODULE hModule = hmodule_from_address(pidd); // module requesting import (where descriptor resides)
		
		_ASSERT(hModule != NULL);

		void *base = const_cast<void*>(hmodule_base_address(hModule));

		idd.grAttrs     = pidd2->grAttrs;
		idd.szName      = pidd2->rvaDLLName.va(base);
		idd.phmod       = pidd2->rvaHmod.va(base);
		idd.pIAT        = pidd2->rvaIAT.va(base);
		idd.pINT        = pidd2->rvaINT.va(base);
		idd.pBoundIAT   = pidd2->rvaBoundIAT.va(base);
		idd.pUnloadIAT  = pidd2->rvaUnloadIAT.va(base);
		idd.dwTimeStamp = pidd2->dwTimeStamp;
	}

	_ASSERT(idd.phmod != NULL);
}

//
//
//

#ifndef REMOVE_THIS

bool image_enum_exports__(HMODULE hModule, unsigned int index, module_symbol_t &symbol)
{
	const void *base = hmodule_base_address(hModule);
	bool image = !LDR_IS_DATAFILE(hModule); // mapped as data file, not as a loader image
	return image_enum_exports2__(base, image, index, symbol);
}

bool image_enum_imports__(HMODULE hModule, unsigned int index, module_symbol_t &symbol)
{
	const void *base = hmodule_base_address(hModule);
	bool image = !LDR_IS_DATAFILE(hModule); // mapped as data file, not as a loader image
	return image_enum_imports2__(base, image, index, symbol);
}

bool image_enum_imports____(module_t module, unsigned int index, module_symbol_t &symbol)
{
	HMODULE hModule = module_handle(module);

	return image_enum_imports__(hModule, index, symbol);
}

bool image_enum_exports____(module_t module, unsigned int index, module_symbol_t &symbol)
{
	HMODULE hModule = module_handle(module);

	return image_enum_exports__(hModule, index, symbol);
}

#endif

module_symbol_t module_export(module_t module, unsigned int index)
{
	module_symbol_t exp = { 0 };

	if (image_enum_exports____(module, index, exp))
	{
	}

	return exp;
}

module_symbol_t module_import(module_t module, unsigned int index)
{
	module_symbol_t imp = { 0 };

	if (image_enum_imports____(module, index, imp))
	{
	}

	return imp;
}

#endif

