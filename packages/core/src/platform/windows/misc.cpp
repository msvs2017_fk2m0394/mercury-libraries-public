/*------------------------------------------------------------------------------

	platform/windows/misc.cpp

------------------------------------------------------------------------------*/

#include "pch.h"
#include "core.h"

#include "../../lib/elements/string.h"

#ifndef REMOVE_THIS

struct system_info_cache_t
{
	lock_t        sysinfo_lock; //
	bool          sysinfo_init; //
	SYSTEM_INFO   sysinfo;      //
	lock_t        osvi_lock;    //
	bool          osvi_init;    //
	OSVERSIONINFO osvi;         //
};

// XREF: core_global_data_t

struct core_global_state_t
{
	core_global_state_t();

	~core_global_state_t();

#ifndef REMOVE_THIS
//	void *operator new(size_t size, void *ptr);
#endif

	system_info_cache_t sysinfo; //
};

core_global_state_t *core_global_state();

#if 1

C_ASSERT(dll_state_uninitialized == 0);

// XXX: startup code...
static core_global_state_t g_global_state;

//
//
//

core_global_state_t *core_global_state()
{
	return &g_global_state;
}

core_global_state_t::core_global_state_t()
{
}

core_global_state_t::~core_global_state_t()
{
}

#ifndef REMOVE_THIS

/*
// XREF: operator new(size_t size, struct mercury::dummy_object_t *ptr)

void *core_global_state_t::operator new(size_t size, void *ptr)
{
	_ASSERT(size == sizeof(core_global_state_t));

	return ptr;
}
*/

#endif

#endif

#endif

#ifndef LITE_RTL

//
//
//

// TODO: use conventional naming...
// XXX: UNICODE/ANSI build-dependent

const OSVERSIONINFO &ART_OSVersion()
{
	system_info_cache_t &info = core_global_state()->sysinfo;

	info.osvi_lock.enter();

	if (!info.osvi_init)
	{
		info.osvi.dwOSVersionInfoSize = sizeof(info.osvi);

		if (GetVersionEx(&info.osvi))
		{
			info.osvi_init = true;

			// system can return " "

			trim_string(info.osvi.szCSDVersion);
		}
		else
		{
			info.osvi.dwOSVersionInfoSize = 0; // indicate failure
		}
	}

	info.osvi_lock.leave();

	return info.osvi;
}

//
//
//

#if 1

// TODO: use conventional naming...

const SYSTEM_INFO &ART_SystemInfo()
{
	system_info_cache_t &info = core_global_state()->sysinfo;

	info.sysinfo_lock.enter();

	if (!info.sysinfo_init)
	{
		GetSystemInfo(&info.sysinfo);

		info.sysinfo_init = true;
	}

	info.sysinfo_lock.leave();

	return info.sysinfo;
}

#endif

//
//
//

// TODO: use conventional naming...

bool ART_IsPlatformNT()
{
	const OSVERSIONINFO &ver = ART_OSVersion();

	return (ver.dwOSVersionInfoSize != 0 &&
		ver.dwPlatformId == VER_PLATFORM_WIN32_NT);
}

#endif

#if 1

// TODO: use conventional naming...

string_t ART_GetComputerName()
{
#if !defined(_WIN32_WCE)

	// XXX: according to docs, retrieves only the NetBIOS name of the local
	// computer.  Need GetComputerNameEx (NT 5.0) to get the DNS name.

	TCHAR szBuffer[MAX_COMPUTERNAME_LENGTH + 1 ];
	DWORD nSize = DIM(szBuffer);

	if (GetComputerName(szBuffer, &nSize))
		return szBuffer;
	else
		return FormatSystemMessage(GetLastError());

#else

	DBG_NOT_IMPLEMENTED2("ART_GetComputerName()");

	return string_error();

#endif
}

#endif

