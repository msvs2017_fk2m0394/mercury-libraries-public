/*------------------------------------------------------------------------------

	dde.cpp - Dynamic Data Exchange (DDE) utilities

	DDE can be thought of as a type of automated clipboard

------------------------------------------------------------------------------*/

#include "pch.h"
#include <dde.h>
#include "win_ui__.h"

#ifndef LITE_RTL

#define DDE_TIMEOUT 10000 // 10 seconds

//
//
//

// a server's connection to a client...

struct dde_client_t
{
	HWND hwndClient;
};

struct dde_topic_t
{
	string_t text;
	ATOM     atom;
};

// a server's advise link to a client...

struct dde_advise_link_t
{
	HWND      hwndClient;
	DDEADVISE ddeadvise;
	ATOM      aItem;
};

class dde_server_window_t : public base_window_t
{
public:
	~dde_server_window_t();

	virtual void dispatch(object_message_t &msg);
	virtual void OnPaint();
	
	dde_topic_t                    m_application;
	dde_topic_t                    m_topic;
	ptr_array_t<dde_client_t>      m_clients;
	ptr_array_t<dde_advise_link_t> m_advise_links;
	dde_callbacks_t                m_callbacks;
	void *                         m_callback_data;

	OBJECT_CLASS_DECLARE();
};

// XREF: core_command_frame_t

struct dde_command_t
{
	string_t cmd;
	string_t params[10];
};

OBJECT_CLASS_BEGIN(dde_server_window_t, base_window_t)
OBJECT_CLASS_END()

static const dde_callbacks_t g_null_callbacks = { 0 };

static dde_client_t *find_client(dde_server_window_t *server, HWND hwndClient);
static string_t get_dde_app_name();
static bool dde_post_ack(const MSG &msg, const DDEACK &ack, ATOM aItem);
static bool dde_post_data(HWND hwndClient, HWND hwndServer, HGLOBAL hData, ATOM aItem);
static bool dde_send_ack(HWND hwndClient, HWND hwndServer, LPCTSTR pszApp, LPCTSTR pszTopic);
static bool on_dde_execute_default(string_t cmd);
static bool on_dde_execute_open(string_t filename);

static trace_link_t g_trace_dde = { NULL, "DDE", TRACE_DISABLE_RELEASE };

static void trace_dde_initiate(HWND hwndClient, ATOM aApp, ATOM aTopic);
static void trace_dde_execute(const char *command);
static void trace_dde_terminate(HWND hwndClient);

//
//
//

bool create_dde_server_window(const char *appname, const char *topic, const dde_callbacks_t *callbacks, void *data)
{
	object_ref_t<dde_server_window_t> window = new dde_server_window_t;

	if (window != NULL)
	{
		string_t app;
		
		if (appname != NULL)
			app = appname;
		else
			app = get_dde_app_name();
				
		window->m_application.text = app;
		window->m_application.atom = GlobalAddAtom(window->m_application.text);
		window->m_topic.text       = topic ? topic : "System";
		window->m_topic.atom       = GlobalAddAtom(window->m_topic.text);
		window->m_callbacks        = callbacks ? *callbacks : g_null_callbacks;
		window->m_callback_data    = data;

		string_t text;

		if (1)
		{
			text += "DDE Server Window: ";
			text += window->m_application.text;
			text += "|";
			text += window->m_topic.text;
		}

		DWORD dwStyle = WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_THICKFRAME;
		
#ifdef _DEBUG
		if (0)
		{
			dwStyle |= WS_VISIBLE;
		}
#endif

		if (0)
		{
			dwStyle |= WS_DISABLED;
		}

		HWND hwnd = WndCreateWindow(0, NULL, text, dwStyle,
			CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, 
			NULL, NULL, window);
		
		if (hwnd != NULL)
		{
			return true;
		}
	}

	return false;
}

//
//
//

dde_server_window_t::~dde_server_window_t()
{
	// cleanup atoms...
	
	if (m_application.atom != 0)
		VERIFY(GlobalDeleteAtom(m_application.atom) == 0);
	
	if (m_topic.atom != 0)
		VERIFY(GlobalDeleteAtom(m_topic.atom) == 0);
}

//
//
//

void dde_server_window_t::dispatch(object_message_t &msg)
{
	switch (win32_message(msg).message)
	{
		//  a DDE client application sends this message to initiate a
		//  conversation with a server application
		
		case WM_DDE_INITIATE:
		{
			HWND hwndClient = (HWND)win32_message(msg).wParam;
			ATOM aApp       = (ATOM)LOWORD(win32_message(msg).lParam);
			ATOM aTopic     = (ATOM)HIWORD(win32_message(msg).lParam);

			trace_dde_initiate(hwndClient, aApp, aTopic);

			if (hwndClient != NULL)
			{
				if (IsWindow(hwndClient))
				{
				}
				else
				{
					DBG_BREAK();
					
					break;
				}
			}
			else
			{
				break;
			}

			if (aApp == 0)
			{
				// a conversation with all applications is requested
			}
			else
			{
				if (m_application.atom == 0)
				{
					if (m_application.text != "")
					{
						m_application.atom = GlobalAddAtom(m_application.text);
					}
				}

				if (m_application.atom != 0)
				{
					if (aApp == m_application.atom)
					{
					}
					else
					{
						break;
					}
				}
				else
				{
					break;
				}
			}
			
			if (aTopic == 0)
			{
				// send ACK for each topic supported
			}
			else
			{
				if (m_topic.atom == 0)
				{
					if (m_topic.text != "")
					{
						m_topic.atom = GlobalAddAtom(m_topic.text);
					}
				}

				if (m_topic.atom != 0)
				{
					if (m_topic.atom == aTopic)
					{
					}
					else
					{
						break;
					}
				}
				else
				{
					break;
				}
			}

			dde_client_t *client = find_client(this, hwndClient);

			if (client == NULL)
			{
				client = m_clients.create_item();

				if (client != NULL)
				{
					client->hwndClient = hwndClient;
				}
			}

			dde_send_ack(hwndClient, win32_message(msg).hwnd, m_application.text, m_topic.text);

			break;
		}

		//
		//
		//
		
		case WM_DDE_TERMINATE:
		{
			_ASSERT(win32_message(msg).lParam == 0);
			
			HWND hwndClient = (HWND)win32_message(msg).wParam;

			trace_dde_terminate(hwndClient);

			dde_client_t *client = find_client(this, hwndClient);

			if (client != NULL)
			{
				if (hwndClient != NULL && hwndClient != HWND_BROADCAST)
				{
					_ASSERT(IsWindow(hwndClient));

					if (PostMessage(hwndClient, WM_DDE_TERMINATE, (WPARAM)win32_message(msg).hwnd, 0))
					{
					}
					else
					{
					}
				}
			}
			else
			{
				DBG_BREAK();
			}

			break;
		}

		//
		//  WM_DDE_ADVISE is posted by the client application to request
		//  notification (via WM_DDE_DATA) of specified item value changes.
		//
		
		case WM_DDE_ADVISE:
		{
			HWND hwndClient = (HWND)win32_message(msg).wParam;

			dde_client_t *client = find_client(this, hwndClient);

			if (client != NULL)
			{
			}
			else
			{
				DBG_BREAK();
			}

#ifdef _WIN64
			UINT_PTR uiLo, uiHi;
#else
			UINT uiLo, uiHi;
#endif
			
			if (UnpackDDElParam(win32_message(msg).message, win32_message(msg).lParam, &uiLo, &uiHi))
			{
				HGLOBAL hOptions = (HGLOBAL)uiLo;
				ATOM    aItem    = (ATOM)uiHi;

				DDEACK wStatus = { 0 };

				if (hOptions != NULL)
				{
					const DDEADVISE *ddeadvise = (DDEADVISE*)GlobalLock(hOptions);

					if (ddeadvise != NULL)
					{
						if (ddeadvise->fDeferUpd)
						{
						}

						if (ddeadvise->fAckReq)
						{
						}

						TCHAR szName[256];
						
						GetClipboardFormatName(ddeadvise->cfFormat, szName, DIM(szName));

						if (ddeadvise->cfFormat == CF_TEXT)
						{
							dde_advise_link_t *link;

							link = m_advise_links.create_item();

							if (link != NULL)
							{
								link->hwndClient = hwndClient;
								link->ddeadvise = *ddeadvise;
								link->aItem = aItem;

								wStatus.fAck = 1;
							}
						}
						
						GlobalUnlock(hOptions);
					}
					else
					{
					}
				}

				if (wStatus.fAck)
				{
					GlobalFree(hOptions);
				}

				dde_post_ack(win32_message(msg), wStatus, aItem);
			}

			break;
		}

		//
		//  WM_DDE_UNADVISE is posted by a client application
		//

		case WM_DDE_UNADVISE:
		{
			HWND hwndClient = (HWND)win32_message(msg).wParam;
			UINT cfFormat   = LOWORD(win32_message(msg).lParam);
			ATOM aItem      = LOWORD(win32_message(msg).lParam);

			dde_client_t *client = find_client(this, hwndClient);

			if (client != NULL)
			{
			}
			else
			{
				DBG_BREAK();
			}

			if (cfFormat == 0)
			{
				// All active advise links are to be terminated
			}

			if (aItem == 0)
			{
			}
			else
			{
			}

			break;
		}

		//
		//
		//

		case WM_DDE_ACK:
		{
			if (0)
			{
#ifdef _M_X64
				UINT_PTR lo = 0, hi = 0;
#else
				UINT lo = 0, hi = 0;
#endif

				UnpackDDElParam(win32_message(msg).message, win32_message(msg).lParam, &lo, &hi);

				GlobalFree((HGLOBAL)hi);

				FreeDDElParam(win32_message(msg).message, win32_message(msg).lParam);
			}

			break;
		}

		//
		//  WM_DDE_DATA is posted by the server application to pass a data
		//  item or to notify the available of a data item
		//

		case WM_DDE_DATA:
		{
			DBG_NOT_IMPLEMENTED();
			
			break;
			
#ifdef _DEBUG

			HWND hwndClient = (HWND)win32_message(msg).wParam;

#ifdef _WIN64
			UINT_PTR uiLo, uiHi;
#else
			UINT uiLo, uiHi;
#endif
			
			if (UnpackDDElParam(win32_message(msg).message, win32_message(msg).lParam, &uiLo, &uiHi))
			{
				HGLOBAL hData = (HGLOBAL)uiLo;
				ATOM aItem = (ATOM)uiHi;

				if (hData != NULL)
				{
					const DDEDATA *ddedata = (DDEDATA*)GlobalLock(hData);

					if (ddedata != NULL)
					{
						if (ddedata->fAckReq)
						{
							DDEACK wStatus = { 0 };

							wStatus.fAck = 1;

							dde_post_ack(win32_message(msg), wStatus, aItem);
						}
						else
						{
							GlobalDeleteAtom(aItem);
						}

						GlobalUnlock(hData);
					}
					else
					{
					}
				}
				else
				{
				}
			}
			else
			{
			}

#endif

			break;
		}

		//
		//  WM_DDE_REQUEST is posted by a client application to request the
		//  value of a data item
		//
		
		case WM_DDE_REQUEST:
		{
#ifdef _DEBUG

			HWND hwndClient = (HWND)win32_message(msg).wParam;
			UINT cfFormat   = LOWORD(win32_message(msg).lParam);
			ATOM aItem      = LOWORD(win32_message(msg).lParam);

			dde_client_t *client = find_client(this, hwndClient);

			if (client != NULL)
			{
			}
			else
			{
				DBG_BREAK();
			}

			if (hwndClient != NULL)
			{
				if (IsWindow(hwndClient))
				{
					TCHAR szName[256];
					
					GetClipboardFormatName(cfFormat, szName, DIM(szName));

					if (cfFormat == CF_TEXT)
					{
						HGLOBAL hData = NULL;
						
						int len = FIELD_OFFSET(DDEDATA, Value) + 13;
						
						hData = GlobalAlloc(GMEM_DDESHARE | GMEM_MOVEABLE, len);

						DDEDATA *ddedata = (DDEDATA*)GlobalLock(hData);

						if (ddedata != NULL)
						{
							ZERO_INIT(*ddedata);

							ddedata->fResponse = 1;
							ddedata->fRelease  = 0;
							ddedata->fAckReq   = 0;
							ddedata->cfFormat  = cfFormat;

							memcpy(&ddedata->Value[0], "Hello, World!", 13);

							hex_dump(ddedata, len);

							GlobalUnlock(hData);
						}

						dde_post_data(hwndClient, win32_message(msg).hwnd, hData, aItem);
					}
				}
			}

#endif

			break;
		}

		//
		//
		//

		case WM_DDE_POKE:
		{
			break;
		}

		//
		//
		//

		case WM_DDE_EXECUTE:
		{
			HWND    hwndClient = (HWND)win32_message(msg).wParam;
			HGLOBAL hCommands  = (HGLOBAL)win32_message(msg).lParam;

			// always ACK the execute command - even if we do nothing

			// unpack the DDE message
#ifdef _WIN64
			UINT_PTR unused = 0;
#else
			UINT unused = 0;
#endif
			HGLOBAL hData;
#ifdef _WIN64
			VERIFY(UnpackDDElParam(WM_DDE_EXECUTE, win32_message(msg).lParam,
				&unused, (UINT_PTR*)&hData));
#else
			VERIFY(UnpackDDElParam(WM_DDE_EXECUTE, win32_message(msg).lParam,
				&unused, (UINT*)&hData));
#endif

			_ASSERT(hCommands == hData);

			// get the command string
			LPCTSTR lpsz = (LPCTSTR)GlobalLock(hData);
			string_t cmd = lpsz; // XXX: need to check with GlobalSize to make sure we don't run off the end
			GlobalUnlock(hData);

			// acknowledge now - before attempting to execute
			PostMessage(hwndClient, WM_DDE_ACK, (WPARAM)win32_message(msg).hwnd,
				ReuseDDElParam(win32_message(msg).lParam, WM_DDE_EXECUTE, WM_DDE_ACK,
#ifdef _WIN64
					(UINT_PTR)0x8000, (UINT_PTR)hData));
#else
					(UINT)0x8000, (UINT)hData));
#endif

			trace_dde_execute(cmd);

			if (m_callbacks.exec != NULL)
			{
				if (m_callbacks.exec(m_callback_data, cmd))
				{
				}
				else
				{
					on_dde_execute_default(cmd);
				}
			}
			else
			{
				on_dde_execute_default(cmd);
			}

			break;
		}

		//
		//
		//

		case WM_DESTROY:
		{
			if (0)
			{
				PostMessage(WM_DDE_TERMINATE);
			}

			break;
		}
	}

	base_window_t::dispatch(msg);
}

//
//
//

void dde_server_window_t::OnPaint()
{
}

//
//
//

static dde_client_t *find_client(dde_server_window_t *server, HWND hwndClient)
{
	if (server != NULL)
	{
		for (unsigned int i = 0; i < server->m_clients.length(); i++)
		{
			if (server->m_clients[i]->hwndClient == hwndClient)
			{
				return server->m_clients[i];
			}
		}
	}

	return NULL;
}

//
//
//

static bool dde_post_ack(const MSG &msg, const DDEACK &ack, ATOM aItem)
{
	HWND hwndClient = (HWND)msg.wParam;

	LPARAM lParam;

	UINT uiLo = *((WORD*)&ack);
	UINT uiHi = aItem;
	
	lParam = ReuseDDElParam(msg.lParam, msg.message, WM_DDE_ACK, uiLo, uiHi);

	if (lParam != 0)
	{
		if (PostMessage(hwndClient, WM_DDE_ACK, (WPARAM)msg.hwnd, lParam))
		{
			return true;
		}
		else
		{
			FreeDDElParam(WM_DDE_DATA, lParam);
		}
	}

	GlobalDeleteAtom(aItem);

	return false;
}

//
//
//

static bool dde_post_data(HWND hwndClient, HWND hwndServer, HGLOBAL hData, ATOM aItem)
{
#ifdef _WIN64
	LPARAM lParam = PackDDElParam(WM_DDE_DATA, (UINT_PTR)hData, aItem);
#else
	LPARAM lParam = PackDDElParam(WM_DDE_DATA, (UINT)hData, aItem);
#endif

	if (lParam != 0)
	{
		if (PostMessage(hwndClient, WM_DDE_DATA, (WPARAM)hwndServer, lParam))
		{
			return true;
		}
		else
		{
			// cleanup

			FreeDDElParam(WM_DDE_DATA, lParam);
		}
	}

	GlobalFree(hData);

	GlobalDeleteAtom(aItem);

	return false;
}

//
//
//

static bool dde_send_ack(HWND hwndClient, HWND hwndServer, LPCTSTR pszApp, LPCTSTR pszTopic)
{
	if (hwndClient != NULL)
	{
		// protect from HWND_BROADCAST
		
		if (IsWindow(hwndClient))
		{
			ATOM aApp = GlobalAddAtom(pszApp);
			ATOM aTopic = GlobalAddAtom(pszTopic);

			if (aApp != 0 && aTopic != 0)
			{
				// send the WM_DDE_ACK (receiver will delete duplicate atoms)

				SendMessage(hwndClient, WM_DDE_ACK, (WPARAM)hwndServer,
					MAKELPARAM(aApp, aTopic));

				return true;
			}

			if (aApp != 0)
				GlobalDeleteAtom(aApp);

			if (aTopic != 0)
				GlobalDeleteAtom(aTopic);
		}
	}

	return false;
}

//
//
//

static string_t get_dde_app_name()
{
	return file_name(module_filename(module_exe()));
}

//
//
//

// XXX: needs work...

static bool scan_command_name(const char *&psz, const char *&cmd_start, int &cmd_len)
{
	while (isspace(*psz))
		psz++;
	
	if (isalpha(*psz))
	{
		cmd_start = psz;
		cmd_len = 0;
		
		while (isalpha(*psz) || isdigit(*psz))
		{
			psz++;
			cmd_len++;
		}

		return true;
	}

	return false;
}

//
//
//

static bool scan_command_param(const char *&psz, const char *&cmd_start, int &cmd_len)
{
	if (*psz == '(')
	{
		psz++;

		bool quote = *psz == '\"';

		if (quote)
			psz++;

		cmd_start = psz;
		cmd_len = 0;

		while (true)
		{
			if (*psz == 0)
				break;
			
			if (quote)
			{
				if (*psz == '\"')
					break;
			}
			else
			{
				if (*psz == ',')
					break;
				
				if (*psz == ')')
					break;
			}

			psz++;
			cmd_len++;
		}

		return true;
	}

	return false;
}

//
//
//

static bool dde_scan_command(const char *&psz, dde_command_t &cmd)
{
	if (*psz == '[')
	{
		psz++;

		strchr(psz, ']');
	}

	const char *cmd_start;
	int cmd_len;
	
	if (scan_command_name(psz, cmd_start, cmd_len))
	{
		cmd.cmd = string(cmd_start, cmd_len);
		
		const char *param_start;
		int param_len;

		psz = cmd_start + cmd_len;
		
		if (scan_command_param(psz, param_start, param_len))
		{
			cmd.params[0] = string(param_start, param_len);

			return true;
		}
	}

	return false;
}

//
//
//

static string_t unquote(const string_t &ref)
{
	const char *psz = ref;

	if (*psz != '\"')
		return ref;

	psz++;

	// XXX: hack...
	
	return string(psz, ref.length() - 2);
}

//
//
//

static bool on_dde_execute_default(string_t cmd)
{
	const char *psz = cmd;

	dde_command_t dde_cmd;
	
	while (dde_scan_command(psz, dde_cmd))
	{
		if (cmpeqi(dde_cmd.cmd, "open"))
		{
			string_t filename = unquote(dde_cmd.params[0]);
			
			return on_dde_execute_open(filename);
		}
	}

	return false;
}

//
//
//

static bool on_dde_execute_open(string_t filename)
{
	doc_create_t cs = make_doc_create(filename);

	return doc_create_document(cs, NULL);
}

//
//
//

// TODO: do this from the debugger instead of polluting our code here

static void trace_dde_initiate(HWND hwndClient, ATOM aApp, ATOM aTopic)
{
	if (trace_enabled(g_trace_dde, trace_severity_debug))
	{
		TCHAR szTopic[256] = TEXT("");
		TCHAR szApp[256] = TEXT("");

		GlobalGetAtomName(aApp, szApp, DIM(szApp));
		GlobalGetAtomName(aTopic, szTopic, DIM(szTopic));

		TRACE("%s: DDE Initiate: hwndClient=%08X, app='%s', topic='%s'\n",
			g_trace_dde.name, hwndClient, szApp, szTopic);
	}
}

static void trace_dde_execute(const char *command)
{
	if (trace_enabled(g_trace_dde, trace_severity_debug))
	{
		TRACE("%s: DDE Execute: %s\n", g_trace_dde.name, command);
	}
}

static void trace_dde_terminate(HWND hwndClient)
{
	if (trace_enabled(g_trace_dde, trace_severity_debug))
	{
		TRACE("%s: DDE Terminate: %08X\n", g_trace_dde.name, hwndClient);
	}
}

//
//
//

#if 0

// TODO: naming convention!

static file_t ART_create_dde_data(const clipboard_format_t &format)
{
	// TODO: use start_length

	// TODO: we need to account for the DDEDATA or DDEPOKE header on the
	// global memory block by treating the file start to be the first byte
	// after the fixed header (e.g. adjust set_size, get_pos, etc.)
	
	clipboard_device_t *device = clipboard_device_create();

	file_t file = io_stream_create_and_release(device);

	if (device != NULL)
	{
		device->m_format = format;

		DDEDATA dde_data = { 0 };
		
		dde_data.cfFormat = format.m_cfFormat;
		
		if (file.write(&dde_data, FIELD_OFFSET(DDEDATA, Value)))
		{
		}
		else
		{
			// XXX
			
//			file = (io_stream_object_t*)NULL;
		}
	}

	return file;
}

#endif

#ifdef _DEBUG

static bool create_dde_client_window(const char *application, const char *topic, const char *cmd)
{
	if (0)
	{
		// similar to what ShellExecute does...

		HGLOBAL hGlobal = GlobalAlloc(GMEM_MOVEABLE | GMEM_DDESHARE, strlen(cmd) + 1);

		if (hGlobal != NULL)
		{
			GlobalLock(hGlobal);

			GlobalUnlock(hGlobal);
		}

#if 0
		LRESULT result = 0;
		
		SendMessageTimeout(HWND_BROADCAST, WM_DDE_INITIATE,
			???, MAKELONG(aApplication, aTopic), SMTO_ABORTIFHUNG, DDE_TIMEOUT, &result);

		PostMessage(???, WM_DDE_EXECUTE, ???, PackDDElParam(WM_DDE_EXECUTE, 0, ???)))
#endif
	}
	
	object_ref_t<dde_server_window_t> window = new dde_server_window_t;

	if (window != NULL)
	{
		string_t text;

		DWORD dwStyle = WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_THICKFRAME;
		
#ifdef _DEBUG
		if (0)
		{
			dwStyle |= WS_VISIBLE;
		}
#endif

		if (0)
		{
			dwStyle |= WS_DISABLED;
		}

		HWND hwnd = WndCreateWindow(0, NULL, text, dwStyle,
			CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, 
			NULL, NULL, window);
		
		if (hwnd != NULL)
		{
			return true;
		}
	}

	return false;
}

#endif

#endif

