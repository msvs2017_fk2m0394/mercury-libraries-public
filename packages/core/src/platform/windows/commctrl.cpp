/*------------------------------------------------------------------------------

	platform/windows/commctrl.cpp
	
------------------------------------------------------------------------------*/

#include "pch.h"
#include <tchar.h>
#include "ui.h"

//
//
//

#ifndef LITE_RTL

int g_comctl32_version_major;
int g_comctl32_version_minor;

int comctl32_version_major()
{
	if (g_comctl32_version_major == 0)
	{
		HMODULE hModule = GetModuleHandle(TEXT("comctl32.dll"));

		if (hModule != NULL)
		{
			module_version_t ver = module_version(module_from_handle(hModule));

			g_comctl32_version_major = HIWORD(ver.dwFileVersionMS);
			g_comctl32_version_minor = LOWORD(ver.dwFileVersionMS);

#ifdef _DEBUG
			TRACE("using comctl32 version: %i.%i\n", g_comctl32_version_major, g_comctl32_version_minor);
#endif
		}
	}

	return g_comctl32_version_major;
}

//
//
//

static bool g_comctl_autoreg_enable = true;
static bool g_comctl_autoreg_status;

bool try_registering_comctl(LPCTSTR pszClassName)
{
	if (g_comctl_autoreg_enable)
	{
		DWORD err = GetLastError();

		if (err == ERROR_CANNOT_FIND_WND_CLASS || err == ERROR_CLASS_DOES_NOT_EXIST)
		{
			// it seems that InitCommonControls is required when running on XP
			// with a manifest set to use comctl V6?!?!?  the "BUTTON" class
			// has moved from USER32.DLL to COMCTL32.DLL

			if ((_tcsicmp(pszClassName, TEXT("BUTTON")) == 0) ||
				(_tcsicmp(pszClassName, TEXT("STATIC")) == 0) ||
				(_tcsicmp(pszClassName, TEXT("LISTBOX")) == 0) ||
				(_tcsicmp(pszClassName, TEXT("COMBOBOX")) == 0))
			{
				InitCommonControls();

				g_comctl_autoreg_status = 1;

				return true;
			}

			// XXX: this is a hack to get our control panel property sheet to
			// work, even if the application doesn't call InitCommonControls

			INITCOMMONCONTROLSEX init = { 0 };
			
			init.dwSize = sizeof(init);

			if (_tcsicmp(pszClassName, WC_TABCONTROL) == 0)
				init.dwICC |= ICC_TAB_CLASSES;

			// help apps that may have some of these common controls in
			// dialogs...
			
			if (_tcsicmp(pszClassName, UPDOWN_CLASS) == 0)
				init.dwICC |= ICC_UPDOWN_CLASS;
			
			if (_tcsicmp(pszClassName, TRACKBAR_CLASS) == 0)
				init.dwICC |= ICC_BAR_CLASSES;

			if (_tcsicmp(pszClassName, PROGRESS_CLASS) == 0)
				init.dwICC |= ICC_PROGRESS_CLASS;

			if (_tcsicmp(pszClassName, STATUSCLASSNAME) == 0)
				init.dwICC |= ICC_BAR_CLASSES;

			if (init.dwICC != 0)
			{
#ifdef _DEBUG
				TRACE("dynamically registering common controls to register %s window!\n",
					pszClassName);
#endif

				if (InitCommonControlsEx(&init))
				{
					g_comctl_autoreg_status = 1;

					return true;
				}
			}
		}
	}

	return false;
}

#endif

