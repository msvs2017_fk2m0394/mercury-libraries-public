/*------------------------------------------------------------------------------

	platform/posix/timer.cpp

------------------------------------------------------------------------------*/

#include "pch.h"
#include <unistd.h>
#include <signal.h>
#include <time.h>
#include <errno.h>
#include <sys/timerfd.h>
#include "posix.h"

thread_id_t gettid();
bool delay__(f32 sec);

//
//
//

#ifndef REMOVE_THIS

// XREF: time_check_interval
// XREF: run_at_freq

f32 wait__(f64 &t2, f32 freq)
{
	f64 t4 = time_monotonic();

	f32 sec = 0.0;

	f32 t5 = 0.0;

	if (freq != 0.0)
		t5 = 1.0 / freq;

	if (t2 + t5 > t4)
		sec = ((t2 + t5) - t4);

	// simple way to wait (using a timer may result in less jitter)

	if (sec > 0)
		delay__(sec);

	f64 t1 = time_monotonic();

	t2 = t1;

	return sec;
}

#endif

#ifndef REMOVE_EXPERIMENTAL

bool delay__(f32 sec)
{
	struct timespec ts = timespec_from_seconds(sec);

	for (;;)
	{
		struct timespec rem = { 0 };

		int result = nanosleep(&ts, &rem);

		if (result == 0)
			return true;

		_ASSERT(result == -1);

		if (errno != EINTR)
			break;

		ts = rem;
	}

	return false;
}

#endif

//
//
//

run_at_freq_t::run_at_freq_t()
{
	freq = 0;
	fd = -1;
}

run_at_freq_t::~run_at_freq_t()
{
	if (fd != -1)
	{
		close_sanely(fd);
		fd = -1;
	}
}

static bool run_at_freq_init(run_at_freq_t &t, f32 hertz)
{
	// NOTE: the timer structure is not designed to be shared across threads

	_ASSERT(t.freq != hertz);

	t.freq = 0;

	if (t.fd != -1)
	{
		close_sanely(t.fd);
		t.fd = -1;
	}

	clockid_t clockid = CLOCK_MONOTONIC;
	int flags = 0;

	int fd = timerfd_create(clockid, flags);

	if (fd != -1)
	{
		t.fd = fd;

		int flags = 0;
		itimerspec t1 = { 0 };

		f32 period = 1.0 / hertz;

		t1.it_value = timespec_from_seconds(period);
		t1.it_interval = timespec_from_seconds(period);

		int result = timerfd_settime(t.fd, flags, &t1, NULL);

		if (check_posix_return("timerfd_settime", result))
		{
			t.freq = hertz;
			return true;
		}
	}
	else
	{
		posix_call_failed("timerfd_create", fd);
	}

	return false;
}

bool run_at_freq(run_at_freq_t &t, f32 hertz)
{
	// NOTE: the run_at_freq_t structure is not designed to be shared across threads

	if (hertz >= 0)
	{
		if (t.freq == hertz || run_at_freq_init(t, hertz))
		{
			uint64_t data = 0;
			size_t len = 0;

			if (read_sanely(t.fd, &data, sizeof(data), len))
			{
				if (len == sizeof(data))
				{
					return true;
				}
			}
		}
	}

	return false;
}

