/*------------------------------------------------------------------------------

	platform/posix/elf.cpp

------------------------------------------------------------------------------*/

// XXX: copied from IDE project...

#include "pch.h"
#include "elf.h"

#ifndef REMOVE_EXPERIMENTAL

//
//
//

STRUCT_BEGIN(Elf32Ehdr)

	STRUCT_ARRAY3(e_ident)

	STRUCT_FIELD(e_ident[EI_MAG0])
		STRUCT_ENUM(ELFMAG0)

	STRUCT_FIELD(e_ident[EI_MAG1])
		STRUCT_ENUM(ELFMAG1)

	STRUCT_FIELD(e_ident[EI_MAG2])
		STRUCT_ENUM(ELFMAG2)

	STRUCT_FIELD(e_ident[EI_MAG3])
		STRUCT_ENUM(ELFMAG3)

	STRUCT_FIELD(e_ident[EI_CLASS])
		STRUCT_ENUM(ELFCLASSNONE)
		STRUCT_ENUM(ELFCLASS32)
		STRUCT_ENUM(ELFCLASS64)

	STRUCT_FIELD(e_ident[EI_DATA])
		STRUCT_ENUM(ELFDATANONE)
		STRUCT_ENUM(ELFDATA2LSB)
		STRUCT_ENUM(ELFDATA2MSB)

	STRUCT_FIELD(e_ident[EI_VERSION])
		STRUCT_ENUM(EV_NONE)
		STRUCT_ENUM(EV_CURRENT)

	STRUCT_FIELD(e_type)
		STRUCT_ENUM(ET_NONE)
		STRUCT_ENUM(ET_REL)
		STRUCT_ENUM(ET_EXEC)
		STRUCT_ENUM(ET_DYN)
		STRUCT_ENUM(ET_CORE)
		STRUCT_ENUM(ET_LOPROC)
		STRUCT_ENUM(ET_HIPROC)
//		STRUCT_ENUM(ET_PROC, 0xFF00, 0x0000)

	STRUCT_FIELD(e_machine)
		STRUCT_ENUM(EM_NONE)
		STRUCT_ENUM(EM_M32)
		STRUCT_ENUM(EM_SPARC)
		STRUCT_ENUM(EM_386)
		STRUCT_ENUM(EM_68K)
		STRUCT_ENUM(EM_88K)
		STRUCT_ENUM(EM_860)
		STRUCT_ENUM(EM_MIPS)
		STRUCT_ENUM(EM_AVR)

	STRUCT_FIELD(e_version)
		STRUCT_ENUM(EV_NONE)
		STRUCT_ENUM(EV_CURRENT)

	STRUCT_FIELD(e_entry)
	STRUCT_FIELD(e_phoff)
	STRUCT_FIELD(e_shoff)
	STRUCT_FIELD(e_flags)
	STRUCT_FIELD(e_ehsize)
	STRUCT_FIELD(e_phentsize)
	STRUCT_FIELD(e_phnum)
	STRUCT_FIELD(e_shentsize)
	STRUCT_FIELD(e_shnum)
	STRUCT_FIELD(e_shstrndx)

STRUCT_END()

//
//
//

STRUCT_BEGIN(Elf32Shdr)

	STRUCT_FIELD(shname)

	STRUCT_FIELD(shtype)
		STRUCT_ENUM(SHT_NULL)
		STRUCT_ENUM(SHT_PROGBITS)
		STRUCT_ENUM(SHT_SYMTAB)
		STRUCT_ENUM(SHT_STRTAB)
		STRUCT_ENUM(SHT_RELA)
		STRUCT_ENUM(SHT_HASH)
		STRUCT_ENUM(SHT_DYNAMIC)
		STRUCT_ENUM(SHT_NOTE)
		STRUCT_ENUM(SHT_NOBITS)
		STRUCT_ENUM(SHT_REL)
		STRUCT_ENUM(SHT_SHLIB)
		STRUCT_ENUM(SHT_DYNSYM)
		STRUCT_ENUM(SHT_LOPROC)
		STRUCT_ENUM(SHT_HIPROC)
//		STRUCT_ENUM(0x7FFFFFFF, 0x70000000, "SHT_PROC")
		STRUCT_ENUM(SHT_LOUSER)
		STRUCT_ENUM(SHT_HIUSER)
//		STRUCT_ENUM(SHT_USER, 0xFFFFFFFF)

	STRUCT_FIELD(shflags)
		STRUCT_BITS2(SHF_WRITE, SHF_WRITE, "SHF_WRITE")
		STRUCT_BITS2(SHF_ALLOC, SHF_ALLOC, "SHF_ALLOC")
		STRUCT_BITS2(SHF_EXECINSTR, SHF_EXECINSTR, "SHF_EXECINSTR")
		STRUCT_BITS2(SHF_MASKPROC, SHF_MASKPROC, "SHF_MASKPROC") // this isn't right

	STRUCT_FIELD(shaddr)
	STRUCT_FIELD(shoffset)
	STRUCT_FIELD(shsize)
	STRUCT_FIELD(shlink)
	STRUCT_FIELD(shinfo)
	STRUCT_FIELD(shaddralign)
	STRUCT_FIELD(shentsize)

STRUCT_END()

#if 1

//static const Elf32Shdr g_section_0 = { 0, SHT_NULL, 0, 0, 0, 0, SHN_UNDEF, 0, 0, 0 };

static const struct { const char *name; u8 shtype; u8 shflags; } g_elf_special_sections[] =
{
	{ ".bss",      SHT_NOBITS,   SHF_ALLOC | SHF_WRITE     }, //
	{ ".comment",  SHT_PROGBITS, 0                         }, //
	{ ".data",     SHT_PROGBITS, SHF_ALLOC | SHF_WRITE     }, //
	{ ".data1",    SHT_PROGBITS, SHF_ALLOC | SHF_WRITE     }, //
	{ ".debug",    SHT_PROGBITS, 0                         }, //
	{ ".dynamic",  SHT_DYNAMIC,  0                         }, //
	{ ".dynstr",   SHT_STRTAB,   SHF_ALLOC                 }, //
	{ ".dynsym",   SHT_DYNSYM,   SHF_ALLOC                 }, //
	{ ".fini",     SHT_PROGBITS, SHF_ALLOC | SHF_EXECINSTR }, //
	{ ".got",      SHT_PROGBITS, 0                         }, //
	{ ".hash",     SHT_HASH,     SHF_ALLOC                 }, //
	{ ".init",     SHT_PROGBITS, SHF_ALLOC | SHF_EXECINSTR }, //
	{ ".interp",   SHT_PROGBITS, 0                         }, //
	{ ".line",     SHT_PROGBITS, 0                         }, //
	{ ".note",     SHT_NOTE,     0                         }, //
	{ ".plt",      SHT_PROGBITS, 0                         }, //
	{ ".relname",  SHT_REL,      0                         }, //
	{ ".relaname", SHT_RELA,     0                         }, //
	{ ".rodata",   SHT_PROGBITS, SHF_ALLOC                 }, //
	{ ".rodata1",  SHT_PROGBITS, SHF_ALLOC                 }, //
	{ ".shstrtab", SHT_STRTAB,   0                         }, //
	{ ".strtab",   SHT_STRTAB,   0                         }, //
	{ ".symtab",   SHT_SYMTAB,   0                         }, //
	{ ".text",     SHT_PROGBITS, SHF_ALLOC | SHF_EXECINSTR }, //
};

static const struct { Elf32Word shtype; const char *name; } g_elf_section_types[] =
{
	{ SHT_NULL,     "SHT_NULL"     },
	{ SHT_PROGBITS, "SHT_PROGBITS" },
	{ SHT_SYMTAB,   "SHT_SYMTAB"   },
	{ SHT_STRTAB,   "SHT_STRTAB"   },
	{ SHT_RELA,     "SHT_RELA"     },
	{ SHT_HASH,     "SHT_HASH"     },
	{ SHT_DYNAMIC,  "SHT_DYNAMIC"  },
	{ SHT_NOTE,     "SHT_NOTE"     },
	{ SHT_NOBITS,   "SHT_NOBITS"   },
	{ SHT_REL,      "SHT_REL"      },
	{ SHT_SHLIB,    "SHT_SHLIB"    },
	{ SHT_DYNSYM,   "SHT_DYNSYM"   },
};

#endif

static unsigned long elfhash(const unsigned char *name)
{
	unsigned long h = 0, g;

	while (*name)
	{
		h = (h << 4) + *name++;

		if (g = h & 0xf0000000)
			h ^= g >> 24;

		h &= ~g;
	}

	return h;
}

void elfdump(io_stream_t io)
{
	char buf[200];

	if (io_stream_read(io, buf, sizeof(buf)))
	{
		hex_dump(buf, sizeof(buf));
	}
}

#endif

