/*------------------------------------------------------------------------------

	platform/posix/clock.cpp

------------------------------------------------------------------------------*/

#include "pch.h"
#include <time.h>
#include <math.h>
#include <sys/time.h>
#include "posix.h"

#include "../../lib/elements/time.h"
#include "../../lib/misc/misc.h"
#include "../../lib/core/clock.h"

//
//
//

static clock_info_t g_time_info_realtime;
static clock_info_t g_time_info_monotonic;

//
//
//

int64_t timespec_nanoseconds(struct timespec ts)
{
	// return number of integral nanoseconds

	_ASSERT(ts.tv_nsec < 1000000000);

	int64_t ns = (int64_t)ts.tv_sec * 1000000000 + ts.tv_nsec;

	return ns;
}

int64_t timespec_microseconds(struct timespec ts)
{
	// return number of integral microseconds

	_ASSERT(ts.tv_nsec < 1000000000);

	int64_t us = (int64_t)ts.tv_sec * 1000000 + ts.tv_nsec / 1000;

	return us;
}

f64 timespec_seconds(struct timespec ts)
{
	// return number of floating point seconds

	_ASSERT(ts.tv_nsec < 1000000000);

	f64 sec = (f64)ts.tv_sec + ts.tv_nsec / 1000000000.0;

	return sec;
}

struct timespec timespec_from_seconds(f64 sec)
{
	struct timespec ts = { 0, 0 };

	ts.tv_sec = sec;
	ts.tv_nsec = fmod(sec, 1.0) * 1000000000;

	return ts;
}

struct timespec operator+(const struct timespec t1, const struct timespec t2)
{
	_ASSERT(t1.tv_nsec < 1000000000);
	_ASSERT(t2.tv_nsec < 1000000000);

	long int nsec = t1.tv_nsec + t2.tv_nsec;

	struct timespec t3;

	t3.tv_nsec = nsec % 1000000000;
	t3.tv_sec = t1.tv_sec + t2.tv_sec + nsec / 1000000000;

	return t3;
}

//
//
//

static double get_time__(int select);
static double get_time(clockid_t clock_id, clock_info_t &info);
static void get_time_init(clockid_t clock_id, clock_info_t &info, s64 t0);
static s64 clock_res(clockid_t clock_id);

//
//
//

art_time_t time_system_utc()
{
	if (0)
	{
		clockid_t clock_id = CLOCK_REALTIME;

		struct timespec ts = { 0 };

		if (check_posix_return("clock_gettime", clock_gettime(clock_id, &ts)))
		{
			int64_t t1 = timespec_nanoseconds(ts);
		}
		else
		{
			return time_error();
		}
	}

	timeval tv = { 0 };

	if (check_posix_return("gettimeofday", gettimeofday(&tv, NULL)))
	{
		_ASSERT(tv.tv_usec < 1000000);

		// seconds between the Windows FILETIME epoch and the UNIX epoch (REF: ???)
		static const u64 SecsTo1970 = 116444736000000000LL;

		// seconds since the UNIX epoch
		double sec = tv.tv_sec + tv.tv_usec / 1000000.0;

		// change epoch and convert to 100ns units
		return time_data(&g_time_type_filetime_utc,
			(SecsTo1970 + sec) * 10000000.0);
	}
	else
	{
		return time_error();
	}
}

#ifndef REMOVE_THIS

bool time_current__(art_time_t &current)
{
	current = time_system_utc();

	return true;
}

art_time_t time_current()
{
	art_time_t t;

	if (time_current__(t))
		return t;

	DBG_NOT_IMPLEMENTED();

	return time_error();
}

art_time_t time_hires()
{
	clockid_t clock_id = CLOCK_MONOTONIC;

	struct timespec ts = { 0 };

	if (check_posix_return("clock_gettime", clock_gettime(clock_id, &ts)))
		return time_data(&g_time_type_perf, timespec_nanoseconds(ts));

	return time_error();
}

double get_timer_period()
{
	// initialize timer info

	get_time(CLOCK_MONOTONIC, g_time_info_monotonic);

	return g_time_info_monotonic.period;
}

#endif

//
//
//

double time_monotonic()
{
	return get_time__(2);
}

double time_realtime()
{
	return get_time__(1);
}

#ifdef _DEBUG

static int g_test_get_time;

static void test_get_time()
{
	for (int select = 1; select <= 2; select++)
	{
		f64 t1 = get_time__(select);
		f64 t2 = t1;

		// wait for change

		for (;;)
		{
			t2 = get_time__(select);

			if (t2 != t1)
				break;
		}

		// wait for change

		t1 = get_time__(select);

		for (;;)
		{
			t2 = get_time__(select);

			if (t2 != t1)
				break;
		}

		trace("test_get_time(): select=", select, ", resolution=~", (t2 - t1) * 1000.0, " msec\n");
	}
}

#endif

static double get_time__(int select)
{
	//
	//
	//

#ifdef _DEBUG
	if (g_test_get_time == 0)
	{
		g_test_get_time = 1;

		test_get_time();
	}
#endif

	//
	//
	//

	switch (select)
	{
		case 1: return get_time(CLOCK_REALTIME, g_time_info_realtime);
		case 2: return get_time(CLOCK_MONOTONIC, g_time_info_monotonic);
		default: DBG_INVALID_CASE(); break;
	}

	//
	//
	//

	return float_qnan();
}

static double get_time(clockid_t clock_id, clock_info_t &info)
{
	struct timespec ts = { 0 };

	if (check_posix_return("clock_gettime", clock_gettime(clock_id, &ts)))
	{
		int64_t t1 = timespec_nanoseconds(ts);

		if (info.init == 0)
			get_time_init(clock_id, info, t1);

		_ASSERT(info.period == clock_res(clock_id) / 1000000000.0); // clock rate is assumed constant

		check_retrograde(info, t1);

		return (t1 - info.base) * info.period; // seconds since first call
	}

	return float_qnan();
}

static void get_time_init(clockid_t clock_id, clock_info_t &info, s64 t0)
{
	if (info.init == 0)
	{
		info.base = t0;

		s64 period = clock_res(clock_id);

		if (period != 0)
		{
			info.period = period / 1000000000.0; // nanoseconds
			info.init = 1;
		}
		else
		{
			info.init = -1;
		}
	}
}

// return the specified clock resolution, in nanoseconds

static int64_t clock_res(clockid_t clock_id)
{
	struct timespec res1 = { 0 };

	if (check_posix_return("clock_getres", clock_getres(clock_id, &res1)))
	{
		int64_t res2 = timespec_nanoseconds(res1);

		_ASSERT(res2 != 0);

		return res2;
	}

	return 0;
}

uint64_t posix_timestamp()
{
#if 1
	clockid_t clock_id = CLOCK_REALTIME;

	struct timespec ts = { 0 };

	if (check_posix_return("clock_gettime", clock_gettime(clock_id, &ts)))
	{
		int64_t us = timespec_microseconds(ts);

		// return number of microseconds since the UNIX epoch

		return us;
	}

	return 0;
#else
	// the Windows implementation does the following (should both posix and windows
	// use the same?):
	
	double t = time_realtime();
	
	return (uint64_t)(t * 1000000); // seconds to microseconds
#endif
}

