/*------------------------------------------------------------------------------

	platform/posix/platform.cpp

------------------------------------------------------------------------------*/

#include "pch.h"
#include "../platform.h"
#include "posix.h"

#ifndef LITE_RTL

const platform_exports_t g_platform_exports =
{
	posix_process_create,
	NULL,
	NULL,
	posix_io_exists,
	posix_io_delete,
	NULL,
	
	NULL,
};

platform_exports_t platform_exports()
{
	return g_platform_exports;
}

#endif

