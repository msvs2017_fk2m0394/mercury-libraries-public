/*------------------------------------------------------------------------------

	platform/posix/module.cpp - POSIX module support

------------------------------------------------------------------------------*/

#include "pch.h"
#include <dlfcn.h>
#include <unistd.h>
#include <sys/stat.h>
#include <errno.h>
#include <mercury/experimental/var.h>
#include "posix.h"

#include "../../lib/core/module.h"
#include "../../lib/elements/time.h"
#include "../../platform/windows/pe2.h"

static string_t g_extensions_dirname = "extensions"; // XXX: startup code

//
//
//

struct linux_module_t : module_object_t
{
	linux_module_t();

	// *** module_object_t methods ***
	virtual void close()
	{
		if (m_module != NULL)
		{
			dlclose(m_module);

			m_module = NULL;

			m_public.status = io_resource_status_closed;
		}
	}

	void *   m_module; // return value from dlopen()
	string_t m_filename;
	int      m_filename_flag;

	OBJECT_CLASS_DECLARE();
};

#ifndef REMOVE_THIS

struct rtl_module_state_t : base_rtl_module_state_t
{
	rtl_module_state_t();

	linux_module_t module_object;
};

#endif

//
//
//

// static-link state for each executable module using library...
static global_thing_t<rtl_module_state_t> g_rtl_module_state;

static global_thing_t<linux_module_t> g_exe_module;

//
//
//

rtl_module_state_t::rtl_module_state_t()
{
	this->module.m_object = &this->module_object.m_public;
}

//
//
//

static string_t exe_name()
{
	string_t name;

#if defined(__linux__)

	char buf[200] = { 0 }; // XXX: what is the proper size here???

	ssize_t n = readlink("/proc/self/exe", buf, sizeof(buf));

	int err = errno;

	if (n >= 0)
	{
		name = string(buf, n);
	}
	else
	{
		_ASSERT(n == -1);

		name = posix_error_message("unable to read executable image name", err);
	}

#elif defined(__MACH__)

	char path[1024] = { 0 };

	uint32_t size = sizeof(path);

	if (_NSGetExecutablePath(path, &size) == 0)
	{
		name = string(path, size);
	}
	else
	{
		name = posix_error_message("unable to read executable image name: buffer to small");
	}

#else

#error not ported

#endif

#ifdef _DEBUG
	message("exe name=", name, "\n");
#endif

	return name;
}

//
//
//

OBJECT_CLASS_BEGIN(linux_module_t, module_object_t)
OBJECT_CLASS_END()

static void linux_module_exe_ctor(linux_module_t *module)
{
	module->m_public.status = io_resource_status_open;
}

module_t module_exe()
{
	return module_t(&g_exe_module->m_public);
}

//
//
//

linux_module_t::linux_module_t()
{
	if ((void*)this == (void*)g_exe_module.storage)
	{
//		linux_module_exe_ctor(this);
		m_public.status = io_resource_status_open;
	}
}

base_rtl_module_state_t *base_rtl_module_state()
{
	return g_rtl_module_state;
}

module_t module_current()
{
	return g_rtl_module_state->module;
}

// NOTE: Windows maintains a user-mode module table in each process.  This
// capability is not readily available on POSIX systems.

module_t module_from_address(const void *addr)
{
	DBG_NOT_IMPLEMENTED();

	return module_error(/* "module_from_address(): not implemented" */);
}

module_object_t *module_from_filename__(string_t filename)
{
	_ASSERT(filename != "");

	DBG_NOT_IMPLEMENTED();

	linux_module_t *data = new linux_module_t;

	if (data != NULL)
	{
		if (filename != "")
		{
			int mode = 0;

			void *ptr = dlopen(filename, mode);

			if (ptr != NULL)
			{
				data->m_module = ptr;
				data->m_public.status = io_resource_status_closed;
				data->m_public.filename = data->m_filename;
			}
			else
			{
			}
		}
	}

	return data;
}

module_object_t *module_map__(string_t filename)
{
	_ASSERT(filename != "");

	DBG_NOT_IMPLEMENTED();

	return NULL;
}

module_version_t module_version(module_t module)
{
	DBG_NOT_IMPLEMENTED();

	module_version_t version;

	clear2(version);

	return version;
}

void *module_address(module_t module, const char *name)
{
/*
		void *sym = dlsym(mod.m_object->object->m_module, name);

		if (sym != NULL)
		{
			return sym;
		}
*/

	DBG_NOT_IMPLEMENTED();

	return NULL;
}

time_data_t module_timestamp(module_t module)
{
	DBG_NOT_IMPLEMENTED();

	return time_data(NULL, 0);
}

string_t module_filename____(module_public_t *pub)
{
	if (pub != NULL)
	{
		if (pub->object != NULL)
		{
			linux_module_t *mod = obj_cast<linux_module_t>(pub->object);

			if (mod != NULL)
			{
				if (mod->m_filename_flag == 0)
				{
					mod->m_filename = exe_name();
					mod->m_public.filename = mod->m_filename; // N.B. makes alias into m_filename buffer
					mod->m_filename_flag = 1;
				}

				return mod->m_filename;
			}
		}
	}

	DBG_NOT_IMPLEMENTED();

	return "";
}

io_stream_t module_resource__(module_t module, resource_id_t id)
{
	DBG_NOT_IMPLEMENTED();

	return io_stream_error("module_resource(): not implemented on this platform");
}

io_stream_t module_resource(resource_id_t id)
{
	return module_resource__(module_current(), id);
}

io_stream_t module_resource(module_t module, resource_id_t id)
{
	return module_resource__(module, id);
}

io_stream_t module_resource(const char *type, const char *name)
{
	return module_resource__(module_current(), resource_id(type, name));
}

io_stream_t module_resource(const char *type, const char *name, const char *lang)
{
	return module_resource__(module_current(), resource_id(type, name, lang));
}

io_stream_t module_resource(module_t module, const char *type, const char *name)
{
	return module_resource__(module, resource_id(type, name));
}

io_stream_t module_resource(module_t module, const char *type, const char *name, const char *lang)
{
	return module_resource__(module, resource_id(type, name, lang));
}

string_t load_string(module_t module, u32 id)
{
	DBG_NOT_IMPLEMENTED();

	return "";
}

string_t bldinfo_text(module_t module)
{
	DBG_NOT_IMPLEMENTED();

	return "";
}

//
//
//

static linux_module_t *linux_module(module_object_t *obj)
{
// !?!?!?!?!?!?!?!?!?!?!?
	if ((void*)obj == (void*)g_exe_module.storage)
	{
		// obj_cast won't work since object doesn't have a header!!!
	}
// !?!?!?!?!?!?!?!?!?!?!?

	return obj_cast<linux_module_t>(obj);
}

static bool module_load__(module_object_t *obj__, string_t filename)
{
	if (obj__ == NULL)
		return false;

	if (is_error(filename))
		return false;
	
	if (is_empty(filename))
		return false;

	linux_module_t *obj = linux_module(obj__);
	
	if (obj == NULL)
		return false;

	if (obj->m_module != NULL)
		return true;

	if (is_ansi(filename))
	{
		int mode = 0;

		void *ptr = dlopen(filename, mode);

		if (ptr != NULL)
		{
			obj->m_module = ptr;
			obj->m_public.status = io_resource_status_open;
			obj->m_public.filename = filename;
			
			return true;
		}
		
		const char *msg = dlerror();
		
		int err = errno;

		obj->m_public.status = io_resource_status_error;

		return false;
	}
	
	if (is_unicode(filename))
	{
		DBG_NOT_IMPLEMENTED();

		return false;
	}

	DBG_INVALID_CASE();

	return false;
}

module_t module_load(string_t filename)
{
	module_t module = module_from_filename(filename);
	
	module_load__(module_object(module.m_object), filename);

	return module;
}

static bool find_module(string_t base, string_array_t path, string_t &filename)
{
	for (int i = 0; i < path.length(); i++)
	{
		string_t temp = base;
		
		temp = file_path_append(temp, path[i]);
		temp = file_path_append(temp, filename);

		if (file_exists(temp))
		{
			filename = temp;

			return true;
		}
	}

	return false;
}

string_t find_extension_module(string_t name)
{
	string_t buffer = name;

	string_array_t path;

	path += "";
	path += g_extensions_dirname;
	
	if (find_module(file_path(module_filename()), path, buffer))
		return buffer;

	return string_error();
}

module_t module_load_extension(string_t name)
{
	return module_load(find_extension_module(name));
}

resource_id_t resource_id(
	resource_id_piece_t type,
	resource_id_piece_t name,
	resource_id_piece_t lang)
{
	resource_id_t id;

	clear2(id);

	id.type = type;
	id.name = name;
	id.lang = lang;

	return id;
}

resource_id_t resource_id(resource_id_piece_t type)
{
	return resource_id(type, resource_id_piece_t(), resource_id_piece_t()); // use default language
}

resource_id_t resource_id(resource_id_piece_t type, resource_id_piece_t name)
{
	return resource_id(type, name, resource_id_piece_t()); // use default language
}

static const void *get_addr__(export_info_t &info, int mode)
{
	DBG_NOT_IMPLEMENTED();
	
	return NULL;
}

const void *get_addr(export_info_t &proc)
{
	return get_addr__(proc, 0);
}

const void *get_addr_sys(export_info_t &proc)
{
	return get_addr__(proc, 1);
}

const void *get_addr_ext(export_info_t &proc)
{
	return get_addr__(proc, 2);
}

string_t error_message(export_info_t &proc)
{
	DBG_NOT_IMPLEMENTED();
	
	return "";
}

