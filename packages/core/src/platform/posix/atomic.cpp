/*------------------------------------------------------------------------------

	platform/posix/atomic.cpp

	BUG: atomics are not correct for ARM builds?!?!?!?

------------------------------------------------------------------------------*/

#include "pch.h"
#include "posix.h"

#ifdef __clang__
#define MEMORY_MODEL __ATOMIC_SEQ_CST // use the strongest memory model
#define __atomic_add_fetch(a,b,c) __sync_add_and_fetch(a,b)
#define __atomic_sub_fetch(a,b,c) __sync_sub_and_fetch(a,b)
#define __atomic_exchange_n(a,b,c) __sync_lock_test_and_set(a,b)
#else
#define __atomic_add_fetch(a,b,c) __sync_add_and_fetch(a,b)
#define __atomic_sub_fetch(a,b,c) __sync_sub_and_fetch(a,b)
#define __atomic_exchange_n(a,b,c) __sync_lock_test_and_set(a,b)
#endif

int xinc(int volatile *ptr)
{
	return __atomic_add_fetch(ptr, 1, MEMORY_MODEL);
}

unsigned int xinc(volatile unsigned int *ptr)
{
	return __atomic_add_fetch(ptr, 1, MEMORY_MODEL);
}

unsigned long xinc(unsigned long volatile *ptr)
{
	return __atomic_add_fetch(ptr, 1, MEMORY_MODEL);
}

int xdec(int volatile *ptr)
{
	return __atomic_sub_fetch(ptr, 1, MEMORY_MODEL);
}

unsigned int xdec(unsigned int volatile *ptr)
{
	return __atomic_sub_fetch(ptr, 1, MEMORY_MODEL);
}

unsigned long xdec(unsigned long volatile *ptr)
{
	return __atomic_sub_fetch(ptr, 1, MEMORY_MODEL);
}

int xadd(int volatile *ptr, int b)
{
	return __atomic_add_fetch(ptr, b, MEMORY_MODEL);
}

unsigned int xadd(unsigned int volatile *ptr, unsigned int b)
{
	return __atomic_add_fetch(ptr, b, MEMORY_MODEL);
}

unsigned long xadd(unsigned long volatile *ptr, unsigned long b)
{
	return __atomic_add_fetch(ptr, b, MEMORY_MODEL);
}

int xsub(int volatile *ptr, int b)
{
	return __atomic_sub_fetch(ptr, b, MEMORY_MODEL);
}

unsigned int xsub(unsigned int volatile *ptr, unsigned int b)
{
	return __atomic_sub_fetch(ptr, b, MEMORY_MODEL);
}

unsigned long xsub(unsigned long volatile *ptr, unsigned long b)
{
	return __atomic_sub_fetch(ptr, b, MEMORY_MODEL);
}

int xchg(int volatile *dest, int value)
{
	return __atomic_exchange_n(dest, value, MEMORY_MODEL);
}

unsigned int xchg(unsigned int volatile *dest, unsigned int value)
{
	return __atomic_exchange_n(dest, value, MEMORY_MODEL);
}

unsigned long xchg(unsigned long volatile *dest, unsigned long value)
{
	return __atomic_exchange_n(dest, value, MEMORY_MODEL);
}

int cmpxchg(int volatile *dest, int comp, int xchg)
{
	return __sync_val_compare_and_swap(dest, comp, xchg);
}

unsigned int cmpxchg(unsigned int volatile *dest, unsigned int comp, unsigned int xchg)
{
	return __sync_val_compare_and_swap(dest, comp, xchg);
}

unsigned long cmpxchg(unsigned long volatile *dest, unsigned long comp, unsigned long xchg)
{
	return __sync_val_compare_and_swap(dest, comp, xchg);
}

