/*------------------------------------------------------------------------------

	platform/posix/lock.cpp - POSIX lock support

------------------------------------------------------------------------------*/

#include "pch.h"
#include "../../lib/core/atomic.h"
#include "posix.h"

#ifdef __linux__
#include <linux/futex.h>
#endif

#ifdef _DEBUG
void debug_validate(const lock_data_t *data);
#else
inline void debug_validate(const lock_data_t *) { }
#endif

#ifdef _DEBUG
static int g_miss1;
#endif

//
//
//

lock_internal_t::lock_internal_t()
{
	owner = 0;
	recursion = 0;

	ZERO_INIT(mutex);
	ZERO_INIT(cond);

	debug_validate(this);
}

//
//
//

lock_internal_t::~lock_internal_t()
{
	debug_validate(this);

//	if (init)
	{
		pthread_cond_destroy(&cond);
		pthread_mutex_destroy(&mutex);
	}

	debug_shred(this, sizeof(*this)); // overwrite object in debug builds
}

//
//
//

static lock_internal_t *lock_enter_init(lock_data_t *lock)
{
	lock_internal_t *data = new lock_internal_t;

	if (data != NULL)
	{
		int result;

		result = pthread_cond_init(&data->cond, NULL);

		check_posix_return(NULL, result);

		result = pthread_mutex_init(&data->mutex, NULL);

		check_posix_return(NULL, result);

		lock_internal_t *prev = (lock_internal_t*)cmpxchg((uintptr_t*)&lock->m_data, (uintptr_t)NULL, (uintptr_t)data);

		if (prev == NULL)
		{
		}
		else
		{
			// someone else already initialized this lock's data structure, so
			// our speculative work needs to be undone, and we'll use the
			// existing data instead

#ifdef _DEBUG
			xinc(g_miss1);
#endif

			delete data;

			data = prev;

//			xinc(data->stat1);
		}
	}

	return data;
}

int lock_enter(lock_data_t *lock, lock_mode_t mode, const f32 *timeout)
{
	debug_validate(lock);

	_ASSERT(mode == lock_mode_exclusive);
	_ASSERT(timeout == NULL);

	lock_internal_t *data = lock->m_data;

	if (data == NULL)
		data = lock_enter_init(lock);

	if (data != NULL)
	{
		thread_id_t owner = current_thread_id();

		_ASSERT(owner != 0);

		pthread_mutex_lock(&data->mutex);

		if (data->owner == owner)
		{
			// we already own the lock

			_ASSERT(data->recursion > 0);
		}
		else
		{
			while (cmpxchg(&data->owner, 0, owner) != 0)
			{
				// atomically releases the mutex and waits for the condition
				// variable to signal, then it reacquires the mutex

				int result = pthread_cond_wait(&data->cond, &data->mutex);

//				struct timespec abstime = { 0 };
//				int result = pthread_cond_timedwait(&data->cond, &data->mutex, &abstime);
			}

			_ASSERT(data->recursion == 0);
		}

		_ASSERT(data->owner == owner);

		data->recursion++;

		pthread_mutex_unlock(&data->mutex);

		return true;
	}

	return false;
}

//
//
//

void lock_leave(lock_data_t *lock)
{
	debug_validate(lock);

	lock_internal_t *data = lock->m_data;

	if (data != NULL)
	{
#ifdef _DEBUG
		thread_id_t owner = current_thread_id();
#endif
		
		_ASSERT(owner != 0);

		_ASSERT(data->owner == owner);
		_ASSERT(data->recursion > 0);

		pthread_mutex_lock(&data->mutex);

		if (--data->recursion == 0)
		{
			data->owner = 0;

			pthread_cond_signal(&data->cond);
		}

		pthread_mutex_unlock(&data->mutex);
	}
	else
	{
		DBG_BREAK();
	}
}

//
//
//

void lock_destroy(lock_data_t *lock)
{
	if (lock != NULL)
	{
		lock_internal_t *data = (lock_internal_t*)xchg(
			(uintptr_t*)&lock->m_data, (uintptr_t)NULL);

		delete data;

		debug_shred(lock, sizeof(*lock)); // overwrite object in debug builds
	}
}

//
//
//

#ifdef _DEBUG

void debug_validate(const lock_data_t *data)
{
	_ASSERT(data != NULL);

	const lock_internal_t *x = data->m_data;

	if (x != NULL)
		debug_validate(x);
}

void debug_validate(const lock_internal_t *data)
{
	_ASSERT(data != NULL);
}

#endif

