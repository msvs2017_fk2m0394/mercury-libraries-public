/*------------------------------------------------------------------------------

	platform/posix/elf.h

------------------------------------------------------------------------------*/

#ifndef __POSIX_ELF_H__
#define __POSIX_ELF_H__

#ifndef REMOVE_EXPERIMENTAL

// XXX: copied from IDE project...

/*

REF: Tool Interface Standards (TIS) Portable Formats Specification, Version 1.1

*/

// e_ident[] indices

#define EI_MAG0    0  // File identification
#define EI_MAG1    1  // File identification
#define EI_MAG2    2  // File identification
#define EI_MAG3    3  // File identification
#define EI_CLASS   4  // File class
#define EI_DATA    5  // Data encoding
#define EI_VERSION 6  // File version
#define EI_PAD     7  // Start of padding bytes
#define EI_NIDENT  16 // Size of e_ident[]

#define ELFMAG0 0x7f //e_ident[EI_MAG0]
#define ELFMAG1 'E' //e_ident[EI_MAG1]
#define ELFMAG2 'L' //e_ident[EI_MAG2]
#define ELFMAG3 'F' //e_ident[EI_MAG3]

#define ELFCLASSNONE 0 //Invalid class
#define ELFCLASS32 1 //32-bit objects
#define ELFCLASS64 2 //64-bit objects

#define ELFDATANONE 0 //Invalid data encoding
#define ELFDATA2LSB 1 //See below
#define ELFDATA2MSB 2 //See below

#define EV_NONE    0 // Invalid version
#define EV_CURRENT 1 // Current version

typedef u16 Elf32Half;  // 2 2 Unsigned medium integer
typedef u32 Elf32Word;  // 4 4 Unsigned large integer
typedef u32 Elf32Addr;  // 4 4 Unsigned program address (virtual)
typedef u32 Elf32Off;   // 4 4 Unsigned file offset
typedef u32 Elf32Sword; // 4 4 Signed large integer

C_ASSERT(sizeof(Elf32Addr) == 4);  // 4 4 Unsigned program address
C_ASSERT(sizeof(Elf32Half) == 2);  // 2 2 Unsigned medium integer
C_ASSERT(sizeof(Elf32Off) == 4);   // 4 4 Unsigned file offset
C_ASSERT(sizeof(Elf32Sword) == 4); // 4 4 Signed large integer
C_ASSERT(sizeof(Elf32Word) == 4);  // 4 4 Unsigned large integer

typedef struct {

	unsigned char e_ident[EI_NIDENT];

	Elf32Half     e_type;      //
	Elf32Half     e_machine;   //
	Elf32Word     e_version;   //
	Elf32Addr     e_entry;     // entry point virtual address
	Elf32Off      e_phoff;     // program header file offset

	Elf32Off      e_shoff;     // section header table file offset
	Elf32Word     e_flags;
	Elf32Half     e_ehsize;    // ELF header size
	Elf32Half     e_phentsize; // program header entry size
	Elf32Half     e_phnum;     // program header entry count
	Elf32Half     e_shentsize; // section header table entry size

	Elf32Half     e_shnum;     // section header table entry count
	Elf32Half     e_shstrndx;  // section header table entry string index

} Elf32Ehdr;

C_ASSERT(sizeof(Elf32Ehdr) == 13 * 4);

#define ET_NONE 0 // No file type
#define ET_REL 1 // Relocatable file
#define ET_EXEC 2 // Executable file
#define ET_DYN 3 // Shared object file
#define ET_CORE 4 // Core file
#define ET_LOPROC 0xff00 // Processor-specific
#define ET_HIPROC 0xffff // Processor-specific


#define EM_NONE 0 // No machine
#define EM_M32 1 // AT&T WE 32100
#define EM_SPARC 2 // SPARC
#define EM_386 3 // Intel 80386
#define EM_68K 4 // Motorola 68000
#define EM_88K 5 // Motorola 88000
#define EM_860 7 // Intel 80860
#define EM_MIPS 8 // MIPS RS3000

#define EM_SPARC32PLUS 18 // Sun SPARC 32+
#define EM_SPARCV9 43 // SPARC V9
#define EM_AMD64 62 // AMD 64

#if 1

//from http://web.stanford.edu/class/cs140/projects/pintos/specs/sysv-abi-update.html/ch4.eheader.html

#define EM_NONE 0 // No machine
#define EM_M32 1 // AT&T WE 32100
#define EM_SPARC 2 // SPARC
#define EM_386 3 // Intel 80386
#define EM_68K 4 // Motorola 68000
#define EM_88K 5 // Motorola 88000
// reserved 6 Reserved for future use (was EM_486) 
#define EM_860 7 // Intel 80860
#define EM_MIPS 8 // MIPS I Architecture
#define EM_S370 9 // IBM System/370 Processor
#define EM_MIPS_RS3_LE 10 // MIPS RS3000 Little-endian
//reserved 11-14 Reserved for future use 
#define EM_PARISC 15 // Hewlett-Packard PA-RISC
//reserved 16 Reserved for future use 
#define EM_VPP500 17 // Fujitsu VPP500
#define EM_SPARC32PLUS 18 // Enhanced instruction set SPARC
#define EM_960 19 // Intel 80960
#define EM_PPC 20 // PowerPC
#define EM_PPC64 21 // 64-bit PowerPC
#define EM_S390 22 // IBM System/390 Processor
//reserved 23-35 Reserved for future use 
#define EM_V800 36 // NEC V800
#define EM_FR20 37 // Fujitsu FR20
#define EM_RH32 38 // TRW RH-32
#define EM_RCE 39 // Motorola RCE
#define EM_ARM 40 // Advanced RISC Machines ARM
#define EM_ALPHA 41 // Digital Alpha
#define EM_SH 42 // Hitachi SH
#define EM_SPARCV9 43 // SPARC Version 9
#define EM_TRICORE 44 // Siemens TriCore embedded processor
#define EM_ARC 45 // Argonaut RISC Core, Argonaut Technologies Inc.
#define EM_H8_300 46 // Hitachi H8/300
#define EM_H8_300H 47 // Hitachi H8/300H
#define EM_H8S 48 // Hitachi H8S
#define EM_H8_500 49 // Hitachi H8/500
#define EM_IA_64 50 // Intel IA-64 processor architecture
#define EM_MIPS_X 51 // Stanford MIPS-X
#define EM_COLDFIRE 52 // Motorola ColdFire
#define EM_68HC12 53 // Motorola M68HC12
#define EM_MMA 54 // Fujitsu MMA Multimedia Accelerator
#define EM_PCP 55 // Siemens PCP
#define EM_NCPU 56 // Sony nCPU embedded RISC processor
#define EM_NDR1 57 // Denso NDR1 microprocessor
#define EM_STARCORE 58 // Motorola Star*Core processor
#define EM_ME16 59 // Toyota ME16 processor
#define EM_ST100 60 // STMicroelectronics ST100 processor
#define EM_TINYJ 61 // Advanced Logic Corp. TinyJ embedded processor family
#define EM_X86_64 62 // AMD x86-64 architecture
#define EM_PDSP 63 // Sony DSP Processor
#define EM_PDP10 64 // Digital Equipment Corp. PDP-10
#define EM_PDP11 65 // Digital Equipment Corp. PDP-11
#define EM_FX66 66 // Siemens FX66 microcontroller
#define EM_ST9PLUS 67 // STMicroelectronics ST9+ 8/16 bit microcontroller
#define EM_ST7 68 // STMicroelectronics ST7 8-bit microcontroller
#define EM_68HC16 69 // Motorola MC68HC16 Microcontroller
#define EM_68HC11 70 // Motorola MC68HC11 Microcontroller
#define EM_68HC08 71 // Motorola MC68HC08 Microcontroller
#define EM_68HC05 72 // Motorola MC68HC05 Microcontroller
#define EM_SVX 73 // Silicon Graphics SVx
#define EM_ST19 74 // STMicroelectronics ST19 8-bit microcontroller
#define EM_VAX 75 // Digital VAX 
#define EM_CRIS 76 // Axis Communications 32-bit embedded processor
#define EM_JAVELIN 77 // Infineon Technologies 32-bit embedded processor
#define EM_FIREPATH 78 // Element 14 64-bit DSP Processor
#define EM_ZSP 79 // LSI Logic 16-bit DSP Processor
#define EM_MMIX 80 // Donald Knuth's educational 64-bit processor
#define EM_HUANY 81 // Harvard University machine-independent object files
#define EM_PRISM 82 // SiTera Prism
#define EM_AVR 83 // Atmel AVR 8-bit microcontroller
#define EM_FR30 84 // Fujitsu FR30
#define EM_D10V 85 // Mitsubishi D10V
#define EM_D30V 86 // Mitsubishi D30V
#define EM_V850 87 // NEC v850
#define EM_M32R 88 // Mitsubishi M32R
#define EM_MN10300 89 // Matsushita MN10300
#define EM_MN10200 90 // Matsushita MN10200
#define EM_PJ 91 // picoJava
#define EM_OPENRISC 92 // OpenRISC 32-bit embedded processor
#define EM_ARC_A5 93 // ARC Cores Tangent-A5
#define EM_XTENSA 94 // Tensilica Xtensa Architecture
#define EM_VIDEOCORE 95 // Alphamosaic VideoCore processor
#define EM_TMM_GPP 96 // Thompson Multimedia General Purpose Processor
#define EM_NS32K 97 // National Semiconductor 32000 series
#define EM_TPC 98 // Tenor Network TPC processor
#define EM_SNP1K 99 // Trebia SNP 1000 processor
#define EM_ST200 100 // STMicroelectronics (www.st.com) ST200 microcontroller

#endif

#define SHN_UNDEF 0// 
#define SHN_LORESERVE 0xff00// 
#define SHN_LOPROC 0xff00// 
#define SHN_HIPROC 0xff1f// 
#define SHN_ABS 0xfff1// 
#define SHN_COMMON 0xfff2// 
#define SHN_HIRESERVE 0xffff// 

typedef struct {
	Elf32Word shname;
	Elf32Word shtype;
	Elf32Word shflags;
	Elf32Addr shaddr;
	Elf32Off  shoffset;
	Elf32Word shsize;
	Elf32Word shlink;
	Elf32Word shinfo;
	Elf32Word shaddralign;
	Elf32Word shentsize;
} Elf32Shdr;

C_ASSERT(sizeof(Elf32Shdr) == 10 * 4);

#define SHT_NULL 0// 
#define SHT_PROGBITS 1// 
#define SHT_SYMTAB 2// 
#define SHT_STRTAB 3// 
#define SHT_RELA 4// 
#define SHT_HASH 5// 
#define SHT_DYNAMIC 6// 
#define SHT_NOTE 7// 
#define SHT_NOBITS 8// 
#define SHT_REL 9// 
#define SHT_SHLIB 10// 
#define SHT_DYNSYM 11// 
#define SHT_LOPROC 0x70000000// 
#define SHT_HIPROC 0x7fffffff// 
#define SHT_LOUSER 0x80000000// 
#define SHT_HIUSER 0xffffffff// 

#define SHF_WRITE 0x1// 
#define SHF_ALLOC 0x2// 
#define SHF_EXECINSTR 0x4// 
#define SHF_MASKPROC 0xf0000000// 

typedef struct {
	Elf32Word     stname;
	Elf32Addr     stvalue;
	Elf32Word     stsize;
	unsigned char stinfo;
	unsigned char stother;
	Elf32Half     stshndx;
} Elf32Sym;

/*

#define ELF32STBIND ( i ) ( ( i ) > > 4 )
#define ELF32STTYPE ( i ) ( ( i ) & 0 x f )
#define ELF32STINFO ( b , t ) ( ( ( b ) < < 4 ) + ( ( t ) & 0 x f ) )

#define STB_LOCAL 0// 
#define STB_GLOBAL 1// 
#define STB_WEAK 2// 
#define STB_LOPROC 13// 
#define STB_HIPROC 15// 

#define STT_NOTYPE 0// 
#define STT_OBJECT 1// 
#define STT_FUNC 2// 
#define STT_SECTION 3// 
#define STT_FILE 4// 
#define STT_LOPROC 13// 
#define STT_HIPROC 15// 

*/

typedef struct {
	Elf32Addr roffset;
	Elf32Word rinfo;
} Elf32Rel;

typedef struct {
	Elf32Addr  roffset;
	Elf32Word  rinfo;
	Elf32Sword raddend;
} Elf32Rela;

/*

#define ELF32RSYM(i) ((i) >> 8)
#define ELF32RTYPE(i) ((unsigned char)(i))
#define ELF32RINFO(s, t) (((s) << 8) + (unsigned char)(t))

#define R_386_NONE 0 // none none
#define R_386_32 1 // word32 S + A
#define R_386_PC32 2 // word32 S + A - P
#define R_386_GOT32 3 // word32 G + A - P
#define R_386_PLT32 4 // word32 L + A - P
#define R_386_COPY 5 // none none
#define R_386_GLOB_DAT 6 // word32 S
#define R_386_JMP_SLOT 7 // word32 S
#define R_386_RELATIVE 8 // word32 B + A
#define R_386_GOTOFF 9 // word32 S + A - GOT
#define R_386_GOTPC 10 // word32 GOT + A - P

typedef struct {
	Elf32Word ptype;
	Elf32Off  poffset;
	Elf32Addr pvaddr;
	Elf32Addr ppaddr;
	Elf32Word pfilesz;
	Elf32Word pmemsz;
	Elf32Word pflags;
	Elf32Word palign;
} Elf32Phdr;

#define PT_NULL 0// 
#define PT_LOAD 1// 
#define PT_DYNAMIC 2// 
#define PT_INTERP 3// 
#define PT_NOTE 4// 
#define PT_SHLIB 5// 
#define PT_PHDR 6// 
#define PT_LOPROC 0x70000000// 
#define PT_HIPROC 0x7fffffff// 

*/

type_t dynamic_type(const Elf32Ehdr *);
type_t dynamic_type(const Elf32Shdr *);

#endif

#endif // __POSIX_ELF_H__

