/*------------------------------------------------------------------------------

	platform/posix/event.cpp

	TODO: check return values from pthread_cond_init, etc.

	TODO: should we call pthread_cond_signal/pthread_cond_broadcast while
	holding the mutex, or after releasing it (as it currently done)

------------------------------------------------------------------------------*/

#include "pch.h"
#include <math.h>
#include <errno.h>
#include "posix.h"

#ifndef REMOVE_EXPERIMENTAL
#ifdef __linux__
#include <linux/futex.h>
#endif
#endif

sync_event_t::sync_event_t()
{
	m_valid = 0;
	m_auto_reset = 0;
	ZERO_INIT(m_cond);
	ZERO_INIT(m_mutex);
	m_state = 0;
}

sync_event_t::~sync_event_t()
{
	close();
}

bool sync_event_t::close()
{
	if (m_valid != 0)
	{
		pthread_cond_destroy(&m_cond);
		pthread_mutex_destroy(&m_mutex);

		m_valid = 0;
	}

	return true;
}

bool sync_event_t::is_valid_handle()
{
	return m_valid;
}

bool sync_event_t::wait()
{
	return wait2(NULL);
}

bool sync_event_t::wait(f32 timeout)
{
	return wait2(&timeout);
}

bool sync_event_t::create_event(bool manual_reset, bool initial_state)
{
	int result;

	result = pthread_cond_init(&m_cond, NULL);

//	check_posix_return("pthread_cond_init", result)

	result = pthread_mutex_init(&m_mutex, NULL);

	m_valid = (result == 0);

	m_auto_reset = !manual_reset;

	m_state = initial_state;

	return m_valid;
}

bool sync_event_t::signal()
{
	pthread_mutex_lock(&m_mutex);

	m_state = 1;

	pthread_mutex_unlock(&m_mutex);

	// ordering here (e.g. ahead of pthread_mutex_unlock?)

	int result;

	if (m_auto_reset)
		result = pthread_cond_signal(&m_cond);
	else
		result = pthread_cond_broadcast(&m_cond);

	if (result == 0)
		return true;

	return false;
}

bool sync_event_t::wait2(const f32 *timeout)
{
	pthread_mutex_lock(&m_mutex);

	bool success = false;

	while (m_state == 0)
	{
		// atomically releases the mutex and waits for the condition variable
		// to signal, then it reacquires the mutex

		if (timeout == NULL)
		{
			int result = pthread_cond_wait(&m_cond, &m_mutex);

			if (result == 0)
			{
				success = true;
			}
			else
			{
				pthread_mutex_unlock(&m_mutex);

				posix_call_failed2("pthread_cond_wait", result);

				return false;
			}
		}
		else
		{
			timespec abstime = { 0 };

			clock_gettime(CLOCK_REALTIME, &abstime);

			abstime = abstime + timespec_from_seconds(*timeout);

			_ASSERT(abstime.tv_nsec < 1000000000);

			int result = pthread_cond_timedwait(&m_cond, &m_mutex, &abstime);

			if (result == 0)
			{
				success = true;
			}
			else if (result == ETIMEDOUT)
			{
				pthread_mutex_unlock(&m_mutex);

				return false;
			}
			else
			{
				posix_call_failed2("pthread_cond_timedwait", result);
			}
		}
	}

//	if (success)
	{

	_ASSERT(m_state == 1);

	if (m_auto_reset)
		m_state = 0;

	}

	pthread_mutex_unlock(&m_mutex);

	return true;
}

u32 event_register_event_callback(sync_event_t *event, event_signal_callback_t function, void *context)
{
	DBG_NOT_IMPLEMENTED();

	return 0;
}

