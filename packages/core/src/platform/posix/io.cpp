/*------------------------------------------------------------------------------

	platform/posix/io.cpp - POSIX I/O support

	TODO: provide options for buffering pipe I/O

------------------------------------------------------------------------------*/

#include "pch.h"
#include <mercury/extensions/io.h>
#include <mercury/experimental/bit.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>
#ifndef PLATFORM_ANDROID
#include <aio.h>
#endif
#include "../../lib/io/network.h"
#include "posix.h"

//
//
//

io_status_t posix_io_exists(string_t filename)
{
	_ASSERT(filename != "");

	int result = access(filename, F_OK);

	// if the file does not exist, simply return false (don't log an error)

	if (result == -1 && errno == ENOENT)
		return false;

	return check_posix_return("access", result);
}

//
//
//

io_status_t posix_io_delete(string_t filename)
{
	_ASSERT(filename != "");

	// TODO: Windows deletes both files and directories in this function,
	// figure out what is appropriate here

	return check_posix_return("unlink", unlink(filename));
}

//
//
//

io_stat_t io_stat(const char *name)
{
	io_stat_t s2 = { 0 };

	struct stat info = { 0 };

//	int result = lstat(name, &info);
	int result = stat(name, &info);

	if (check_posix_return("stat", result))
	{
		if (S_ISLNK(info.st_mode))
		{
			// stat.st_size is the length of the pathname, without a terminator
		}

		if (S_ISDIR(info.st_mode))
		{
		}
	}
	else
	{
	}

	return s2;
}

#ifndef REMOVE_THIS

string_t file_path(string_t filename)
{
	if (filename != "") // ignore empty strings and errors
	{
		struct stat info = { 0 };

		if (check_posix_return("stat", stat(filename, &info)))
		{
			if (!S_ISDIR(info.st_mode))
			{
				if (is_ansi(filename))
				{
					const char *lpFilePart = strrchr(filename, '\\');

					if (lpFilePart == NULL)
						lpFilePart = strrchr(filename, '/');

					if (lpFilePart != NULL)
					{
						return left(filename, lpFilePart - (const char *)filename);
					}
				}
			}
		}
		else
		{
			int err = errno;

			return posix_error_message("stat", err);
		}
	}

	return filename;
}

#endif

bool is_dir_sep(const char *ptr)
{
	// file system specific...

	return (ptr != NULL && ptr[0] == '/');
}

io_status_t io_directory_create(string_t filename)
{
	// XXX: copied from Windows implementation of io_directory_create()

	string_t path;

	const char *ptr = filename;

	for (;;)
	{
		//
		//  Add next path component (begins with directory separator)
		//

		int dir_count = 1;

		for (; *ptr != 0; ptr++)
		{
			if (is_dir_sep(ptr))
			{
				int temp = dir_count;

				dir_count--;

				if (temp == 0)
					break;
			}

			path += *ptr;
		}

		//
		//  Any more components added?
		//

		if (dir_count == 0)
			break;

		//
		//  Check if directory already exists
		//

		if (!is_directory(path))
		{
#ifndef PLATFORM_ANDROID
			mode_t mode = __S_IREAD | __S_IWRITE | __S_IEXEC;
#else
			mode_t mode = 0;
#endif

			int result = mkdir(path, mode);

			if (!check_posix_return("mkdir", result))
				return false;
		}
	}

	return true;
}

io_status_t is_directory(string_t filename)
{
	io_status_t ret;

	io_stat_t s2 = { 0 };

	struct stat info = { 0 };

	int result = stat(filename, &info);

	// if the directory does not exist, simply return false (don't log an error)

	if (result == -1 && errno == ENOENT)
		return false;

	if (check_posix_return("stat", result))
	{
		if (S_ISDIR(info.st_mode))
		{
			ret = 1;
		}
	}
	else
	{
	}

	return ret;
}

#ifndef REMOVE_THIS // this looks portable...

extern "C" {
FILE * __crt_stdin() { return stdin; }
FILE * __crt_stdout() { return stdout; }
FILE * __crt_stderr() { return stderr; }
}

#endif

io_status_t file_last_write_date_utc(string_t filename, art_time_t &t)
{
	DBG_NOT_IMPLEMENTED();

	if (filename != "") // ignore empty strings and errors
	{
		struct stat info = { 0 };

		if (check_posix_return("stat", stat(filename, &info)))
		{
#ifndef PLATFORM_ANDROID
			t = timespec_to_utc(info.st_mtim);
#else
			t = timespec_to_utc(info.st_mtime);
#endif
		}
		else
		{
		}
	}

	return false;
}

string_t io_full_path(string_t str)
{
	DBG_NOT_IMPLEMENTED();

	return "";
}

string_t full_path_name(string_t filename, string_t base)
{
	DBG_NOT_IMPLEMENTED();

	return "";
}

//
//
//

class posix_pipe_t : public base_io_stream_t
{
public:
	// *** base_io_stream_t methods ***
	virtual bool close();
	virtual bool read(void *buffer, u32 count, u32 &count_out);
	virtual bool write(const void *buffer, u32 count, u32 &count_out);

	posix_file_descriptor_t m_fd_read;
	posix_file_descriptor_t m_fd_write;

	OBJECT_CLASS_DECLARE();
};

OBJECT_CLASS_BEGIN(posix_pipe_t, base_io_stream_t)
OBJECT_CLASS_END()

bool posix_pipe_t::read(void *buffer, u32 count, u32 &count_out)
{
	size_t len = 0;
	
	bool result = m_fd_read.read(buffer, count, len);
	
	count_out = len;
	
	return result;
}

bool posix_pipe_t::write(const void *buffer, u32 count, u32 &count_out)
{
	size_t len = 0;
	
	bool result = m_fd_write.write(buffer, count, len);
	
	count_out = len;
	
	return result;
}

bool posix_pipe_t::close()
{
	m_fd_read.close();
	m_fd_write.close();

	return true;
}

io_stream_t io_pipe_create()
{
	int pipefd[2] = { -1, -1 };

	int result = pipe(pipefd);

	int err;

	if (check_posix_return("pipe", result, &err))
	{
		posix_pipe_t *obj = new posix_pipe_t;

		if (obj != NULL)
		{
			obj->m_fd_read.m_fd = pipefd[0];
			obj->m_fd_write.m_fd = pipefd[1];

			io_stream_t stream = io_stream_create_and_release(obj);

			if (!is_error(stream))
				stream.m_object->status = io_resource_status_open;

			return stream;
		}

		return io_stream_error/*_out_of_memory*/();
	}

	return io_stream_error(strerror(err));
}

int pipe_read_descriptor(file_t file)
{
	posix_pipe_t *device = io_stream_cast<posix_pipe_t>(file);

	if (device != NULL)
		return device->m_fd_read.m_fd;

	return -1;
}

int pipe_write_descriptor(file_t file)
{
	posix_pipe_t *device = io_stream_cast<posix_pipe_t>(file);

	if (device != NULL)
		return device->m_fd_write.m_fd;

	return -1;
}

void pipe_close_read_descriptor(file_t file)
{
	posix_pipe_t *device = io_stream_cast<posix_pipe_t>(file);

	if (device != NULL)
		device->m_fd_read.close();
}

void pipe_close_write_descriptor(file_t file)
{
	posix_pipe_t *device = io_stream_cast<posix_pipe_t>(file);

	if (device != NULL)
		device->m_fd_write.close();
}

//
//
//

const char *get_socket_error_string(int error)
{
	return strerror(error);
}

//
//
//

db_cursor_t db_find_file(string_t filespec, unsigned int max_depth)
{
	DBG_NOT_IMPLEMENTED();

	return db_cursor_error();
}

//
//  network I/O support
//

// copied from "../windows/network.cpp"

#ifdef _DEBUG

struct socket_option_t
{
	int          level;
	int          optname;
	int          type;    // 1=BOOL, 2=int, 3=GROUP, 4=LINGER, 5=unsigned int, ...
	const char * label;
};

static const socket_option_t g_socket_options[] =
{
	{ SOL_SOCKET,  SO_DEBUG,                  7, "SO_DEBUG"                  },
	{ SOL_SOCKET,  SO_REUSEADDR,              1, "SO_REUSEADDR"              },
	{ SOL_SOCKET,  SO_TYPE,                   2, "SO_TYPE"                   }, // not supported on Windows
	{ SOL_SOCKET,  SO_ERROR,                  2, "SO_ERROR"                  },
	{ SOL_SOCKET,  SO_DONTROUTE,              1, "SO_DONTROUTE"              },
	{ SOL_SOCKET,  SO_BROADCAST,              7, "SO_BROADCAST"              },
	{ SOL_SOCKET,  SO_SNDBUF,                 2, "SO_SNDBUF"                 },
	{ SOL_SOCKET,  SO_RCVBUF,                 2, "SO_RCVBUF"                 },
	{ SOL_SOCKET,  SO_KEEPALIVE,              1, "SO_KEEPALIVE"              },
	{ SOL_SOCKET,  SO_OOBINLINE,              1, "SO_OOBINLINE"              },
	{ SOL_SOCKET,  SO_LINGER,                 4, "SO_LINGER"                 },
	{ SOL_SOCKET,  SO_RCVLOWAT,               2, "SO_RCVLOWAT"               }, // not supported on Windows
	{ SOL_SOCKET,  SO_SNDLOWAT,               2, "SO_SNDLOWAT"               }, // not supported on Windows
	{ SOL_SOCKET,  SO_RCVTIMEO,               2, "SO_RCVTIMEO"               },
	{ SOL_SOCKET,  SO_RCVTIMEO,               7, "SO_RCVTIMEO"               },
	{ SOL_SOCKET,  SO_SNDTIMEO,               2, "SO_SNDTIMEO"               },
	{ SOL_SOCKET,  SO_SNDTIMEO,               7, "SO_SNDTIMEO"               },
	{ SOL_SOCKET,  SO_ACCEPTCONN,             7, "SO_ACCEPTCONN"             }, // not supported on Windows

	{ IPPROTO_IP,  IP_OPTIONS,                0, "IP_OPTIONS"                },
	{ IPPROTO_IP,  IP_HDRINCL,                7, "IP_HDRINCL"                },
	{ IPPROTO_IP,  IP_TOS,                    7, "IP_TOS"                    },
	{ IPPROTO_IP,  IP_TTL,                    7, "IP_TTL"                    },
	{ IPPROTO_IP,  IP_MULTICAST_IF,           7, "IP_MULTICAST_IF"           },
	{ IPPROTO_IP,  IP_MULTICAST_TTL,          7, "IP_MULTICAST_TTL"          },
	{ IPPROTO_IP,  IP_MULTICAST_LOOP,         7, "IP_MULTICAST_LOOP"         },
	{ IPPROTO_IP,  IP_ADD_MEMBERSHIP,         0, "IP_ADD_MEMBERSHIP"         },
	{ IPPROTO_IP,  IP_DROP_MEMBERSHIP,        0, "IP_DROP_MEMBERSHIP"        },
	{ IPPROTO_IP,  IP_UNBLOCK_SOURCE,         0, "IP_UNBLOCK_SOURCE"         },
	{ IPPROTO_IP,  IP_BLOCK_SOURCE,           0, "IP_BLOCK_SOURCE"           },
	{ IPPROTO_IP,  IP_ADD_SOURCE_MEMBERSHIP,  0, "IP_ADD_SOURCE_MEMBERSHIP"  },
	{ IPPROTO_IP,  IP_DROP_SOURCE_MEMBERSHIP, 0, "IP_DROP_SOURCE_MEMBERSHIP" },

	{ IPPROTO_TCP, TCP_NODELAY,               1, "TCP_NODELAY"               }, // Disables the Nagle algorithm for send coalescing.
//	{ IPPROTO_TCP, TCP_MAXSEG,                2, "TCP_MAXSEG"                }, // Get TCP maximum segment size

//	{ IPPROTO_UDP, UDP_NOCHECKSUM,            0, "UDP_NOCHECKSUM"            },
//	{ IPPROTO_UDP, UDP_CHECKSUM_COVERAGE,     0, "UDP_CHECKSUM_COVERAGE"     },
};

typedef int SOCKET;

static void dump_socket_helper(SOCKET s, int level, int optname, int type, const char *label);

void dump_socket(int s)
{
	trace("Dumping socket ", s, "...\n");

	trace_indent();

	sockaddr name;

	socklen_t namelen;

	ZERO_INIT(name);

	namelen = sizeof(name);

	if (getsockname(s, &name, &namelen) == 0)
		trace("getsockname: ", dump_addr(&name, namelen), "\n");

	ZERO_INIT(name);

	namelen = sizeof(name);

	if (getpeername(s, &name, &namelen) == 0)
		trace("getpeername: ", dump_addr(&name, namelen), "\n");

	for (int i = 0; i < DIM(g_socket_options); i++)
	{
		dump_socket_helper(s,
			g_socket_options[i].level,
			g_socket_options[i].optname,
			g_socket_options[i].type,
			g_socket_options[i].label);
	}

	trace_unindent();
}

//
//
//

static void dump_socket_helper(SOCKET s, int level, int optname, int type, const char *label)
{
	union
	{
		int              i;
		unsigned int     u;
//		char             c;
	} value = { 0 };

	socklen_t optlen = sizeof(value);

	int result = getsockopt(s, level, optname, (char*)&value, &optlen);

	trace(label, "=");

	int error;

	if (check_result(result, error))
	{
		switch (type)
		{
//			case 1: /*_ASSERT(optlen == sizeof(value.b));*/ TRACE("%i", value.b); break; // XXX: asserts (optlen == 1 && sizeof(value.b) == 4)
//			case 1: if (optlen == 1) { _ASSERT(optlen == sizeof(value.c)); TRACE("%i", value.c); } else { _ASSERT(optlen == sizeof(value.b)); TRACE("%i", value.b); } break;
			case 2: _ASSERT(optlen == sizeof(value.i)); trace(value.i); break;
//			case 3: _ASSERT(optlen == sizeof(value.g)); TRACE("%i", value.g); break; // XXX: asserts on CE
//			case 4: _ASSERT(optlen == sizeof(value.l)); TRACE("%i,%i", value.l.l_onoff, value.l.l_linger); break;
			case 5: _ASSERT(optlen == sizeof(value.u)); trace(value.u); break;
//			case 6: _ASSERT(optlen == sizeof(value.p)); TRACE("szProtocol=%s\n", value.p.szProtocol); hex_dump(value.p); break;
//			case 7: _ASSERT(optlen == sizeof(value.d)); TRACE("%i", value.d); break;
			default: trace("\n"); hex_dump(&value, optlen); break;
		}

		trace("\n");
	}
	else
	{
	}
}

#endif

//
//  serial I/O support
//

static void split(const char *name_and_config, string_t &comm_name, string_t &comm_config)
{
	comm_name = "";
	comm_config = "";

	if (name_and_config != NULL)
	{
		const char *ptr = strstr(name_and_config, ":");

		if (ptr != NULL)
		{
			comm_name = trim(left(name_and_config, ptr - name_and_config));
			comm_config = trim(ptr + 1);
		}
		else
		{
			comm_name = trim(name_and_config);
		}
	}
}

static bool next_param(string_t &config, string_t &name, string_t &value)
{
	name = "";
	value = "";

	const char *ptr = config;

	while (isspace(*ptr))
		ptr++;

	while (*ptr != 0 && *ptr != '=')
		name += *ptr++;

	if (*ptr == '=')
		ptr++;

	while (*ptr != 0 && *ptr != ' ')
		value += *ptr++;

	config = ptr;

	return name != "";
}

static const struct { const char *name; const char *value; unsigned int cflag_mask; unsigned int cflag_value; unsigned int speed; } g_options[] =
{
	{ "BAUD",   "4800",   CBAUDEX,         0,               B4800   },
	{ "BAUD",   "9600",   CBAUDEX,         0,               B9600   },
	{ "BAUD",   "19200",  CBAUDEX,         0,               B19200  },
	{ "BAUD",   "38400",  CBAUDEX,         0,               B38400  },
	{ "BAUD",   "57600",  CBAUDEX,         CBAUDEX,         B57600  },
	{ "BAUD",   "115200", CBAUDEX,         CBAUDEX,         B115200 },
	{ "BAUD",   "230400", CBAUDEX,         CBAUDEX,         B230400 },
	{ "DATA",   "5",      CSIZE,           CS5,             0       },
	{ "DATA",   "6",      CSIZE,           CS6,             0       },
	{ "DATA",   "7",      CSIZE,           CS7,             0       },
	{ "DATA",   "8",      CSIZE,           CS8,             0       },
	{ "PARITY", "N",      PARENB | PARODD, 0,               0       },
	{ "PARITY", "O",      PARENB | PARODD, PARENB | PARODD, 0       },
	{ "PARITY", "E",      PARENB | PARODD, PARENB,          0       },
	{ "STOP",   "1",      CSTOPB,          0,               0       },
	{ "STOP",   "2",      CSTOPB,          CSTOPB,          0       },
	{ "TO",     "off",    0,               0,               0       }, // NOTE: just a placeholder (timeout is always off in this implementation)
};

static void config_comm_port(struct termios &tios, string_t config)
{
	unsigned int cflag_mask_applied = 0;

	string_t name, value;

	while (next_param(config, name, value))
	{
#ifdef _DEBUG
		trace("COM param: name=", name, ", value=", value, "\n");
#endif

		int recognized = 0;

		for (unsigned int i = 0; i < DIM(g_options); i++)
		{
			if (strcmpi(name, g_options[i].name) == 0 &&
				strcmpi(value, g_options[i].value) == 0)
			{
				recognized = 1;

				if (g_options[i].cflag_mask != 0)
				{
					if ((cflag_mask_applied & g_options[i].cflag_mask) != 0)
					{
#ifdef _DEBUG
						trace("COM param: parameter specified more than once\n");
#endif
					}

					cflag_mask_applied |= g_options[i].cflag_mask;

					tios.c_cflag = bit_field_modify(tios.c_cflag, g_options[i].cflag_mask, g_options[i].cflag_value);
				}

				if (g_options[i].speed != 0)
				{
					cfsetispeed(&tios, g_options[i].speed);
					cfsetospeed(&tios, g_options[i].speed);
				}
			}
		}

#ifdef _DEBUG
		if (!recognized)
			trace("COM param: not recognized\n");
#endif
	}
}

// NOTE: it appears that async I/O doesn't work well on Linux, but does on BSD
// Unix flavors ?!?!?!?!?!?!?

class comm_device_t : public base_io_stream_t
{
public:
	comm_device_t()
	{
	}

	~comm_device_t()
	{
		close();
	}

	// *** base_io_stream_t methods ***
	virtual bool read(io_read_t &data);
	virtual bool write(io_write_t &data);
	virtual bool close();

#ifndef PLATFORM_ANDROID
	struct async_op_t
	{
		struct aiocb block; // AIO control block
	};
#endif

/*
	async_op_t m_pending_read;
	async_op_t m_pending_write;
*/
	posix_file_descriptor_t m_fd__;
	unsigned int m_async : 1;
};

#ifndef PLATFORM_ANDROID

static bool wait_for_io(struct aiocb *block, ssize_t &size)
{
	for (;;)
	{
		struct aiocb *blocks[] = { block };

		int err;

		int result = aio_suspend(blocks, DIM(blocks), NULL);

		if (check_posix_return("aio_suspend", result, &err))
		{
			err = aio_error(blocks[0]);

			if (err == 0)
			{
				size = aio_return(block);

				return true;
			}

			break;
		}

		if (err != EINTR)
			break;

		// system call was interrupted by signal delivery
	}

	return false;
}

#endif

bool comm_device_t::read(io_read_t &info)
{
	if (info.async_id != NULL)
		return base_io_stream_t::read(info);

	if (m_async == 0)
		return m_fd__.read(info.buf, info.cap, info.len);

#ifndef PLATFORM_ANDROID

	async_op_t pending = { 0 };

	pending.block.aio_fildes = m_fd__.m_fd;
	pending.block.aio_buf = info.buf;
	pending.block.aio_nbytes = info.cap;
	pending.block.aio_sigevent.sigev_notify = SIGEV_NONE;

	if (check_posix_return("aio_read", aio_read(&pending.block)))
	{
		ssize_t size = 0;

		if (wait_for_io(&pending.block, size))
		{
			_ASSERT(size <= info.cap);

			info.len = size;

			return true;
		}
	}

#else

	DBG_NOT_IMPLEMENTED();

#endif

	return false;
}

bool comm_device_t::write(io_write_t &info)
{
	if (info.async_id != NULL)
		return base_io_stream_t::write(info);

	if (m_async == 0)
		return m_fd__.write(info.buf, info.cap, info.len);

#ifndef PLATFORM_ANDROID

	async_op_t pending = { 0 };

	pending.block.aio_fildes = m_fd__.m_fd;
	pending.block.aio_buf = const_cast<void*>(info.buf);
	pending.block.aio_nbytes = info.cap;
	pending.block.aio_sigevent.sigev_notify = SIGEV_NONE;

	if (check_posix_return("aio_write", aio_write(&pending.block)))
	{
		ssize_t size = 0;

		if (wait_for_io(&pending.block, size))
		{
			_ASSERT(size <= info.cap);

			info.len = size;

			return true;
		}
	}

#else

	DBG_NOT_IMPLEMENTED();

#endif

	return false;
}

bool comm_device_t::close()
{
	return m_fd__.close();
}

//
//
//

io_stream_t open_comm_port_args(string_t device_and_config, tcflag_t c_iflag, tcflag_t c_oflag, tcflag_t c_lflag, bool aio)
{
	//
	//  propagate error
	//

	if (is_error(device_and_config))
		return io_stream_error(device_and_config);

	//
	//  allocate control structures
	//

	comm_device_t *obj = new comm_device_t;

	if (obj == NULL)
		return io_stream_error/*_out_of_memory*/();
	
	obj->m_async = aio;

	io_stream_t stream = io_stream_create_and_release(obj);

	if (is_error(stream))
		return stream;

	//
	//  open the device
	//

	string_t device;
	string_t config;

	split(device_and_config, device, config);

	int fd = open(device, O_RDWR | O_NOCTTY | O_SYNC);

	int err = errno;

	if (fd == -1)
		return io_stream_error("Unable to open " + device + ": " + strerror(err));

	//
	//  initialize control structures
	//

	obj->m_fd__.m_fd = fd;

	stream.m_object->status = io_resource_status_open;

	//
	//  configure the port
	//

	struct termios tios = { 0 };

	// default to 8 data bits, 1 stop bit, no parity

	tios.c_cflag |= CS8;
	tios.c_cflag |= CREAD; // enable receiver
	tios.c_cflag |= CLOCAL; // ignore modem control lines
	
	tios.c_iflag = c_iflag;
	tios.c_oflag = c_oflag;
	tios.c_lflag = c_lflag;

	// setup timing so reads block until at least one byte is available (this is
	// consistent with the meaning of the "TO=off" configuration setting)

	tios.c_cc[VMIN] = 1;
	tios.c_cc[VTIME] = 0;

	config_comm_port(tios, config);

	int result = tcsetattr(fd, TCSANOW, &tios);

	check_posix_return("tcsetattr", result);

	return stream;
}

io_stream_t open_comm_port(string_t device_and_config)
{
	return open_comm_port_args(device_and_config, 0, 0, 0, true);
}

//
//
//

int select_internal(int nfds, fd_set *readfds, fd_set *writefds, fd_set *exceptfds, const f32 *timeout)
{
	timespec tm;

	timespec *ptm = NULL;

	if (timeout != NULL)
	{
		tm = timespec_from_seconds(*timeout);

		ptm = &tm;
	}

	int n;

	for (;;)
	{
		n = pselect(nfds, readfds, writefds, exceptfds, ptm, NULL);

		if (n != -1) // success
			break;

		int error = posix_call_failed("pselect", n);

		if (error != EINTR)
			break;
	}

	return n;
}

