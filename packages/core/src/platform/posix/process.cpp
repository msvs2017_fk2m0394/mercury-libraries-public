/*------------------------------------------------------------------------------

	platform/posix/process.cpp

------------------------------------------------------------------------------*/

#include "pch.h"
#include <errno.h>
#include <unistd.h>
#include <stdio.h>
#include <signal.h>
#include <sys/ptrace.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "../../lib/core/process.h"
#include "posix.h"

//
//
//

struct linux_process_t : process_object_t
{
	pid_t m_pid;
};

//
//
//

static void redirect_stdio(io_stream_t *stream, int stdio)
{
	_ASSERT(stdio == STDIN_FILENO || stdio == STDOUT_FILENO || stdio == STDERR_FILENO);

	if (stream != NULL)
	{
		int fd[] = {
			pipe_read_descriptor(*stream),
			pipe_write_descriptor(*stream)
		};

#ifdef _DEBUG
		fprintf(stderr, "*** fd[]=%i,%i\n", fd[0], fd[1]);
#endif

		int fd2 = (stdio == STDIN_FILENO) ? fd[0] : fd[1];
		int result = dup2(fd2, stdio);

#ifdef _DEBUG
		fprintf(stderr, "*** dup2(%i,%i)=%i\n", fd2, stdio, result);
#endif

		io_stream_close(*stream);

#ifdef _DEBUG
		fprintf(stderr, "*** fd[] after close %i,%i\n",
			pipe_read_descriptor(*stream), pipe_write_descriptor(*stream));
#endif
	}
}

//
//
//

process_object_t *posix_process_create(const process_create_t *args)
{
	_ASSERT(args != NULL);

	// not supported on this platform

	if (args->console != 0)
	{
		DBG_NOT_IMPLEMENTED();
	}

	//
	//
	//

	process_object_t *d = NULL;

#ifdef _DEBUG
	fprintf(stderr, "*** in parent process, pid=%i\n", getpid());
	fprintf(stderr, "*** in parent process, cmd=%s\n", (const char *)args->cmd);
#endif

	pid_t child = fork();

	if (child == 0)
	{
		// we're in the child process

#ifdef _DEBUG
		fprintf(stderr, "*** in child process, pid=%i\n", getpid());
		fprintf(stderr, "*** in child process, cmd=%s\n", (const char *)args->cmd);
#endif

		redirect_stdio(args->stdio.in, STDIN_FILENO);
		redirect_stdio(args->stdio.out, STDOUT_FILENO);
		redirect_stdio(args->stdio.err, STDERR_FILENO);

		//
		//
		//

		if (args->dir != "")
			chdir(args->dir);

		//
		//
		//

		if (args->debug != 0)
		{
			ptrace(PTRACE_TRACEME, 0, NULL, NULL);
		}

		//
		//
		//

		if (args->env != NULL)
			DBG_NOT_IMPLEMENTED();

		//
		//
		//

		const char *cmd = args->cmd;

		int result = execl("/bin/sh", "sh", "-c", cmd, NULL);

		// returns only if there's an error

		int err = errno;

		_ASSERT(result == -1);

		const char *txt = strerror(err);

#ifdef _DEBUG
		fprintf(stderr, "exec failed: %s\n", txt);
#endif

		variant_t temp = string_t(txt);

		process_error_t *xxx = new process_error_t;

		d = xxx;

		if (xxx != NULL)
			xxx->m_error = temp;
	}
	else if (child == -1)
	{
		int err = errno;

		const char *txt = strerror(err);

#ifdef _DEBUG
		fprintf(stderr, "fork failed: %s\n", txt);
#endif

		variant_t temp = string_t(txt);

		process_error_t *xxx = new process_error_t;

		d = xxx;

		if (xxx != NULL)
			xxx->m_error = temp;
	}
	else
	{
#ifdef _DEBUG
		message("*** in parent process, child pid=", child, "\n");
#endif

		// the call succeeded

		linux_process_t *xxx = new linux_process_t;

		d = xxx;

		if (xxx != NULL)
		{
			xxx->m_public.object = xxx;
			xxx->m_public.id     = child;
			xxx->m_pid           = child;
		}
	}

	//
	//
	//

	return d;
}

//
//
//

string_t process_command_line()
{
#if 0
	return load_file_as_string("/proc/self/cmdline"); // NOTE: stdio_file_device_t::get_length is broken on this platform, so we cannot use this currently
#else
	FILE *f = fopen("/proc/self/cmdline", "rb");

	if (f != NULL)
	{
		string_t cmdline;

		for (;;)
		{
			int c = fgetc(f);

			if (c <= 0)
				break;

			cmdline += (char)c;
		}

		fclose(f);

		return cmdline;
	}
	
	DBG_NOT_IMPLEMENTED();

	return string_error();
#endif
}

//
//
//

bool process_resume1(process_t p)
{
	DBG_NOT_IMPLEMENTED();
	
	return false;
}

