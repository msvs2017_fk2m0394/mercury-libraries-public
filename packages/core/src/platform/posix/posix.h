/*------------------------------------------------------------------------------

	platform/posix/posix.h - POSIX platform declarations

------------------------------------------------------------------------------*/

#ifndef __POSIX_POSIX_H__
#define __POSIX_POSIX_H__

#include "../../lib/core/thread.h"
#include "../../lib/misc/posix.h"

struct lock_internal_t
{
	lock_internal_t();
	~lock_internal_t();

	uintptr_t       owner;      //
	uintptr_t       recursion;  //
//	uintptr_t       contention; //
	pthread_mutex_t mutex;      //
	pthread_cond_t  cond;       //
};

#ifdef _DEBUG
void debug_validate(const lock_internal_t *data);
#else
inline void debug_validate(const lock_internal_t *) { }
#endif

struct posix_core_thread_state_t : core_thread_state_t
{
	pthread_t thread;
	thread_id_t thread_id_host;
	sync_event_t thread_exit_event;

	OBJECT_CLASS_DECLARE();
};

#ifdef __CORE_SCHEDULER_H__

struct task_thread_t : task_processor_t
{
	task_thread_t(task_processor_group_t *pg)
	{
		m_group = pg;
	}

	object_ref_t<task_manager_t> m_tm; //
//	task_processor_group_t *m_processor_group;

	task_t * m_dedicated;
};

#endif

extern "C" void Sleep(int ms); // Windows-compatible call

//
//
//

string_t posix_error_message(const char *prefix, int err);

//
//
//

int pipe_read_descriptor(io_stream_t stream);
int pipe_write_descriptor(io_stream_t stream);
void pipe_close_read_descriptor(io_stream_t stream);
void pipe_close_write_descriptor(io_stream_t stream);

struct io_stat_t
{
	int dummy;
};

io_stat_t io_stat(const char *name);

io_status_t posix_io_exists(string_t filename);
io_status_t posix_io_delete(string_t filename);

//
//
//

#ifndef PLATFORM_ANDROID
art_time_t timespec_to_utc(timespec t1);
#else
art_time_t timespec_to_utc(unsigned long t1);
#endif

int64_t timespec_nanoseconds(struct timespec ts);
int64_t timespec_microseconds(struct timespec ts);
f64     timespec_seconds(struct timespec ts);

struct timespec timespec_from_seconds(f64 sec);

struct timespec operator+(const struct timespec t1, const struct timespec t2);

extern const time_type_t g_time_type_filetime_utc;
extern const time_type_t g_time_type_100ns;
extern const time_type_t g_time_type_perf;

//
//
//

process_object_t *posix_process_create(const process_create_t *args);

#endif // __POSIX_POSIX_H__

