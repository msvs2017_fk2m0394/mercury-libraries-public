/*------------------------------------------------------------------------------

	platform/android/android.cpp

------------------------------------------------------------------------------*/

#include "pch.h"
#include "android.h"

#define PLATFORM_ANDROID

#include <jni.h>
#include <errno.h>

#include <string.h>
#include <unistd.h>
#include <sys/resource.h>

// e.g. from C:\ProgramData\Microsoft\AndroidNDK64\android-ndk-r11c\platforms\android-19\arch-arm\usr\include\android

#include <android/api-level.h>

#include <zconf.h>
#include <zlib.h>
#include <dlfcn.h>

#include <android/log.h>

#include <android/bitmap.h>

#if __ANDROID_API__ > 9
#include <android/asset_manager.h>
#include <android/asset_manager_jni.h>
#include <android/configuration.h>
#include <android/input.h>
#include <android/keycodes.h>
#include <android/looper.h>
#include <android/native_activity.h>
#include <android/native_window.h>
#include <android/native_window_jni.h>
#include <android/obb.h>
#include <android/rect.h>
#include <android/sensor.h>
#include <android/storage_manager.h>
#include <android/window.h>
#endif

#if __ANDROID_API__ > 13
#include <android/tts.h>
#endif

#include <EGL/egl.h>
#include <EGL/eglext.h>
#include <EGL/eglplatform.h>   
#include <GLES/gl.h>
#include <GLES/glext.h>
#include <GLES/glplatform.h>

#if __ANDROID_API__ > 23
// Android 6.0 and higher
#include <android/trace.h>
#endif

#if __ANDROID_API__ > 24
// Android 7.0 and higher
#include <vulkan/vulkan.h>
#include <GLES3/gl32.h>
#include <GLES3/gl32ext.h>
#endif

// reuse our existing POSIX support...

#include "../posix/atomic.cpp"
#include "../posix/clock.cpp"
#include "../posix/event.cpp"
#include "../posix/graphics.cpp"
#include "../posix/io.cpp"
#include "../posix/lock.cpp"
#include "../posix/module.cpp"
#include "../posix/platform.cpp"
#include "../posix/posix.cpp"
#include "../posix/process.cpp"
#include "../posix/thread.cpp"
#include "../posix/time.cpp"

/*

------ Build started: Project: SharedObject1, Configuration: Debug ARM ------
ANDROID_HOME=C:\Program Files (x86)\Android\android-sdk
ANT_HOME=C:\Program Files (x86)\Microsoft Visual Studio 14.0\Apps\apache-ant-1.9.3\
JAVA_HOME=C:\Program Files (x86)\Java\jdk1.7.0_55
NDK_ROOT=C:\ProgramData\Microsoft\AndroidNDK64\android-ndk-r11c\

------ Build started: Project: Android1.Packaging, Configuration: Debug ARM ------
ANDROID_HOME=C:\Program Files (x86)\Android\android-sdk
ANT_HOME=C:\Program Files (x86)\Microsoft Visual Studio 14.0\Apps\apache-ant-1.9.3\
JAVA_HOME=C:\Program Files (x86)\Java\jdk1.7.0_55
java.home=
NDK_ROOT=C:\ProgramData\Microsoft\AndroidNDK64\android-ndk-r11c\
ANDROID_NDK_HOME=C:\ProgramData\Microsoft\AndroidNDK64\android-ndk-r11c\
Buildfile: C:\projects\temp\Android1\Android1\Android1.Packaging\ARM\Debug\Package\build.xml

*/

#include "../../crt/crt.h"

static crt_status_t g_crt_status;

crt_status_t ART_API_CALLTYPE crt_status()
{
	return g_crt_status;
}

art_time_t timespec_to_utc(unsigned long t1)
{
	DBG_NOT_IMPLEMENTED();

	return time_error();
}

