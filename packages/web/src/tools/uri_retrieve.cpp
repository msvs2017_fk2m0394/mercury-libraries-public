/*------------------------------------------------------------------------------

	uri_retrieve.cpp

------------------------------------------------------------------------------*/

#include <mercury/web.h>
#include <mercury/experimental/core_crt_stdio.h>

#define MERCURY_LINK_SUBSYSTEM 1
#include <mercury/link.h>
#include <mercury/link_web.h>

int main(int argc, char *argv[])
{
	string_t uri_text;
	
	if (argc > 1)
		uri_text = argv[1];

	uri2_t uri = uri_scan(uri_text);

	message("resolving URI: ", uri, "\n");

	web_session_t *session = NULL; // not used in this application
	
	io_stream_t s = uri_resolve(session, uri);

	if (is_open(s))
	{
		char buf[512];
		size_t c;

		while (io_stream_read(s, buf, sizeof(buf), c))
		{
			if (c == 0)
				return 0;

			fprintf(stdout, "%.*s", (int)c, buf);
			fflush(stdout);
		}
	}
	else
	{
		message(io_error_message(s), "\n");
	}

	return -1;
}

