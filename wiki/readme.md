# readme.md

API Headers

```
#include <mercury/core.h>
#include <mercury/graphics.h>
#include <mercury/link.h>
#include <mercury/link_core.h>
#include <mercury/odbc.h>
#include <mercury/resource.h>
#include <mercury/streams.h>
#include <mercury/ui.h>
#include <mercury/web.h>
#include <mercury/x86.h>
```

string_t
art_time_t
variant_t
ptr_array_t
module_t
process_t
thread_t
